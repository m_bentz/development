Folder Structure
~~~~~~~~~~~~~~~~

/src  - Where development occurs and is synced with the github repo
    /scss
        /_UFSA_THEME.scss - This is a scss partial that can be imported into another scss file. This is included so that the theme remains independent of any other project
        /UFSA.scss - This is the scss file that will be transpiled 
        /*Component Level scss* - scss for all components
/dist - Where the transpiled and concatenated css files are located (generated)
    css/
        /UFSA.css
        /UFSA.min.css 
