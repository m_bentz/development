var orgChart = [];
var root = "";
var children = "";
var chartDepth = 0;
const minimumDepth = 2;


// TODO Create method for adding the necessary html for a node
// Move out the Person class and import
function initializeChart(personArray, root) {
    // printDivider("ROOT");
    createChart(personArray);
    assignChildren();

    /*
    |  With how the orgchart library works it needs a root node specified 
    |  SEPARATE from the children. Therefore we need to split the two and
    | pass them in as different objects.
    */

    // Get the children
    children = orgChart[0].getChildren();
    let childJSON = getJSON(children);
    console.log("Children: " + children);
    console.log(JSON.stringify(childJSON));

    // Get the root
    let oldRoot = orgChart[0];
    root = new Person("" ,"" ,oldRoot.getName(), oldRoot.getTitle(),oldRoot.getImage());
    console.log(root);
    root.removeChildren();
    
    // Display the chart
    displayChart(root,childJSON);
}

function createChart(personArray) {
    printDivider("CREATE CHART");
    let count = 0;
    let parentKeyList = [];
    let primaryKey = "";
    let parentKey = "";
    let nodeDepth = 0;

    // Create an instance of Person for each index in the personArray
    personArray.forEach(element => {
        // let attributes = element.split("\n");       // Get the attributes
        let attributes = element.split("  ");       // Get the attributes
        console.log(...attributes);
        let person = new Person(...attributes);     // Create a person with the attributes
        orgChart.push(person);                      // Add the person to the organization chart

        // Get the primaryKey and parentKey
        primaryKey = attributes[0];
        parentKey = attributes[1];

        // Get the max depth
        nodeDepth = person.getLevel();
        chartDepth = (chartDepth > nodeDepth) ? chartDepth : nodeDepth;
    });
    console.log(orgChart);
}

function include(arr, obj) {
    return (arr.indexOf(obj) != -1);
}

// Assigns the correct children to each node
function assignChildren() {
    printDivider("ASSIGN CHILDREN");
    console.log(orgChart);
    let searchKey = "";
    let indexesToRemove = [];

    orgChart.forEach(parent => {        
        // Get the key to search for in the children
        let searchKey = parent.getPrimaryKey();

        // Search through the rest to see if they have a parent
        console.log('\n');
        console.log("PARENT: " + parent.getName());
        console.log("###########################");
        console.log('\n');
        orgChart.forEach(child => {
            console.log("child: " + child.getName());
            if (child.hasParent(searchKey)) { 
                parent.addChild(child); 
                // Remove from root orgchart
                console.log(child.getName() + " is the child of:  " + parent.getName());
                var index = orgChart.indexOf(child);
                indexesToRemove.push(index);
            }
        });
    });

    console.log("Need to remove these indices: " + indexesToRemove);

    // Create a new array to hold the updated org chart
    let newOrgChart = [];

    // Create the organization chart with the updated parent/child relationships
    orgChart.forEach((element, ind) => {
        if (indexesToRemove.indexOf(ind) == -1) {
            newOrgChart.push(element);
        }
    });

    // Assign the org chart to the updated chart
    orgChart = newOrgChart;
    newOrgChart = "";
    console.log(orgChart);
}

// TODO: UNUSED
function isMiddleLevel(person) {
    console.log(person.toString());
    return person.getLevel().includes(LEVELS[2]);
}

// Gets the JSON of all the nodes within a tree
function getJSON(tree) {
    printDivider("CREATE JSON");
    console.log(tree);

    let info = [];
    tree.forEach(element => {
        info.push(element.getJSON());
    });

    return info;
}

// TODO: UNUSED
function assignLevel() {
    printDivider("ASSIGN LEVELS");
    console.log(orgChart);

    // For every top level person iterate through the children
    orgChart.forEach(person => {
        console.log("Iterating over " + person.getName());
        iterate(person, 0);
    });
}

function displayChart(root, children) {
    var datasource = {
        "name": `${root.getImage()}`,
        "title": `<p class="name">${root.getName()}</p> <p>${root.getTitle()}</p>`,
        "className": LEVELS[0],
        "children": children
    };

    console.log("chart depth: " + chartDepth);
    var chartContainer = $('#chart-container');
     var oc = $('#chart-container').orgchart({
        // "chartContainer": "#chart-container",
        "data": datasource,
        // "exportButton": true,
        // "exportFilename": 'OrgChart',
        // "exportFileextension": 'pdf',
        "verticalDepth": chartDepth <= minimumDepth ? minimumDepth + 1 : minimumDepth + 1,
        "depth": chartDepth <= minimumDepth ? minimumDepth : chartDepth,
//         "pan": true,
        //"zoom": true,
        "nodeContent": "title"
        // 'toggleSiblingsResp': true,
        // 'createNode': function($node, data) {
        //   $node.on('click', function(event) {
        //     if (!$(event.target).is('.edge, .toggleBtn')) {
        //       var $this = $(this);
        //       var $chart = $this.closest('.orgchart');
        //       var newX = window.parseInt(($chart.outerWidth(true)/2) - ($this.offset().left - $chart.offset().left) - ($this.outerWidth(true)/2));
        //       var newY = window.parseInt(($chart.outerHeight(true)/2) - ($this.offset().top - $chart.offset().top) - ($this.outerHeight(true)/2));
        //       $chart.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
        //     }
        //   });
        // }
    });
        // orgchart = new OrgChart({
        //     "chartContainer": "#chart-container",
        //     "data": datasource,
        //     "depth": chartDepth - 1,
        //     // "pan": true,
        //     // "zoom": true,
        //     "nodeContent": "title",
        //     // "exportButton": true,
        //     // "exportFilename": 'OrgChart'
        // }); 
    $('#chart-container').on('touchmove', function (event) {
        event.preventDefault();
    });

    // oc.setChartScale($('.orgchart'),0.5);
}

const LEVELS = {
    0: "root-level",
    1: "top-level",
    2: "middle-level",
    3: "bottom-level",
    4: "low-level",
    5: "lowest-level"
}

class Person {
    constructor(primaryKey, parentKey, name, title, img, level) {
        this.primaryKey = primaryKey;
        this.parentKey = parentKey;
        this.name = name;
        this.title = title;
        this.children = [];
        this.img = img;
        this.level = this.calculateLevel(level);
        this.className = LEVELS[this.calculateLevel(level)];
    }

    calculateLevel(level) {
        const MAX_LEVEL = 5;
        return (level > MAX_LEVEL) ? MAX_LEVEL : level;
    }

    getJSONstr() {
        return this.JSON_str;
    }

    getPrimaryKey() {
        return this.primaryKey;
    }

    getParentKey() {
        return this.parentKey;
    }

    getName() {
        return this.name;
    }

    getTitle() {
        return this.title;
    }

    hasParent(key) {
        // console.log("key: " + key);
        if (key) {
            if (key === this.parentKey) return true;
        } 
        return false;
    }
    
    addChild(child) {
        this.children.push(child);
    }

    getChildren() {
        return this.children;
    }

    hasChildren() {
        return (this.children.length === 0) ? false : true;
    }

    removeChildren() {
        this.children = "";
    }

    getImage() {
        return this.img;
    }

    getLevel() {
        return this.level;
    }
    
    // toString() {
    //     console.log(`Primary key: ${this.primaryKey}`);
    //     console.log(`Parent key: ${this.parentKey}`);
    //     console.log(`Name: ${this.name}`);
    //     console.log(`Title: ${this.title}`);
    //     console.log(`Children: ${this.children}`);
    // }

    getJSON() {
        if (!this.hasChildren()) {
            return {
                name: `${this.img}`,
                // title: this.title,
                title: `<p class="name">${this.name}</p> <p>${this.title}</p>`,
                className: this.className
            }
        } else {
            return {
                name: `${this.img}`,
                title: `<p class="name">${this.name}</p> <p>${this.title}</p>`,
                className: this.className,
                children: this.iterate(this)
            }
        }
    }

    // Update this
    iterate(current) {
        let children = current.getChildren();
        let json_str = [];
        let child = "";
        for (let i = 0, len = children.length; i < len; i++) {
            child = children[i];
            json_str.push(child.getJSON());
            this.iterate(children[i]);
        }
        return json_str;
    }
}

function printDivider(text){
    console.log('\n');
    console.log("###########################");
    console.log(text);
    console.log("###########################");
}