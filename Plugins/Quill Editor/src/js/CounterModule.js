class Counter {
    constructor(quill, options) {
        this.quill = quill;
        this.options = options;
        this.editor = $(`${options.editor}`);
        this.container = $(`${options.container}`);
        this.counter = options.counter;
        this.maxCount = options.maxCount;
        this.nonTextKeys = [8, 46, 35, 36, 37, 38, 39, 40];         // backspace, delete, end, home, left, up, right, down arrow

        $(options.editor).on("keydown", () => this.handleKeyDown(event));
        $(options.editor).on("paste", () => this.handlePaste(event), this.updateCounter());
        this.quill.on('text-change', () => this.updateCounter());
    }

    calculate() {
        let text = this.getLength(); //this.quill.getLength(); //getText();
        // console.log("Update -> Calculate called.");
        // console.log(`length ${text}`);
        if (this.options.unit === 'word') {
            text = text.trim();
            // Splitting empty text returns a non-empty array
            return text.length > 0 ? text.split(/\s+/).length : 0;
        } else {
            return text;
        }
    }

    handleKeyDown(e) {
        if (this.limitMet()) {
            // console.log("limit met");  
            // Allow non text keys to be pressed
            if (this.isTextKey(e.keyCode)) e.preventDefault();
        } 
    }

    updateCounter() {
        // console.log("text change");
        let length = this.calculate();
        let label = this.options.unit;

        if (this.limitMet()) {
            $(this.options.editor).addClass('red');
            $(this.options.counter).addClass('red');
        } else {
            $(this.options.editor).removeClass('red');
            $(this.options.counter).removeClass('red');
        }

        if (length !== 1) {
            label += 's';
        }
        this.container.children(this.counter).text(`${length} ${label} of ${this.maxCount} characters, ${this.maxCount - length} remaining`);
    }

    handlePaste(e) {
        let pastedData = e.clipboardData.getData("text");
        let editorLength = this.getLength(); //quill.getLength();
        let postPasteLength = editorLength + pastedData.length;
        let newLength = postPasteLength;

        quill.focus();

        // console.log("editor length: " + editorLength, "pastedata: " + pastedData.length, "maxword count: " + this.maxCount);
        if (editorLength < this.maxCount) {
            if (postPasteLength > this.maxCount) { // If the pasted text exceeds the max word count, cut it down
                let newPastedText = pastedData.substring(0, this.maxCount - editorLength);

                // console.log(newPastedText);
                // Since we are canceling the paste operation, we need to manually
                // paste the data into the document.
                let remainingLength = postPasteLength - this.maxCount;
                let range = quill.getSelection();

                quill.insertText(range.index, newPastedText, "silent");
                // this.update(e);
                
                // Get the new length to set the cursor
                newLength = editorLength + newPastedText.length;       
                
                // This is necessary to prevent the default paste action.
                e.preventDefault();
            }
        } else { // There is no more space left
            newLength = this.maxCount;
            e.preventDefault();
        }
        quill.setSelection(newLength, 1, "silent");
    }

    limitMet() {
        // return this.quill.getLength() === this.maxCount;
        return this.getLength() === this.maxCount;
    }

    isTextKey(keyCode) {
        return this.nonTextKeys.indexOf(keyCode) === -1;
    }

    getLength() {
        return this.editor.text().length;
    }
}

Quill.register('modules/counter', Counter);
