// https://bitbucket.org/m_bentz/quill-modules/src/master/TextReplacementModule.js
// https://bb.githack.com/m_bentz/quill-modules/raw/master/TextReplacementModule.js

/*
    TextReplacement is a QuillJS Module that searches for Text Replacements
    i.e. #PROGRAM_NAME#. The delimiter "#" can be specified using the APEX Plugin
*/
class TextReplacement {
    constructor(quill, options) {
        this.quill = quill;
        this.options = options;
        this.delimiter = options.delimiter;
        this.labelMap = options.labelMap;
        this.SPACE_KEY = " ";
        this.NEW_LINE = "\n";
        
        // Check for text replacement
        this.update();
    }

    update() {
        // Get the scope
        let self = this;

        $('.ql-editor').on('keydown', function(e) {
            // console.log("keydown event on quill");
            let index = "";
            let keyID = e.keyCode || e.charCode;
            
            if (keyID == 8 || keyID == 46) { // Backspace or Delete
                index = parseInt(self.getCursorIndex());
            }
            
            if (keyID == 8) { //Backspace
                if (self.noCharRemoved()) {                          // Need to check if a char has already been removed
                    index = index - 1;
                    self.searchAndRemove(index, self, 0);
                }
                self.resetIndicator();
            }

            if (keyID == 46) { //Delete
                if (self.noCharRemoved()) {                         // Need to check if a char has already been removed
                    index = index + 1;
                    self.searchAndRemove(index, self, 1);
                }
                self.resetIndicator();
            }
            // console.log("\n");
            // console.log("\n");
        });

        this.quill.on('selection-change', function(range, ...args) {    // Check on selection change
            // console.log("selection change occured");
            if (range) {                                                // If cursor inside of editor
                let cursorIndex = range.index;                          // Get the index
                self.quill.getSelection(true);                          // Set the focus, otherwise getSelection() returns null
                self.search(cursorIndex, self);
                // console.log("\n");
                // console.log("\n");
            } else {
                // console.log('Cursor outside of editor');
            }
            self.resetIndicator();
        });
    }

    search(index, self, opts) {
        // console.log("index: " + index);
        if (self.cursorLocationValid(index)) {
            let direction = self.getDirection(index);
            let range = self.getLabelBounds(direction, index);
            if (range) {
                self.setSelection(range);
            }
            if (opts === 'y') return range;
        }
    }

    noCharRemoved() {
        return !haveRemoved;
    }

    resetIndicator() {
        haveRemoved = false;
    }

    searchAndRemove(index, self, offset) {
        let range = self.search(index, self, 'y');
        // The label exists
        if (range) {
            self.removeSelection(range);
        } else { // The label was not found, delete the character anyway
            self.removeSelection({ startIndex: index - offset, length: 1 });
        }
    }

    getCursorIndex() {
        quill.getSelection(true);
        // console.log("Current Index: " + quill.getSelection().index);
        return quill.getSelection().index;
    }

    cursorLocationValid(cursorIndex) {
        // Get the character to the right and the left
        let rightChar = quill.getText(cursorIndex, 1);
        let leftChar = quill.getText(cursorIndex - 1, 1);

        // console.log("right:" + rightChar);//.charCodeAt(0));
        // console.log("left:" + leftChar);//.charCodeAt(0));
        // console.log(quill.getContents(cursorIndex, 1));
        // console.log(quill.getContents(cursorIndex - 1, 1));
        // console.log("right code:" + rightChar.charCodeAt(0));
        // console.log("left code:" + leftChar.charCodeAt(0));

        // Check if the limits of the editor have been reached
        if (rightChar && leftChar) {
            // In the case these conditions, a replacement text cannot occur
            if (rightChar === this.SPACE_KEY || rightChar === this.NEW_LINE
                || leftChar === this.SPACE_KEY || leftChar === this.NEW_LINE
                || (rightChar === this.delimiter && leftChar === this.delimiter)) {
                // Therefore do nothing
                // console.log("Text replacement cannot occur");
                return false;
            } else {
                // If the right character is delimiter then set the direction to go left first.
                // Otherwise, go right first
                return true;
            }
        } else {
            // console.log("reached the limits of the editor");
            return false;
        }
    }

    getDirection(index) {
        let rightChar = quill.getText(index, 1);
        // console.log("direction: " + direction);
        return (rightChar === this.delimiter) ? -1 : 1;
    }

    getLabelBounds(direction, cursorIndex) {
        let asciiVal = 0;

        cursorIndex += (1 * direction);                                 // Offset the cursor to initialize the search also for left/right arrow
        let delimiterOne = false;
        let delimiterTwo = false;
        let endingIndex = 0;
        let startingIndex = cursorIndex;
        let length = 0;

        while (this.isValid(cursorIndex) && !delimiterOne) {            // Search in one direction
            if (quill.getText(cursorIndex, 1) === this.delimiter) {     // Check if char is the delimiter
                endingIndex = cursorIndex;                              // Set the ending index
                direction = direction * -1;                             // Change the direction
                delimiterOne = true;                                    // Mark the delimiter as found
                // console.log("del one found");
            }
            // console.log("found: " + quill.getText(cursorIndex, 1)); 
            asciiVal += quill.getText(cursorIndex, 1).charCodeAt(0);
            cursorIndex = this.updateIndex(cursorIndex, direction);     // Update the index
            length++;                                                   // Increment the length
        }

        // Set the cursor index to the initial starting position offset by 1 to 
        // avoid checking the same position twice
        cursorIndex = this.updateIndex(startingIndex, direction);
        
        if (delimiterOne) {                                                 // Only continue the search if a delimiter was found
            while (this.isValid(cursorIndex) && !delimiterTwo) {            // Search to the other side
                if (quill.getText(cursorIndex, 1) === this.delimiter) {     // Check if char is the delimiter
                    startingIndex = cursorIndex;                            // Set the starting index
                    delimiterTwo = true;                                    // Set the delimiter as found
                    // console.log("del two found");
                } 
                // console.log("found: " + quill.getText(cursorIndex, 1)); 
                asciiVal += quill.getText(cursorIndex, 1).charCodeAt(0);
                cursorIndex = this.updateIndex(cursorIndex, direction);     // Update the index
                length++;                                                   // Increment the length
            }

            if (!this.isValidLabel(asciiVal)) return 0;

            if (delimiterTwo) {
                // Since the original direction of the search 
                startingIndex = (startingIndex < endingIndex) ? startingIndex : endingIndex;
                
                // Highlight the replacement text, passing "silent" to prevent 
                // calling a change event on the quill editor
                var range = {
                    startIndex: startingIndex,
                    length: length
                };
                return range;
            }
            return 0;
        } else {
            // console.log("delimiter pair not found");
            return 0;
        }
    }

    isValidLabel(label) {
        return this.labelMap.indexOf(label) >= 0;
    }

    isValid(cursorIndex) {
        let char = quill.getText(cursorIndex,1);                        // Get the character at the cursor
        // var re = /[A-Z0-9#_]/;
        var re = /[A-Z#_]/;
        return re.test(char);
        //return (char !== this.SPACE_KEY && char !== this.NEW_LINE);     // Check if it's not equal to a " " (space)
    }

    updateIndex(index, direction) {
        return index += (1 * direction);     // Increment the cursor with respect to direction
    }

    setSelection(range) {
        quill.setSelection(range.startIndex, range.length, "silent");
    }

    removeSelection(range) {
        // var range = quill.getSelection();
        // console.log("js range: " + range);
        quill.deleteText(range.startIndex, range.length);
    }
}

Quill.register('modules/text_replacement', TextReplacement);