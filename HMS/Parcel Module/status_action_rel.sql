
-- Get the action and the resulting status
SELECT act.title AS ACTION, act.result_trk_st_t_id AS "RESULT_STATUS" 
FROM pc_track_action act
INNER JOIN pc_track_stat_type trk
ON act.trk_st_t_id = trk.id;

-- Get the intial status, action and resulting status
SELECT status.title "INITIAL STATUS", status.action, result_status.title AS "RESULTING STATUS"
FROM 
  (SELECT trk.title AS TITLE, act.title AS ACTION, act.result_trk_st_t_id AS res_id
  FROM pc_track_action act
  INNER JOIN pc_track_stat_type trk
  ON act.trk_st_t_id = trk.id) status
INNER JOIN pc_track_stat_type result_status
ON status.res_id = result_status.id;

-- Get the actions associated with a status
SELECT status.title AS "Parcel Status", action.title AS "Action"
FROM pc_track_action action
INNER JOIN pc_track_stat_type status
ON act.trk_st_t_id = trk.id;


select areas.*,
   '<a href="f?p=&APP_ID.:30:'||'&APP_SESSION.'||':::RIR:IREQ_AREA_NM:' || areas.title||':"><b>View Buildings</b></a>' 
    || '::' ||
   '<a href="' || APEX_UTIL.PREPARE_URL('f?p=' || :APP_ID ||':125:'||:APP_SESSION||'::::P125_AREA_ID,P125_PREV_PG:' || areas.area_id ||',126:','SESSION') || '">Edit Area</a>'
     || '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 
   as Action
from areas;


--SELECT status.title AS "Parcel Status", action.title AS "Action"
--FROM pc_track_action action
--INNER JOIN pc_track_stat_type status
--ON action.trk_st_t_id = status.id;

-- Get the Parcel status and all associated actions
SELECT status.title AS "Parcel Status", listagg(action.title,' :: ') 
WITHIN GROUP(ORDER BY status.title) AS "Action" 
FROM pc_track_action action
INNER JOIN pc_track_stat_type status
ON action.trk_st_t_id = status.id
GROUP BY status.id, status.title
ORDER BY status.id;

