ALTER SESSION SET CURRENT_SCHEMA = hmsdata;


-- Send daily emails
DECLARE
  wait_limit NUMBER := 6;

-- A valid parcel is NOT suspicious, waiting at the correct location
-- AND has NOT been waiting for more than N days
  CURSOR valid_parcels IS
   SELECT prcl_area.title AS location, trunc(sysdate - prcl.receive_dt), prcl.track_num, prcl.receive_dt, person_pkg.get_email_address(prsn.id, 1) email, trk_st.title
    FROM pc_parcel prcl
    -- Parcel tracking status
      INNER JOIN pc_track_stat_type trk_st
      ON prcl.trk_st_t_id = trk_st.id
        -- Person the parcel belongs to
        INNER JOIN person prsn
        ON prcl.prsn_id = prsn.id
          -- Get the location
          INNER JOIN (
            SELECT prcl_list.id AS list_id, area.id AS area_id, area.title
            FROM pc_parcel_list prcl_list
            INNER JOIN area
            ON prcl_list.area_id = area.id) prcl_area
          ON prcl.prcl_list_id = prcl_area.list_id  
    -- The parcel is not suspicious
    WHERE prcl.id NOT IN (
       -- Find the suspicious parcels to filter by
        SELECT cond.prcl_id
        FROM pc_parcel_cond cond
          INNER JOIN pc_cond_stat_type typ
          ON cond.pc_cond_stat_id = typ.id
        WHERE typ.id = 1
        AND cond.active_ind = 'Y')
    -- The parcel is Waiting
    AND trk_st.id IN (2)
    -- At the right location
    AND pc_parcel_pkg.is_location_valid(prcl.id, prcl_area.area_id) = 'Y' 
    -- And they haven't been waiting for longer than N days
    AND trunc(sysdate - prcl.receive_dt) < wait_limit;

BEGIN
  FOR v_c IN valid_parcels
  LOOP
    DBMS_OUTPUT.put_line(' ');
    DBMS_OUTPUT.put_line('Email information: ');
    DBMS_OUTPUT.put_line('location: ' || v_c.location || ' track num: ' || v_c.track_num || ' email: ' || v_c.email);
  END LOOP;
END;
/




WITH current_parcel AS (  -- current non suspicious
  SELECT *
  FROM pc_parcel
  WHERE trunc(sysdate - receive_dt) < 6
), 
suspicious_parcels AS (  -- suspicious parcels
  SELECT prcl_id
  FROM pc_parcel_cond cond
  INNER JOIN pc_cond_stat_type typ
    ON cond.pc_cond_stat_id = typ.id
  WHERE typ.id = 1 
  AND cond.active_ind = 'Y'
), 
at_destination_parcels AS ( -- parcels at their final destination
  SELECT prcl.id AS prcl_id
  FROM current_parcel prcl
  INNER JOIN pc_parcel_list prcl_list
  ON prcl_list.id = prcl.prcl_list_id    
  INNER JOIN area
    ON prcl_list.area_id = area.id
  WHERE pc_parcel_pkg.is_location_valid(prcl.id, prcl_list.area_id) = 'Y' 
), 
available_parcels AS ( -- parcels there are current, not suspicious and at waiting their final destination
  SELECT prcl.*
  FROM current_parcel prcl
  INNER JOIN pc_track_stat_type trk_st
    ON prcl.trk_st_t_id = trk_st.id
  WHERE trk_st.id = 2   -- Are waiting
  AND prcl.id NOT IN (
    SELECT prcl_id FROM suspicious_parcels
    UNION ALL
    SELECT prcl_id FROM at_destination_parcels
  ) 
)
SELECT * FROM available_parcels order by id; 


DECLARE
  CURSOR available_parcels IS
    WITH current_parcel AS (  -- current waiting parcels
      SELECT *
      FROM pc_parcel
      WHERE trunc(sysdate - receive_dt) < 5
      AND trk_st_t_id = 2
    ), 
    suspicious_parcels AS (  -- suspicious parcels
      SELECT prcl_id
      FROM pc_parcel_cond cond
      INNER JOIN pc_cond_stat_type typ
        ON cond.pc_cond_stat_id = typ.id
      WHERE typ.id = 1 
      AND cond.active_ind = 'Y'
    ), 
    at_destination_parcels AS ( -- parcels at their final destination
      SELECT 
        prcl.*,
        area.title AS area_name 
      FROM current_parcel prcl
      INNER JOIN pc_parcel_list prcl_list
      ON prcl_list.id = prcl.prcl_list_id    
      INNER JOIN area
        ON prcl_list.area_id = area.id
      WHERE pc_parcel_pkg.is_location_valid(prcl.id, prcl_list.area_id) = 'Y' 
    ), 
    available_parcels AS ( -- parcels that are current, not suspicious and at waiting their final destination
      SELECT 
        prcl.*,
        person_pkg.get_email_address(prsn.id, 1) AS email
      FROM at_destination_parcels prcl
      INNER JOIN person prsn
        ON prcl.prsn_id = prsn.id
      WHERE prcl.id NOT IN (
        SELECT prcl_id FROM suspicious_parcels
      ) 
    )
    SELECT * FROM available_parcels order by id; 
    v_count NUMBER := 0;
BEGIN
  BEGIN
    JOSEMO.sp_create_apex_session(501,1);
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('no session created');
  END;
  FOR parcel IN available_parcels LOOP
    DBMS_OUTPUT.put_line('Area Desk: ' || parcel.area_name || ' Email information: ' || parcel.email); 
    DBMS_OUTPUT.put_line('person id: ' || parcel.prsn_id || ' id: ' || parcel.id || ' track num: ' || parcel.track_num || ' carrier: ' || parcel.carrier);
    DBMS_OUTPUT.put_line(' ');
    IF v_count = 0 THEN
      email_ua_pkg.send_pkg_received(
        p_prsn_id   => parcel.prsn_id,
        p_eml_addr  => parcel.email,
        p_prcl_id   => parcel.id,
        p_area      => parcel.area_name);
    END IF;
    v_count := v_count + 1;
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
    DBMS_OUTPUT.put_line('An error occured');
END;
/