var regex = [];

regex[0] = /(\b96\d{20}\b)|(\b\d{15}\b)|(\b\d{12}\b)/;
regex[1] = /\b((98\d\d\d\d\d?\d\d\d\d|98\d\d) ?\d\d\d\d ?\d\d\d\d( ?\d\d\d)?)\b/;
regex[2] = /^[0-9]{15}$/;

regex[3] = /\b(1Z ?[0-9A-Z]{3} ?[0-9A-Z]{3} ?[0-9A-Z]{2} ?[0-9A-Z]{4} ?[0-9A-Z]{3} ?[0-9A-Z]|[\dT]\d\d\d ?\d\d\d\d ?\d\d\d)\b/;

regex[4] = /(\b\d{30}\b)|(\b91\d+\b)|(\b\d{20}\b)/;
regex[5] = /^E\D{1}\d{9}\D{2}$|^9\d{15,21}$/;
regex[6] = /^91[0-9]+$/;
regex[7] = /^[A-Za-z]{2}[0-9]+US$/;
regex[8] = /\S{5}-\d{6}/;
regex[9] = /^[0-9]{10}$/;

var re = /\\/g;
for(var i = 0; i < regex.length; i++) {
    console.log(regex[i].toString().replace(re, '%5C'));
}

ups
420326089341989700090071302330