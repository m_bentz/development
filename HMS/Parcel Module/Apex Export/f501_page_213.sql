set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   15:51 Friday August 3, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00213
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>213);
end;
/
prompt --application/pages/page_00213
begin
wwv_flow_api.create_page(
 p_id=>213
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Parcel Form'
,p_page_mode=>'NORMAL'
,p_step_title=>'PRCL: Parcel Form'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_javascript_code=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var PAGE_TAG = "Page 213: ";',
'var RECEIVE = 1;',
'var NON_RES_RECEIVE = 2;',
'var FORWARD = 3;',
'var FORWARD_OFFCAMPUS = 9;',
'var DELIVER = 4;',
'var RETURN_TO_SENDER = 5;',
'var FORWARD_RECEIVE = 6;',
'var DEL_RETURN_SEND = 7;',
'var RETURN_RETURN_WAIT = 8;',
'',
'var ROW_FORWARDED = ''3'';',
'var ROW_DELIVERED = ''4'';',
'var ROW_RETURN = ''5'';',
'',
'var actions = [];',
'var regexs = [];',
'',
'',
'function getSQLDate() {',
'    var date;',
'    date = new Date();',
'    return (date.getMonth() + 1) + ''/'' + date.getDate() + ''/'' +  date.getFullYear();',
'}',
'',
'function closeDialog(dialogSelector) {',
'    $(dialogSelector).dialog(''close'');',
'}',
'',
'function submitPageItems(pageItems) {',
'    apex.server.process("Submit Page Items",{',
'        pageItems: pageItems',
'    },{',
'        dataType:"text"',
'    });',
'}',
'',
'/**',
' * The following happens when an action is selected.',
' *',
' * 1. An AJAX call "Get Associated Action" receives the associated action from the id returning',
' *    all of the table attributes in JSON format',
' * 2. The action is parsed to see if it needs to be confirmed or an inline',
' *    dialog needs to be opened.',
' * 3. The resulting action is retrieved with an AJAX call "Get Resulting Status" and the new status',
' *    ID is set.',
' * 4. The Action table is updated to reflect the current status of the parcel',
' *',
'**/',
'',
'function getAndSetNextAction(actionId, actionDescr) {',
'    apex.server.process("Get Resulting Status", {',
'        x01: actionId,',
'        x02: actionDescr,',
'        pageItems: "#P213_LOCATION"',
'    }, {',
'        dataType: "text",',
'        success: function(pData) {',
'           apex.item("P213_NEW_STAT_ID").setValue(pData.trim());',
'           updateRows("P213_STAT_ID","P213_NEW_STAT_ID");',
'           updateActionTable();',
'           apex.event.trigger("#ACTIONS", "apexrefresh");',
'        }',
'    });',
'}',
'',
'function updateActionTable() {',
'    // If the status of the package is currently waiting',
'    if (isWaiting()){                ',
'        //console.log(''package is waiting'');',
'        updateWaitStatus();',
'    } else {',
'        resetWaitStatus();',
'    }',
'    updateWaitActions();   ',
'    updateParcelInformation();',
'    showActionList();',
'}',
'',
'function updateRows(oldPageItem_StatusID, newPageItem_StatusID) {',
'    var oldRow = getRow(oldPageItem_StatusID);',
'    var newRow = getRow(newPageItem_StatusID);',
'',
'    // Disable the old row IFF there is a different resulting status',
'    if (hasChanged(oldRow, newRow)) {',
'',
'        // Remove the highlighting and disable buttons for old status',
'        disableRow(oldRow);',
'        // Highlight and enable buttons for result status',
'        enableRow(newRow);',
'',
'        // Update the old status',
'        updateOldRow(oldPageItem_StatusID, newRow);',
'    }',
'}',
'',
'function updateRow(pageItem_StatusID) {',
'    var row = getRow(pageItem_StatusID);',
'    enableRow(row);',
'}',
'',
'function getRow(pageItem_StatusID) {',
'    var statusID = apex.item(pageItem_StatusID).getValue();',
'    var status = $("#status-" + statusID);',
'    var td = status.parent("td");',
'',
'    var row = td.parent("tr");',
'    console.log(row);',
'    return row = {',
'        "status": statusID,',
'        "prclStatus": status,',
'        "container": row',
'    }',
'}',
'',
'function hideRow(row) {',
'    hide($("#status-" + row).closest(''tr''));//.addClass(''hide'');',
'}',
'',
'function disableRow(row) {',
'    // Buttons',
'    row.container.find($(".action-button")).each(function () {',
'        console.log($(this));',
'        $(this).removeClass("button-active");',
'        $(this).addClass("button-disabled");',
'    });',
'    ',
'    // Statuses',
'    row.prclStatus.removeClass("status-active");',
'    row.prclStatus.addClass("status-disabled");',
'}',
'',
'function enableRow(row) {',
'    // Buttons',
'    row.container.find($(".action-button")).each(function () {',
'        $(this).addClass("button-active");',
'        $(this).removeClass("button-disabled");',
'    });',
'    ',
'    // Statuses',
'    row.prclStatus.addClass("status-active");',
'    row.prclStatus.removeClass("status-disabled");',
'}',
'',
'function hasChanged(oldRow, newRow) {',
'    return oldRow.status !== newRow.status;',
'}',
'',
'function updateOldRow(oldPageItem_StatusID, newRow) {',
'    apex.item(oldPageItem_StatusID).setValue(newRow.status);',
'}',
'',
'function getActionID(element) {',
'    var ID = $(element).attr("id");',
'    var actionID =  ID.substr(ID.length - 1);',
'    return actionID;',
'}',
'',
'function buildURL(apexItemBaseURL, targetPage, targetPageItems, pageItemValues) {',
'    targetPageItems = (targetPageItems) ? targetPageItems.join(",") : "";',
'    pageItemValues = (pageItemValues) ? pageItemValues.join(",") : "";',
'    ',
'    ',
'    console.log(''target '', targetPageItems);',
'    console.log(''pass '', pageItemValues);',
'    ',
'    var l_url = "f?p="+$v("pFlowId")+":"+targetPage+":"+$v("pInstance")+"::NO::"+targetPageItems+":"+pageItemValues+"&"+apexItemBaseURL;',
'    console.log(l_url);',
'    return l_url;',
'}',
'',
'function openModal(l_url, options) {',
'    apex.navigation.dialog(l_url, {',
'        title: options.title,',
'        height: options.height,',
'        width: options.width,',
'        maxWidth: options.maxWidth,',
'        model: true,',
'        dialog: null},',
'        ''t-Dialog--standard'',',
'        $(this));',
'}',
'',
'function hideAction(id) {',
'    console.log(''hiding element: '' + id);',
'    var element = $("#action-" + id + "");',
'    element.addClass("hide");',
'}',
'',
'function showAction(id) {',
'    var element = $("#action-" + id + "");',
'    element.removeClass("hide");',
'}',
'',
'function enableAction(id) {',
'    var element = $("#action-" + id + "");',
'    //console.log(element);',
'    element.removeClass("button-disabled");',
'    element.addClass("button-active");',
'}',
'function disableAction(id) {',
'    var element = $("#action-" + id + "");',
'    //console.log(element);',
'    element.removeClass("button-active");',
'    element.addClass("button-disabled");',
'}',
'',
'/*',
' | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert',
' | @param duration  : (Optional) The amount of time the toast remains on the page in ms.',
' |                     The default time is 3s.',
'*/',
'function createToastAlert(PAGE_ITEM, duration) {',
'    // Check to see if a duration was given',
'    duration = (typeof duration === ''undefined'') ? 3000 : duration;',
'    ',
'    // Remove any previous toasts',
'    $(''.alert-toast'').remove();',
'    ',
'    // Prepend the toast',
'    $(''#t_Body_content'').prepend(PAGE_ITEM.getValue());',
'    ',
'    // Get the toast that was just added ',
'    let alertToastBody= $(''#t_Body_content'').children(".alert-toast:first");',
'    let alertToastContent = alertToastBody.children(".t-Alert:first");',
'    ',
'    // Fade in the alert',
'    alertToastContent.fadeTo(1, 1000);',
'    ',
'    // Toasts are always automatically dismissed',
'    dismissAlert(alertToastBody, duration);',
'}',
'',
'/*',
' | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert',
' | @param duration  : (Optional) The amount of time the message remains on the page.',
' |                     By entering a duration the message auto dismisses like a toast.',
' |                     Otherwise it needs to be manually closed by clicking the X.',
'*/',
'function createMsgAlert(PAGE_ITEM, duration) {',
'    // Check to see if a duration was given',
'    duration = (typeof duration === ''undefined'') ? 0 : duration;',
'    ',
'    // Remove any previous messages',
'    $(''.alert-msg'').remove();',
'    ',
'    // Prepend the message',
'    $(''#t_Body_content'').prepend(PAGE_ITEM.getValue());',
'    ',
'    // Get the message that was just added ',
'    let alertMessageBody= $(''#t_Body_content'').children(".alert-msg:first");',
'    let alertMessageContent = alertMessageBody.children(".t-Alert:first");',
'    ',
'    // Fade in the alert',
'    alertMessageContent.fadeTo(1, 1000);',
'    ',
'    if (duration !== 0) {',
'        dismissAlert(alertMessageBody, duration);',
'    }',
'}',
'',
'// Dimisses the alert after the designated amount of time',
'function dismissAlert(alertBody, delay){',
'    // Animate the alert content and then remove the body',
'    let alertContent =  alertBody.children(".t-Alert:first");',
'    window.setTimeout(function(e) {',
'        alertContent.slideUp(''slow'', function(e) {',
'           alertBody.remove();',
'       });',
'    }, delay);',
'}',
'',
'// On-click listener for alert messages',
'$(''.t-Body-content'').on(''click'', ''.alert-msg .t-Button'',  function(e) {',
'   console.log(''button clicked''); ',
'    ',
'    let element = $(''.t-Button'').closest(''.t-Alert'');',
'    element.slideUp(''slow'', function(e) {',
'        element.closest(''.t-Body-alert'').remove();',
'    });',
'});',
'',
'// Updates the wait actions depending on if the parcel is currently',
'// at the correct location and if the recipient is a resident or not',
'function updateWaitActions() {',
'    var ai_uspsInd = apex.item("P213_USPS_IND");',
'    var ai_nonResInd = apex.item("P213_NON_RES_IND");',
'    ',
'    if (isWaiting()) {',
'        if (ai_nonResInd.getValue() === ''Y'') {              // If the recipient is NOT a resident',
'            hideAction(FORWARD);                            // Disable forward action',
'            showAction(FORWARD_OFFCAMPUS);',
'            if (ai_uspsInd.getValue() === ''Y'') {            // If the package is from USPS',
'                enableAction(FORWARD_OFFCAMPUS);',
'                disableAction(DELIVER);                     // Disable the deliver action',
'            } else {                                        // If the package is NOT from USPS',
'                disableAction(DELIVER);                     // Disable deliver action',
'                disableAction(FORWARD_OFFCAMPUS);',
'            }',
'        } else {                                            // If the recipient IS a resident  ',
'            if (locationInvalid()) {                          // If the location is VALID           ',
'                disableAction(DELIVER);                     // Disable deliver action',
'            } else {                                        // The location is INVALID',
'                disableAction(FORWARD);                     // Disable forward action',
'            }   ',
'        }',
'    }   ',
'}',
'',
'function hideNonResidentStatuses() { ',
'    if (isNonResident()) {            // If the package does not belong to a resident',
'        console.log(''non resident'');',
'        //hideRow(ROW_FORWARDED);',
'        hideRow(ROW_DELIVERED);',
'        //hideRow(ROW_RETURN);',
'    }',
'}',
'',
'function locationInvalid(){',
'    var flag = getValue("P213_INVALID_LOCATION");',
'    return flag === ''Y'';',
'}',
'',
'function updateParcelInformation() {',
'    var status = apex.item("P213_STAT_ID").getValue();',
'    if (status == ROW_RETURN) {',
'        show($("#P213_RETURN_RSN_CONTAINER"));//.addClass(''hide'');',
'        console.log(''status is returning'');',
'    } else {',
'        hide($("#P213_RETURN_RSN_CONTAINER"));',
'    }',
'}',
'',
'//function isNewParcel() {',
'//    return apex.item("P213_STAT_ID").getValue() == 1;',
'//}',
'',
'function isSuspicious() {',
'    return getValue("P213_SUSP_IND") === ''Y'';',
'}',
'',
'// Returns a Date object given a jquery element ',
'// with the format MM/DD/YYYY (sql default)',
'function getParcelDate(jqueryElement) {',
'    // Parse through the information',
'    var month = parseInt(jqueryElement.text().substring(0,2));',
'    var day = parseInt(jqueryElement.text().substring(3,5));',
'    var year = parseInt(jqueryElement.text().substring(6,10));',
'    ',
'    // Need to return month - 1 because month in JS starts at 0',
'    return new Date(year, month - 1, day);',
'}',
'',
'// Calculates the amount of days that have passed from the current date',
'// And returns the corresponding amount',
'function getDaysPassed(parcelDate) {',
'    var secondsInDay = 86400;',
'    var milliseconds = 1000;',
'    ',
'    // Get the current date',
'    var currentDate = new Date();',
'',
'    // Calculate the difference',
'    var difference = parseInt((currentDate - parcelDate)/milliseconds/secondsInDay);',
'',
'    // Check if the difference is greater than a day',
'    if (difference > 0) {',
'        // Return the amount of days',
'        return difference + " day" + (difference > 1 ? "s" : "");',
'    }',
'    return "";',
'}',
'',
'function isWaiting(){',
'    return apex.item("P213_STAT_ID").getValue() == 2;',
'}',
'',
'function isNonResident() {',
'    console.log(''checking if package is non resident'');',
'    return apex.item("P213_NON_RES_IND").getValue() === ''Y'';',
'}',
'',
'function updateWaitStatus() {',
'    var element = $("#P213_RECEIVED_ON");',
'    ',
'    var parcelDate = getParcelDate(element);',
'    console.log(PAGE_TAG + "parcel date: " + parcelDate);',
'    ',
'    ',
'    //$("#status-2").text("Waiting " + getDaysPassed(parcelDate));',
'}',
'',
'function resetWaitStatus() {',
'    $("#status-2").text("Waiting");',
'}',
'',
'function logAction() {',
'    var action = $("#action-"+ getValue("P213_ACTION_ID")).text();',
'    var oldStatus = $("#status-" + getValue("P213_STAT_ID")).text();',
'    var newStatus = $("#status-" + getValue("P213_NEW_STAT_ID")).text();',
'    ',
'    var headline = "Action ''" + action + "'' was selected";',
'    var description = "The status of the parcel was changed from ''" + oldStatus + "'' to ''" + newStatus + "''";',
'    ',
'    var action = {',
'        ''start_date'' : getValue("P213_CURRENT_DATE"),',
'        ''end_date'': getValue("P213_CURRENT_DATE"),',
'        ''headline'': headline,',
'        ''description'': description',
'    }',
'    actions.push(action);',
'}',
'',
'function getValue(id) {',
'    return apex.item(id).getValue();',
'}',
'',
'function setValue(id, value) {',
'    apex.item(id).setValue(value);',
'}',
'',
'function showActionList() {',
'    $(".t-Body-contentInner > .container").css(''max-width'', ''1440px'');',
'    $("#main-content").parent().removeClass(''col-12'');',
'    $("#main-content").parent().addClass(''col-9'');',
'    $("#side-content").fadeIn();',
'}',
'',
'function determineCarrier(trackNum) {',
'    var carrier_id = null;',
'',
'    // Search through the regex for the associated carrier',
'    for(regex in regexs) {',
'        console.log(regexs[regex]);',
'        // If the tracking number matches a carrier then set the dropdown',
'        if (trackNum.match(regexs[regex].pattern)) {',
'            console.log(''found in: '' + regexs[regex].carrier_id);',
'            carrier_id = regexs[regex].carrier_id;',
'            return carrier_id;',
'            ',
'            //apex.item("P213_CARRIER_ID").setValue(carrier_id);',
'            //hideCarrierOpt();',
'            //return;',
'        }',
'    } ',
'    // Carrier could not be matched',
'    // Set the carrier to Other and allow user to enter a carrier',
'    console.log(''not found'');',
'    return 0;',
'    //apex.item("P213_CARRIER_ID").setValue(0);',
'    //showCarrierOpt();   ',
'}',
'',
'function hideCarrierOpt() {',
'    apex.item("P213_CARRIER_OPT").hide();',
'}',
'',
'function showCarrierOpt() {',
'    apex.item("P213_CARRIER_OPT").show();',
'    apex.item("P213_CARRIER_OPT").setFocus();',
'}',
'',
'function hide(jqueryElement) {',
'    jqueryElement.addClass(''hide'');',
'}',
'',
'function show(jqueryElement) {',
'    jqueryElement.removeClass(''hide'');',
'}'))
,p_inline_css=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'.t-Body-contentInner > .container {',
'    max-width: 1440px;',
'}',
'',
'.a-IRR-sortWidget {',
'    z-index: 999;',
'}',
'',
'.a-IRR-header {',
'    visibility: visible !important;',
'}',
'',
'.t-Button.t-Button--simple.button-disabled {',
'    background-color: #e6e6e6;',
'    color: #b1aaaa;',
'    box-shadow: none;',
'    cursor: not-allowed;',
'}',
'',
'.mismatch {',
'    border-color:#e24c0e !important;',
'}',
'',
'.hidden {',
'    visibility: hidden;',
'}',
'',
'.button-disabled:hover {',
'    background-color: inherit;',
'    color: inherit;',
'}',
'',
'.t-Button.t-Button--simple.button-active {',
'    color: blue;',
'    transition: 0.1s;',
'}',
'',
'.t-Button.t-Button--simple.button-active:hover {',
'    background-color: white;',
'    transform: scale(1.05);',
'}',
'',
'.prcl-status {',
'    font-size: 1.2em;',
'    font-weight: bold;',
'}',
'',
'.status-active {',
'    width: 100%;',
'    color: #317735;',
'}',
'',
'.status-disabled {',
'    color: #b1aaaa;',
'}',
'',
'.hide {',
'    display: none;',
'}',
'',
'.hidden-row {',
'    visibility: hidden;',
'}',
'',
'.content-Mask {',
'    position: absolute;',
'    height: 100%;',
'    width: 100%;',
'    z-index: 2; ',
'    background-color: rgba(173, 173, 173, 0.36);',
'}',
'',
'.t-BreadcrumbRegion-titleText {',
'    height: 50px;',
'}',
'',
'.mask-Button {',
'    z-index: 3;',
'}',
'',
'',
'.t-Body-contentInner {',
'    padding-top: 40px;',
'}',
'',
'.t-Button + .t-Button {',
'    margin: 0;',
'}',
'',
'span.action-button.t-Button {',
'    margin: 0 .4rem;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180803155111'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62860625920742447)
,p_plug_name=>'DB Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224168893844909914)
,p_plug_name=>'Mask Container'
,p_region_name=>'mask-container'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Region--noUI:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224569784595521918)
,p_plug_name=>'Main Content'
,p_region_name=>'main-content'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_grid_column_span=>9
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62860753937742448)
,p_plug_name=>'Parcel Information'
,p_parent_plug_id=>wwv_flow_api.id(224569784595521918)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36909130176773106)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(65016184374038540)
,p_plug_name=>'Button Region'
,p_parent_plug_id=>wwv_flow_api.id(224569784595521918)
,p_region_template_options=>'#DEFAULT#:t-ButtonRegion--noUI'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(65169890172713726)
,p_plug_name=>'Parcel Processing'
,p_parent_plug_id=>wwv_flow_api.id(224569784595521918)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62860802206742449)
,p_plug_name=>'Options'
,p_parent_plug_id=>wwv_flow_api.id(65169890172713726)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Region--noUI:t-Form--slimPadding:t-Form--large:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36909130176773106)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_grid_column_span=>5
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542206797766619)
,p_plug_name=>'Non-Resident mask'
,p_region_name=>'non-res-mask'
,p_parent_plug_id=>wwv_flow_api.id(62860802206742449)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(63992294336219808)
,p_name=>'Parcel Actions'
,p_region_name=>'PROCCESS_GRID'
,p_parent_plug_id=>wwv_flow_api.id(65169890172713726)
,p_template=>wwv_flow_api.id(36903682925773102)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#:t-Form--large'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight:t-Report--horizontalBorders'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
' -- Get each status and the associated actions',
'SELECT ',
'    ''<span id="status-''||status.id ||''" class="prcl-status status-disabled">'' || status.title || ''</span>'' AS "STATUS", ',
'    listagg(''<span data-action-id="'' || action.id || ''" id="action-''|| action.id ||''" class="action-button t-Button t-Button--simple button-disabled">'' || initcap(action.title) || ''</span>'','' '') ',
'    WITHIN GROUP(ORDER BY status.title, action.result_trk_st_t_id) AS "ACTION" ',
'FROM pc_track_action action',
'INNER JOIN pc_track_stat_type status',
'ON action.trk_st_t_id = status.id',
'GROUP BY status.id, status.title',
'ORDER BY status.id;'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36946831572773134)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(65169959891713727)
,p_query_column_id=>1
,p_column_alias=>'STATUS'
,p_column_display_sequence=>1
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(65170073451713728)
,p_query_column_id=>2
,p_column_alias=>'ACTION'
,p_column_display_sequence=>2
,p_column_heading=>'Action'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(65171325185713741)
,p_plug_name=>'Track #'
,p_parent_plug_id=>wwv_flow_api.id(224569784595521918)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Region--noUI:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224569809252521919)
,p_plug_name=>'Side Content'
,p_region_name=>'side-content'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_column=>10
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(224569994802521920)
,p_name=>'Actions Taken Report'
,p_region_name=>'ACTIONS'
,p_parent_plug_id=>wwv_flow_api.id(224569809252521919)
,p_template=>wwv_flow_api.id(36885065544773090)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#:t-Cards--featured:t-Cards--float:t-Cards--hideBody'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT c001 As CARD_TITLE',
'FROM apex_collections',
'WHERE collection_name = ''ACTIONS''',
'ORDER BY d001 '))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36937050985773127)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224571303707521934)
,p_query_column_id=>1
,p_column_alias=>'CARD_TITLE'
,p_column_display_sequence=>1
,p_column_heading=>'Card title'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224571479655521935)
,p_plug_name=>'Pending Actions'
,p_parent_plug_id=>wwv_flow_api.id(224569809252521919)
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useRegionTitle:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36931374122773123)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225356483065569841)
,p_plug_name=>'Indicator Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225356756332569844)
,p_plug_name=>'UI/Nav Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226171987927305438)
,p_plug_name=>'Receive'
,p_region_name=>'RECEIVE_RES'
,p_region_template_options=>'#DEFAULT#:js-dialog-size720x480:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36901794136773101)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226410616907345921)
,p_plug_name=>'Residents'
,p_region_name=>'RESIDENTS'
,p_parent_plug_id=>wwv_flow_api.id(226171987927305438)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36903682925773102)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM (',
'  WITH area_desk AS (',
'    SELECT',
'      id,',
'      title',
'    FROM area',
'    WHERE hsng_t_id = 1)',
'  SELECT',
'    person_pkg.get_med_img(prsn.id) AS "Img",',
'    initcap(prsn.first_name || '' '' || prsn.last_name) AS "Student",',
'    prsn.ufid AS "UFID",',
'    term.title AS "Semester",',
'    area_desk.title AS "Area",',
'    trim(LEADING ''0'' FROM room.rm_num) AS "Room Number",',
'    ''<button type="button" data-prsn-ufid="''|| prsn.ufid ||''" data-area-id="'' || area_desk.id || ''" data-name="'' || initcap(prsn.first_name || '' '' || prsn.last_name) || ''" '' ||',
'         ''class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>'' AS Action',
'  FROM v_ua_assign_status assign',
'  INNER JOIN space_room_term srt',
'    ON assign.sp_rm_trm_id = srt.id',
'  INNER JOIN room_term rt',
'    ON srt.rm_trm_id = rt.id',
'  INNER JOIN room',
'    ON rt.rm_id = room.id',
'  INNER JOIN term',
'    ON rt.trm_id = term.id',
'  INNER JOIN building b',
'    ON room.bldg_id = b.id',
'  INNER JOIN area_desk ',
'    ON b.area_id = area_desk.id',
'  INNER JOIN contract_ua c',
'    ON assign.conu_id = c.id',
'  INNER JOIN agreement agree',
'    ON c.agree_id = agree.id',
'  INNER JOIN person prsn',
'    ON agree.prsn_id = prsn.id',
'  LEFT OUTER JOIN checkin_appt_resident car',
'    ON assign.asg_id = car.asg_id',
'  LEFT OUTER JOIN checkin_appt_config config',
'    ON car.cki_apt_cnf_id = config.id',
'  LEFT OUTER JOIN v_checkin_appt_time cat',
'    ON car.cki_apt_tm_id = cat.id',
'  WHERE ( assign.checkin_dt IS NULL OR assign.checkin_dt + 21 > SYSDATE )',
'    AND assign.deleted_ind = ''N''',
'    AND assign.end_dt > SYSDATE',
'    AND ( assign.same_sp_prev_trm_ind = ''N'' OR assign.moved_in_ind = ''N'' )',
'    AND NOT ( assign.moved_out_ind = ''N'' AND assign.same_sp_prev_trm_ind = ''Y'')',
'  ORDER BY decode(area_desk.id, :P213_DESK_AREA_ID, 1, 2), term.id asc, prsn.last_name)',
'UNION ',
'SELECT *',
'FROM (',
'  WITH area_desk AS (',
'    SELECT',
'      id,',
'      title',
'    FROM area',
'    WHERE hsng_t_id = 1)',
'  SELECT',
'    person_pkg.get_med_img(prsn.id) AS "Img",',
'    initcap(prsn.first_name || '' '' || prsn.last_name) AS "Student",',
'    prsn.ufid AS "UFID",',
'    res.semester AS "Semester",',
'    area_desk.title AS "Area",',
'    trim(LEADING ''0'' FROM res.room_apt) AS "Room",',
'    ''<button type="button" data-prsn-ufid="''|| prsn.ufid ||''" data-area-id="'' || area_desk.id || ''" data-name="'' || initcap(prsn.first_name || '' '' || prsn.last_name) || ''" '' ||',
'       ''class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>'' AS Action',
'  FROM person prsn ',
'  INNER JOIN v_curr_next_res res ',
'      ON prsn.ufid = res.ufid ',
'  INNER JOIN term',
'      ON res.semester = term.title',
'  INNER JOIN area_desk ',
'      ON area_desk.title LIKE res.area_village',
'  ORDER BY decode (area_desk.id, :P213_DESK_AREA, 1, 5), term.id asc, prsn.last_name asc);'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
end;
/
begin
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(226410938323345924)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'MICHAELBE'
,p_internal_uid=>226410938323345924
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226411830050345933)
,p_db_column_name=>'Img'
,p_display_order=>10
,p_column_identifier=>'C'
,p_column_label=>'Img'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997438439581326)
,p_db_column_name=>'Student'
,p_display_order=>20
,p_column_identifier=>'D'
,p_column_label=>'Student'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997551577581327)
,p_db_column_name=>'UFID'
,p_display_order=>30
,p_column_identifier=>'E'
,p_column_label=>'Ufid'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997650659581328)
,p_db_column_name=>'Semester'
,p_display_order=>40
,p_column_identifier=>'F'
,p_column_label=>'Semester'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997753522581329)
,p_db_column_name=>'Area'
,p_display_order=>50
,p_column_identifier=>'G'
,p_column_label=>'Area'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997855353581330)
,p_db_column_name=>'Room Number'
,p_display_order=>60
,p_column_identifier=>'H'
,p_column_label=>'Room number'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743997934293581331)
,p_db_column_name=>'ACTION'
,p_display_order=>70
,p_column_identifier=>'I'
,p_column_label=>'Action'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(226489887608169264)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'2264899'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>50
,p_report_columns=>'Img:Student:UFID:Semester:Area:Room Number:ACTION'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226172591559305444)
,p_plug_name=>'Receive (Non-Resident)'
,p_region_name=>'RECEIVE_NON_RES'
,p_region_template_options=>'#DEFAULT#:js-dialog-size480x320:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36901794136773101)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226172666747305445)
,p_plug_name=>'Forward'
,p_region_name=>'FORWARD'
,p_region_template_options=>'#DEFAULT#:js-dialog-size600x400:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36901794136773101)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(226574394529916902)
,p_name=>'Resident Location'
,p_region_name=>'RES_LOCATION'
,p_parent_plug_id=>wwv_flow_api.id(226172666747305445)
,p_template=>wwv_flow_api.id(36922375712773118)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT',
'    title AS "Current Student Area",',
'    ''<button type="button" data-area-id="''|| id ||''" data-area-title="'' || area.title || ''" '' ||',
'       ''class="select-area t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Area</button>'' AS Action',
'FROM area',
'WHERE hsng_t_id = 1',
'AND title LIKE (',
'    SELECT area_village',
'    FROM v_curr_next_res',
'    WHERE ufid = :P213_RES_UFID',
'    GROUP BY area_village)',
'ORDER BY title',
'    '))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P213_RES_UFID'
,p_query_row_template=>wwv_flow_api.id(36946831572773134)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(226575405643916913)
,p_query_column_id=>1
,p_column_alias=>'Current Student Area'
,p_column_display_sequence=>1
,p_column_heading=>'Current student area'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(226575527172916914)
,p_query_column_id=>2
,p_column_alias=>'ACTION'
,p_column_display_sequence=>2
,p_column_heading=>'Action'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226172738054305446)
,p_plug_name=>'Return to Sender'
,p_region_name=>'RETURN'
,p_region_template_options=>'#DEFAULT#:js-dialog-size480x320'
,p_plug_template=>wwv_flow_api.id(36901794136773101)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226445700972383903)
,p_plug_name=>'Deliver Package'
,p_region_name=>'DELIVER'
,p_region_template_options=>'#DEFAULT#:js-dialog-size600x400'
,p_plug_template=>wwv_flow_api.id(36901794136773101)
,p_plug_display_sequence=>80
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(226446432392383949)
,p_name=>'Residents'
,p_parent_plug_id=>wwv_flow_api.id(226445700972383903)
,p_template=>wwv_flow_api.id(36922375712773118)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noPadding:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT',
'    person_pkg.get_med_img(prsn.id) AS Picture,',
'    initcap(prsn.first_name || '' '' || prsn.last_name) AS "Name"',
'FROM person prsn',
'WHERE prsn.ufid = :P213_RES_UFID'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36946831572773134)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(226409589778345910)
,p_query_column_id=>1
,p_column_alias=>'PICTURE'
,p_column_display_sequence=>1
,p_column_heading=>'Picture'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_report_column_width=>30
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(226409673475345911)
,p_query_column_id=>2
,p_column_alias=>'Name'
,p_column_display_sequence=>2
,p_column_heading=>'Name'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(226447168848383952)
,p_plug_name=>'UFID Container'
,p_parent_plug_id=>wwv_flow_api.id(226445700972383903)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noPadding:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224168958997909915)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(224168893844909914)
,p_button_name=>'MARK_SAFE'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974615710773160)
,p_button_image_alt=>'Mark Safe'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'mask-Button'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(65016215137038541)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(65016184374038540)
,p_button_name=>'Submit'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconLeft'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Confirm'
,p_button_position=>'BELOW_BOX'
,p_icon_css_classes=>'fa-check'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(226408900760345904)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(226172738054305446)
,p_button_name=>'RETURN'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconLeft'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Confirm'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-check'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(226411456459345929)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(226172591559305444)
,p_button_name=>'RECEIVE_NON_RES'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974615710773160)
,p_button_image_alt=>'Receive'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(226413055461345945)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(226171987927305438)
,p_button_name=>'RECEIVE_RES'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconLeft'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Confirm Resident'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-check'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(226446030986383905)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(226445700972383903)
,p_button_name=>'DELIVER_PKG'
,p_button_static_id=>'DELIVER_PKG'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconLeft'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Deliver'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-check'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(226574999837916908)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(226172666747305445)
,p_button_name=>'FORWARD'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconLeft'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Confirm Area'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-check'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(65016888672038547)
,p_branch_name=>'Return to Area Desk'
,p_branch_action=>'f?p=&APP_ID.:212:&SESSION.::&DEBUG.:213::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(62860977114742450)
,p_name=>'P213_OPTIONS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(62860802206742449)
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT title AS d,',
'       id AS r',
'FROM pc_cond_stat_type',
'ORDER BY id;'))
,p_lov_display_null=>'YES'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--xlarge'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63992978478219815)
,p_name=>'P213_OPT_DESCR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(62860802206742449)
,p_prompt=>'Optional Description'
,p_placeholder=>'This package is...'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>5
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63993265019219818)
,p_name=>'P213_TRACK_NUM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(65171325185713741)
,p_prompt=>'Scan or enter parcel id/tracking #'
,p_placeholder=>'Scan tracking #'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974382285773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--xlarge'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63993364947219819)
,p_name=>'P213_RES_UFID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63993490762219820)
,p_name=>'P213_CARRIER_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Carrier:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'SELECT title, id FROM pc_carrier ORDER BY decode (id, 0, 4, 1)'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_colspan=>4
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63995075181219836)
,p_name=>'P213_PRCL_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63995444379219840)
,p_name=>'P213_NEW_STAT_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63995593038219841)
,p_name=>'P213_ACTION_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(63995993358219845)
,p_name=>'P213_STAT_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64104912510999118)
,p_name=>'P213_URL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(225356756332569844)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64105071757999119)
,p_name=>'P213_COND_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225356756332569844)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64363182449950346)
,p_name=>'P213_ACTION'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64512122065944737)
,p_name=>'P213_AREA_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65015675249038535)
,p_name=>'P213_LOCATION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Selected Pickup Area: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65167823043713706)
,p_name=>'P213_REQ_TYPE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(225356756332569844)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65169061122713718)
,p_name=>'P213_RECIPIENT_NAME'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Recipient:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65170282019713730)
,p_name=>'P213_INVALID_LOCATION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65170398749713731)
,p_name=>'P213_ALERT'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225356756332569844)
,p_use_cache_before_default=>'NO'
,p_source=>'APP_ALERT_MSG'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65170489628713732)
,p_name=>'P213_DESK_AREA_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65171279840713740)
,p_name=>'P213_DESK_AREA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65542282512992530)
,p_name=>'P213_RECEIVED_ON'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Received:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65542855340992536)
,p_name=>'P213_CURRENT_DATE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224041348332352845)
,p_name=>'P213_VIEW_ONLY'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224167604988909902)
,p_name=>'P213_SUSP_IND'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224568975596521910)
,p_name=>'P213_REGEX'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224569339928521914)
,p_name=>'P213_CARRIER_OPT'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Carrier opt'
,p_placeholder=>'Specify the Carrier'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225356560933569842)
,p_name=>'P213_USPS_IND'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225356654908569843)
,p_name=>'P213_NON_RES_IND'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225357399165569850)
,p_name=>'P213_ACTION_PROMPT_IND'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225540533610766602)
,p_name=>'P213_ACTION_CONFIRMED_IND'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225541752610766614)
,p_name=>'P213_RETURNED_PAGE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(225356756332569844)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225543180850766628)
,p_name=>'P213_NON_RES_F_NAME'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225543272824766629)
,p_name=>'P213_NON_RES_L_NAME'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225545002212766647)
,p_name=>'P213_RETURN_RSN'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(62860753937742448)
,p_prompt=>'Reason for return'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_display_when=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_prcl    pc_parcel%ROWTYPE;',
'BEGIN',
'    v_prcl := pc_parcel_cru.get_pc_parcel(:P213_PRCL_ID);',
'    ',
'    IF v_prcl.return_rsn IS NULL THEN',
'        RETURN TRUE;',
'    ELSIF v_prcl.return_rsn IS NOT NULL AND :P213_STAT_ID = pc_track_stat_type_pkg.get_returning_to_sender() THEN',
'        RETURN TRUE;',
'    ELSE',
'        RETURN TRUE;',
'    END IF;',
'END;'))
,p_display_when_type=>'FUNCTION_BODY'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225640221240787701)
,p_name=>'P213_DIALOG_IND'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226168286785305401)
,p_name=>'P213_RES_SEMESTER'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226408717640345902)
,p_name=>'P213_IDA_PRE_RETURN_RSN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(226172738054305446)
,p_prompt=>'Ida pre return rsn'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Not a Housing Resident,Resident Failed to Collect, Resident Failed to Collect from Locker, Resident Requested Return,Other'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Select a Reason'
,p_cHeight=>1
,p_tag_css_classes=>'focus-field'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226408819864345903)
,p_name=>'P213_IDA_USR_RETURN_RSN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(226172738054305446)
,p_prompt=>'Ida usr return rsn'
,p_placeholder=>'Enter Reason..'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>5
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226409377617345908)
,p_name=>'P213_CURRENT_IDA'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_api.id(62860625920742447)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226410307351345918)
,p_name=>'P213_IDA_UFID_ERROR_TXT'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(226447168848383952)
,p_prompt=>'UFID does not match.'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>11
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_css_classes=>'hidden error-text'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226410554416345920)
,p_name=>'P213_RESIDENTS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(226171987927305438)
,p_prompt=>'Residents'
,p_display_as=>'PLUGIN_BE.CTB.SELECT2'
,p_lov=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT ',
'    person_pkg.get_med_img(prsn.id) || ',
'    ''<div class="info-container">'' ||',
'        ''<div class="inner-info-container"><p class="disp-info">'' || ',
'            initcap(prsn.first_name) ||'' ''|| ',
'            initcap(prsn.last_name) || ''</p>'' || ''<p class="info">'' || '' UFID: '' || prsn.ufid || ''</p>'' || ''<p class="info">'' || res.area_village || '', Room #'' || ltrim(res.room_apt,''0'') || ''</p><p class="info">'' || res.semester ||  ',
'        ''</p></div></div>'' ||',
'    ''<span style="display:none">'' || '''''''' || prsn.ufid || ''</span>'' AS NAME, ',
'    prsn.ufid || '':'' ||  res.semester ',
'FROM person prsn ',
'INNER JOIN v_curr_next_res res ',
'    ON prsn.ufid = res.ufid ',
'INNER JOIN area ',
'    ON area.title LIKE res.area_village ',
'WHERE hsng_t_id IN 1 AND rownum < 1000 ',
'ORDER BY decode (res.area_village, :P213_COND_ID, 1, 5), prsn.last_name asc;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'test'
,p_lov_null_value=>'dddd'
,p_colspan=>12
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_escape_on_http_output=>'N'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
,p_attribute_01=>'SINGLE'
,p_attribute_08=>'MW'
,p_attribute_10=>'500'
,p_attribute_13=>'This person is not a resident. Choose Receive (Non-Resident) instead to proceed.'
,p_attribute_14=>'Y'
,p_attribute_15=>'5'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226411237465345927)
,p_name=>'P213_IDA_NON_RES_F_NAME'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(226172591559305444)
,p_prompt=>'Ida non res f name'
,p_placeholder=>'First Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_css_classes=>'focus-field'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226411377987345928)
,p_name=>'P213_IDA_NON_RES_L_NAME'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(226172591559305444)
,p_prompt=>'Ida non res l name'
,p_placeholder=>'Last Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226411538727345930)
,p_name=>'P213_NON_RES_ERROR_TEXT'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(226172591559305444)
,p_prompt=>'First Name is required.'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>11
,p_grid_column_css_classes=>'hidden error-text'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226412996342345944)
,p_name=>'P213_IDA_SELECTED_RESIDENT'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(226171987927305438)
,p_prompt=>'Selected Resident: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226447554230383952)
,p_name=>'P213_IDA_UFID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(226447168848383952)
,p_prompt=>'UFID'
,p_placeholder=>'Scan/Enter UFID'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_css_classes=>'focus-field ufid-scan'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226574742859916906)
,p_name=>'P213_IDA_SELECTED_AREA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(226172666747305445)
,p_prompt=>'Selected Area: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>3
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226577430276916933)
,p_name=>'P213_RETURN_IND'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226577546861916934)
,p_name=>'P213_RECEIVE_IND'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226774483493099501)
,p_name=>'P213_SUSP_STAT_CHANGED'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_api.id(225356483065569841)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(65170781686713735)
,p_validation_name=>'Tracking # not null'
,p_validation_sequence=>10
,p_validation=>'P213_TRACK_NUM'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Please enter the tracking number.'
,p_always_execute=>'N'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(65168055294713708)
,p_validation_name=>'Recipient not null'
,p_validation_sequence=>20
,p_validation=>'P213_RECIPIENT_NAME'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'The package needs to be received first.'
,p_always_execute=>'N'
,p_validation_condition=>':P213_NON_RES_IND = ''N'''
,p_validation_condition_type=>'PLSQL_EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(224569672914521917)
,p_validation_name=>'Carrier not null'
,p_validation_sequence=>40
,p_validation=>'P213_CARRIER_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Please select a carrier.'
,p_always_execute=>'N'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(63994447047219830)
,p_name=>'Action Selection'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.action-button.button-active'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(apex.item("P213_TRACK_NUM").getValue() !== "" && apex.item("P213_CARRIER_ID").getValue() !== "")'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'tr'
,p_bind_event_type=>'click'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(63994673278219832)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(65016184374038540)
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Get the action ID of the action that was selected',
'var id = getActionID($(this)[0].triggeringElement);',
'apex.item("P213_ACTION_ID").setValue(id);',
'',
'console.log(this.affectedElements);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225356383449569840)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var trackNum = apex.item("P213_TRACK_NUM");',
'var carrierID = apex.item("P213_CARRIER_ID");',
'',
'if (!trackNum.getValue()) {',
'    trackNum.setFocus();',
'} else {',
'    carrierID.setFocus();',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(63995243377219838)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_track_action      pc_track_action%ROWTYPE;',
'    v_res_track_status  pc_track_stat_type%ROWTYPE;',
'    v_app               NUMBER := v(''APP_ID'');',
'    v_session           NUMBER := v(''APP_SESSION'');',
'    v_pg_num            VARCHAR2(10);',
'BEGIN',
'     -- Default the values',
'    :P213_DIALOG_IND := NULL; ',
'    :P213_ACTION_PROMPT_IND := ''N'';',
'',
'    v_track_action := pc_track_action_cru.get_pc_track_action(:P213_ACTION_ID);                             -- Get the selected action',
'    v_res_track_status := pc_track_stat_type_cru.get_pc_track_stat_type(v_track_action.result_trk_st_t_id); -- Get the resulting status',
'    ',
'    :P213_NEW_STAT_ID := v_res_track_status.id;     -- Set the new status id',
'    ',
'    -- Create a Psuedo JS object',
'    SELECT initcap(title) || '','' || prompt_ind || '','' || msg  || '','' || ',
'        CASE query',
'            WHEN ''N'' THEN ''N'' ELSE ''Y''',
'        END || '','' || id  INTO :P213_ACTION FROM pc_track_action WHERE id = :P213_ACTION_ID;',
'    CASE ',
'        -- When the action is NOT associated with a query NOR requires a prompt.',
'        WHEN v_track_action.dialog_ind = ''N'' AND v_track_action.prompt_ind = ''N'' THEN ',
'            pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(v_track_action.id, :P213_LOCATION),''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'            IF pc_parcel_pkg.is_location_valid(:P213_PRCL_ID, :P213_AREA_ID) = ''N'' THEN :P213_INVALID_LOCATION := ''Y''; END IF;',
'        -- When the action requires a prompt. The action is added after confirmation',
'        WHEN v_track_action.dialog_ind = ''N'' AND v_track_action.prompt_ind = ''Y'' THEN ',
'            --:P213_COND_ID := NULL;',
'            :P213_ACTION_PROMPT_IND := ''Y'';',
'        -- When the action has an associated query. The action is added after returning from the modal dialog',
'        WHEN v_track_action.dialog_ind = ''Y'' THEN',
'            :P213_DIALOG_IND := ''Y'';',
'            CASE ',
'                WHEN v_track_action.id = pc_track_action_pkg.get_external_receive() THEN                 -- RECEIVE Dialog',
'                    :P213_COND_ID := :P213_LOCATION;     ',
'                    apps.logger.debug(''receive: '' || :P213_COND_ID || '' location: '' || :P213_LOCATION);',
'                    v_pg_num := ''214'';',
'                WHEN v_track_action.id = pc_track_action_pkg.get_res_forward() THEN                 -- FORWARD Dialog',
'                    :P213_COND_ID := to_char(:P213_RES_UFID);     ',
'                    v_pg_num := ''214'';',
'                ELSE v_pg_num := ''219'';',
'            END CASE;',
'            apps.logger.debug(''condition id: '' || :P213_COND_ID);',
'            ',
'            -- Get the url',
'            :P213_URL := apex_nav_pkg.get_dialog_url(v_pg_num, v_app, v_session);',
'    END CASE;',
'    -- Update the receive date',
'    IF v_track_action.id = pc_track_action_pkg.get_internal_receive() THEN --(6) THEN -- Receive after redirect',
'        :P213_RECEIVED_ON := sysdate;',
'    END IF;',
'END;',
''))
,p_attribute_02=>'P213_ACTION_ID,P213_RES_UFID,P213_STAT_ID,P213_LOCATION,P213_AREA_ID,P213_PRCL_ID,P213_RECIPIENT_NAME,P213_USPS_IND,P213_COND_ID'
,p_attribute_03=>'P213_NEW_STAT_ID,P213_URL,P213_COND_ID,P213_ACTION,P213_RECEIVED_ON,P213_ACTION_PROMPT_IND,P213_DIALOG_IND'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(63995377110219839)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_NON_RES_IND'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// P213_ACTION = { title, reqConfirm, msg }',
'var action = getPageItem("P213_ACTION");',
'var title = action[0];',
'var reqConfirm = action[1];',
'var msg = action[2];',
'var query = action[3];',
'var type = action[4];',
'',
'console.log(action);',
'',
'var conditionID = apex.item("P213_COND_ID").getValue();',
'var conditionID2 = apex.item("P213_RES_SEMESTER").getValue();',
'var ai_dialogInd = apex.item("P213_DIALOG_IND").getValue();',
'var actionID = apex.item("P213_ACTION_ID").getValue();',
'',
'// If the action is associated with a modal dialog then',
'if (ai_dialogInd) {',
'    if (query === ''N'') {',
'        var targetPageItems = ["P219_REQ_TYPE"];',
'        var pageItemValues = [type];',
'        openModal(buildURL(apex.item("P213_URL").getValue(), 219, targetPageItems, pageItemValues), {title: title, height: 250, width: 600});',
'    } else {',
'        var targetPageItems = ["P214_REQ_TYPE", "P214_COND_ID","P214_COND_ID2", "P214_ACTION_ID"];',
'        var pageItemValues = [type, conditionID, conditionID2, actionID];',
'        openModal(buildURL(apex.item("P213_URL").getValue(), 214, targetPageItems, pageItemValues), {title: title, height: 525, width: 375, maxWidth: 960});',
'    } ',
'} else { // Otherwise, check if the action requires confirmation',
'    //console.log(PAGE_TAG + "status id: " + apex.item("P213_STAT_ID").getValue());',
'    if (requiresConfirmation()) {',
'        if (confirm(msg)) {',
'            apex.item("P213_ACTION_CONFIRMED_IND").setValue(''Y'');',
'            updateRows("P213_STAT_ID","P213_NEW_STAT_ID");',
'            showActionList();',
'        } else {',
'            apex.item("P213_ACTION_CONFIRMED_IND").setValue(''N'');',
'        }',
'    } else { // Non-modal and non-query actions',
'        updateRows("P213_STAT_ID","P213_NEW_STAT_ID");    ',
'        showActionList();',
'    }',
'    ',
'    // If the status of the package is currently waiting',
'    if (isWaiting()){                ',
'        //console.log(''package is waiting'');',
'        updateWaitStatus();',
'    } else {',
'        resetWaitStatus();',
'    }',
'    updateWaitActions();   ',
'    updateParcelInformation();',
'}',
'',
'',
'function requiresConfirmation(){',
'    //console.log(''rconfirm: '' + reqConfirm);',
'    return reqConfirm === ''Y'';',
'}',
'',
'function getPageItem(pageItem) {',
'    return apex.item(pageItem).getValue().split('','');',
'}',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225540467835766601)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'-- If the action required a prompt and that prompt was confirmed',
'IF :P213_ACTION_PROMPT_IND = ''Y'' AND :P213_ACTION_CONFIRMED_IND = ''Y'' THEN',
'    -- Add the action to the collection the other cases are handled on page update',
'    pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(:P213_ACTION_ID, :P213_LOCATION),''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'END IF;'))
,p_attribute_02=>'P213_ACTION_ID,P213_ACTION_PROMPT_IND,P213_LOCATION,P213_ACTION_CONFIRMED_IND'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224570479749521925)
,p_event_id=>wwv_flow_api.id(63994447047219830)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224569994802521920)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(63994758655219833)
,p_name=>'Focus track # input field'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'ITEM_IS_NULL_OR_ZERO'
,p_display_when_cond=>'P213_TRACK_NUM'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(63994822997219834)
,p_event_id=>wwv_flow_api.id(63994758655219833)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_TRACK_NUM'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64103744749999106)
,p_name=>'Marked Suspicious Parcel'
,p_event_sequence=>40
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#P213_OPTIONS_0'
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224571519868521936)
,p_event_id=>wwv_flow_api.id(64103744749999106)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224569994802521920)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(64103878472999107)
,p_event_id=>wwv_flow_api.id(64103744749999106)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'console.log(''checked susp'');',
'',
'var isChecked = $(this)[0].triggeringElement.checked;',
'var headline;',
'',
'if (isChecked) {',
'    alert(''Marking a parcel as suspicious will notify local housing authorities when saving this page. Mark as suspicious?'');',
'    setValue("P213_SUSP_IND",''Y'');',
'    headline = ''Parcel was marked as suspicious'';',
'    console.log(''warning accepted'');',
'} else {',
'    setValue("P213_SUSP_IND",''N'');',
'    headline = ''Parcel was marked safe'';',
'    console.log(''unchecked suspicious'');',
'}',
'',
'showActionList();',
'apex.server.process("Create Action",{',
'    x01: headline,',
'    x02: ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue(),',
'    pageItems : "#P213_SUSP_IND"',
'}, {',
'    dataType: "text",',
'    success: function(pData) {',
'        apex.event.trigger("#ACTIONS", ''apexrefresh'');',
'    }',
'});',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(223428724253918710)
,p_event_id=>wwv_flow_api.id(64103744749999106)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'-- The package was marked as safe',
'IF :P213_SUSP_IND = ''N'' THEN',
'    pc_track_action_pkg.add_action(''ACTIONS'',''Parcel was marked safe'',''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'ELSE',
'    pc_track_action_pkg.add_action(''ACTIONS'',''Parcel was marked as suspicious'',''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'END IF;',
''))
,p_attribute_02=>'P213_SUSP_IND'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64362374468950338)
,p_name=>'Update Page'
,p_event_sequence=>60
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'window'
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterclosedialog'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(64362572960950340)
,p_event_id=>wwv_flow_api.id(64362374468950338)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_RECIPIENT_NAME'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Get the data and request type from the modal',
'var pg_receive_forward = ''214'';',
'var pg_non_res_receive_RTS = ''219'';',
'',
'var data = Object.values(this.data);                        // Need to do this so we can abstract the information passed back from the dialog',
'var pageNum = data[0];                                      // Get the dialog that was returned from',
'',
'console.log(this.data);',
'',
'if (pageNum === pg_receive_forward) {                       // If the page returned from 214',
'    var data = this.data.P214_SELECT.split('':'');',
'    var reqType = this.data.P214_REQ_TYPE;',
'    ',
'    var id = data[0];',
'    var semester = data[1];',
'    ',
'    console.log(id,semester);',
'    ',
'    if (reqType == RECEIVE) {                               // If the request was a receive request',
'        apex.item("P213_RES_UFID").setValue(id);',
'        apex.item("P213_RES_SEMESTER").setValue(semester);',
'    } else {                                                // If the request was a forward request',
'        apex.item("P213_AREA_ID").setValue(id);',
'    }',
'} else if (pageNum === pg_non_res_receive_RTS) {            // If the page returned from 219',
'    var reqType = this.data.P219_REQ_TYPE;',
'    ',
'    if (reqType == NON_RES_RECEIVE) {',
'        var firstName = this.data.P219_FIRST_NAME;',
'        var lastName = this.data.P219_LAST_NAME;',
'',
'        apex.item("P213_NON_RES_F_NAME").setValue(firstName);',
'        apex.item("P213_NON_RES_L_NAME").setValue(lastName);',
'    } else if (reqType == RETURN_TO_SENDER) {',
'        apex.item("P213_RETURN_RSN").setValue(this.data.P219_RTS_REASON);',
'        $("#P213_RETURN_RSN").removeClass(''hide'');',
'    }',
'}',
'apex.item("P213_REQ_TYPE").setValue(reqType);              // Update the rows and set the request type ',
'apex.item("P213_RETURNED_PAGE").setValue(pageNum);',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65167777058713705)
,p_event_id=>wwv_flow_api.id(64362374468950338)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_location        v_curr_next_res.area_village%TYPE;',
'    v_area            area%ROWTYPE;',
'BEGIN',
'    SELECT * INTO v_area FROM area WHERE id = :P213_DESK_AREA_ID;                            -- Get the current area',
'    IF :P213_RETURNED_PAGE = ''214'' THEN',
'        SELECT area_village INTO v_location FROM v_curr_next_res WHERE ufid = :P213_RES_UFID AND semester = :P213_RES_SEMESTER;    -- Get the residents area',
'        ',
'        -- If the dialog returned from a receive request',
'        IF :P213_REQ_TYPE LIKE pc_track_action_pkg.get_external_receive() THEN',
'            :P213_RECEIVED_ON := sysdate;',
'',
'            :P213_RECIPIENT_NAME := initcap(person_pkg.get_name_by_ufid(:P213_RES_UFID));',
'',
'            -- Check if the current area desk is different from the resident''s housing location',
'            IF v_area.title <> v_location THEN              ',
'                :P213_INVALID_LOCATION := ''Y'';',
'                :P213_ALERT := apex_ui_pkg.get_center_info_msg(''The parcel needs to be forwarded to the resident''''s location.'', :P213_PRCL_ID); -- Create an alert',
'            ELSE',
'                :P213_INVALID_LOCATION := ''N'';',
'            END IF;',
'',
'            -- Add the action to the collection',
'            pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(:P213_ACTION_ID,v_area.title),''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'',
'        -- If the dialog returned from a redirect request',
'        ELSIF :P213_REQ_TYPE LIKE pc_track_action_pkg.get_res_forward() OR :P213_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_forward() THEN',
'            SELECT title INTO :P213_LOCATION FROM area WHERE id = :P213_AREA_ID;    -- Set the title of the new location',
'',
'            -- The location is valid',
'            :P213_INVALID_LOCATION := ''N'';',
'            :P213_ALERT := apex_ui_pkg.get_success_msg(''The delivery location of the parcel has been updated to '' || v_location || ''.'', :P213_PRCL_ID);',
'',
'            -- Add the action to the collection',
'            pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(:P213_ACTION_ID,:P213_LOCATION),''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'        END IF;',
'        :P213_NON_RES_IND := ''N'';',
'    ELSIF :P213_RETURNED_PAGE = ''219'' THEN',
'        IF :P213_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_receive() THEN',
'            :P213_RECEIVED_ON := sysdate;',
'            :P213_NON_RES_IND := ''Y'';',
'            :P213_RECIPIENT_NAME := initcap(:P213_NON_RES_F_NAME || '' '' || :P213_NON_RES_L_NAME);',
'            pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(:P213_ACTION_ID,v_area.title),''Intended Recipient: '' || :P213_RECIPIENT_NAME,sysdate);',
'        ELSIF :P213_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender() THEN',
'            pc_track_action_pkg.add_action(''ACTIONS'', pc_track_action_pkg.get_headline(:P213_ACTION_ID,v_area.title),''Reason for Return: '' || :P213_RETURN_RSN,sysdate);',
'        END IF;',
'        :P213_ALERT := NULL;',
'    END IF;',
'END;'))
,p_attribute_02=>'P213_RES_UFID,P213_RES_SEMESTER,P213_AREA_ID,P213_REQ_TYPE,P213_ACTION_ID,P213_RECIPIENT_NAME,P213_RETURNED_PAGE,P213_NON_RES_F_NAME,P213_NON_RES_L_NAME,P213_RETURN_RSN'
,p_attribute_03=>'P213_LOCATION,P213_RECIPIENT_NAME,P213_INVALID_LOCATION,P213_ALERT,P213_RECEIVED_ON,P213_NON_RES_IND'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65170117095713729)
,p_event_id=>wwv_flow_api.id(64362374468950338)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'updateRows("P213_STAT_ID","P213_NEW_STAT_ID");',
'createMsgAlert(apex.item("P213_ALERT"));',
'updateWaitActions();',
'hideNonResidentStatuses();',
'updateParcelInformation();',
'showActionList();'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224570578742521926)
,p_event_id=>wwv_flow_api.id(64362374468950338)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224569994802521920)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64862921127100719)
,p_name=>'Highlight parcel status'
,p_event_sequence=>70
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65167539381713703)
,p_event_id=>wwv_flow_api.id(64862921127100719)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Update the row with the current parcel status',
'updateRow("P213_STAT_ID");',
'//console.log(PAGE_TAG + "stat id: " + apex.item("P213_STAT_ID").getValue());'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(64863044177100720)
,p_event_id=>wwv_flow_api.id(64862921127100719)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// TODO Remove Receive and Deliver',
'// The label does not provide information about the resident',
'// therefore we don''t need this option anymore',
'//hideAction(RECEIVE_DELIVER);',
'updateWaitStatus();',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65016382517038542)
,p_name=>'Submit Form'
,p_event_sequence=>80
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(65016215137038541)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65017113049038550)
,p_event_id=>wwv_flow_api.id(65016382517038542)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65171762572713745)
,p_name=>'Create View Mask'
,p_event_sequence=>100
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65171836851713746)
,p_event_id=>wwv_flow_api.id(65171762572713745)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'console.log(PAGE_TAG + "action id: " + apex.item("P213_STAT_ID").getValue());',
'console.log(PAGE_TAG + "view only: " + getValue("P213_VIEW_ONLY"));',
'if (isSuspicious()) {',
'    var bodyContent = $(".t-Body-content");',
'    var contentTitle = $("h1.t-BreadcrumbRegion-titleText");',
'',
'    var mask = "<div class=''content-Mask''></div>";',
'    var maskButton = "<span class=''mask-Button t-Button t-Button--large''>Edit Form</span>";',
'',
'    bodyContent.prepend(mask);',
'    //bodyContent.prepend(maskButton);',
'} else {',
'    $(''#mask-container'').css(''display'',''none'');',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65539749379992505)
,p_name=>'Update Breadcrumb Label'
,p_event_sequence=>120
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65539831751992506)
,p_event_id=>wwv_flow_api.id(65539749379992505)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var breadcrumb = $("span.t-Breadcrumb-label");',
'var label = breadcrumb.text();',
'var currentDesk = apex.item("P213_DESK_AREA").getValue();',
'//if (!isViewOnly()) {',
'    breadcrumb.text(label + " > " + currentDesk);',
'//} else {',
'    //breadcrumb',
'//}',
' '))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65541944840992527)
,p_name=>'Tracking # input'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P213_TRACK_NUM'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65542096672992528)
,p_event_id=>wwv_flow_api.id(65541944840992527)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var trackNum = apex.item("P213_TRACK_NUM").getValue();',
'var carrier_id = determineCarrier(trackNum);',
'apex.item("P213_CARRIER_ID").setValue(carrier_id);',
'',
'var self = this;',
'',
'apex.server.process("Check Duplicate Track Num", {',
'    x01: trackNum,',
'    pageItems: "#P213_CARRIER_ID"',
'},{',
'    dataType: "text",',
'    success: function(isDuplicate) {',
'        if (isDuplicate.trim() === ''Y'') {',
'            //createMsgAlert(apex.item("P213_ALERT"))',
'            alert("This tracking number has received before");',
'        }     ',
'    }',
'});'))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65542671103992534)
,p_name=>'Update Days Waiting'
,p_event_sequence=>140
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65542755232992535)
,p_event_id=>wwv_flow_api.id(65542671103992534)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'updateWaitStatus();',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(223431187682918734)
,p_name=>'Carrier Options'
,p_event_sequence=>150
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(223431260885918735)
,p_event_id=>wwv_flow_api.id(223431187682918734)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'$("#P213_CARRIER_ID").prop("readonly",true);',
'',
'var carrier = apex.item("P213_CARRIER_ID").getValue();',
'console.log(typeof carrier);',
'if (carrier && carrier == 0) {',
'    console.log(''show the carrier opt'');',
'    apex.item("P213_CARRIER_OPT").show();',
'} else {',
'    apex.item("P213_CARRIER_OPT").hide();',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224169019253909916)
,p_name=>'Mark Safe'
,p_event_sequence=>160
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(224168958997909915)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224169520288909921)
,p_event_id=>wwv_flow_api.id(224169019253909916)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CONFIRM'
,p_attribute_01=>'Are you sure you want to mark this parcel as safe?'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224169186924909917)
,p_event_id=>wwv_flow_api.id(224169019253909916)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'console.log("mask button pressed");',
'var mask = $(".content-Mask");       // Get the mask',
'mask.css("display", "none");         // Hide the mask',
'$(''#P213_OPTIONS_0'').click();        // Uncheck suspicious '))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224169238073909918)
,p_event_id=>wwv_flow_api.id(224169019253909916)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224168893844909914)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224569185108521912)
,p_name=>'Load Regex'
,p_event_sequence=>170
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224569222375521913)
,p_event_id=>wwv_flow_api.id(224569185108521912)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Get the JSON for the carrier regex',
'var obj = JSON.parse(apex.item("P213_REGEX").getValue());',
'',
'// Need to replace %5C because it is the HEX value of \ ',
'// which is invalid for JSON',
'var re = /%5C/g;',
'',
'// Get the carriers',
'var carriers = obj.carriers;',
'',
'// Iterate through the different carriers',
'for (var carrier in carriers) {',
'    if (carriers.hasOwnProperty(carrier)) {',
'        var carrierRegex = carriers[carrier];',
'        // Iterate through the regex of each carrier',
'        for (var regex in carrierRegex) {',
'            var pattern = carrierRegex[regex].replace(re,''\\'');',
'            // Create an array of the patterns',
'            regexs.push({',
'                carrier_id: carrier,',
'                pattern: pattern',
'            });',
'        }',
'    }',
'}',
'',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224571666541521937)
,p_name=>'Hide Actions'
,p_event_sequence=>180
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224571757381521938)
,p_event_id=>wwv_flow_api.id(224571666541521937)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224569809252521919)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224571861755521939)
,p_event_id=>wwv_flow_api.id(224571666541521937)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'console.log($("#main-content").parent());',
'hideActionList();',
'$(".t-Body-contentInner > .container").css(''max-width'', ''1280px'');',
'$("#main-content").parent().removeClass(''col-9'');',
'$("#main-content").parent().addClass(''col-12'');',
'',
'',
'function hideActionList() {',
'    $(".t-Body-contentInner > .container").css(''max-width'', ''1280px'');',
'    $("#main-content").parent().removeClass(''col-9'');',
'    $("#main-content").parent().addClass(''col-12'');',
'}',
'',
''))
,p_stop_execution_on_error=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224572594224521946)
,p_name=>'Carrier Selection'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P213_CARRIER_ID'
,p_triggering_condition_type=>'NOT_EQUALS'
,p_triggering_expression=>'0'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224572663703521947)
,p_event_id=>wwv_flow_api.id(224572594224521946)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_USPS_IND'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var USPS = ''4'';',
'var carrier_id = apex.item("P213_CARRIER_ID").getValue();',
'var ai_uspsInd = apex.item("P213_USPS_IND");',
'',
'if (carrier_id === USPS) {',
'    console.log(''usps was selected'');',
'    ai_uspsInd.setValue(''Y'');',
'} else {',
'    ai_uspsInd.setValue(''N'');',
'}',
'',
'updateWaitActions();',
'hideCarrierOpt();'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224572708958521948)
,p_event_id=>wwv_flow_api.id(224572594224521946)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'apex.item("P213_USPS_IND").setValue(''N'');',
'showCarrierOpt();'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225640362103787702)
,p_name=>'Initialize Parcel Actions'
,p_event_sequence=>200
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225640414043787703)
,p_event_id=>wwv_flow_api.id(225640362103787702)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'hideNonResidentStatuses();',
'hideAction(FORWARD_OFFCAMPUS);',
'updateWaitActions();',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225927224559532831)
,p_name=>'Update Parcel Info'
,p_event_sequence=>210
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225927397026532832)
,p_event_id=>wwv_flow_api.id(225927224559532831)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'updateParcelInformation();'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226172357191305442)
,p_name=>'Action Pressed NEW'
,p_event_sequence=>230
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.action-button.button-active'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(apex.item("P213_TRACK_NUM").getValue() !== "" && apex.item("P213_CARRIER_ID").getValue() !== "")'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'tr'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226172461475305443)
,p_event_id=>wwv_flow_api.id(226172357191305442)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_ACTION_ID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var action;',
'var actionId = this.triggeringElement.getAttribute(''data-action-id'');',
'apex.item("P213_ACTION_ID").setValue(actionId);',
'',
'console.log(''setting reason'', apex.item("P213_ACTION_ID").getValue());',
'console.log(actionId);',
'',
'/**',
' * The following happens when an action is selected.',
' *',
' * 1. An AJAX call "Get Associated Action" receives the associated action from the id returning',
' *    all of the table attributes in JSON format',
' * 2. The action is parsed to see if it needs to be confirmed or an inline',
' *    dialog needs to be opened.',
' * 3. The resulting action is retrieved with an AJAX call "Get Resulting Status" and the new status',
' *    ID is set.',
' * 4. The Action table is updated to reflect the current status of the parcel',
' *',
'**/',
'',
'apex.server.process("Get Associated Action", {',
'    x01: actionId',
'}, {',
'    dataType: "text",',
'    success: function(pData) {',
'        action = JSON.parse(pData).action;',
'        console.log(''action pressed'', action);',
'        handleAction(action);',
'    }',
'});',
'',
'',
'function handleAction(action) {',
'    if (action.prompt_ind === ''Y'') {',
'        if (confirm(action.msg)) {',
'            console.log(''confirmed'');',
'            getAndSetNextAction(actionId, ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'            console.log(action);',
'            if (action.newReceive) {',
'                apex.item("P213_RECEIVED_ON").setValue(getSQLDate());',
'                apex.item("P213_RECEIVE_IND").setValue(''Y'');',
'                submitPageItems("#P213_RECEIVED_ON,#P213_RECEIVE_IND");',
'            }',
'        }',
'    } else if (action.dialog_ind === ''Y'') {',
'        $(action.dialog).dialog(''open'');',
'        apex.item("P213_CURRENT_IDA").setValue(action.dialog);',
'        $(action.dialog + " .error-text").addClass(''hidden'');',
'        $(action.dialog + " .focus-field").removeClass(''mismatch'');',
'        $(action.dialog + " .focus-field").val('''');',
'        $(action.dialog + " .focus-field").focus();',
'    } else {',
'        getAndSetNextAction(actionId, ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'    }',
'}',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226173159162305450)
,p_event_id=>wwv_flow_api.id(226172357191305442)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var trackNum = apex.item("P213_TRACK_NUM");',
'var carrierID = apex.item("P213_CARRIER_ID");',
'',
'if (!trackNum.getValue()) {',
'    trackNum.setFocus();',
'} else {',
'    carrierID.setFocus();',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226420503403538455)
,p_name=>'Return Reason selected'
,p_event_sequence=>240
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P213_IDA_PRE_RETURN_RSN'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'Other'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226421440477538477)
,p_event_id=>wwv_flow_api.id(226420503403538455)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_IDA_USR_RETURN_RSN'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226420937245538477)
,p_event_id=>wwv_flow_api.id(226420503403538455)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_IDA_USR_RETURN_RSN'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226421990792538477)
,p_event_id=>wwv_flow_api.id(226420503403538455)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_IDA_USR_RETURN_RSN'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226409027655345905)
,p_name=>'Confirm Return Reason'
,p_event_sequence=>250
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(226408900760345904)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226409139938345906)
,p_event_id=>wwv_flow_api.id(226409027655345905)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var preReturnRsn = $("#P213_IDA_PRE_RETURN_RSN");',
'var usrReturnRsn = $("#P213_IDA_USR_RETURN_RSN");',
'var dialogSelector = apex.item("P213_CURRENT_IDA").getValue();',
'',
'if (preReturnRsn.val() === ''Other'') {',
'    preReturnRsn.removeClass(''mismatch'');',
'    ',
'    if (usrReturnRsn.val() !== "") {',
'        setReason(usrReturnRsn.val());',
'    } else {',
'        usrReturnRsn.addClass(''mismatch'');',
'    }',
'} else if (preReturnRsn.val() !== '''') {',
'    setReason(preReturnRsn.val());',
'} else {',
'    preReturnRsn.addClass(''mismatch'');',
'    usrReturnRsn.removeClass(''mismatch'');',
'}',
'',
'function setReason(reason) {',
'    apex.item("P213_RETURN_RSN").setValue(reason);',
'    submitPageItems("#P213_RETURN_RSN");',
'    preReturnRsn.removeClass(''mismatch'');',
'    usrReturnRsn.removeClass(''mismatch'');',
'    getAndSetNextAction(apex.item("P213_ACTION_ID").getValue(), ''Reason for Return: '' + reason);',
'    apex.item("P213_RETURN_IND").setValue(''Y'');',
'    submitPageItems("#P213_RETURN_IND");',
'    closeDialog(dialogSelector);',
'}',
'',
'//$(dialogSelector).dialog(''close'');',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226409948904345914)
,p_name=>'UFID Scan Enter Pressed'
,p_event_sequence=>260
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'$(''.ufid-scan'')'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13'
,p_bind_type=>'bind'
,p_bind_event_type=>'keypress'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226410060607345915)
,p_event_id=>wwv_flow_api.id(226409948904345914)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'console.log(''enter pressed'');',
'$(''.ufid-scan'').blur();',
'$(''#DELIVER_PKG'').trigger(''click'');',
'event.preventDefault();'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226410121299345916)
,p_name=>'Deliver Package'
,p_event_sequence=>270
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(226446030986383905)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226410203057345917)
,p_event_id=>wwv_flow_api.id(226410121299345916)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var enteredUFID = $(''.ufid-scan'').val();',
'var resUFID = apex.item("P213_RES_UFID").getValue();',
'var dialogSelector = apex.item("P213_CURRENT_IDA").getValue();',
'',
'console.log(''deliver package'');',
'',
'// check ufid against student ufid',
'if (enteredUFID == resUFID) {',
'    $(dialogSelector + '' .error-text'').addClass(''hidden'');    ',
'    $(dialogSelector + '' .focus-field'').removeClass(''mismatch'');    ',
'    getAndSetNextAction(apex.item("P213_ACTION_ID").getValue(), ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'    closeDialog(dialogSelector);',
'} else {',
'    $(dialogSelector + '' .error-text'').removeClass(''hidden'');    ',
'    $(dialogSelector + '' .focus-field'').addClass(''mismatch'');    ',
'}',
'',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226411615752345931)
,p_name=>'Receive Non Resident'
,p_event_sequence=>280
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(226411456459345929)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226411761423345932)
,p_event_id=>wwv_flow_api.id(226411615752345931)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_NON_RES_IND'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var firstName = apex.item("P213_IDA_NON_RES_F_NAME").getValue();',
'var lastName = apex.item("P213_IDA_NON_RES_L_NAME").getValue();',
'var dialogSelector = apex.item("P213_CURRENT_IDA").getValue();',
'',
'if (firstName == '''') {',
'    $(dialogSelector + '' .error-text'').removeClass(''hidden'');    ',
'    $(dialogSelector + '' .focus-field'').addClass(''mismatch'');    ',
'} else {',
'    $(dialogSelector + '' .error-text'').addClass(''hidden'');  ',
'    $(dialogSelector + '' .focus-field'').removeClass(''mismatch'');   ',
'    apex.item("P213_NON_RES_F_NAME").setValue(firstName);',
'    apex.item("P213_NON_RES_L_NAME").setValue(lastName);',
'    apex.item("P213_RECIPIENT_NAME").setValue(firstName + '' '' + lastName);',
'    apex.item("P213_RECEIVED_ON").setValue(getSQLDate());',
'    apex.item("P213_NON_RES_IND").setValue(''Y'');',
'    submitPageItems("#P213_RECIPIENT_NAME, #P213_NON_RES_IND, #P213_RECEIVED_ON");',
'    getAndSetNextAction(apex.item("P213_ACTION_ID").getValue(), ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'    closeDialog(dialogSelector);',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226413239985345947)
,p_name=>'Select Resident'
,p_event_sequence=>290
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.select-res'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'#RESIDENTS'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226413336671345948)
,p_event_id=>wwv_flow_api.id(226413239985345947)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_NON_RES_IND,P213_RES_UFID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var ufid = this.triggeringElement.getAttribute(''data-prsn-ufid'');',
'var areaId = this.triggeringElement.getAttribute(''data-area-id'');',
'var resName = this.triggeringElement.getAttribute(''data-name'');',
'',
'apex.item("P213_RES_UFID").setValue(ufid);',
'apex.item("P213_AREA_ID").setValue(areaId);',
'apex.item("P213_NON_RES_IND").setValue(''N'');',
'apex.item("P213_IDA_SELECTED_RESIDENT").setValue(resName);',
'console.log(''person ufid:'', ufid, ''area id:'', areaId);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226575641709916915)
,p_event_id=>wwv_flow_api.id(226413239985345947)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P213_NON_RES_IND'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(743999238324581344)
,p_event_id=>wwv_flow_api.id(226413239985345947)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_ALERT'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226413542512345950)
,p_name=>'Confirm Resident'
,p_event_sequence=>300
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(226413055461345945)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'apex.item("P213_IDA_SELECTED_RESIDENT").getValue() !== '''''
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226574210879916901)
,p_event_id=>wwv_flow_api.id(226413542512345950)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_RECIPIENT_NAME'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var areaId = apex.item("P213_AREA_ID").getValue();',
'var resName = apex.item("P213_IDA_SELECTED_RESIDENT").getValue();',
'var dialogSelector = apex.item("P213_CURRENT_IDA").getValue();',
'',
'/***',
' * Check if the student location is also the current area desk',
' * The data coming back ',
' *     { ',
' *       "validLocation" : '''',',
' *       "alertMsg: '''' ',
' *     }',
' *',
' ***/',
'apex.server.process("Validate Location",{',
'    x01: areaId,',
'    pageItems: "#P213_DESK_AREA_ID, #P213_PRCL_ID"',
'},{',
'    dataType: "text",',
'    success: function(pData) { ',
'        //alert(pData);',
'        var data = JSON.parse(pData.trim());',
'        var invalidLocation = data.invalidLocation;',
'        ',
'        console.log(invalidLocation);',
'        ',
'        if (invalidLocation === ''Y'') {',
'            createMsgAlert(apex.item("P213_ALERT"));',
'        } else {',
'            //apex.item("P213_RECEIVE_IND").setValue(''Y'');',
'        }',
'        ',
'        updateWaitActions();',
'        apex.item("P213_AREA_ID").setValue(apex.item("P213_DESK_AREA_ID").getValue()); // This needs to be done because the session gets set',
'        apex.item("P213_RECIPIENT_NAME").setValue(resName);',
'        apex.item("P213_INVALID_LOCATION").setValue(invalidLocation);',
'        apex.item("P213_RECEIVED_ON").setValue(getSQLDate());',
'        apex.item("P213_RECEIVE_IND").setValue(''Y'');',
'        apex.event.trigger("#RES_LOCATION", ''apexrefresh'');',
'        submitPageItems("#P213_RECIPIENT_NAME, #P213_INVALID_LOCATION, #P213_RECEIVED_ON, #P213_RECEIVE_IND");',
'        getAndSetNextAction(apex.item("P213_ACTION_ID").getValue(), ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'        closeDialog(dialogSelector);',
'    }',
'});',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226575025668916909)
,p_name=>'Select Area'
,p_event_sequence=>310
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.select-area'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'#RES_LOCATION'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226575172592916910)
,p_event_id=>wwv_flow_api.id(226575025668916909)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_IDA_SELECTED_AREA'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var areaId = this.triggeringElement.getAttribute(''data-area-id'');',
'var areaTitle = this.triggeringElement.getAttribute(''data-area-title'');',
'',
'apex.item("P213_AREA_ID").setValue(areaId);',
'apex.item("P213_IDA_SELECTED_AREA").setValue(areaTitle);',
'',
'console.log(''selecting area'', areaId);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226575234449916911)
,p_name=>'Confirm Area'
,p_event_sequence=>320
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(226574999837916908)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'apex.item("P213_IDA_SELECTED_AREA").getValue() !== '''''
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226575368400916912)
,p_event_id=>wwv_flow_api.id(226575234449916911)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P213_LOCATION'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var dialogSelector = apex.item("P213_CURRENT_IDA").getValue();',
'',
'updateWaitActions();',
'apex.item("P213_LOCATION").setValue(apex.item("P213_IDA_SELECTED_AREA").getValue());',
'getAndSetNextAction(apex.item("P213_ACTION_ID").getValue(), ''Intended Recipient: '' + apex.item("P213_RECIPIENT_NAME").getValue());',
'closeDialog(dialogSelector);',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226575822043916917)
,p_event_id=>wwv_flow_api.id(226575234449916911)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P213_AREA_ID'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226577267507916931)
,p_name=>'Carrier Opt'
,p_event_sequence=>330
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P213_CARRIER_OPT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226577346613916932)
,p_event_id=>wwv_flow_api.id(226577267507916931)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P213_CARRIER_OPT'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226813691435659904)
,p_name=>'Breadcrumb Selected'
,p_event_sequence=>340
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.t-Breadcrumb-item'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'apex.item("P213_ACTION_ID").getValue() !== '''';'
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226813775182659905)
,p_event_id=>wwv_flow_api.id(226813691435659904)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'if(!confirm("Changes to the package have not been saved. Do you want to continue?")) {',
'    this.browserEvent.preventDefault();',
'}'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65543688327992544)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Update Parcel'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_parcel         pc_parcel%ROWTYPE;',
'BEGIN',
'apps.logger.debug(''ufid:'' || :P213_RES_UFID);    ',
'    IF :P213_NON_RES_IND = ''N'' THEN',
'        v_parcel := pc_parcel_pkg.set_values(',
'            p_id => :P213_PRCL_ID,',
'            p_prcl_list => pc_parcel_list_pkg.get_list_by_area(:P213_AREA_ID).id,',
'            p_trk_st_t_id => :P213_STAT_ID,',
'            p_track_num => :P213_TRACK_NUM,',
'            p_carrier_id => :P213_CARRIER_ID,',
'            p_carrier_opt => :P213_CARRIER_OPT,',
'            p_misc_data => :P213_OPT_DESCR,',
'            p_return_rsn => :P213_RETURN_RSN,',
'            --p_receive_dt => sysdate,',
'            p_prsn_id => person_pkg.get_prsn_id(:P213_RES_UFID)',
'        );',
'    ELSE',
'        v_parcel := pc_parcel_pkg.set_values(',
'            p_id => :P213_PRCL_ID,',
'            p_prcl_list => pc_parcel_list_pkg.get_list_by_area(:P213_AREA_ID).id,',
'            p_trk_st_t_id => :P213_STAT_ID,',
'            p_track_num => :P213_TRACK_NUM,',
'            p_carrier_id => :P213_CARRIER_ID,',
'            p_carrier_opt => :P213_CARRIER_OPT,',
'            p_misc_data => :P213_OPT_DESCR,',
'            p_return_rsn => :P213_RETURN_RSN,',
'            --p_receive_dt => sysdate,',
'            p_non_res_f_name => :P213_NON_RES_F_NAME,',
'            p_non_res_l_name => :P213_NON_RES_L_NAME',
'        );',
'    END IF;',
'    ',
'    IF :P213_RECEIVE_IND = ''Y'' THEN',
'        v_parcel.receive_dt := sysdate; --to_date(:P213_RECEIVED_ON, ''MM/DD/YYYY'');',
'    END IF; ',
'    ',
'    pc_parcel_cru.sav_pc_parcel(v_parcel);',
'    ',
'    IF :P213_SUSP_IND = ''N'' AND pc_parcel_pkg.is_suspicious(:P213_PRCL_ID) = ''Y'' THEN',
'        :P213_SUSP_STAT_CHANGED := ''Y'';',
'    END IF;',
'    ',
'    IF :P213_PRCL_ID IS NOT NULL THEN',
'        pc_parcel_pkg.update_conditions(v_parcel.id,:P213_OPTIONS);     -- Update the parcel conditions',
'    ELSE',
'        pc_parcel_pkg.create_conditions(v_parcel.id, :P213_OPTIONS);      -- Create the conditions for a parcel',
'    END IF;',
'    ',
'    :P213_PRCL_ID := v_parcel.id;',
'EXCEPTION',
'    WHEN OTHERS THEN',
'        RAISE;',
'        -- create an alert',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65544151452992549)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Update parcel history'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_prcl_hist       pc_parcel_hist%ROWTYPE;',
'BEGIN',
'    -- Loop through the collection and add them into the parcel history table',
'    FOR c_p IN (',
'        SELECT d001, d002, c001, c002  ',
'        FROM apex_collections',
'        WHERE collection_name = ''ACTIONS''',
'    )LOOP',
'        v_prcl_hist := NULL;',
'        v_prcl_hist.prcl_id := :P213_PRCL_ID;',
'        v_prcl_hist.start_dt := c_p.d001;',
'        v_prcl_hist.end_dt := c_p.d002;',
'        v_prcl_hist.headline := c_p.c001;',
'        v_prcl_hist.descr := c_p.c002;',
'        ',
'        pc_parcel_hist_cru.sav_pc_parcel_hist(v_prcl_hist);',
'        /*',
'        apps.logger.debug(''Parcel id: '' || :P213_PRCL_ID); ',
'        apps.logger.debug(''Start date: '' || c_p.d001);',
'        apps.logger.debug(''End date: '' || c_p.d002);',
'        apps.logger.debug(''Headline: '' || c_p.c001);',
'        apps.logger.debug(''Description: '' || c_p.c002);',
'        */',
'    END LOOP;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(224041136678352843)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Send Email Notifications'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_email_addr    VARCHAR2(200);',
'    v_prsn_id       person.id%TYPE;',
'    v_gatorlink     NUMBER := 1;',
'    v_prcl_hist     pc_parcel_hist%ROWTYPE;',
'BEGIN',
'    v_prsn_id := pc_parcel_pkg.get_recipient(:P213_PRCL_ID).id;',
'    v_email_addr := person_pkg.get_email_address(v_prsn_id, v_gatorlink);',
'',
'    IF :P213_SUSP_IND = ''Y'' THEN             -- Email for suspicious packages',
'        email_ua_pkg.send_susp_pkg_received(',
'            p_prsn_id   =>  v_prsn_id,',
'            p_eml_addr  => ''VictorH@housing.ufl.edu'',',
'            p_prcl_id   => :P213_PRCL_ID,',
'            p_area      => :P213_LOCATION);',
'    ELSIF :P213_SUSP_STAT_CHANGED = ''Y'' THEN    -- marked as safe',
'        v_prcl_hist := pc_parcel_hist_pkg.get_latest_action(:P213_PRCL_ID);',
'        ',
'        IF INSTR(v_prcl_hist.headline, ''returned to sender'') > 0 THEN   ',
'            apps.logger.debug(''has returned'');',
'            email_ua_pkg.send_pkg_returned(',
'                p_prsn_id   => v_prsn_id,',
'                p_eml_addr  => v_email_addr,',
'                p_prcl_id   => :P213_PRCL_ID,',
'                p_area      => :P213_LOCATION,',
'                p_return_dt => sysdate);',
'        ELSIF INSTR(v_prcl_hist.headline, ''was received'') > 0  THEN',
'            apps.logger.debug(''was received'');',
'            email_ua_pkg.send_pkg_received(',
'                p_prsn_id   => v_prsn_id,',
'                p_eml_addr  => v_email_addr,',
'                p_prcl_id   => :P213_PRCL_ID,',
'                p_area      => :P213_LOCATION);',
'        END IF;',
'    ELSIF :P213_RETURN_IND = ''Y'' THEN        -- Email when package is received',
'        email_ua_pkg.send_pkg_returned(',
'            p_prsn_id   => v_prsn_id,',
'            p_eml_addr  => v_email_addr,',
'            p_prcl_id   => :P213_PRCL_ID,',
'            p_area      => :P213_LOCATION,',
'            p_return_dt => sysdate);',
'    ELSIF :P213_RECEIVE_IND = ''Y'' AND :P213_SUSP_IND = ''N'' AND :P213_INVALID_LOCATION = ''N'' THEN --AND :P213_NON_RES_IND = ''N''  THEN',
'        email_ua_pkg.send_pkg_received(',
'            p_prsn_id   => v_prsn_id,',
'            p_eml_addr  => v_email_addr,',
'            p_prcl_id   => :P213_PRCL_ID,',
'            p_area      => :P213_LOCATION);',
'    END IF;',
'END;',
'',
'',
'    -- This will only send an email when the package has arrived at the correct area desk',
'    -- Use this to get the TS of when the package was received',
'    --SELECT start_dt, to_date(create_ts,''HH:MI AM  mm/dd/yy'') as times, headline, descr, null',
'    --FROM pc_parcel_hist',
'        --email_pkg.send_simple_email(',
'        --    v_email_addr,',
'        --    v_prcl_hist.headline,',
'        --    v_prcl_hist.descr);',
'        ',
'        /*',
'         -- Get the person id',
'        SELECT prsn_id',
'        INTO v_prsn_id',
'        FROM pc_parcel',
'        WHERE id = :P213_PRCL_ID;',
'',
'        -- Get the gatorlink email address',
'        --v_email_addr := person_pkg.get_email_address(v_prsn_id, v_gatorlink);',
'        */',
'',
'        /*',
'            Only sending out the latest action regardless of if the package was suspicious or not',
'        */',
'',
'        -- If the parcel was initially marked as suspicious send out the latest action that took place',
'        --IF :P213_SUSP_IND = ''Y'' THEN',
'           ',
'                ',
'                /*',
'            <<latest_action>>',
'            DECLARE',
'                v_prcl_hist    pc_parcel_hist%ROWTYPE;',
'            BEGIN',
'                SELECT latest_entry.*',
'                INTO v_prcl_hist',
'                FROM (',
'                  SELECT * ',
'                  FROM pc_parcel_hist',
'                  WHERE prcl_id = :P213_PRCL_ID',
'                  ORDER BY updt_ts desc) latest_entry',
'                WHERE ROWNUM = 1;',
'                ',
'                apps.logger.debug(''Parcel initially marked as '' || :P213_SUSP_IND);',
'                apps.logger.debug(''Latest action taken place: '' || v_prcl_hist.headline);',
'                ',
'                email_pkg.send_simple_email( ',
'                v_email_addr,       ',
'                v_prcl_hist.headline, -- headline',
'                v_prcl_hist.descr); -- description',
'            END;',
'            */',
'        --ELSE',
'            -- The parcel was not marked as suspicious and the resident should be notified of all actions',
'            -- Loop through the collection and email the resident with the information',
'            /*',
'            FOR c_p IN (',
'                SELECT d001, d002, c001, c002  ',
'                FROM apex_collections',
'                WHERE collection_name = ''ACTIONS''',
'            )LOOP    ',
'                email_pkg.send_simple_email( ',
'                    v_email_addr, --''micbentz@gmail.com'',        ',
'                    ''Parcel Update'',',
'                     c_p.c001);',
'',
'                apps.logger.debug('''');',
'                --apps.logger.debug(''Parcel id: '' || :P213_PRCL_ID); ',
'                --apps.logger.debug(''Start date: '' || c_p.d001);',
'                --apps.logger.debug(''End date: '' || c_p.d002);',
'',
'',
'                apps.logger.debug(''Description: '' || c_p.c002);',
'                apps.logger.debug(''Headline: '' || c_p.c001);',
'                apps.logger.debug(''notified: '' || v_email_addr);',
'            END LOOP;',
'            */',
'        --END IF;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65544288565992550)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Create action collection'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'-- Create the collection',
'apex_collection.create_or_truncate_collection(''ACTIONS'');'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(64103530262999104)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Initialize page items'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_person    person%ROWTYPE;',
'    v_parcel    pc_parcel%ROWTYPE;',
'    v_location  v_curr_next_res.area_village%TYPE;   ',
'    v_area      area%ROWTYPE;  ',
'BEGIN',
'    -- If the page was navigated to from an existing parcel',
'    IF :P213_PRCL_ID IS NOT NULL THEN',
'        v_parcel := pc_parcel_cru.get_pc_parcel(:P213_PRCL_ID); -- Retreive the parcel information',
'        v_person := pc_parcel_pkg.get_recipient(:P213_PRCL_ID); -- Get the person associated with the parcel        ',
'        v_area := pc_parcel_pkg.get_area(:P213_PRCL_ID);        -- Get the area and location of the parcel',
'        ',
'        -- Populate page items',
'        :P213_DESK_AREA_ID := v_area.id;',
'        :P213_DESK_AREA := v_area.title;',
'        :P213_RECEIVED_ON := v_parcel.receive_dt; ',
'        :P213_AREA_ID := :P213_DESK_AREA_ID;',
'        :P213_LOCATION := :P213_DESK_AREA;',
'        ',
'        -- Populate parcel information',
'        :P213_CARRIER_OPT := v_parcel.carrier;',
'        :P213_CARRIER_ID := v_parcel.pc_carrier_id;',
'        :P213_STAT_ID := v_parcel.trk_st_t_id;',
'        :P213_OPT_DESCR := v_parcel.misc_data;',
'        :P213_TRACK_NUM := v_parcel.track_num;',
'        :P213_NON_RES_F_NAME := v_parcel.non_res_first_name;',
'        :P213_NON_RES_L_NAME := v_parcel.non_res_last_name;',
'        :P213_RETURN_RSN := v_parcel.return_rsn;',
'        :P213_RES_UFID := v_person.ufid;',
'        ',
'        -- Check if parcel is for a resident',
'        IF pc_parcel_pkg.is_for_resident(:P213_PRCL_ID) = ''Y'' THEN',
'            :P213_RECIPIENT_NAME := initcap(v_person.first_name || '' '' || v_person.last_name);',
'            :P213_NON_RES_IND := ''N'';',
'        ELSE',
'            :P213_RECIPIENT_NAME := initcap(v_parcel.non_res_first_name) || '' '' || v_parcel.non_res_last_name;',
'            :P213_NON_RES_IND := ''Y'';',
'        END IF;',
'        ',
'         -- Get the resident''s housing location',
'        <<resident_location>>',
'        BEGIN',
'            SELECT area_village',
'            INTO v_location',
'            FROM v_curr_next_res',
'            WHERE ufid = :P213_RES_UFID',
'            GROUP BY area_village;',
'        EXCEPTION',
'            WHEN NO_DATA_FOUND THEN',
'                NULL;',
'        END;',
'',
'         -- Check if the current area desk is different from the resident''s housing location',
'        IF :P213_DESK_AREA <> v_location THEN',
'            :P213_INVALID_LOCATION := ''Y'';',
'            :P213_ALERT := apex_ui_pkg.get_info_msg(''The parcel needs to be redirected to the resident''''s location.'', :P213_PRCL_ID);  -- Create an alert',
'        ELSE',
'            :P213_INVALID_LOCATION := ''N'';',
'        END IF;',
'        ',
'        pc_parcel_pkg.custom_log(v_parcel.id);',
'    ELSE -- We are receiving a new parcel',
'        -- Get the area location',
'        v_area := pc_parcel_pkg.get_area(:P213_PRCL_ID);',
'        ',
'        SELECT title INTO :P213_DESK_AREA FROM area WHERE id = :P213_DESK_AREA_ID;',
'        ',
'        --:P213_DESK_AREA := v_area.title;',
'        :P213_AREA_ID := :P213_DESK_AREA_ID;',
'        :P213_LOCATION := :P213_DESK_AREA;',
'        :P213_STAT_ID := pc_track_stat_type_pkg.get_new();                    -- Set the status ID to NEW',
'    END IF;',
'    ',
'    :P213_CURRENT_DATE := sysdate;',
'END;'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65169709621713725)
,p_process_sequence=>40
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Populate checkbox values'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'BEGIN',
'    WITH',
'      selected_conditions',
'    AS(',
'      SELECT *',
'      FROM pc_parcel_cond cond',
'      INNER JOIN pc_parcel prcl',
'      ON cond.prcl_id = prcl.id',
'      WHERE active_ind = ''Y''',
'      AND prcl_id = :P213_PRCL_ID)',
'    SELECT listagg(cond.pc_cond_stat_id, '':'')',
'    WITHIN GROUP (ORDER BY cond.prcl_id)',
'    INTO :P213_OPTIONS',
'    FROM selected_conditions cond;',
'    ',
'    -- If the parcel is marked as suspicious then don''t send the resident emails',
'    IF (INSTR(:P213_OPTIONS, ''1'')) > 0 THEN',
'        :P213_SUSP_IND := ''Y'';',
'    ELSE',
'        :P213_SUSP_IND := ''N'';',
'    END IF;',
'    apps.logger.debug(:P213_OPTIONS);',
'    apps.logger.debug(INSTR(:P213_OPTIONS,''1''));',
'EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'        NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(224569091695521911)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Load Carrier Regex'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    CURSOR carrier_regex IS',
'        SELECT pc_carrier_id AS carr_id, pattern',
'        FROM pc_carrier_regex;',
'        ',
'    v_curr_id NUMBER := 0;',
'    v_carriers VARCHAR2(32767);',
'BEGIN',
'    FOR regex IN carrier_regex',
'    LOOP',
'        -- For every different carrier',
'        IF v_curr_id <> regex.carr_id THEN',
'            ',
'            -- If it isn''t the initialized value',
'            IF v_curr_id <> 0 THEN',
'                -- Close the array for a carrier',
'                --APEX_JSON.CLOSE_ARRAY; -- ]',
'                v_carriers := rtrim(v_carriers, '','');',
'                v_carriers := v_carriers || ''],'';',
'            ELSE ',
'                -- Initial object open',
'                --APEX_JSON.OPEN_OBJECT; -- {',
'                v_carriers := v_carriers || ''{ "carriers": {'';',
'            END IF;',
'            ',
'            -- Set the current id',
'            v_curr_id := regex.carr_id;',
'            ',
'            -- Create an array for the carrier',
'            --APEX_JSON.OPEN_ARRAY(''pc_carrier_'' || to_char(regex.carr_id)); -- "carr_N": [',
'            v_carriers := v_carriers || ''"'' || to_char(regex.carr_id) || ''":'' || ''['';',
'            --APEX_JSON.WRITE(regex.pattern); -- ,"pattern"',
'            v_carriers := v_carriers || ''"''|| regex.pattern || ''",'';',
'        ELSE',
'            -- Still looping through the same carrier',
'            --APEX_JSON.WRITE(regex.pattern);',
'            v_carriers := v_carriers || ''"'' || regex.pattern || ''",'';',
'        END IF;',
'    END LOOP;',
'    --APEX_JSON.CLOSE_ALL;',
'    v_carriers := rtrim(v_carriers, '','');',
'    v_carriers := v_carriers || '']}}'';',
'    --DBMS_OUTPUT.PUT_LINE(APEX_JSON.GET_CLOB_OUTPUT);',
'    :P213_REGEX := v_carriers;',
'    --apps.logger.debug(APEX_JSON.GET_CLOB_OUTPUT);',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(226172893763305447)
,p_process_sequence=>10
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Associated Action'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_action_id    pc_track_action.id%TYPE;',
'    v_action_obj   VARCHAR2(200 CHAR);',
'    v_new_receive  VARCHAR2(1 CHAR);',
'BEGIN',
'    v_action_id := apex_application.g_x01;',
'    IF v_action_id = 6 THEN v_new_receive := ''Y''; END IF;',
'    ',
'    -- Create a JSON string of the object values',
'    SELECT ',
'        ''{"action" : {'' ||',
'            ''"dialog_ind": "'' || dialog_ind || ''",'' ||',
'            ''"prompt_ind": "'' || prompt_ind || ''",'' || ',
'            ''"dialog": "''     || dialog     || ''",'' ||',
'            ''"msg": "''        || msg        || ''",'' ||',
'            ''"newReceive": "'' || v_new_receive || ''"'' ||',
'         ''}}''',
'    INTO v_action_obj',
'    FROM pc_track_action',
'    WHERE id = v_action_id;',
'    ',
'    htp.p(v_action_obj);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(226173067236305449)
,p_process_sequence=>20
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Resulting Status'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_selected_action_id     pc_track_action.id%TYPE;',
'    v_track_action           pc_track_action%ROWTYPE;',
'    v_res_track_status       pc_track_stat_type%ROWTYPE;',
'    v_action_descr           pc_parcel_hist.descr%TYPE;',
'BEGIN',
'    v_selected_action_id  := apex_application.g_x01;',
'    v_action_descr        := apex_application.g_x02;',
'    v_track_action        := pc_track_action_cru.get_pc_track_action(v_selected_action_id);                        -- Get the selected action',
'    v_res_track_status    := pc_track_stat_type_cru.get_pc_track_stat_type(v_track_action.result_trk_st_t_id);     -- Get the resulting status',
'    ',
'    --pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(v_selected_action_id, :P213_LOCATION),''Intended Recipient: '' || :P213_RECIPIENT_NAME, sysdate);',
'    pc_track_action_pkg.add_action(''ACTIONS'',pc_track_action_pkg.get_headline(v_selected_action_id, :P213_LOCATION), v_action_descr, sysdate);',
'    ',
'    ',
'    htp.p(v_res_track_status.id);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(226413475189345949)
,p_process_sequence=>30
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Validate Location'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_selected_area_id    area.id%TYPE;',
'    v_invalid_location    VARCHAR2(1 CHAR);',
'    v_return              VARCHAR2(32767 CHAR);',
'BEGIN',
'    v_selected_area_id := apex_application.g_x01;',
'    IF v_selected_area_id <> :P213_DESK_AREA_ID THEN',
'        v_invalid_location := ''Y'';',
'        APEX_UTIL.SET_SESSION_STATE (',
'            p_name     =>    ''APP_ALERT_MSG'',',
'            p_value    => apex_ui_pkg.get_center_info_msg(''The parcel needs to be forwarded to the resident''''s location.'', :P213_PRCL_ID));',
'    ELSE',
'        v_invalid_location := ''N'';',
'    END IF;',
'    ',
'    v_return := ''{ '' ||',
'                    ''"invalidLocation": "'' || v_invalid_location || ''"'' ||',
'                ''}'';',
'    ',
'    htp.p(v_return);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(226575920311916918)
,p_process_sequence=>40
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Submit Page Items'
,p_process_sql_clob=>'NULL;'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(226576449967916923)
,p_process_sequence=>50
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Create Action'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_headline    pc_parcel_hist.headline%TYPE;',
'    v_descr       pc_parcel_hist.descr%TYPE;',
'BEGIN',
'    v_headline := apex_application.g_x01;',
'    v_descr    := apex_application.g_x02;',
'',
'    pc_track_action_pkg.add_action(''ACTIONS'', v_headline, v_descr , sysdate);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(743999169187581343)
,p_process_sequence=>60
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Check Duplicate Track Num'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_track_num    pc_parcel.track_num%TYPE;',
'    v_res          VARCHAR2(1 CHAR);',
'BEGIN',
'    v_track_num := apex_application.g_x01;',
'    ',
'    SELECT',
'        CASE',
'            WHEN EXISTS(',
'                SELECT 1',
'                FROM pc_parcel',
'                WHERE track_num = v_track_num)',
'            THEN ''Y''',
'            ELSE ''N''',
'        END',
'    INTO v_res',
'    FROM dual;',
'    ',
'    --IF v_res = ''Y'' THEN',
'    --    APEX_UTIL.SET_SESSION_STATE (',
'    --        p_name     =>    ''APP_ALERT_MSG'',',
'    --        p_value    =>     apex_ui_pkg.get_center_error_msg(''This tracking number has received before.'', :P213_PRCL_ID));',
'    --END IF;',
'    ',
'    htp.p(v_res);',
'END;    '))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
