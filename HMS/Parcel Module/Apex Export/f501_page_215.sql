set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:19 Wednesday July 18, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00215
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>215);
end;
/
prompt --application/pages/page_00215
begin
wwv_flow_api.create_page(
 p_id=>215
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: History'
,p_page_mode=>'NORMAL'
,p_step_title=>'PRCL: History'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180718081337'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(65830816998515001)
,p_plug_name=>'Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(65831091996515003)
,p_plug_name=>'History'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT start_dt, end_dt, headline, descr, null',
'FROM pc_parcel_hist',
'WHERE prcl_id = :P215_PRCL_ID'))
,p_plug_source_type=>'PLUGIN_DE.DANIELH.TIMELINE'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'&P215_NULL.'
,p_attribute_02=>'&P215_NULL.'
,p_attribute_04=>'test'
,p_attribute_05=>'PTSerif-PTSans'
,p_attribute_06=>'en'
,p_attribute_07=>'100%'
,p_attribute_08=>'600'
,p_attribute_09=>'START_DT'
,p_attribute_10=>'END_DT'
,p_attribute_11=>'HEADLINE'
,p_attribute_12=>'DESCR'
,p_attribute_13=>'NULL'
,p_attribute_14=>'FULL'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65830906543515002)
,p_name=>'P215_PRCL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(65830816998515001)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65831471873515007)
,p_name=>'P215_TRACK_NUM'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(65830816998515001)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65831815579515011)
,p_name=>'P215_NULL'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(65830816998515001)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65831214150515005)
,p_name=>'Update Breadcrumb label'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65831356555515006)
,p_event_id=>wwv_flow_api.id(65831214150515005)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var breadcrumb = $("span.t-Breadcrumb-label");',
'var label = breadcrumb.text();',
'var trackNum = apex.item("P215_TRACK_NUM").getValue();',
'',
'breadcrumb.text(label + " > " + trackNum);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65831569374515008)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Load Track #'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT track_num',
'INTO :P215_TRACK_NUM',
'FROM pc_parcel',
'WHERE id = :P215_PRCL_ID;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
