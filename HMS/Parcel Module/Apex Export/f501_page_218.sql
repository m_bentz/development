set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:19 Wednesday July 18, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00218
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>218);
end;
/
prompt --application/pages/page_00218
begin
wwv_flow_api.create_page(
 p_id=>218
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Track Num (Modal)'
,p_page_mode=>'MODAL'
,p_step_title=>'PRCL: Track Num (Modal)'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_inline_css=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'.mismatch {',
'    border-color:#e24c0e !important;',
'}',
'',
'#P218_INFO_DISPLAY {',
'    font-size: 1.4rem;',
'    font-weight: 600;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180227155326'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225231087584989934)
,p_plug_name=>'Main'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225355380940569830)
,p_plug_name=>'Whitespace'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542724015766624)
,p_plug_name=>'DB Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542837473766625)
,p_plug_name=>'Return Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542991254766626)
,p_plug_name=>'Received Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225231392191989937)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(225231087584989934)
,p_button_name=>'CHECK'
,p_button_static_id=>'check_btn'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974578276773159)
,p_button_image_alt=>'Check'
,p_button_position=>'BODY'
,p_icon_css_classes=>'fa-check'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225231186666989935)
,p_name=>'P218_TRACK_NUM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542991254766626)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225231245133989936)
,p_name=>'P218_TRACK_INPUT'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(225231087584989934)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Track input'
,p_placeholder=>'Scan Tracking #'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>11
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225231913542989943)
,p_name=>'P218_RESPONSE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542837473766625)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225232254438989946)
,p_name=>'P218_PRCL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225542724015766624)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225352591289569802)
,p_name=>'P218_INFO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(225231087584989934)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225231479382989938)
,p_name=>'Focus Input'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225231501798989939)
,p_event_id=>wwv_flow_api.id(225231479382989938)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P218_TRACK_INPUT'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225231662839989940)
,p_name=>'Check Tracking Num'
,p_event_sequence=>20
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(225231392191989937)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225231764219989941)
,p_event_id=>wwv_flow_api.id(225231662839989940)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var trackNum = apex.item("P218_TRACK_NUM").getValue();',
'var userTrackNum = apex.item("P218_TRACK_INPUT").getValue();',
'var input = $("#P218_TRACK_INPUT");',
'',
'if (trackNum === userTrackNum) {',
'    apex.item("P218_RESPONSE").setValue(''Y'');',
'    apex.navigation.dialog.close(true,["P218_RESPONSE","P218_PRCL_ID"]);   ',
'} else {',
'    input.toggleClass(''mismatch'');',
'    apex.item("P218_INFO").setValue("The entered tracking number does not match with the selected package.");',
'}',
'console.log(apex.item("P218_TRACK_NUM").getValue())',
'console.log(apex.item("P218_TRACK_INPUT").getValue())'))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225353223616569809)
,p_name=>'Enter pressed'
,p_event_sequence=>30
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_bind_event_type=>'keypress'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225353374728569810)
,p_event_id=>wwv_flow_api.id(225353223616569809)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var enterKey = 13;',
'',
'if (this.browserEvent.keyCode === enterKey) {',
'    this.browserEvent.preventDefault();',
'    $("#P218_TRACK_INPUT").blur();',
'    $("#check_btn").focus();',
'    $("#check_btn").click();',
'}',
''))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(225354369387569820)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get track num'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT track_num',
'INTO :P218_TRACK_NUM',
'FROM pc_parcel',
'WHERE id = :P218_PRCL_ID;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
