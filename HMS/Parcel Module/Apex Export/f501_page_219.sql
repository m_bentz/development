set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:19 Wednesday July 18, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00219
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>219);
end;
/
prompt --application/pages/page_00219
begin
wwv_flow_api.create_page(
 p_id=>219
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Non-Resident Form'
,p_page_mode=>'MODAL'
,p_step_title=>'PRCL: Non-Resident Form'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_inline_css=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'.mismatch {',
'    border-color:#e24c0e !important;',
'}',
'',
'#P219_INFO_DISPLAY {',
'    font-size: 1.4rem;',
'    font-weight: 600;',
'}',
'',
'.hidden {',
'    visibility: hidden;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180307130932'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225540704775766604)
,p_plug_name=>'Form'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225540827163766605)
,p_plug_name=>'Whitespace'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225541238628766609)
,p_plug_name=>'Button Container'
,p_region_template_options=>'#DEFAULT#:t-ButtonRegion--slimPadding:t-ButtonRegion--noUI'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225543097363766627)
,p_plug_name=>'Returning Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225543524855766632)
,p_plug_name=>'Passed In Page Values'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225541393704766610)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(225541238628766609)
,p_button_name=>'Submit'
,p_button_static_id=>'submit-btn'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974578276773159)
,p_button_image_alt=>'Submit'
,p_button_position=>'BELOW_BOX'
,p_icon_css_classes=>'fa-check'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225541077250766607)
,p_name=>'P219_FIRST_NAME'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'First name'
,p_placeholder=>'First Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_receive()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225541129252766608)
,p_name=>'P219_LAST_NAME'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'New'
,p_placeholder=>'Last Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_receive()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225541427612766611)
,p_name=>'P219_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225543097363766627)
,p_source=>':APP_PAGE_ID'
,p_source_type=>'FUNCTION'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225542091427766617)
,p_name=>'P219_RCV_INFO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'Rcv info'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_receive()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225543441140766631)
,p_name=>'P219_PRE_RTS_REASON'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'Reason for returning'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Not a Housing Resident,Resident Failed to Collect, Resident Failed to Collect from Locker, Resident Requested Return,Other'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225543662046766633)
,p_name=>'P219_REQ_TYPE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225543524855766632)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225543724351766634)
,p_name=>'P219_USR_RTS_REASON'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'Usr rts reason'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_css_classes=>'hidden'
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225544728310766644)
,p_name=>'P219_RTS_INFO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(225540704775766604)
,p_prompt=>'Rts info'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_grid_label_column_span=>0
,p_display_when=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender()'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225544958343766646)
,p_name=>'P219_RTS_REASON'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225543097363766627)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225590093780814432)
,p_name=>'Submit Modal (Non-res receive)'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(225541393704766610)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_non_res_receive()'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225590419104814434)
,p_event_id=>wwv_flow_api.id(225590093780814432)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var firstName = apex.item("P219_FIRST_NAME").getValue();',
'var input = $("#P219_FIRST_NAME");',
'console.log(input);',
'if (firstName !== "") {',
'    apex.navigation.dialog.close(true,["P219_ID","P219_REQ_TYPE","P219_FIRST_NAME","P219_LAST_NAME"]);   ',
'} else {',
'    // Only available in APEX 5.1',
'    // apex.message.alert("not valid");',
'    input.toggleClass(''mismatch'');',
'    apex.item("P219_RCV_INFO").setValue("First name is required.");',
'    //alert("Tracking numbers do not match");',
'}',
'',
'',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225541873431766615)
,p_name=>'Focus Input'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225541918280766616)
,p_event_id=>wwv_flow_api.id(225541873431766615)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P219_FIRST_NAME'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225615836435243345)
,p_name=>'Enter pressed'
,p_event_sequence=>30
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_bind_event_type=>'keypress'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225616268377243345)
,p_event_id=>wwv_flow_api.id(225615836435243345)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var enterKey = 13;',
'if (this.browserEvent.keyCode === enterKey) {',
'    this.browserEvent.preventDefault();',
'    $("#P219_FIRST_NAME").blur();',
'    $("#submit-btn").focus();',
'    $("#submit-btn").click();',
'}',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225544093867766637)
,p_name=>'Reason selected'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P219_PRE_RTS_REASON'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'Other'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender()'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225544245264766639)
,p_event_id=>wwv_flow_api.id(225544093867766637)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P219_USR_RTS_REASON'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225544408412766641)
,p_event_id=>wwv_flow_api.id(225544093867766637)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$("#P219_USR_RTS_REASON_CONTAINER").removeClass(''hidden'')'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225544161777766638)
,p_event_id=>wwv_flow_api.id(225544093867766637)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P219_USR_RTS_REASON'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225544348209766640)
,p_event_id=>wwv_flow_api.id(225544093867766637)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P219_USR_RTS_REASON'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225544503685766642)
,p_name=>'Submit Modal (Return to sender)'
,p_event_sequence=>60
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(225541393704766610)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':P219_REQ_TYPE LIKE pc_track_action_pkg.get_return_to_sender()'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225544673126766643)
,p_event_id=>wwv_flow_api.id(225544503685766642)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var preReason = $("#P219_PRE_RTS_REASON");',
'//apex.item("P219_PRE_RTS_REASON").getValue();',
'var usrReason = $("#P219_USR_RTS_REASON");',
'//var reason = apex.item("P219_USR_RTS_REASON").getValue();',
'',
'',
'var input = $("#P219_REASON_OPT");',
'',
'console.log(input);',
'if (preReason.val() === ''Other'') {',
'    if (usrReason.val() !== "") {',
'        setReason(usrReason.val());',
'        closeDialog();',
'    } else {',
'        usrReason.toggleClass(''mismatch'');',
'        apex.item("P219_RTS_INFO").setValue("Reason is required.");',
'    }',
'} else if (preReason.val()) {',
'    setReason(preReason.val());',
'    closeDialog();',
'} else {',
'    preReason.toggleClass(''mismatch'');',
'    apex.item("P219_RTS_INFO").setValue("Reason is required.");',
'}',
'',
'',
'function setReason(value) {',
'    apex.item("P219_RTS_REASON").setValue(value);',
'}',
'',
'function closeDialog() {',
'    apex.navigation.dialog.close(true,["P219_ID","P219_REQ_TYPE","P219_RTS_REASON"]); ',
'}'))
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
