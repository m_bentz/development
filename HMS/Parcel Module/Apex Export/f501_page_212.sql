set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   15:50 Friday August 3, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00212
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>212);
end;
/
prompt --application/pages/page_00212
begin
wwv_flow_api.create_page(
 p_id=>212
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Manage Parcels'
,p_page_mode=>'NORMAL'
,p_step_title=>'PC: Manage Parcels'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_javascript_code=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'function setTitle(region, title) {',
'    $(region + " .t-Region-title").text(title); ',
'}',
'',
'function toggleRegion(region) {',
'    $(region).find("button.t-Button--hideShow").eq(0).click();',
'}',
'    ',
'function isCollapsed(region) {',
'    return $(region).hasClass("is-collapsed");',
'}',
'',
'// Includes polyfill for IE',
'if (!String.prototype.includes) {',
'  String.prototype.includes = function(search, start) {',
'    ''use strict'';',
'    if (typeof start !== ''number'') {',
'      start = 0;',
'    }',
'',
'    if (start + search.length > this.length) {',
'      return false;',
'    } else {',
'      return this.indexOf(search, start) !== -1;',
'    }',
'  };',
'}',
'',
'// Update the Waiting status to reflect the amount of days',
'// have passed since receiving the parcel',
'function updateDaysWaiting() {',
'    var element, parcelDate;',
'    ',
'    // For all the the fields that contain waiting',
'    $("tr td:contains(''Waiting'')").each(function (){',
'        // Get the element holding the date',
'        element = $(this).next();',
'',
'        // Get the parcel date',
'        parcelDate = getParcelDate(element);',
'',
'        if (validDate(parcelDate)) {',
'            $(this).text("Waiting " + getDaysPassed(parcelDate));                ',
'        }',
'    });',
'}',
'',
'// Returns a Date object given a jquery element ',
'// with the format MM/DD/YYYY (sql default)',
'function getParcelDate(jqueryElement) {',
'    var date_regex = /\d{2}\/\d{2}\/\d{4}/;',
'    ',
'    if (jqueryElement.text().match(date_regex)) { ',
'        // Parse through the information',
'        var month = parseInt(jqueryElement.text().substring(0,2));',
'        var day = parseInt(jqueryElement.text().substring(3,5));',
'        var year = parseInt(jqueryElement.text().substring(6,10));',
'',
'        // Need to return month - 1 because month in JS starts at 0',
'        return new Date(year, month - 1, day);   ',
'    }',
'}',
'',
'// Calculates the amount of days that have passed from the current date',
'// And returns the corresponding amount',
'function getDaysPassed(parcelDate) {',
'    var secondsInDay = 86400;',
'    var milliseconds = 1000;',
'    ',
'    // Get the current date',
'    var currentDate = new Date();',
'',
'    // Calculate the difference',
'    var difference = parseInt((currentDate - parcelDate)/milliseconds/secondsInDay);',
'',
'    // Check if the difference is greater than a day',
'    //if (difference > 0) {',
'        // Return the amount of days',
'        return difference + " day" + (difference != 1 ? "s" : "");',
'    //}',
'    return "";',
'}',
'',
'$(window).on("apexwindowresized", function(e) {',
'    console.log(e.type);',
'});',
'',
'function validDate(date) {',
'    return !isNaN(date);',
'}',
'',
'function updateFilterButtons() {',
'    // Get the filter buttons',
'    var filterButtons = $(''.filter-Button'');',
'',
'    // Get the active filter',
'    var activeFilter = $(''#P212_PRCL_LIST_control_panel a.a-IRR-controlsLabel span'')[1].innerText;',
'',
'    filterButtons.each(function(e, button) {',
'    var filterType = button.innerText;',
'',
'        if (!activeFilter.includes(filterType)) {',
'            disableFilterButton(filterType);',
'            //$(button).find(''span.t-Icon'').css(''display'', ''none'');',
'        } else {',
'            enableFilterButton(filterType);',
'        }',
'        //console.log(filterType);',
'    });',
'    ',
'    //console.log(''active filter: '' + activeFilter);',
'}',
'',
'function disableFilterButton(filter) {',
'    $(".filter-Button:contains(''"+ filter + "'')").find(''span.t-Icon--left'').css(''display'', ''none'');',
'}',
'',
'function enableFilterButton(filter) {',
'    $(".filter-Button:contains(''"+ filter + "'')").find(''span.t-Icon--left'').css(''display'', ''inline-block'');',
'}'))
,p_inline_css=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'.t-Body-contentInner > .t-Region {',
'    max-width: 1280px;',
'    margin: 0 auto;',
'}',
'',
'#P212_AREA_ID_CONTAINER {',
'    margin: 5px 0 15px 0;',
'    display: flex;',
'    flex-direction: column;',
'}',
'',
'#P212_AREA_ID_LABEL {',
'    font-weight: bold;',
'    font-size: 1.6rem;',
'    padding: 0;',
'}',
'',
'',
'#P212_PRCL_LIST_control_panel li.a-IRR-controls-item--highlight, #P212_PRCL_LIST_control_panel button.a-IRR-button--controls {',
'    display: none;',
'}',
'',
'.t-Button+.t-Button {',
'    margin-left: 0;',
'}',
'',
'.filter-Button , .past-due {',
'    margin-top: 31px;',
'    min-width: fit-content;',
'}',
'',
'#P212_PAST_DUE_DISPLAY {',
'    color: red;',
'}'))
,p_step_template=>wwv_flow_api.id(36873697557773080)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180803155048'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62858430677742425)
,p_plug_name=>'Manage Parcels'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62859785776742438)
,p_plug_name=>'Parcels at &P212_AREA_TITLE. Area Desk'
,p_region_name=>'P212_AREA_DESK_CONT'
,p_parent_plug_id=>wwv_flow_api.id(62858430677742425)
,p_region_template_options=>'#DEFAULT#:is-expanded:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(36895207067773096)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62860183603742442)
,p_plug_name=>'P212_PRCL_LIST'
,p_region_name=>'P212_PRCL_LIST'
,p_parent_plug_id=>wwv_flow_api.id(62859785776742438)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36903682925773102)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'WITH resident_parcel AS (',
'    SELECT',
'        --prcl.*,',
'        prcl.id AS prcl_id,',
'        prcl.receive_dt,',
'        prcl.track_num,',
'        area.title AS area_title,',
'        stat.title AS status_title,',
'        area.id AS area_id,',
'        initcap(prsn.first_name || '' '' || prsn.last_name) AS recipient,',
'        prsn.ufid AS prsn_ufid,',
'        pc_parcel_pkg.is_suspicious(prcl.id) AS suspicious_ind,',
'        pc_parcel_pkg.is_past_due(prcl.id, area.id) AS past_due_ind,',
'        pc_parcel_pkg.is_location_valid(prcl.id, area.id) AS location_valid_ind,',
'        stat.title ||'' '' ||',
'            CASE',
'            WHEN stat.title = ''Waiting'' THEN',
'                trunc(sysdate - receive_dt) || '' day(s)''',
'        END AS prcl_status',
'    FROM pc_parcel prcl',
'    INNER JOIN pc_parcel_list prcl_list',
'        ON prcl.prcl_list_id = prcl_list.id',
'    INNER JOIN area',
'        ON prcl_list.area_id = area.id',
'    LEFT OUTER JOIN person prsn',
'        ON prcl.prsn_id = prsn.id',
'    INNER JOIN pc_track_stat_type stat',
'        ON prcl.trk_st_t_id = stat.id',
'    WHERE prsn_id IS NOT NULL',
'    AND nvl(delete_ind, ''N'') = ''N''',
')',
'SELECT ',
'    area_title AS Area,',
'    track_num AS "Tracking #",',
'    recipient AS "Resident",',
'    prsn_ufid AS UFID,',
'    prcl_status || '' '' || ',
'        CASE',
'            WHEN suspicious_ind = ''Y'' THEN ''[Suspicious]''',
'            WHEN past_due_ind = ''Y'' THEN ''[Past Due]'' ',
'            WHEN location_valid_ind = ''N'' THEN ''[Needs to be redirected]''',
'            WHEN location_valid_ind = ''Y'' THEN',
'                decode(prcl_status, ''Waiting'', ''[Awaiting pickup]'', ''Forwarded'', ''[In transit]'', '''')',
'        END ',
'    AS status,',
'    prcl.receive_dt AS "Received On",',
'    ''<div class="button-Container">'' ||',
'    ''<button type="button" data-prcl-id="''|| prcl_id ||''"'' ||',
'       ''class="view-prcl t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-archive"></span>Manage Parcel</button>'' ||',
'    ''<button type="button" data-prcl-id="''|| prcl_id ||''"'' ||',
'       ''class="view-hist t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-history"></span>View History</button>'' ||',
'    ''</div>'' AS "Actions"',
'FROM resident_parcel prcl',
'WHERE area_id  = ',
'    CASE',
'        WHEN :P212_AREA_ID = 0 THEN area_id',
'        WHEN :P212_AREA_ID <> 0 THEN to_number(:P212_AREA_ID)',
'    END',
'ORDER BY ',
'    CASE ',
'        WHEN status LIKE ''%[Past Due]'' THEN 1',
'        WHEN status LIKE ''%[Suspicious]'' THEN 2',
'        WHEN status LIKE ''%[Needs to be redirected]'' THEN 3',
'        WHEN status LIKE ''%[In transit]'' THEN 4',
'        WHEN status LIKE ''%Returning To Sender%'' THEN 5',
'        WHEN status LIKE ''%Delivered%'' THEN 6',
'        ELSE  7',
'    END,',
'    prcl.receive_dt desc'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(62860298214742443)
,p_max_row_count=>'1000000'
,p_no_data_found_message=>'There are no parcels at this desk.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'MICHAELBE'
,p_internal_uid=>62860298214742443
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226576284659916921)
,p_db_column_name=>'AREA'
,p_display_order=>220
,p_column_identifier=>'BE'
,p_column_label=>'Area'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998104247581333)
,p_db_column_name=>'Tracking #'
,p_display_order=>230
,p_column_identifier=>'BF'
,p_column_label=>'Tracking #'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998235575581334)
,p_db_column_name=>'Resident'
,p_display_order=>240
,p_column_identifier=>'BG'
,p_column_label=>'Resident'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998395992581335)
,p_db_column_name=>'UFID'
,p_display_order=>250
,p_column_identifier=>'BH'
,p_column_label=>'Ufid'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998495380581336)
,p_db_column_name=>'STATUS'
,p_display_order=>260
,p_column_identifier=>'BI'
,p_column_label=>'Status'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998550889581337)
,p_db_column_name=>'Received On'
,p_display_order=>270
,p_column_identifier=>'BJ'
,p_column_label=>'Received on'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(743998639625581338)
,p_db_column_name=>'Actions'
,p_display_order=>280
,p_column_identifier=>'BK'
,p_column_label=>'Actions'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(63978467590846741)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'639785'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>50
,p_report_columns=>'UFID_TITLE:AREA:Tracking #:Resident:UFID:STATUS:Received On:Actions'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(226919335445113111)
,p_report_id=>wwv_flow_api.id(63978467590846741)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'STATUS'
,p_operator=>'='
,p_expr=>'Forwarded'
,p_condition_sql=>' (case when ("STATUS" = #APXWS_EXPR#) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# = ''Forwarded''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#FFFF99'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(226919797726113111)
,p_report_id=>wwv_flow_api.id(63978467590846741)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'STATUS'
,p_operator=>'contains'
,p_expr=>'Suspicious'
,p_condition_sql=>' (case when (upper("STATUS") like ''%''||upper(#APXWS_EXPR#)||''%'') then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''Suspicious''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#FF7755'
);
wwv_flow_api.create_worksheet_condition(
 p_id=>wwv_flow_api.id(226920118895113113)
,p_report_id=>wwv_flow_api.id(63978467590846741)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'STATUS'
,p_operator=>'contains'
,p_expr=>'redirected'
,p_condition_sql=>' (case when (upper("STATUS") like ''%''||upper(#APXWS_EXPR#)||''%'') then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''redirected''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#99CCFF'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224172448234909950)
,p_plug_name=>'Container'
,p_parent_plug_id=>wwv_flow_api.id(62858430677742425)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noPadding:t-Region--noBorder:t-Region--scrollBody:t-Form--noPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62859507355742436)
,p_plug_name=>'Desk Area Selection'
,p_parent_plug_id=>wwv_flow_api.id(224172448234909950)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224169740229909923)
,p_plug_name=>'Filters'
,p_parent_plug_id=>wwv_flow_api.id(62859507355742436)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36909130176773106)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'--f?p=                                      App:Page:Session:Request:Debug:ClearCache:itemNames:itemValues:PrinterFriendly',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_INFO,IR_STATUS:'') || ''">All</a>'');',
'htp.p('' : '');',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_INFO,IR_STATUS:Suspicious'') || ''">Suspicious</a>'');',
'htp.p('' : '');',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_STATUS,IR_STATUS:Redirected'') || ''">Redirected</a>'');',
'htp.p('' : '');',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_STATUS,IR_STATUS:Waiting'') || ''">Waiting</a>'');',
'htp.p('' : '');',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_STATUS,IR_STATUS:Delivered'') || ''">Delivered</a>'');',
'htp.p('' : '');',
'htp.p(''<a href="'' || apex_util.prepare_url(''/f?p=501:212:&SESSION.::&DEBUG.:RIR:IR_INFO,IR_STATUS:Past Due'') || ''">Past Due</a>'');'))
,p_plug_source_type=>'NATIVE_PLSQL'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225821333718130304)
,p_plug_name=>'Filter Buttons'
,p_parent_plug_id=>wwv_flow_api.id(224172448234909950)
,p_region_template_options=>'#DEFAULT#:t-ButtonRegion--slimPadding:t-ButtonRegion--noBorder:t-Form--noPadding'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225821575095130306)
,p_plug_name=>'Non Resident Parcels at &P212_AREA_TITLE. Area Desk'
,p_region_name=>'P212_AREA_DESK_NON_RES'
,p_parent_plug_id=>wwv_flow_api.id(62858430677742425)
,p_region_template_options=>'#DEFAULT#:is-expanded:t-Region--noBorder:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(36895207067773096)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225821645270130307)
,p_plug_name=>'P212_NON_RES_PRCL_LIST'
,p_parent_plug_id=>wwv_flow_api.id(225821575095130306)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36903682925773102)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'WITH non_resident_parcel AS (',
'    SELECT',
'        prcl.id AS prcl_id,',
'        prcl.receive_dt,',
'        prcl.track_num,',
'        area.title AS area_title,',
'        stat.title AS status_title,',
'        area.id AS area_id,',
'        initcap(non_res_first_name || '' '' || non_res_last_name) AS recipient',
'    FROM pc_parcel prcl',
'    INNER JOIN pc_parcel_list prcl_list',
'        ON prcl.prcl_list_id = prcl_list.id',
'    INNER JOIN area',
'        ON prcl_list.area_id = area.id',
'    INNER JOIN pc_track_stat_type stat',
'        ON prcl.trk_st_t_id = stat.id',
'    WHERE prsn_id IS NULL',
'    AND nvl(delete_ind, ''N'') = ''N''',
')',
'SELECT ',
'    area_title AS Area,',
'    track_num AS "Tracking #",',
'    recipient,',
'    status_title AS Status,',
'    prcl.receive_dt AS "Received On",',
'    ''<div class="button-Container">'' ||                                    ',
'    ''<button type="button" data-prcl-id="''|| prcl_id ||''"'' ||',
'       ''class="view-prcl t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-archive"></span>Manage Parcel</button>'' ||',
'    ''<button type="button" data-prcl-id="''|| prcl_id ||''"'' ||',
'       ''class="view-hist t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-history"></span>View History</button>'' ||',
'    ''</div>'' AS "Actions"',
'FROM non_resident_parcel prcl',
'WHERE area_id  = ',
'    CASE',
'        WHEN :P212_AREA_ID = 0 THEN area_id',
'        WHEN :P212_AREA_ID <> 0 THEN to_number(:P212_AREA_ID)',
'    END',
'ORDER BY receive_dt desc;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(225823819410130329)
,p_max_row_count=>'1000000'
,p_no_data_found_message=>'There are no parcels at this desk.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'MICHAELBE'
,p_internal_uid=>225823819410130329
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(225925771999532816)
,p_db_column_name=>'AREA'
,p_display_order=>80
,p_column_identifier=>'AC'
,p_column_label=>'Area'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
,p_display_condition_type=>'PLSQL_EXPRESSION'
,p_display_condition=>':P212_AREA_ID = 0'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226578751740916946)
,p_db_column_name=>'Tracking #'
,p_display_order=>90
,p_column_identifier=>'AD'
,p_column_label=>'Tracking #'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226578828926916947)
,p_db_column_name=>'RECIPIENT'
,p_display_order=>100
,p_column_identifier=>'AE'
,p_column_label=>'Recipient'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226578909236916948)
,p_db_column_name=>'STATUS'
,p_display_order=>110
,p_column_identifier=>'AF'
,p_column_label=>'Status'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226579033369916949)
,p_db_column_name=>'Received On'
,p_display_order=>120
,p_column_identifier=>'AG'
,p_column_label=>'Received on'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(226579177220916950)
,p_db_column_name=>'Actions'
,p_display_order=>130
,p_column_identifier=>'AH'
,p_column_label=>'Actions'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(225920874022865747)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'2259209'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>50
,p_report_columns=>'NON_RES_FIRST_NON_RES_LAST_RETURN_RSN:AREA:Tracking #:RECIPIENT:STATUS:Received On:Actions'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(62858587073742426)
,p_plug_name=>'Parcel Options'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36909130176773106)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542314631766620)
,p_plug_name=>'DB Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542412734766621)
,p_plug_name=>'UI/Nav Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224568271289521903)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Reset'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Reset'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-none'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224169814680909924)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Suspicious'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Suspicious'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224170490346909930)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Waiting'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Waiting'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224170575852909931)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Delivered'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Delivered'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224170614546909932)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Past_Due'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Past Due'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224170327250909929)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Forward'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Forwarded'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225821221097130303)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_api.id(225821333718130304)
,p_button_name=>'Returning'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--simple:t-Button--iconLeft:t-Button--padLeft:t-Button--padRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Returning'
,p_button_position=>'BELOW_BOX'
,p_button_css_classes=>'filter-Button column-status'
,p_icon_css_classes=>'fa-check-square'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(62858791224742428)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(62858587073742426)
,p_button_name=>'RECEIVE'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--noUI'
,p_button_template_id=>wwv_flow_api.id(36974615710773160)
,p_button_image_alt=>'Receive Parcel'
,p_button_position=>'BODY'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(224000869961940902)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(62858587073742426)
,p_button_name=>'SEARCH'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--noUI'
,p_button_template_id=>wwv_flow_api.id(36974615710773160)
,p_button_image_alt=>'Search All'
,p_button_position=>'BODY'
,p_button_redirect_url=>'f?p=&APP_ID.:216:&SESSION.::&DEBUG.:RP::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225171283461118743)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(62858587073742426)
,p_button_name=>'PAST_DUE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--noUI'
,p_button_template_id=>wwv_flow_api.id(36974615710773160)
,p_button_image_alt=>'Process Past Due'
,p_button_position=>'BODY'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(64866035804100750)
,p_branch_name=>'Receive Parcel (Parcel Form)'
,p_branch_action=>'f?p=&APP_ID.:213:&SESSION.::&DEBUG.:RP,213:P213_DESK_AREA_ID:&P212_AREA_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'BEFORE_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>20
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST IN (''RECEIVE'')'
);
end;
/
begin
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(226171738831305436)
,p_branch_name=>'View Parcel (Parcel Form)'
,p_branch_action=>'f?p=&APP_ID.:213:&SESSION.::&DEBUG.:RP,213:P213_DESK_AREA_ID,P213_PRCL_ID:&P212_AREA_ID.,&P212_PRCL_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'BEFORE_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST IN (''VIEW'')'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(226171157536305430)
,p_branch_name=>'Go to Page 215 (History)'
,p_branch_action=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP:P215_PRCL_ID:&P212_PRCL_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'BEFORE_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST IN (''HISTORY'')'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(225228714140989911)
,p_branch_name=>'Go To Page 217 (Bulk Process)'
,p_branch_action=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:RP,217:P217_AREA_ID:&P212_AREA_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'BEFORE_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>50
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST = ''PAST_DUE'''
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(62859277913742433)
,p_name=>'P212_AREA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(62859507355742436)
,p_prompt=>'Area Desk'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT ''All'', 0',
'FROM DUAL',
'UNION',
'SELECT title, id',
'FROM (',
'    SELECT *',
'    FROM area',
'    WHERE hsng_t_id IN 1',
'    AND id <> 61',
'    ORDER BY title)'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Select an Area Desk'
,p_cHeight=>1
,p_colspan=>3
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--xlarge'
,p_is_persistent=>'U'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224171544916909941)
,p_name=>'P212_FILTER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225821333718130304)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224171681957909942)
,p_name=>'P212_COLUMN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225821333718130304)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224568304621521904)
,p_name=>'P212_PAST_DUE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(62859507355742436)
,p_prompt=>'Past due: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_grid_column=>11
,p_grid_label_column_span=>1
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_css_classes=>'past-due'
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--xlarge'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225228215830989906)
,p_name=>'P212_URL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542412734766621)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225925212835532811)
,p_name=>'P212_SHOW_NON_RES'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(62859507355742436)
,p_prompt=>'Show non res'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:Show Non Resident Packages;Y,'
,p_lov_display_null=>'YES'
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226170698832305425)
,p_name=>'P212_PRCL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542314631766620)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(65012456321038503)
,p_validation_name=>'Area not null'
,p_validation_sequence=>10
,p_validation=>'P212_AREA_ID'
,p_validation_type=>'ITEM_NOT_NULL_OR_ZERO'
,p_error_message=>'An area desk must be selected.'
,p_always_execute=>'N'
,p_associated_item=>wwv_flow_api.id(62859277913742433)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64864967864100739)
,p_name=>'Change area desk'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P212_AREA_ID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65012531799038504)
,p_event_id=>wwv_flow_api.id(64864967864100739)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P212_AREA_ID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var value = apex.item("P212_AREA_ID").getValue();',
'var region = "#P212_AREA_DESK_CONT";',
'var nonResRegion = "#P212_AREA_DESK_NON_RES";',
'',
'if (value) { ',
'    var areaDesk = apex.jQuery("#P212_AREA_ID option[value=" + value + "]").text();',
'    setTitle(region, "Resident Parcels at " + areaDesk + " Area Desk");',
'    setTitle(nonResRegion, "Non Resident Parcels at " + areaDesk + " Area Desk");',
'      ',
'    if (isCollapsed(region)) {',
'        toggleRegion(region);',
'    } ',
'} else {',
'    setTitle(region, "");',
'    setTitle(nonResRegion, "");',
'}',
'',
'apex.event.trigger("#P212_PRCL_LIST","change");'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(223431878608918741)
,p_event_id=>wwv_flow_api.id(64864967864100739)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P212_AREA_ID'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(223431737053918740)
,p_event_id=>wwv_flow_api.id(64864967864100739)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(62860183603742442)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225924258220532801)
,p_event_id=>wwv_flow_api.id(64864967864100739)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225821645270130307)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64865745117100747)
,p_name=>'Open Parcel Form'
,p_event_sequence=>30
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(62858791224742428)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224001290602940906)
,p_event_id=>wwv_flow_api.id(64865745117100747)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'RECEIVE'
,p_attribute_02=>'Y'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65542453064992532)
,p_name=>'Update Days Waiting'
,p_event_sequence=>40
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_api.id(62860183603742442)
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterrefresh'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65542530307992533)
,p_event_id=>wwv_flow_api.id(65542453064992532)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'updateDaysWaiting();'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65832002142515013)
,p_name=>'Show sidebar'
,p_event_sequence=>50
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65832154909515014)
,p_event_id=>wwv_flow_api.id(65832002142515013)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(62858587073742426)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224171252808909938)
,p_name=>'Report Filter'
,p_event_sequence=>70
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.filter-Button'
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224171489972909940)
,p_event_id=>wwv_flow_api.id(224171252808909938)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Get the column the filter is going to be applied to',
'var button = $(this.triggeringElement);',
'var re = /column-(\D{4,6})/;',
'',
'// Get the value of the column',
'var column = button.attr(''class'').match(re)[1].toUpperCase();',
'',
'console.log(''column: '' + column);',
'// Set the values of the filter and column',
'apex.item("P212_FILTER").setValue(this.triggeringElement.innerText);',
'apex.item("P212_COLUMN").setValue(column);   ',
'',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224171385533909939)
,p_event_id=>wwv_flow_api.id(224171252808909938)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'-- Apply the selected filter to the IR',
'DECLARE',
'    v_region_id    NUMBER;',
'    v_report_id    NUMBER;',
'BEGIN',
'    -- Get the region id given the static id',
'    SELECT region_id',
'    INTO v_region_id',
'    FROM apex_application_page_regions',
'    WHERE static_id = ''P212_PRCL_LIST'';',
'    ',
'    APEX_IR.RESET_REPORT(',
'        p_page_id    => :APP_PAGE_ID,',
'        p_region_id  => v_region_id,',
'        p_report_id  => NULL);',
'        ',
'    IF :P212_COLUMN <> ''RESET'' THEN',
'         -- Add the filter if one is specified',
'        APEX_IR.ADD_FILTER(',
'            p_page_id    => :APP_PAGE_ID,',
'            p_region_id  => v_region_id,',
'            p_report_column => trim(:P212_COLUMN),',
'            p_filter_value => :P212_FILTER,',
'            p_operator_abbr => ''C'',',
'            p_report_id => NULL);',
'    END IF;',
'END;'))
,p_attribute_02=>'P212_FILTER,P212_COLUMN'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224170245243909928)
,p_event_id=>wwv_flow_api.id(224171252808909938)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(62860183603742442)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224172133472909947)
,p_name=>'Update Filter Buttons'
,p_event_sequence=>90
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_api.id(62860183603742442)
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterrefresh'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224171929496909945)
,p_event_id=>wwv_flow_api.id(224172133472909947)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'updateFilterButtons();'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224568491436521905)
,p_name=>'Count Past Due'
,p_event_sequence=>100
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_api.id(62860183603742442)
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterrefresh'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224568519413521906)
,p_event_id=>wwv_flow_api.id(224568491436521905)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var pastDue = $("td:contains(''Past Due'')");',
'var count = -1;',
'pastDue.each(function(e, val){',
'    count++;',
'})',
'apex.item("P212_PAST_DUE").setValue(count);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225227837519989902)
,p_name=>'Area Submit to Session'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P212_AREA_ID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225227909531989903)
,p_event_id=>wwv_flow_api.id(225227837519989902)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P212_AREA_ID'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225925312083532812)
,p_name=>'Non Res Checkbox'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P212_SHOW_NON_RES'
,p_triggering_condition_type=>'IN_LIST'
,p_triggering_expression=>'Y'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225925632644532815)
,p_event_id=>wwv_flow_api.id(225925312083532812)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225821575095130306)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225925516010532814)
,p_event_id=>wwv_flow_api.id(225925312083532812)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225821575095130306)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225926355129532822)
,p_event_id=>wwv_flow_api.id(225925312083532812)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225821645270130307)
,p_stop_execution_on_error=>'Y'
,p_da_action_comment=>'The refresh is necessary because the column title row does is not visible otherwise.'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225926508739532824)
,p_name=>'Collapsible region clicked'
,p_event_sequence=>130
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#P212_AREA_DESK_CONT .a-Collapsible-icon'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'apex.item("P212_SHOW_NON_RES").getValue()[0] === ''Y'''
,p_bind_type=>'bind'
,p_bind_event_type=>'custom'
,p_bind_event_type_custom=>'click'
,p_da_event_comment=>'In the case that the non-resident list is toggled when this region is collapsed the column title row of the IR does not show. Refreshing the region fixes the issue.'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225926680880532825)
,p_event_id=>wwv_flow_api.id(225926508739532824)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'console.log(''resident region toggled'');'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225926738623532826)
,p_event_id=>wwv_flow_api.id(225926508739532824)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(62860183603742442)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226169520726305414)
,p_name=>'View Parcel'
,p_event_sequence=>140
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.view-prcl'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'#P212_PRCL_LIST'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226169752620305416)
,p_event_id=>wwv_flow_api.id(226169520726305414)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P212_PRCL_ID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var prclId = this.triggeringElement.getAttribute(''data-prcl-id'');',
'apex.item("P212_PRCL_ID").setValue(prclId);',
'',
'console.log(''viewing'', prclId);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226170855586305427)
,p_event_id=>wwv_flow_api.id(226169520726305414)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'VIEW'
,p_attribute_02=>'Y'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(226169832802305417)
,p_name=>'View Parcel History'
,p_event_sequence=>150
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.view-hist'
,p_bind_type=>'live'
,p_bind_delegate_to_selector=>'#P212_PRCL_LIST'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226170037059305419)
,p_event_id=>wwv_flow_api.id(226169832802305417)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P212_PRCL_ID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var prclId = this.triggeringElement.getAttribute(''data-prcl-id'');',
'apex.item("P212_PRCL_ID").setValue(prclId);',
'',
'console.log(''history'', prclId);'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(226170935683305428)
,p_event_id=>wwv_flow_api.id(226169832802305417)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'HISTORY'
,p_attribute_02=>'Y'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(65012265121038501)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Area'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'--:P212_AREA := ''8'';',
'NULL;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(225171814660118749)
,p_process_sequence=>10
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Log'
,p_process_sql_clob=>'apps.logger.debug(''Request:'' || :REQUEST);'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
