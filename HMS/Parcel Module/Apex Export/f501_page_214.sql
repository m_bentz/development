set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:19 Wednesday July 18, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00214
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>214);
end;
/
prompt --application/pages/page_00214
begin
wwv_flow_api.create_page(
 p_id=>214
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: 214 (Modal)'
,p_page_mode=>'MODAL'
,p_step_title=>'PRCL: 214'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_javascript_code_onload=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'',
''))
,p_step_template=>wwv_flow_api.id(36870268071773076)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180606134744'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(64104666910999115)
,p_plug_name=>'Dropdown Select'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'Y'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(64361041608950325)
,p_plug_name=>'Button Region'
,p_region_template_options=>'#DEFAULT#:t-ButtonRegion--slimPadding:t-ButtonRegion--noUI'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225356969485569846)
,p_plug_name=>'Received Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225357080136569847)
,p_plug_name=>'Returning Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(64360885805950323)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(64361041608950325)
,p_button_name=>'P214_SUBMIT'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974578276773159)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-check'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64312747900292555)
,p_name=>'P214_SELECT'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(64104666910999115)
,p_prompt=>'&P214_MSG.'
,p_display_as=>'PLUGIN_BE.CTB.SELECT2'
,p_lov=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    query_string     varchar2(2000);',
'BEGIN',
'    query_string := pc_track_action_pkg.build_query(:P214_ACTION_ID, :P214_COND_ID, :P214_COND_ID2 );',
'    :P214_QUERY := query_string;',
'    return query_string;',
'END;',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'&P214_HELP_TEXT.'
,p_lov_null_value=>'&P214_NO_RESULT_MSG.'
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_attribute_01=>'SINGLE'
,p_attribute_08=>'MW'
,p_attribute_13=>'&P214_NO_RESULT_MSG.'
,p_attribute_14=>'Y'
,p_attribute_15=>'5'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64363086279950345)
,p_name=>'P214_MSG'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(64104666910999115)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(64512054282944736)
,p_name=>'P214_REQ_TYPE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225357080136569847)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65540311893992511)
,p_name=>'P214_ACTION_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225356969485569846)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(65540463349992512)
,p_name=>'P214_COND_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225356969485569846)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(222468794709330639)
,p_name=>'P214_QUERY'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(64104666910999115)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224039924048352831)
,p_name=>'P214_HELP_TEXT'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(64104666910999115)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224572412880521945)
,p_name=>'P214_NO_RESULT_MSG'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(64104666910999115)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225541660820766613)
,p_name=>'P214_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225357080136569847)
,p_source=>':APP_PAGE_ID'
,p_source_type=>'FUNCTION'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(226168599521305404)
,p_name=>'P214_COND_ID2'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(225356969485569846)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(64361373682950328)
,p_name=>'Submit Modal'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(64360885805950323)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(64361403276950329)
,p_event_id=>wwv_flow_api.id(64361373682950328)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'//var semeseter = this.triggeringElement.getAttribute(''data-semester'');',
'console.log(this.triggeringElement);',
'console.log("Page 214: after dialog close")',
'var item = apex.item("P214_SELECT").getValue();',
'console.log(''item'', item);',
'if (item) {',
'    apex.navigation.dialog.close(true,["P214_ID","P214_SELECT","P214_REQ_TYPE"]);   ',
'} else {',
'    alert("Field cannot be empty");',
'}',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(65171485680713742)
,p_name=>'Option Selected'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P214_SELECT'
,p_bind_type=>'bind'
,p_bind_event_type=>'PLUGIN_BE.CTB.SELECT2|ITEM TYPE|slctselect'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224168590237909911)
,p_event_id=>wwv_flow_api.id(65171485680713742)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'$(''#select2-P214_SELECT-container'').removeAttr(''title'');',
'$(this).trigger(''change'');'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(65171580794713743)
,p_event_id=>wwv_flow_api.id(65171485680713742)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_api.id(64360885805950323)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(64362718519950342)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Load Action'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_track_action     pc_track_action%ROWTYPE;',
'BEGIN',
'    v_track_action := pc_track_action_cru.get_pc_track_action(:P214_ACTION_ID);',
'    ',
'    :P214_MSG := v_track_action.msg;',
'    :P214_HELP_TEXT := v_track_action.help_text;',
'    :P214_NO_RESULT_MSG := v_track_action.no_result_msg;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
