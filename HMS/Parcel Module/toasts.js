
/*
 | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert
 | @param duration  : (Optional) The amount of time the toast remains on the page in ms.
 |                     The default time is 3s.
*/
function createToastAlert(PAGE_ITEM, duration) {
    // Check to see if a duration was given
    duration = (typeof duration === 'undefined') ? 3000 : duration;
    
    // Remove any previous toasts
    $('.alert-toast').remove();
    
    // Prepend the toast
    $('#t_Body_content').prepend(PAGE_ITEM.getValue());
    
    // Get the toast that was just added 
    let alertToastBody= $('#t_Body_content').children(".alert-toast:first");
    let alertToastContent = alertToastBody.children(".t-Alert:first");
    
    // Fade in the alert
    alertToastContent.fadeTo(1, 1000);
    
    // Toasts are always automatically dismissed
    dismissAlert(alertToastBody, duration);
}

/*
 | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert
 | @param duration  : (Optional) The amount of time the message remains on the page.
 |                     By entering a duration the message auto dismisses like a toast.
 |                     Otherwise it needs to be manually closed by clicking the X.
*/
function createMsgAlert(PAGE_ITEM, duration) {
    // Check to see if a duration was given
    duration = (typeof duration === 'undefined') ? 0 : duration;
    
    // Remove any previous messages
    $('.alert-msg').remove();
    
    // Prepend the message
    $('#t_Body_content').prepend(PAGE_ITEM.getValue());
    
    // Get the message that was just added 
    let alertMessageBody= $('#t_Body_content').children(".alert-msg:first");
    let alertMessageContent = alertMessageBody.children(".t-Alert:first");
    
    // Fade in the alert
    alertMessageContent.fadeTo(1, 1000);
    
    if (duration !== 0) {
        dismissAlert(alertMessageBody, duration);
    }
}

// Dimisses the alert after the designated amount of time
function dismissAlert(alertBody, delay){
    // Animate the alert content and then remove the body
    let alertContent =  alertBody.children(".t-Alert:first");
    window.setTimeout(function(e) {
        alertContent.slideUp('slow', function(e) {
           alertBody.remove();
       });
    }, delay);
}
