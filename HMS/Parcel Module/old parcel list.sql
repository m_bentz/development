WITH resident_parcel AS (
    SELECT
        --prcl.*,
        prcl.id AS prcl_id,
        prcl.receive_dt,
        prcl.track_num,
        area.title AS area_title,
        stat.title AS status_title,
        area.id AS area_id,
        initcap(prsn.first_name || ' ' || prsn.last_name) AS recipient,
        prsn.ufid AS prsn_ufid,
        pc_parcel_pkg.is_suspicious(prcl.id) AS suspicious_ind,
        pc_parcel_pkg.is_past_due(prcl.id, prcl_list.area_id) AS past_due_ind,
        pc_parcel_pkg.is_location_valid(prcl.id, area.id) AS location_valid_ind,
        pc_parcel_pkg.get_status(prcl.id) AS prcl_status
    FROM pc_parcel prcl
    INNER JOIN pc_parcel_list prcl_list
        ON prcl.prcl_list_id = prcl_list.id
    INNER JOIN area
        ON prcl_list.area_id = area.id
    LEFT OUTER JOIN person prsn
        ON prcl.prsn_id = prsn.id
    INNER JOIN pc_track_stat_type stat
        ON prcl.trk_st_t_id = stat.id
    WHERE prsn_id IS NOT NULL
    AND nvl(delete_ind, 'N') = 'N'
)
SELECT 
    area_title AS Area,
    CASE
        WHEN suspicious_ind = 'Y' THEN 'Suspicious'
        WHEN past_due_ind = 'Y' THEN 'Past Due ' --| '<a href="' || apex_util.prepare_url('f?p=&APP_ID.:217:&APP_SESSION.::NO:217:P217_PRCL_ID:' || prcl.id) || '">' || '<span aria-hidden="true" class="fa fa-paper-plane"></span>' || '</a>' 
        WHEN location_valid_ind = 'N' THEN 'Needs to be redirected'
        WHEN location_valid_ind = 'Y' THEN
            '- '--decode(prcl_status, 'Waiting', 'Awaiting pickup', 'Forwarded', 'In transit', '-')
        ELSE  '-'
    END AS Info,
    track_num AS "Tracking #",
    prsn_ufid AS UFID,
    recipient,
    status_title AS Status,
    prcl.receive_dt AS "Received On",
    '<div class="button-Container">' ||                                    --f?p=App    :Pg :Session    :R:Debug:ClearCache:itemNames:itemValues:PrinterFriendly
    --'<a href="' || apex_util.prepare_url('f?p=&APP_ID.:213:&APP_SESSION.::NO:213:P213_PRCL_ID:' || prcl_id) || '">' || '<span aria-hidden="true" class="fa fa-eye fa-2x"></span>' || '</a>' || 
    --'<a href="' || apex_util.prepare_url('f?p=&APP_ID.:215:&APP_SESSION.::NO:215:P215_PRCL_ID:' || prcl_id) || '">' || '<span aria-hidden="true" class="fa fa-history fa-2x"></span></a>' ||
    '<button type="button" data-prcl-id="'|| prcl_id ||'"' ||
       'class="view-prcl t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-archive"></span>Manage Parcel</button>' ||
    '<button type="button" data-prcl-id="'|| prcl_id ||'"' ||
       'class="view-hist t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true" class="t-Icon t-Icon--left fa fa-history"></span>View History</button>' ||
    '</div>' AS "Actions"
FROM resident_parcel prcl
WHERE area_id  = 
    CASE
        WHEN :P212_AREA_ID = 0 THEN area_id
        WHEN :P212_AREA_ID <> 0 THEN to_number(:P212_AREA_ID)
    END
ORDER BY decode (Info, 'Past Due', 1, 'Suspicious', 2, 'Needs to be redirected', 3 , 'In transit', 4, 5), decode(status_title, 'Returning To Sender', 6, 'Delivered', 7, 5), prcl.receive_dt desc;
