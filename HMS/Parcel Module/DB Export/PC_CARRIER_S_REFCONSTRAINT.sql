--------------------------------------------------------
--  Ref Constraints for Table PC_CARRIER$S
--------------------------------------------------------

  ALTER TABLE "PC_CARRIER$S" ADD CONSTRAINT "PC_CARRIER$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_CARRIER" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
