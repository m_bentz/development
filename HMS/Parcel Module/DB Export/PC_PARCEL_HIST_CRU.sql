--------------------------------------------------------
--  DDL for Package PC_PARCEL_HIST_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_HIST_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_parcel_hist
  PROCEDURE ins_pc_parcel_hist (p_rec IN OUT pc_parcel_hist%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_parcel_hist
  PROCEDURE upd_pc_parcel_hist(p_rec IN OUT pc_parcel_hist%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_parcel_hist
  FUNCTION get_pc_parcel_hist(p_id IN NUMBER) RETURN pc_parcel_hist%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_parcel_hist
  PROCEDURE sav_pc_parcel_hist(p_rec IN OUT pc_parcel_hist%ROWTYPE);
END;

/
