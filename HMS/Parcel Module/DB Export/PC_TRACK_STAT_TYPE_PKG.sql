--------------------------------------------------------
--  DDL for Package PC_TRACK_STAT_TYPE_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_TRACK_STAT_TYPE_PKG" AS 
  
  c_new CONSTANT NUMBER := 1;
  c_waiting CONSTANT NUMBER := 2;
  c_forwarded CONSTANT NUMBER := 3;
  c_delivered CONSTANT NUMBER := 4;
  c_returning_to_sender CONSTANT NUMBER := 5;
  
  FUNCTION get_new RETURN NUMBER;
  FUNCTION get_waiting RETURN NUMBER;
  FUNCTION get_forwarded RETURN NUMBER;
  FUNCTION get_delivered RETURN NUMBER;
  FUNCTION get_returning_to_sender RETURN NUMBER;

END PC_TRACK_STAT_TYPE_PKG;

/
