DECLARE
  CURSOR areas IS
    SELECT title, id
    FROM (
        SELECT *
        FROM area
        WHERE hsng_t_id IN 1
        AND id <> 61
        ORDER BY title);
  v_prcl_list   pc_parcel_list%ROWTYPE;
  v_count       NUMBER := 1;
BEGIN
  FOR area IN areas LOOP
    v_prcl_list.id := v_count;
    v_prcl_list.area_id := area.id;
    pc_parcel_list_cru.sav_pc_parcel_list(v_prcl_list);
    
    v_count := v_count + 1;
--    dbms_output.put_line('area: ' || area.title || ' ' || area.id);
--    dbms_output.put_line(pc_parcel_list_pkg.get_list_by_area(8).id);
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('error occurred creating area desk parcel lists');
END;
/

INSERT INTO pc_parcel(id, prcl_list_id, trk_st_t_id, delete_ind)
VALUES(1,1,1,'Y');