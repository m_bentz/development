--------------------------------------------------------
--  Constraints for Table PC_PARCEL_HIST$S
--------------------------------------------------------

  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("UPDT_SYS" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("UPDT_TS" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("UPDT_BY" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("CREATE_TS" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("CREATE_BY" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" ADD PRIMARY KEY ("S_ID") ENABLE;
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("DESCR_M_IND" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("HEADLINE_M_IND" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("END_DT_M_IND" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("START_DT_M_IND" NOT NULL ENABLE);
  ALTER TABLE "PC_PARCEL_HIST$S" MODIFY ("PRCL_ID_M_IND" NOT NULL ENABLE);
