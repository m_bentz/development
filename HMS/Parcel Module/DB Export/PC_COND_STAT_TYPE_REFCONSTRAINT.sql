--------------------------------------------------------
--  Ref Constraints for Table PC_COND_STAT_TYPE
--------------------------------------------------------

  ALTER TABLE "PC_COND_STAT_TYPE" ADD CONSTRAINT "PC_COND_STAT_CREATE_S_FK00" FOREIGN KEY ("CREATE_S_ID")
	  REFERENCES "PC_COND_STAT_TYPE$S" ("S_ID") DISABLE;
  ALTER TABLE "PC_COND_STAT_TYPE" ADD CONSTRAINT "PC_COND_STAT_CURR_S_FK00" FOREIGN KEY ("CURR_S_ID")
	  REFERENCES "PC_COND_STAT_TYPE$S" ("S_ID") DISABLE;
