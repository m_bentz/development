set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:05 Wednesday August 1, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00217
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>217);
end;
/
prompt --application/pages/page_00217
begin
wwv_flow_api.create_page(
 p_id=>217
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Past Due'
,p_page_mode=>'NORMAL'
,p_step_title=>'Return past due package'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_javascript_code=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'/*',
' | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert',
' | @param duration  : (Optional) The amount of time the toast remains on the page in ms.',
' |                     The default time is 3s.',
'*/',
'function createToastAlert(PAGE_ITEM, duration) {',
'    // Check to see if a duration was given',
'    duration = (typeof duration === ''undefined'') ? 3000 : duration;',
'    ',
'    // Remove any previous toasts',
'    $(''.alert-toast'').remove();',
'    ',
'    // Prepend the toast',
'    $(''#t_Body_content'').prepend(PAGE_ITEM.getValue());',
'    ',
'    // Get the toast that was just added ',
'    let alertToastBody= $(''#t_Body_content'').children(".alert-toast:first");',
'    let alertToastContent = alertToastBody.children(".t-Alert:first");',
'    ',
'    // Fade in the alert',
'    alertToastContent.fadeTo(1, 1000);',
'    ',
'    // Toasts are always automatically dismissed',
'    dismissAlert(alertToastBody, duration);',
'    ',
'    PAGE_ITEM.setValue();',
'}',
'',
'/*',
' | @param PAGE_ITEM : The APEX ITEM that contains the html for the alert',
' | @param duration  : (Optional) The amount of time the message remains on the page.',
' |                     By entering a duration the message auto dismisses like a toast.',
' |                     Otherwise it needs to be manually closed by clicking the X.',
'*/',
'function createMsgAlert(PAGE_ITEM, duration) {',
'    // Check to see if a duration was given',
'    duration = (typeof duration === ''undefined'') ? 0 : duration;',
'    ',
'    // Remove any previous messages',
'    $(''.alert-msg'').remove();',
'    ',
'    // Prepend the message',
'    $(''#t_Body_content'').prepend(PAGE_ITEM.getValue());',
'    ',
'    // Get the message that was just added ',
'    let alertMessageBody= $(''#t_Body_content'').children(".alert-msg:first");',
'    let alertMessageContent = alertMessageBody.children(".t-Alert:first");',
'    ',
'    // Fade in the alert',
'    alertMessageContent.fadeTo(1, 1000);',
'    ',
'    if (duration !== 0) {',
'        dismissAlert(alertMessageBody, duration);',
'    }',
'}',
'',
'// Dimisses the alert after the designated amount of time',
'function dismissAlert(alertBody, delay){',
'    // Animate the alert content and then remove the body',
'    let alertContent =  alertBody.children(".t-Alert:first");',
'    window.setTimeout(function(e) {',
'        alertContent.slideUp(''slow'', function(e) {',
'           alertBody.remove();',
'       });',
'    }, delay);',
'}',
'',
'// On-click listener for alert messages',
'$(''.t-Body-content'').on(''click'', ''.alert-msg .t-Button'',  function(e) {',
'   console.log(''button clicked''); ',
'    ',
'    let element = $(''.t-Button'').closest(''.t-Alert'');',
'    element.slideUp(''slow'', function(e) {',
'        element.closest(''.t-Body-alert'').remove();',
'    });',
'});',
'',
'function buildURL(apexItemBaseURL, targetPage, targetPageItems, pageItemValues) {',
'    targetPageItems = (targetPageItems) ? targetPageItems.join(",") : "";',
'    pageItemValues = (pageItemValues) ? pageItemValues.join(",") : "";',
'    ',
'    ',
'    console.log(''target '', targetPageItems);',
'    console.log(''pass '', pageItemValues);',
'    ',
'    var l_url = "f?p="+$v("pFlowId")+":"+targetPage+":"+$v("pInstance")+"::NO::"+targetPageItems+":"+pageItemValues+"&"+apexItemBaseURL;',
'    console.log(l_url);',
'    return l_url;',
'}',
'',
'function openModal(l_url, options) {',
'    apex.navigation.dialog(l_url, {',
'        title: options.title,',
'        height: options.height,',
'        width: options.width,',
'        maxWidth: options.maxWidth,',
'        model: true,',
'        dialog: null},',
'        ''t-Dialog--standard'',',
'        $(this));',
'}',
''))
,p_inline_css=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'.t-Body-contentInner > .container {',
'    max-width: 1440px;',
'}',
'',
'.t-Body-contentInner {',
'    padding-top: 40px;',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180801080551'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225167739956118708)
,p_plug_name=>'Buttons'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225228520028989909)
,p_plug_name=>'Main Content'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(225168209594118713)
,p_name=>'Past Due Parcels'
,p_parent_plug_id=>wwv_flow_api.id(225228520028989909)
,p_template=>wwv_flow_api.id(36909130176773106)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--scrollBody:t-Form--noPadding:t-Form--large:t-Form--leftLabels'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT',
'    apex_item.checkbox2(',
'        p_idx => 1,',
'        p_value => prcl.id, --prcl.track_num,',
'        p_attributes => ''id="''|| prcl.id ||''" class="prcl"'',',
'        p_checked_values => null,',
'        p_checked_values_delimiter => '','') AS Selected,',
'    track_num, ',
'    prsn.ufid AS "UFID",',
'    initcap(prsn.first_name || '' '' || prsn.last_name) AS Recipient,',
'    (SELECT title || '' '' || pc_parcel_pkg.get_age(id, sysdate) || '' days''',
'     FROM pc_track_stat_type stat',
'     WHERE stat.id = prcl.trk_st_t_id) AS Status,',
'    prcl.receive_dt AS "Received On"',
'FROM pc_parcel prcl',
'    INNER JOIN pc_parcel_list prcl_list',
'    ON prcl.prcl_list_id = prcl_list.id',
'    LEFT OUTER JOIN person prsn',
'    ON prcl.prsn_id = prsn.id',
'WHERE prcl_list.area_id = :P217_AREA_ID',
'AND delete_ind = ''N''',
'AND pc_parcel_pkg.is_past_due(prcl.id, prcl_list.area_id) = ''Y''',
'',
'--ORDER BY decode (Info, ''Needs to be redirected'', 1 , ''Waiting to be received'', 2, 3), decode(Status, ''Returning To Sender'', 4, ''Delivered'', 5, 3), prcl.receive_dt desc;',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36946831572773134)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_no_data_found=>'There are no past due parcels.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355416020569831)
,p_query_column_id=>1
,p_column_alias=>'SELECTED'
,p_column_display_sequence=>1
,p_column_heading=>'Selected'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355594688569832)
,p_query_column_id=>2
,p_column_alias=>'TRACK_NUM'
,p_column_display_sequence=>2
,p_column_heading=>'Track num'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355698940569833)
,p_query_column_id=>3
,p_column_alias=>'UFID'
,p_column_display_sequence=>3
,p_column_heading=>'Ufid'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355780563569834)
,p_query_column_id=>4
,p_column_alias=>'RECIPIENT'
,p_column_display_sequence=>4
,p_column_heading=>'Recipient'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355860640569835)
,p_query_column_id=>5
,p_column_alias=>'STATUS'
,p_column_display_sequence=>5
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225355963350569836)
,p_query_column_id=>6
,p_column_alias=>'Received On'
,p_column_display_sequence=>6
,p_column_heading=>'Received on'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225188874380047529)
,p_plug_name=>'Button Region'
,p_parent_plug_id=>wwv_flow_api.id(225228520028989909)
,p_region_template_options=>'#DEFAULT#:t-ButtonRegion--noUI'
,p_plug_template=>wwv_flow_api.id(36886227514773091)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225228693831989910)
,p_plug_name=>'Side Content'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_column=>11
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(225229980596989923)
,p_name=>'Returning Packages'
,p_parent_plug_id=>wwv_flow_api.id(225228693831989910)
,p_template=>wwv_flow_api.id(36922375712773118)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_component_template_options=>'#DEFAULT#:t-Cards--compact:t-Cards--float'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT prsn.first_name || '' '' || prsn.last_name AS CARD_TITLE, track_num AS CARD_SUBTEXT',
'FROM pc_parcel prcl',
'    INNER JOIN pc_parcel_list prcl_list',
'    ON prcl.prcl_list_id = prcl_list.id',
'        INNER JOIN person prsn',
'        ON prcl.prsn_id = prsn.id',
'WHERE prcl_list.area_id = :P217_AREA_ID',
'AND INSTR(:P217_PRCL_LIST, track_num) > 0',
'    '))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36937050985773127)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225230522376989929)
,p_query_column_id=>1
,p_column_alias=>'CARD_TITLE'
,p_column_display_sequence=>2
,p_column_heading=>'Card title'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(225230473941989928)
,p_query_column_id=>2
,p_column_alias=>'CARD_SUBTEXT'
,p_column_display_sequence=>1
,p_column_heading=>'Card subtext'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225267120817920826)
,p_plug_name=>'Returning'
,p_parent_plug_id=>wwv_flow_api.id(225228693831989910)
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useRegionTitle'
,p_plug_template=>wwv_flow_api.id(36931374122773123)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542532584766622)
,p_plug_name=>'DB Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(225542658100766623)
,p_plug_name=>'UI/Nav Page Items'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225169369833118724)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(225167739956118708)
,p_button_name=>'Add'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--primary'
,p_button_template_id=>wwv_flow_api.id(36974578276773159)
,p_button_image_alt=>'Add'
,p_button_position=>'BODY'
,p_icon_css_classes=>'fa-plus'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(225167945618118710)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(225188874380047529)
,p_button_name=>'Return'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#:t-Button--large:t-Button--primary:t-Button--iconRight'
,p_button_template_id=>wwv_flow_api.id(36974781421773160)
,p_button_image_alt=>'Return to Sender'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_icon_css_classes=>'fa-send'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225167606092118707)
,p_name=>'P217_PRCL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225542532584766622)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225167843541118709)
,p_name=>'P217_TRACK_NUM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225167739956118708)
,p_prompt=>'Track num'
,p_placeholder=>'Tracking #'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_colspan=>10
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225171542985118746)
,p_name=>'P217_AREA_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(225542532584766622)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225229192221989915)
,p_name=>'P217_PRCL_LIST'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542532584766622)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225230929118989933)
,p_name=>'P217_URL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(225542658100766623)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225354611711569823)
,p_name=>'P217_ALERT'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(225542658100766623)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(225356073454569837)
,p_name=>'P217_DESK_AREA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(225542532584766622)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(225353571517569812)
,p_validation_name=>'Parcel list not null'
,p_validation_sequence=>10
,p_validation=>'P217_PRCL_LIST'
,p_validation_type=>'ITEM_NOT_NULL_OR_ZERO'
,p_error_message=>'Please select a package(s) to return'
,p_always_execute=>'N'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225169505218118726)
,p_name=>'Add package to return list'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(225169369833118724)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225169769847118728)
,p_event_id=>wwv_flow_api.id(225169505218118726)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_prcl    pc_parcel%ROWTYPE;',
'BEGIN',
'    v_prcl := pc_parcel_cru.get_pc_parcel(:P217_PRCL_ID);',
'    ',
'    apps.logger.debug(''source: '' || v_prcl.track_num || ''entered: '' || :P217_TRACK_NUM); ',
'    IF v_prcl.track_num = :P217_TRACK_NUM then',
'        -- Add the package to a collection ',
'        apex_collection.add_member(',
'            p_collection_name => ''PAST_DUE'',',
'            p_n001 => :P217_PRCL_ID,',
'            p_c001 => ''Y''   -- Indicator to determine if to return',
'        );',
'        --RETURN TRUE;',
'        apps.logger.debug(''equivalent'');',
'    ELSE',
'        apps.logger.debug(''NOT equivalent'');',
'    END IF;',
'    --RETURN FALSE;',
'END;'))
,p_attribute_02=>'P217_TRACK_NUM'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225169642492118727)
,p_event_id=>wwv_flow_api.id(225169505218118726)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'console.log(''tracking number is equivalent'');'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225170578568118736)
,p_name=>'Focus Input'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225170620012118737)
,p_event_id=>wwv_flow_api.id(225170578568118736)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P217_TRACK_NUM'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225229299875989916)
,p_name=>'Checkbox selected'
,p_event_sequence=>30
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.prcl'
,p_bind_type=>'live'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225230831421989932)
,p_event_id=>wwv_flow_api.id(225229299875989916)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_app              NUMBER := v(''APP_ID'');',
'    v_session          NUMBER := v(''APP_SESSION'');',
'BEGIN',
'    :P217_URL := apex_nav_pkg.get_dialog_url(218,v_app,v_session);',
'END;'))
,p_attribute_03=>'P217_URL'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225229357101989917)
,p_event_id=>wwv_flow_api.id(225229299875989916)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var element = $(this.triggeringElement);',
'var isChecked = $(this)[0].triggeringElement.checked;',
'',
'if (isChecked) {',
'    console.log(''not checked'');',
'    var prclID = element.val();',
'    var targetPageItems = ["P218_PRCL_ID"];',
'    var targetPageValues = [prclID];',
'    openModal(buildURL(apex.item("P217_URL").getValue(), 218, targetPageItems, targetPageValues), {title: ''Verify tracking #'', width: 600, height: 200}); ',
'    this.browserEvent.preventDefault();',
'} else {',
'    console.log(this.triggeringElement);',
'    console.log(element);',
'     var',
'      //DOM object for APEX Item that holds list.',
'      apexItemIDList = apex.item("P217_PRCL_LIST"),',
'      //Convert comma list into an array or blank array',
'      //Note: Not sure about the "?" syntax see: http://www.talkapex.com/2009/07/javascript-if-else.html',
'      ids = apexItemIDList.getValue().length === 0 ? [] : apexItemIDList.getValue().split('','');',
'      //Index of current ID. If it''s not in array, value will be -1',
'      idIndex = ids.indexOf(element.val())',
'    ;',
'',
'    // Remove',
'    ids.splice(idIndex, 1);',
'    //Convert array back to comma delimited list',
'    apexItemIDList.setValue(ids.join('',''));',
'}',
'',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225230692043989930)
,p_name=>'Update Page'
,p_event_sequence=>40
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'window'
,p_bind_type=>'bind'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225230737997989931)
,p_event_id=>wwv_flow_api.id(225230692043989930)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P217_PRCL_ID'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'// Get the data and request type from the modal',
'var response = this.data.P218_RESPONSE;',
'var prclID = this.data.P218_PRCL_ID;',
'',
'console.log(this.data);',
'console.log(''218 Dialog closed'');',
'console.log(response);',
'',
'if (response === ''Y'') {',
'    apex.item("P217_PRCL_ID").setValue(prclID);',
'    var $checkBox = $("#" + prclID);',
'    $checkBox.prop(''checked'', true);',
'    //$($checkBox).prop(''checked'', true);',
'    ',
'    var',
'      //DOM object for APEX Item that holds list.',
'      apexItemIDList = apex.item("P217_PRCL_LIST"),',
'      //Convert comma list into an array or blank array',
'      //Note: Not sure about the "?" syntax see: http://www.talkapex.com/2009/07/javascript-if-else.html',
'      ids = apexItemIDList.getValue().length === 0 ? [] : apexItemIDList.getValue().split('','');',
'      //Index of current ID. If it''s not in array, value will be -1',
'      idIndex = ids.indexOf($checkBox.val())',
'    ;',
'',
'    ids.push($checkBox.val());',
'    ',
'    //Convert array back to comma delimited list',
'    apexItemIDList.setValue(ids.join('',''));',
'} else {',
'    return;',
'}'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225230216224989926)
,p_event_id=>wwv_flow_api.id(225230692043989930)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_prcl    pc_parcel%ROWTYPE;',
'BEGIN',
'    v_prcl := pc_parcel_cru.get_pc_parcel(:P217_PRCL_ID);',
'    ',
'    :P217_ALERT := apex_ui_pkg.get_success_toast(v_prcl.track_num || '' was added to the return list.'');',
'END;'))
,p_attribute_02=>'P217_PRCL_LIST,P217_PRCL_ID'
,p_attribute_03=>'P217_ALERT'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225355280739569829)
,p_event_id=>wwv_flow_api.id(225230692043989930)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'createToastAlert(apex.item("P217_ALERT"));'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225354976332569826)
,p_event_id=>wwv_flow_api.id(225230692043989930)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225228520028989909)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225354047496569817)
,p_name=>'Return package(s)'
,p_event_sequence=>50
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(225167945618118710)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
,p_display_when_type=>'ITEM_IS_NULL_OR_ZERO'
,p_display_when_cond=>'P217_PRCL_LIST'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225354160611569818)
,p_event_id=>wwv_flow_api.id(225354047496569817)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_table        DBMS_UTILITY.UNCL_ARRAY;',
'    v_tablen       BINARY_INTEGER;    ',
'    v_prcl         pc_parcel%ROWTYPE;',
'    c_returned     NUMBER(10,0) := 5;',
'    c_LOG          VARCHAR2(100) := ''Page 217 Return Package DA '';',
'BEGIN',
'    apps.logger.debug(c_LOG || ''parcel list: '' || :P217_PRCL_LIST);',
'    ',
'    -- Need to append a letter because COMMA_TO_TABLE does not support',
'    -- anything that does not conform to Oracle naming rules (starting with digit)',
'    -- Get all of the id''s of the selected packages',
'    DBMS_UTILITY.COMMA_TO_TABLE(',
'        list =>  ''A'' || replace(:P217_PRCL_LIST, '','' ,'',A''), -- put an A at the start of each entry',
'        tablen => v_tablen,',
'        tab => v_table);',
'    ',
'    apps.logger.debug(c_LOG || ''Table length: '' || v_tablen);',
'    :P217_ALERT := apex_ui_pkg.get_success_toast(v_tablen || '' package(s) were returned.'');',
'    ',
'    -- Loop through the IDs and update the packages',
'    FOR i IN 1..v_tablen',
'    LOOP',
'        v_table(i) := substr(v_table(i),2);-- take the A off each entry',
'        apps.logger.debug(c_LOG || ''Parcel ID: '' || v_table(i));',
'        ',
'        v_prcl := null;',
'        -- Get the parcel',
'        v_prcl := pc_parcel_cru.get_pc_parcel(to_number(v_table(i)));',
'        ',
'        apps.logger.debug(c_LOG || ''settings status of track num: '' || v_prcl.track_num || '' to returned'');',
'        apps.logger.debug(c_LOG || ''prcl list id: '' || v_prcl.prcl_list_id);',
'        ',
'        -- set the status of the parcel to returned to sender',
'        v_prcl.trk_st_t_id := c_returned;',
'        ',
'        -- Update the parcel',
'        apps.logger.debug(c_LOG || ''parcel track id '' || v_prcl.trk_st_t_id);',
'        pc_parcel_cru.sav_pc_parcel(v_prcl);',
'    END LOOP;',
'    ',
'    EXCEPTION',
'        WHEN OTHERS THEN',
'            apps.logger.debug(c_LOG || ''something went wrong'');',
'END;'))
,p_attribute_02=>'P217_PRCL_LIST'
,p_attribute_03=>'P217_ALERT'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225354722104569824)
,p_event_id=>wwv_flow_api.id(225354047496569817)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'createToastAlert(apex.item("P217_ALERT"));'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225354831234569825)
,p_event_id=>wwv_flow_api.id(225354047496569817)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(225168209594118713)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(225488474529339993)
,p_name=>'Update Breadcrumb Label'
,p_event_sequence=>60
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(225488869883339997)
,p_event_id=>wwv_flow_api.id(225488474529339993)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var breadcrumb = $("span.t-Breadcrumb-label");',
'var label = breadcrumb.text();',
'var currentDesk = apex.item("P217_DESK_AREA").getValue();',
'//if (!isViewOnly()) {',
'    breadcrumb.text(label + " > " + currentDesk);',
'//} else {',
'    //breadcrumb',
'//}',
' '))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(225353653039569813)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'New'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'DECLARE',
'    v_table        DBMS_UTILITY.UNCL_ARRAY;',
'    v_tablen       BINARY_INTEGER;    ',
'BEGIN',
'    DBMS_UTILITY.COMMA_TO_TABLE(',
'        list =>  ''A''||replace(:P217_PRCL_LIST,'','','',A''), -- put an A at the start of each entry',
'        tablen => v_tablen,',
'        tab => v_table);',
'    ',
'    FOR i IN 1..v_table.COUNT',
'    LOOP',
'        v_table(i) := substr(v_table(i),2);-- take the A off each entry',
'        apps.logger.debug(v_table(i));',
'    END LOOP;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(225169903487118730)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Create Collection'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'-- Create the collection',
'apex_collection.create_or_truncate_collection(''PAST_DUE'');'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(225356148694569838)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Area'
,p_process_sql_clob=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT title',
'INTO :P217_DESK_AREA',
'FROM area',
'WHERE id = :P217_AREA_ID'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
