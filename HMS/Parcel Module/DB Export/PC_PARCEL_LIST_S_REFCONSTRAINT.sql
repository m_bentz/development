--------------------------------------------------------
--  Ref Constraints for Table PC_PARCEL_LIST$S
--------------------------------------------------------

  ALTER TABLE "PC_PARCEL_LIST$S" ADD CONSTRAINT "PRCL_LIST$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_PARCEL_LIST" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
