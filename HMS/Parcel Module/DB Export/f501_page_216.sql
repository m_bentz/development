set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   08:19 Wednesday July 18, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

prompt --application/pages/delete_00216
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>216);
end;
/
prompt --application/pages/page_00216
begin
wwv_flow_api.create_page(
 p_id=>216
,p_user_interface_id=>wwv_flow_api.id(30859849058148730)
,p_name=>'PRCL: Search'
,p_page_mode=>'MODAL'
,p_step_title=>'Search'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'MICHAELBE'
,p_last_upd_yyyymmddhh24miss=>'20180220150323'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224001378370940907)
,p_plug_name=>'Search container'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(36885065544773090)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224037872999352810)
,p_plug_name=>'Parcel'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Region--noUI:t-Form--noPadding:t-Form--large:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(224001651135940910)
,p_name=>' Parcel Information'
,p_parent_plug_id=>wwv_flow_api.id(224037872999352810)
,p_template=>wwv_flow_api.id(36903682925773102)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#:t-Report--stretch:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT',
'    CASE',
'        WHEN pc_parcel_pkg.is_suspicious(prcl.id) = ''Y'' THEN ''Suspicious''',
'        WHEN pc_parcel_pkg.is_location_valid(prcl.id, area.id) = ''N'' THEN ''Needs to be redirected''',
'        WHEN pc_parcel_pkg.is_location_valid(prcl.id, area.id) = ''Y'' THEN ',
'            CASE',
'                WHEN pc_parcel_pkg.get_status(prcl.id) = ''Waiting'' THEN ''Awaiting pickup''',
'                WHEN pc_parcel_pkg.get_status(prcl.id) = ''Forwarded'' THEN ''In transit''',
'            END ',
'        ELSE  ''NA''',
'    END AS Info,',
'    area.title Area,',
'    --nvl(track_num, ''Test data'') AS "Tracking #",',
'    prsn.ufid AS "UFID",',
'    initcap(prsn.first_name || '' '' || prsn.last_name) AS Recipient,',
'    (SELECT title',
'     FROM pc_track_stat_type stat',
'     WHERE stat.id = prcl.trk_st_t_id) AS Status,',
'    prcl.receive_dt AS "Received On",',
'    ''<a href="'' || apex_util.prepare_url(''f?p=&APP_ID.:213:&APP_SESSION.::NO::P213_PRCL_ID,P213_VIEW_ONLY:'' || prcl.id || '',Y'') || ''">'' || ''<span aria-hidden="true" class="fa fa-eye fa-2x"></span>'' || ''</a>'' AS "View",',
'    ''<a href="'' || apex_util.prepare_url(''f?p=&APP_ID.:215:&APP_SESSION.::NO::P215_PRCL_ID:'' || prcl.id) || ''">'' || ''<span aria-hidden="true" class="fa fa-history fa-2x"></span></a>'' AS "History"',
'FROM pc_parcel prcl',
'    INNER JOIN pc_parcel_list prcl_list',
'    ON prcl.prcl_list_id = prcl_list.id',
'    LEFT OUTER JOIN person prsn',
'    ON prcl.prsn_id = prsn.id',
'    LEFT OUTER JOIN area',
'    ON prcl_list.area_id = area.id',
'WHERE prcl.id = :P216_PRCL_ID',
'--ORDER BY decode (Info, ''Needs to be redirected'', 1 , ''Waiting to be received'', 2, 3), decode(Status, ''Returning To Sender'', 4, ''Delivered'', 5, 3), prcl.receive_dt desc;',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(36946831572773134)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039116927352823)
,p_query_column_id=>1
,p_column_alias=>'INFO'
,p_column_display_sequence=>1
,p_column_heading=>'Info'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039204132352824)
,p_query_column_id=>2
,p_column_alias=>'AREA'
,p_column_display_sequence=>2
,p_column_heading=>'Area'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039393940352825)
,p_query_column_id=>3
,p_column_alias=>'UFID'
,p_column_display_sequence=>3
,p_column_heading=>'Ufid'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039462919352826)
,p_query_column_id=>4
,p_column_alias=>'RECIPIENT'
,p_column_display_sequence=>4
,p_column_heading=>'Recipient'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039585954352827)
,p_query_column_id=>5
,p_column_alias=>'STATUS'
,p_column_display_sequence=>5
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039672734352828)
,p_query_column_id=>6
,p_column_alias=>'Received On'
,p_column_display_sequence=>6
,p_column_heading=>'Received on'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039755796352829)
,p_query_column_id=>7
,p_column_alias=>'View'
,p_column_display_sequence=>7
,p_column_heading=>'View'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(224039882054352830)
,p_query_column_id=>8
,p_column_alias=>'History'
,p_column_display_sequence=>8
,p_column_heading=>'History'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(224037983086352811)
,p_plug_name=>'Whitespace'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody:t-Region--noUI:t-Form--slimPadding:t-Form--xlarge:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(36922375712773118)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224001496281940908)
,p_name=>'P216_SEARCH'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(224001378370940907)
,p_display_as=>'PLUGIN_BE.CTB.SELECT2'
,p_lov=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'SELECT ''<div class="info-container"><p class="disp-info flex_60">''|| prcl.track_num || ''</p>'' || ''<div class="inner-info-container flex_40"><p class="info">'' || ''Area: '' || area.title ||'' '' || ''</p>'' ||''<p class="info">'' || initcap(prsn.first_name ||'
||' '' '' || prsn.last_name) || ''</p></div></div>'' AS INFO, prcl.id',
'FROM pc_parcel prcl',
'INNER JOIN person prsn',
'ON prcl.prsn_id = prsn.id',
'    INNER JOIN pc_parcel_list prcl_list',
'    ON prcl.prcl_list_id = prcl_list.id',
'        FULL OUTER JOIN area',
'        ON prcl_list.area_id = area.id',
'WHERE hsng_t_id IN 1'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Search By Tracking #, Area  AND/OR  Resident '
,p_grid_label_column_span=>0
,p_field_template=>wwv_flow_api.id(36974194919773159)
,p_item_template_options=>'#DEFAULT#:t-Form-fieldContainer--stretchInputs:t-Form-fieldContainer--large'
,p_lov_display_extra=>'YES'
,p_escape_on_http_output=>'N'
,p_attribute_01=>'SINGLE'
,p_attribute_03=>'2'
,p_attribute_07=>'Y'
,p_attribute_08=>'MW'
,p_attribute_14=>'Y'
,p_attribute_15=>'5'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(224001810562940912)
,p_name=>'P216_PRCL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(224001378370940907)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224001906422940913)
,p_name=>'Parcel selection'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P216_SEARCH'
,p_bind_type=>'bind'
,p_bind_event_type=>'PLUGIN_BE.CTB.SELECT2|ITEM TYPE|slctselect'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224002058496940914)
,p_event_id=>wwv_flow_api.id(224001906422940913)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_utilities.join(wwv_flow_t_varchar2(
'var prcl_id = apex.item("P216_SEARCH").getValue();',
'apex.item("P216_PRCL_ID").setValue(prcl_id);'))
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224002122276940915)
,p_event_id=>wwv_flow_api.id(224001906422940913)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P216_PRCL_ID'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224004079440940934)
,p_event_id=>wwv_flow_api.id(224001906422940913)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(224001651135940910)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(224038840054352820)
,p_name=>'Focus Search'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_required_patch=>wwv_flow_api.id(485624926060445456)
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(224038985543352821)
,p_event_id=>wwv_flow_api.id(224038840054352820)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P216_SEARCH'
,p_attribute_01=>'$("#P216_SEARCH").select2("open");'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
