--------------------------------------------------------
--  DDL for Package Body PC_PARCEL_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_PARCEL_CRU" IS

  --------------------------------------------------------------
  -- create procedure for table pc_parcel
  PROCEDURE ins_pc_parcel(p_rec IN OUT pc_parcel%ROWTYPE) IS
    v_id pc_parcel.id%TYPE;
  BEGIN
    INSERT INTO pc_parcel VALUES p_rec RETURNING id INTO p_rec.id;
  END ins_pc_parcel;

  --------------------------------------------------------------
  -- update procedure for table pc_parcel
  PROCEDURE upd_pc_parcel(p_rec IN OUT pc_parcel%ROWTYPE ) IS
    v_curr_ts TIMESTAMP := NULL;
  BEGIN
    -- find out what time stamp is in the db right now
    SELECT updt_ts INTO v_curr_ts FROM pc_parcel WHERE id = p_rec.id;
    IF (p_rec.updt_ts = v_curr_ts) THEN
      UPDATE pc_parcel SET row = p_rec WHERE ID = p_rec.id;
    ELSE
      RAISE_APPLICATION_ERROR(-20001, 'pc_parcel_cru '
        || 'Current version of data in database has changed since user initiated '
        || 'update process. current timestamp = ' || v_curr_ts || ', item timestamp = '
        || p_rec.updt_ts || '.');
    END IF;
  END upd_pc_parcel;

  --------------------------------------------------------------
  -- read function for table pc_parcel
  FUNCTION get_pc_parcel(p_id NUMBER) RETURN pc_parcel%ROWTYPE IS
    v_rec pc_parcel%ROWTYPE;
  BEGIN
    SELECT * INTO v_rec FROM pc_parcel WHERE id = p_id;
    RETURN v_rec;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_pc_parcel;

  -------------------------------------------------------------
  -- save procedure for table pc_parcel
  PROCEDURE sav_pc_parcel(p_rec IN OUT pc_parcel%ROWTYPE ) IS
    v_id pc_parcel.id%TYPE;
    v_exist_rec NUMBER;
  BEGIN
    SELECT id INTO v_exist_rec FROM pc_parcel WHERE id = p_rec.id;
    upd_pc_parcel(p_rec);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ins_pc_parcel(p_rec);
  END sav_pc_parcel;

END;

/
