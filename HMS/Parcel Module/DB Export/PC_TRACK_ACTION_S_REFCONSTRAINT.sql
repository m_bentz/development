--------------------------------------------------------
--  Ref Constraints for Table PC_TRACK_ACTION$S
--------------------------------------------------------

  ALTER TABLE "PC_TRACK_ACTION$S" ADD CONSTRAINT "TRK_ACT$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_TRACK_ACTION" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
