--------------------------------------------------------
--  DDL for Package PC_PARCEL_LIST_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_LIST_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_parcel_list
  PROCEDURE ins_pc_parcel_list (p_rec IN OUT pc_parcel_list%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_parcel_list
  PROCEDURE upd_pc_parcel_list(p_rec IN OUT pc_parcel_list%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_parcel_list
  FUNCTION get_pc_parcel_list(p_id IN NUMBER) RETURN pc_parcel_list%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_parcel_list
  PROCEDURE sav_pc_parcel_list(p_rec IN OUT pc_parcel_list%ROWTYPE);
END;

/
