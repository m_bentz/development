--------------------------------------------------------
--  Ref Constraints for Table PC_PARCEL_HIST$S
--------------------------------------------------------

  ALTER TABLE "PC_PARCEL_HIST$S" ADD CONSTRAINT "PRCL_HIST$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_PARCEL_HIST" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
