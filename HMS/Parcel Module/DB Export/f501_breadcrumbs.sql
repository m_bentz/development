set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050000 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2013.01.01'
,p_release=>'5.0.4.00.12'
,p_default_workspace_id=>1277816132656182
,p_default_application_id=>501
,p_default_owner=>'HMSDATA'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 501 - UA Staff
--
-- Application Export:
--   Application:     501
--   Name:            UA Staff
--   Date and Time:   07:49 Friday August 3, 2018
--   Exported By:     MICHAELBE
--   Flashback:       0
--   Export Type:     Component Export
--   Manifest
--     BREADCRUMB: Parcel Management
--     BREADCRUMB ENTRY: Parcel Management: Process Past Due (217)
--     BREADCRUMB ENTRY: Parcel Management: Area Desk Selection (212)
--     BREADCRUMB ENTRY: Parcel Management: History (215)
--     BREADCRUMB ENTRY: Parcel Management: Parcel Form (213)
--   Manifest End
--   Version:         5.0.4.00.12
--   Instance ID:     63118591303588
--

-- C O M P O N E N T    E X P O R T
begin
  wwv_flow_api.g_mode := 'REPLACE';
end;
/
prompt --application/shared_components/navigation/breadcrumbs/parcel_management
begin
wwv_flow_api.create_menu(
 p_id=>wwv_flow_api.id(65066675613619004)
,p_name=>'Parcel Management'
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65066964790623375)
,p_parent_id=>0
,p_short_name=>'Area Desk Selection'
,p_link=>'f?p=&APP_ID.:212:&SESSION.::&DEBUG.:::'
,p_page_id=>212
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65067103154626291)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_short_name=>'Parcel Form'
,p_link=>'f?p=&APP_ID.:213:&SESSION.::&DEBUG.:::'
,p_page_id=>213
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65845267410174669)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_short_name=>'History'
,p_link=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:::'
,p_page_id=>215
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(225254192380302933)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_short_name=>'Process Past Due'
,p_link=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:::'
,p_page_id=>217
);
end;
/
prompt --application/shared_components/navigation/breadcrumbentry/225254192380302933
begin
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(225254192380302933)
,p_menu_id=>wwv_flow_api.id(65066675613619004)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_option_sequence=>10
,p_short_name=>'Process Past Due'
,p_link=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:::'
,p_page_id=>217
);
end;
/
prompt --application/shared_components/navigation/breadcrumbentry/65066964790623375
begin
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65066964790623375)
,p_menu_id=>wwv_flow_api.id(65066675613619004)
,p_parent_id=>0
,p_option_sequence=>10
,p_short_name=>'Area Desk Selection'
,p_link=>'f?p=&APP_ID.:212:&SESSION.::&DEBUG.:::'
,p_page_id=>212
);
end;
/
prompt --application/shared_components/navigation/breadcrumbentry/65845267410174669
begin
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65845267410174669)
,p_menu_id=>wwv_flow_api.id(65066675613619004)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_option_sequence=>10
,p_short_name=>'History'
,p_link=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:::'
,p_page_id=>215
);
end;
/
prompt --application/shared_components/navigation/breadcrumbentry/65067103154626291
begin
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(65067103154626291)
,p_menu_id=>wwv_flow_api.id(65066675613619004)
,p_parent_id=>wwv_flow_api.id(65066964790623375)
,p_option_sequence=>10
,p_short_name=>'Parcel Form'
,p_link=>'f?p=&APP_ID.:213:&SESSION.::&DEBUG.:::'
,p_page_id=>213
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false), p_is_component_import => true);
commit;
end;
/
set verify on feedback on define on
prompt  ...done
