--------------------------------------------------------
--  DDL for Package PC_PARCEL_HIST_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_HIST_PKG" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  FUNCTION get_latest_action(
    p_prcl_id      pc_parcel.id%TYPE
  )RETURN pc_parcel_hist%ROWTYPE;  

END PC_PARCEL_HIST_PKG;

/
