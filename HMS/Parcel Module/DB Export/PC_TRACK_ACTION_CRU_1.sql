--------------------------------------------------------
--  DDL for Package Body PC_TRACK_ACTION_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_TRACK_ACTION_CRU" IS

  --------------------------------------------------------------
  -- create procedure for table pc_track_action
  PROCEDURE ins_pc_track_action(p_rec IN OUT pc_track_action%ROWTYPE) IS
    v_id pc_track_action.id%TYPE;
  BEGIN
    INSERT INTO pc_track_action VALUES p_rec RETURNING id INTO p_rec.id;
  END ins_pc_track_action;

  --------------------------------------------------------------
  -- update procedure for table pc_track_action
  PROCEDURE upd_pc_track_action(p_rec IN OUT pc_track_action%ROWTYPE ) IS
    v_curr_ts TIMESTAMP := NULL;
  BEGIN
    -- find out what time stamp is in the db right now
    SELECT updt_ts INTO v_curr_ts FROM pc_track_action WHERE id = p_rec.id;
    IF (p_rec.updt_ts = v_curr_ts) THEN
      UPDATE pc_track_action SET row = p_rec WHERE ID = p_rec.id;
    ELSE
      RAISE_APPLICATION_ERROR(-20001, 'pc_track_action_cru '
        || 'Current version of data in database has changed since user initiated '
        || 'update process. current timestamp = ' || v_curr_ts || ', item timestamp = '
        || p_rec.updt_ts || '.');
    END IF;
  END upd_pc_track_action;

  --------------------------------------------------------------
  -- read function for table pc_track_action
  FUNCTION get_pc_track_action(p_id NUMBER) RETURN pc_track_action%ROWTYPE IS
    v_rec pc_track_action%ROWTYPE;
  BEGIN
    SELECT * INTO v_rec FROM pc_track_action WHERE id = p_id;
    RETURN v_rec;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_pc_track_action;

  -------------------------------------------------------------
  -- save procedure for table pc_track_action
  PROCEDURE sav_pc_track_action(p_rec IN OUT pc_track_action%ROWTYPE ) IS
    v_id pc_track_action.id%TYPE;
    v_exist_rec NUMBER;
  BEGIN
    SELECT id INTO v_exist_rec FROM pc_track_action WHERE id = p_rec.id;
    upd_pc_track_action(p_rec);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ins_pc_track_action(p_rec);
  END sav_pc_track_action;

END;

/
