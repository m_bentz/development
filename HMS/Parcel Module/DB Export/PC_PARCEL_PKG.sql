--------------------------------------------------------
--  DDL for Package PC_PARCEL_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_PKG" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
  c_hold_limit CONSTANT NUMBER := 5;
  
  FUNCTION is_location_valid(
    p_id       pc_parcel.id%TYPE,
    p_curr_area  area.id%TYPE
  )RETURN VARCHAR2;  
  
  FUNCTION is_past_due(
    p_id        pc_parcel.id%TYPE,
    p_area_id   pc_parcel_list.area_id%TYPE
  )RETURN VARCHAR2;  
  
  FUNCTION is_suspicious(
    p_id      pc_parcel.id%TYPE
  )RETURN VARCHAR2;
  
  FUNCTION get_status(
    p_id      pc_parcel.id%TYPE
  )RETURN pc_track_stat_type.title%TYPE;  
  
  FUNCTION get_age(
    p_id      pc_parcel.id%TYPE,
    p_dt      DATE
  )RETURN NUMBER; 
  
  FUNCTION get_area(
    p_id      pc_parcel.id%TYPE
  )RETURN area%ROWTYPE;
  
  FUNCTION get_recipient(
    p_id      pc_parcel.id%TYPE
  )RETURN person%ROWTYPE;
  
  FUNCTION is_for_resident(
    p_id      pc_parcel.id%TYPE
  )RETURN VARCHAR2;  
  
  PROCEDURE create_conditions(
    p_id        pc_parcel.id%TYPE,
    p_options   VARCHAR2
  );  
  
  PROCEDURE update_conditions(
    p_id        pc_parcel.id%TYPE,
    p_options   VARCHAR2
  );  
  
  FUNCTION set_values(
    p_id              pc_parcel.id%TYPE,
    p_prcl_list       pc_parcel.prcl_list_id%TYPE,
    p_trk_st_t_id     pc_parcel.trk_st_t_id%TYPE,
    p_track_num       pc_parcel.track_num%TYPE,
    p_carrier_id      pc_parcel.pc_carrier_id%TYPE,
    p_carrier_opt     pc_parcel.carrier%TYPE,
    p_misc_data       pc_parcel.misc_data%TYPE,
    p_return_rsn      pc_parcel.return_rsn%TYPE,
    p_receive_dt      pc_parcel.receive_dt%TYPE DEFAULT NULL,
    p_prsn_id         pc_parcel.prsn_id%TYPE DEFAULT NULL,
    p_non_res_f_name  pc_parcel.non_res_first_name%TYPE DEFAULT NULL,
    p_non_res_l_name  pc_parcel.non_res_last_name%TYPE DEFAULT NULL
  )RETURN pc_parcel%ROWTYPE;  
  
  PROCEDURE custom_log(
    p_id      pc_parcel.id%TYPE
  );  

END PC_PARCEL_PKG;

/
