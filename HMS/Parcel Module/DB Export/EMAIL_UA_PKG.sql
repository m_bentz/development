--------------------------------------------------------
--  DDL for Package EMAIL_UA_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "EMAIL_UA_PKG" 
AS
/*------------------------------------------------------------------------------
 02/05/2015   Tuann   Added send_flpp_noncovered_charge, send_flpp_validated.
 03/05/2015   Tuann   Added logic for sending email to parent to
                      send_flpp_noncovered_charge, send_flpp_validated.
 03/26/2015   Jeffreyp Added send_rmsel_on_notice which is sent to sudent when
                       to let them know when their freshman room selection
                       process will begin.
 05/03/2017   Tuann    Modified get_area_eml_addr_in_array adding
                       AREA_EML_RECIP_T_ID = 1
------------------------------------------------------------------------------*/

  PROCEDURE send_pkg_received(
    p_prsn_id     person.id%TYPE,
    p_eml_addr    VARCHAR2,
    p_prcl_id     pc_parcel.id%TYPE,
    p_area        area.title%TYPE,
    p_testing     BOOLEAN DEFAULT FALSE
  );

  PROCEDURE send_susp_pkg_received(
    p_prsn_id   person.id%TYPE,
    p_eml_addr  VARCHAR2,
    p_prcl_id   pc_parcel.id%TYPE,
    p_area      area.title%TYPE,
    p_testing     BOOLEAN DEFAULT FALSE
  );
  
  PROCEDURE send_pkg_returned(
    p_prsn_id     person.id%TYPE,
    p_eml_addr    VARCHAR2,
    p_prcl_id     pc_parcel.id%TYPE,
    p_area        area.title%TYPE,
    p_return_dt   date,
    p_testing     BOOLEAN DEFAULT FALSE
  );

  PROCEDURE send_conu_chng_req(
      p_prsn_id person.id%TYPE,
      p_template_id email_template.id%TYPE,
      p_req_id contract_change_req.id%TYPE,
      p_app_alias application.updt_sys%type,
      p_is_staff BOOLEAN
  );
  PROCEDURE send_contract_complete_to_stud(
      p_offer_id offer.id%TYPE,
      p_fl_prepaid_num contract_ua.prepaid_num%TYPE,
      p_testing BOOLEAN DEFAULT FALSE
  );
  PROCEDURE send_contu_need_med_doc_stud(
      p_offer_id offer.id%type
  );
  PROCEDURE send_contu_need_parent_sig(
      p_offer_id offer.id%TYPE,
      p_testing BOOLEAN DEFAULT FALSE
  );
  PROCEDURE send_cki_appt(
    p_config_id checkin_appt_config.id%TYPE
  , p_time_id   checkin_appt_time.id%TYPE
  , p_asg_id    assign.id%TYPE
  );
  PROCEDURE send_trans_req_created(
    p_trans_req_id trans_req.id%type
  , p_app_alias trans_req.updt_sys%type
  , p_current_assignment varchar2
  , p_prsn_id person.id%type
  );

  PROCEDURE send_canc_req_approved(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_w_wh_check(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_withdraw(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_cnc_admit(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_approve_job(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_deny_job(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_accounts(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_req_accounts_stf(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_rev_accounts(
    l_ids         objectidlist
  , p_agree       agreement%ROWTYPE
  );

  PROCEDURE send_canc_stf_req_student(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_ap_req(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_ap_req_approved(
    p_req      cancel_req%ROWTYPE
  );

  PROCEDURE send_canc_ap_req_denied(
    p_req      cancel_req%ROWTYPE
  );
  PROCEDURE send_trans_offer_accepted(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2,
      p_transfer_fee varchar2,
      p_rent_diff varchar2
  );

  PROCEDURE send_trans_offer_rejected(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2
  );

  PROCEDURE send_trans_req_approved(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2,
      p_transfer_fee varchar2,
      p_rent_diff varchar2
  );

  PROCEDURE send_trans_req_denied(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type
  );

  PROCEDURE send_rm_select_confirmed(
      p_sp_rm_trm_id space_room_term.id%TYPE,
      p_prsn_id person.id%TYPE,
      p_testing VARCHAR2 DEFAULT 'N',
      p_only_bldg_type_ind VARCHAR2 DEFAULT 'N'
  );

  PROCEDURE send_rmmt_request_notice(
    p_sp_rm_trm_id space_room_term.id%TYPE,
    p_held_by_prsn_id person.id%TYPE,
    p_held_for_prsn_id person.id%TYPE,
    p_testing VARCHAR2 DEFAULT 'N'
  );

  PROCEDURE send_flpp_noncovered_charge(
      p_trn_id        acc_trans.id%type,
      p_conu_id       contract_ua.id%type,
      p_prsn_id       person.id%type
  );

  PROCEDURE send_flpp_validated(
      p_trn_id        acc_trans.id%type,
      p_conu_id       contract_ua.id%type,
      p_prsn_id       person.id%type
  );

  PROCEDURE send_rmsel_on_notice(
      p_conu_id contract_ua.id%TYPE
  );

  PROCEDURE send_secured_deferment(
    p_prsn_id   person.id%TYPE
  , p_conu_id   contract_ua.id%TYPE
  );

  PROCEDURE roommate_invite( p_hsngmate_invite_id IN housingmate_invite.id%TYPE );

  PROCEDURE send_cnc_mv_out_to_area(
    p_prsn_id   person.id%TYPE
  , p_area_id   area.id%TYPE
  , p_name      VARCHAR2
  , p_ufid      VARCHAR2
  , p_terms     VARCHAR2
  , p_bldg      VARCHAR2
  , p_room      VARCHAR2
  , p_space     VARCHAR2
  , p_date      VARCHAR2
  , p_time      VARCHAR2
  );

  PROCEDURE movein_complete( p_assign_id IN assign.id%TYPE );


  PROCEDURE move_in_survey( p_assign_id IN assign.id%TYPE );


  PROCEDURE moveout_complete( p_assign_id IN assign.id%TYPE );

  PROCEDURE send_eq_checkout_conf_to_res(
    p_co_id eq_checkout.id%TYPE
  );

  FUNCTION send_eq_checkout_overdue(
    p_co_id IN  eq_checkout.id%TYPE
  , p_first IN  BOOLEAN
  ) RETURN email.id%TYPE;

  PROCEDURE send_eq_lost_item(
    p_co_id   eq_checkout.id%TYPE
  );

  PROCEDURE temp_key_issued( p_assign_id IN assign.id%TYPE, p_key_id IN key.id%TYPE );

  PROCEDURE temp_key_overdue( p_assign_id IN assign.id%TYPE, p_key_id IN key.id%TYPE );

  PROCEDURE lockout_warning(
    p_assign_id IN assign.id%TYPE,
    p_lockout_cnt IN NUMBER,
    p_lockout_max IN NUMBER );

  PROCEDURE lockout_charge(
    p_assign_id IN assign.id%TYPE,
    p_lockout_cnt key_charge.lockout_cnt%TYPE );

  PROCEDURE key_lost( p_assign_id IN assign.id%TYPE,
    p_key_id IN key.id%TYPE );

  PROCEDURE key_damaged( p_assign_id IN assign.id%TYPE,
    p_key_id IN key.id%TYPE );

  PROCEDURE lock_change(
    p_assign_id IN assign.id%TYPE,
    p_key_t_id IN key.key_t_id%TYPE
  );

  PROCEDURE clerk_lock_change(
    p_assign_id IN assign.id%TYPE,
    p_key_t_id IN key.key_t_id%TYPE );

  PROCEDURE staff_lockout_charge(
    p_assign_id IN assign.id%TYPE );


  PROCEDURE alert_stud_rmmt_change(
    p_prsn_id       person.id%TYPE
  , p_trm_group     term_typegrp.title%TYPE
  );

  PROCEDURE alert_std_rmmt_chng_aft_chkin(
    p_prsn_id       person.id%TYPE
  , p_trm_group     term_typegrp.title%TYPE
  );

  PROCEDURE send_rle_notification(
    p_prsn_id           person.id%TYPE
  , p_training_title    rle_training_session.descr%TYPE
  , p_date              rle_training_session.start_dt%TYPE
  , p_loc               VARCHAR2
  , p_body              VARCHAR2
  );

END EMAIL_UA_PKG;

/
