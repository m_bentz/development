--------------------------------------------------------
--  DDL for Package PC_PARCEL_LIST_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_LIST_PKG" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  FUNCTION get_list_by_area(
    p_area_id       area.id%TYPE
  )RETURN pc_parcel_list%ROWTYPE;  

END PC_PARCEL_LIST_PKG;

/
