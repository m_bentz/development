--------------------------------------------------------
--  DDL for Package PC_PARCEL_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_parcel
  PROCEDURE ins_pc_parcel (p_rec IN OUT pc_parcel%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_parcel
  PROCEDURE upd_pc_parcel(p_rec IN OUT pc_parcel%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_parcel
  FUNCTION get_pc_parcel(p_id IN NUMBER) RETURN pc_parcel%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_parcel
  PROCEDURE sav_pc_parcel(p_rec IN OUT pc_parcel%ROWTYPE);
END;

/
