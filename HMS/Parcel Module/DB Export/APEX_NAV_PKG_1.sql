--------------------------------------------------------
--  DDL for Package Body APEX_NAV_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "APEX_NAV_PKG" AS

  FUNCTION get_dialog_url(
    p_page      NUMBER,
    p_app       VARCHAR2,
    p_sess      VARCHAR2
  )RETURN VARCHAR2 AS
  v_url       VARCHAR2(2000);
  v_start     NUMBER;
  v_end       NUMBER;
  BEGIN
     v_url := APEX_UTIL.PREPARE_URL(
            p_url => 'f?p=' || p_app || ':' || p_page || ':' || p_sess || '::NO:',
            p_checksum_type => 'SESSION');
     apps.logger.debug(v_url);
     v_start := instr(v_url,'\u')+6;
     v_end := instr(v_url, '{t')-3;
     
     
     apps.logger.debug(substr(v_url, v_start, v_end - v_start + 1));
     return substr(v_url, v_start, v_end - v_start + 1);
  END get_dialog_url;

END APEX_NAV_PKG;

/
