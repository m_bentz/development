--------------------------------------------------------
--  DDL for Package Body PC_TRACK_ACTION_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_TRACK_ACTION_PKG" AS


  FUNCTION get_external_receive RETURN NUMBER AS
  BEGIN
    RETURN c_external_receive;
  END;
  
  FUNCTION get_non_res_receive RETURN NUMBER AS
  BEGIN
    RETURN c_non_res_receive;
  END;
  
  FUNCTION get_res_forward RETURN NUMBER AS
  BEGIN
    RETURN c_res_forward;
  END;
  
  FUNCTION get_deliver RETURN NUMBER AS
  BEGIN
    RETURN c_deliver;
  END;
  
  FUNCTION get_return_to_sender RETURN NUMBER AS
  BEGIN
    RETURN c_return_to_sender;
  END;
  
  FUNCTION get_internal_receive RETURN NUMBER AS
  BEGIN
    RETURN c_internal_receive;
  END;
  
  FUNCTION get_del_return_to_waiting RETURN NUMBER AS
  BEGIN
    RETURN c_del_return_to_waiting;
  END;
  
  FUNCTION get_rts_return_to_waiting RETURN NUMBER AS
  BEGIN
    RETURN c_rts_return_to_waiting;
  END;
  
  FUNCTION get_non_res_forward RETURN NUMBER AS
  BEGIN
    RETURN c_non_res_forward;
  END;
  
  FUNCTION get_result_status(
    p_id          PC_TRACK_ACTION.id%TYPE
  )RETURN PC_TRACK_STAT_TYPE%ROWTYPE AS
  BEGIN
    -- TODO: Implementation required for FUNCTION PC_TRACK_ACTION_PKG.get_result_status
    RETURN NULL;
  END get_result_status;

  FUNCTION build_query(
    p_id          PC_TRACK_ACTION.id%TYPE,
    p_cond_id     VARCHAR2,
    p_cond_id2    VARCHAR2 DEFAULT NULL
  )RETURN PC_TRACK_ACTION.query%TYPE AS
    v_track_action   pc_track_action%ROWTYPE;
    v_str_to_rep     VARCHAR2(10) := ':1';
  BEGIN
    -- Get the track action
    v_track_action := pc_track_action_cru.get_pc_track_action(p_id);
    
    -- Return the generated query
    return REPLACE(REPLACE(v_track_action.query, v_str_to_rep, p_cond_id), ':2', p_cond_id2);
  END build_query;

  FUNCTION get_headline(
    p_id        PC_TRACK_ACTION.id%TYPE,
    p_cond      VARCHAR2
  )RETURN PC_TRACK_ACTION.msg%TYPE AS
    v_track_action  pc_track_action%ROWTYPE;
    v_msg           pc_track_action.msg%TYPE;
  BEGIN
    -- Get the track action
    v_track_action := pc_track_action_cru.get_pc_track_action(p_id);
    
    -- Get the respective message for the track action
    CASE 
      WHEN v_track_action.id = 1 OR v_track_action.id = 6 THEN 
        v_msg := 'Parcel was received at ' || p_cond;
      WHEN v_track_action.id = 2 THEN
        v_msg := 'Non res parcel was received at ' || p_cond;
      WHEN v_track_action.id = 3 THEN
        v_msg := 'Parcel is being forwarded to ' || p_cond;
      WHEN v_track_action.id = 4 THEN
        v_msg := 'Parcel was delivered at ' || p_cond;
      WHEN v_track_action.id = 5 THEN
        v_msg := 'Parcel is being returned to sender';
      WHEN v_track_action.id = 7 OR v_track_action.id = 8 THEN 
        v_msg := 'Parcel has returned to waiting';
      WHEN v_track_action.id = 9 THEN
        v_msg := 'USPS parcel being forwarded';
    END CASE;      
    
    RETURN v_msg;
  END get_headline;
  
  PROCEDURE add_action(
    p_coll_name   VARCHAR2,
    p_msg         VARCHAR2,
    p_descr       VARCHAR2,
    p_date        DATE
  )AS
  v_msg           VARCHAR2(200);
  BEGIN
    apex_collection.add_member(
      p_collection_name => p_coll_name,
      p_d001 => p_date,
      p_d002 => p_date,
      p_c001 => p_msg,
      p_c002 => p_descr
    );
  END add_action;
END PC_TRACK_ACTION_PKG;

/
