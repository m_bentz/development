--------------------------------------------------------
--  DDL for Package PC_PARCEL_COND_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_PARCEL_COND_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_parcel_cond
  PROCEDURE ins_pc_parcel_cond (p_rec IN OUT pc_parcel_cond%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_parcel_cond
  PROCEDURE upd_pc_parcel_cond(p_rec IN OUT pc_parcel_cond%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_parcel_cond
  FUNCTION get_pc_parcel_cond(p_id IN NUMBER) RETURN pc_parcel_cond%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_parcel_cond
  PROCEDURE sav_pc_parcel_cond(p_rec IN OUT pc_parcel_cond%ROWTYPE);
END;

/
