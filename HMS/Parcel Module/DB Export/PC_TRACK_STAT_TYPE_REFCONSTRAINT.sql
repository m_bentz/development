--------------------------------------------------------
--  Ref Constraints for Table PC_TRACK_STAT_TYPE
--------------------------------------------------------

  ALTER TABLE "PC_TRACK_STAT_TYPE" ADD CONSTRAINT "TRK_ST_T_CREATE_S_FK00" FOREIGN KEY ("CREATE_S_ID")
	  REFERENCES "PC_TRACK_STAT_TYPE$S" ("S_ID") DISABLE;
  ALTER TABLE "PC_TRACK_STAT_TYPE" ADD CONSTRAINT "TRK_ST_T_CURR_S_FK00" FOREIGN KEY ("CURR_S_ID")
	  REFERENCES "PC_TRACK_STAT_TYPE$S" ("S_ID") DISABLE;
