--------------------------------------------------------
--  Ref Constraints for Table PC_PARCEL$S
--------------------------------------------------------

  ALTER TABLE "PC_PARCEL$S" ADD CONSTRAINT "PRCL$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_PARCEL" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
