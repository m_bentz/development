--------------------------------------------------------
--  DDL for Table PC_PARCEL_HISTORY
--------------------------------------------------------

  CREATE TABLE "PC_PARCEL_HISTORY" 
   (	"ID" NUMBER(10,0), 
	"PRCL_ID" NUMBER(10,0), 
	"START_DT" DATE, 
	"END_DT" DATE, 
	"HEADLINE" VARCHAR2(100), 
	"DESCR" VARCHAR2(250)
   ) ;
