--------------------------------------------------------
--  DDL for Package Body PC_PARCEL_HIST_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_PARCEL_HIST_PKG" AS

  FUNCTION get_latest_action(
    p_prcl_id      pc_parcel.id%TYPE
  )RETURN pc_parcel_hist%ROWTYPE AS
    v_prcl_hist    pc_parcel_hist%ROWTYPE;
  BEGIN
    SELECT lastest_entry.*
    INTO v_prcl_hist
    FROM (
        SELECT *
        FROM pc_parcel_hist
        WHERE prcl_id = p_prcl_id
        AND headline NOT LIKE '%suspicious' AND headline NOT LIKE '%safe'
        ORDER BY updt_ts desc) lastest_entry
    WHERE ROWNUM = 1;
    
    RETURN v_prcl_hist;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_latest_action;

END PC_PARCEL_HIST_PKG;

/
