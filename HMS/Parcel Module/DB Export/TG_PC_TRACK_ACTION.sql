--------------------------------------------------------
--  DDL for Trigger TG$PC_TRACK_ACTION
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TG$PC_TRACK_ACTION" 
  BEFORE INSERT OR UPDATE ON HMSDATA.pc_track_action
  FOR EACH ROW
DECLARE
  -- Setting these to N as default sets it to N for the newly added rows
  v_dialog_ind_m VARCHAR2(1 CHAR) := 'N';
  v_help_text_m VARCHAR2(1 CHAR) := 'N';
  v_no_result_msg_m VARCHAR2(1 CHAR) := 'N';
  v_dialog_m VARCHAR2(1 CHAR) := 'N';
  v_trk_st_t_id_m VARCHAR2(1 CHAR) := 'N';
  v_trk_st_t_id NUMBER := NULL;
  v_title_m VARCHAR2(1 CHAR) := 'N';
  v_descr_m VARCHAR2(1 CHAR) := 'N';
  v_result_trk_st_t_id_m VARCHAR2(1 CHAR) := 'N';
  v_result_trk_st_t_id NUMBER := NULL;
  v_prompt_ind_m VARCHAR2(1 CHAR) := 'N';
  v_msg_m VARCHAR2(1 CHAR) := 'N';
  v_query_m VARCHAR2(1 CHAR) := 'N';
  v_new_state_id NUMBER := NULL;
  cur_seq NUMBER;
  v_my_timestamp TIMESTAMP := SYSTIMESTAMP;
  v_migration_ind BOOLEAN := FALSE;
  v_anymod_ind BOOLEAN := FALSE;
BEGIN
  IF SYS_CONTEXT('USERENV','CLIENT_INFO') = 'MIGRATION' THEN
    v_migration_ind := TRUE;
  END IF;

  IF INSERTING THEN
    IF :NEW.id IS NULL THEN
      -- No ID passed, get one from the sequence
      :NEW.id := sq$pc_track_action.NEXTVAL;
    ELSE
      -- ID was set via insert, so update the sequence
      cur_seq := sq$pc_track_action.NEXTVAL;
      WHILE cur_seq <= :NEW.id LOOP
        cur_seq := sq$pc_track_action.NEXTVAL;
      END LOOP;
    END IF;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.dialog_ind <> :OLD.dialog_ind, TRUE)
      AND NOT (:NEW.dialog_ind IS NULL
        AND :OLD.dialog_ind IS NULL)
    )
  ) THEN
    v_dialog_ind_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.help_text <> :OLD.help_text, TRUE)
      AND NOT (:NEW.help_text IS NULL
        AND :OLD.help_text IS NULL)
    )
  ) THEN
    v_help_text_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.no_result_msg <> :OLD.no_result_msg, TRUE)
      AND NOT (:NEW.no_result_msg IS NULL
        AND :OLD.no_result_msg IS NULL)
    )
  ) THEN
    v_no_result_msg_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.dialog <> :OLD.dialog, TRUE)
      AND NOT (:NEW.dialog IS NULL
        AND :OLD.dialog IS NULL)
    )
  ) THEN
    v_dialog_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.trk_st_t_id <> :OLD.trk_st_t_id, TRUE)
      AND NOT (:NEW.trk_st_t_id IS NULL
        AND :OLD.trk_st_t_id IS NULL)
    )
  ) THEN
    v_trk_st_t_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.title <> :OLD.title, TRUE)
      AND NOT (:NEW.title IS NULL
        AND :OLD.title IS NULL)
    )
  ) THEN
    v_title_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.descr <> :OLD.descr, TRUE)
      AND NOT (:NEW.descr IS NULL
        AND :OLD.descr IS NULL)
    )
  ) THEN
    v_descr_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.result_trk_st_t_id <> :OLD.result_trk_st_t_id, TRUE)
      AND NOT (:NEW.result_trk_st_t_id IS NULL
        AND :OLD.result_trk_st_t_id IS NULL)
    )
  ) THEN
    v_result_trk_st_t_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.prompt_ind <> :OLD.prompt_ind, TRUE)
      AND NOT (:NEW.prompt_ind IS NULL
        AND :OLD.prompt_ind IS NULL)
    )
  ) THEN
    v_prompt_ind_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.msg <> :OLD.msg, TRUE)
      AND NOT (:NEW.msg IS NULL
        AND :OLD.msg IS NULL)
    )
  ) THEN
    v_msg_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.query <> :OLD.query, TRUE)
      AND NOT (:NEW.query IS NULL
        AND :OLD.query IS NULL)
    )
  ) THEN
    v_query_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  v_trk_st_t_id := :NEW.trk_st_t_id;
  v_result_trk_st_t_id := :NEW.result_trk_st_t_id;

  IF INSERTING OR v_anymod_ind THEN
    IF(:NEW.updt_ts IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_ts := v_my_timestamp;
    END IF;

    IF(:NEW.updt_by IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_by := hmsdata.utility_pkg.get_user();
    END IF;

    IF(:NEW.updt_sys IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_sys := hmsdata.utility_pkg.get_system();
    END IF;

    IF INSERTING THEN
      :NEW.create_ts := :NEW.updt_ts;
      :NEW.create_by := :NEW.updt_by;
    END IF;
  END IF;

  :NEW.apex_app_id := hmsdata.utility_pkg.get_apex_app_id();
  :NEW.apex_page_id := hmsdata.utility_pkg.get_apex_page_id();

  IF :NEW.updt_ts IS NULL THEN
    :NEW.updt_ts := :OLD.updt_ts;
  END IF;
  IF :NEW.updt_by IS NULL THEN
    :NEW.updt_by := :OLD.updt_by;
  END IF;
  IF :NEW.updt_sys IS NULL THEN
    :NEW.updt_sys := :OLD.updt_sys;
  END IF;

  IF UPDATING THEN
    IF :NEW.create_ts IS NOT NULL AND :NEW.create_ts <> :OLD.create_ts THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_track_action.create_ts on record #'||:OLD.id||' from "'
          ||:OLD.create_ts||'" to "'||:NEW.create_ts||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_ts := :OLD.create_ts;

    IF :NEW.create_by IS NOT NULL AND :NEW.create_by <> :OLD.create_by THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_track_action.create_by on record #'||:OLD.id||' from "'
          ||:OLD.create_by||'" to "'||:NEW.create_by||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_by := :OLD.create_by;

    IF :NEW.create_s_id IS NOT NULL AND :NEW.create_s_id <> :OLD.create_s_id THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_track_action.create_s_id on record #'||:OLD.id||' from "'
          ||:OLD.create_s_id||'" to "'||:NEW.create_s_id||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_s_id := :OLD.create_s_id;

    IF :NEW.curr_s_id IS NOT NULL AND :NEW.curr_s_id <> :OLD.curr_s_id THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_track_action.curr_s_id on record #'||:OLD.id||' from "'
          ||:OLD.curr_s_id||'" to "'||:NEW.curr_s_id||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.curr_s_id := :OLD.curr_s_id;
  END IF;

  IF(
    INSERTING
    OR v_anymod_ind
    OR :NEW.create_s_id = -1
    OR :NEW.create_s_id IS NULL
    OR :NEW.curr_s_id = -1
    OR :NEW.curr_s_id IS NULL
  ) THEN
    INSERT INTO pc_track_action$s (
      id,
      trk_st_t_id,
      trk_st_t_id_m_ind,
      title,
      title_m_ind,
      descr,
      descr_m_ind,
      result_trk_st_t_id,
      result_trk_st_t_id_m_ind,
      prompt_ind,
      prompt_ind_m_ind,
      msg,
      msg_m_ind,
      query,
      query_m_ind,
      create_by,
      create_ts,
      updt_by,
      updt_ts,
      updt_sys,
      apex_app_id,
      apex_page_id,
      help_text,
      help_text_m_ind,
      no_result_msg,
      no_result_msg_m_ind,
      dialog_ind,
      dialog_ind_m_ind,
      dialog,
      dialog_m_ind
    ) VALUES (
      :NEW.id,
      v_trk_st_t_id,
      v_trk_st_t_id_m,
      :NEW.title,
      v_title_m,
      :NEW.descr,
      v_descr_m,
      v_result_trk_st_t_id,
      v_result_trk_st_t_id_m,
      :NEW.prompt_ind,
      v_prompt_ind_m,
      :NEW.msg,
      v_msg_m,
      :NEW.query,
      v_query_m,
      :NEW.create_by,
      :NEW.create_ts,
      :NEW.updt_by,
      :NEW.updt_ts,
      :NEW.updt_sys,
      :NEW.apex_app_id,
      :NEW.apex_page_id,
      :NEW.help_text,
      v_help_text_m,
      :NEW.no_result_msg,
      v_no_result_msg_m,
      :NEW.dialog_ind,
      v_dialog_ind_m,
      :NEW.dialog,
      v_dialog_m
    ) RETURNING s_id INTO v_new_state_id;
    -- Set the state ids for convenience
    :NEW.curr_s_id := v_new_state_id;
    IF INSERTING OR :NEW.create_s_id = -1 OR :NEW.create_s_id IS NULL THEN
      :NEW.create_s_id := v_new_state_id;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "TG$PC_TRACK_ACTION" ENABLE;
