--------------------------------------------------------
--  DDL for Package Body EMAIL_UA_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "EMAIL_UA_PKG" 
AS

  PROCEDURE send_pkg_received(
      p_prsn_id     person.id%TYPE,
      p_eml_addr    VARCHAR2,
      p_prcl_id     pc_parcel.id%TYPE,
      p_area        area.title%TYPE,
      p_testing BOOLEAN DEFAULT FALSE
  )AS
      c_template_id       CONSTANT email_template.id%type := 100353;
      c_proc_nm           CONSTANT VARCHAR2(50) DEFAULT 'email_ua_pkg.send_pkg_received';
      c_success_msg       CONSTANT VARCHAR2(100) DEFAULT 'Email sent to student successfully';
      c_person_name       CONSTANT VARCHAR2(50) DEFAULT '#PERSON_NAME#';
      c_area_name         CONSTANT VARCHAR2(50) DEFAULT '#AREA_NAME#';
      c_track_num         CONSTANT VARCHAR2(50) DEFAULT '#TRACK_NUM#';
      c_carrier           CONSTANT VARCHAR2(50) DEFAULT '#CARRIER#';
      c_receive_dt        CONSTANT VARCHAR2(50) DEFAULT '#RECEIVE_DT#';
      v_new_email_id      email.id%type;
      v_body              CLOB;
      v_subject           apps.email_pkg.t_subject;
      v_from              apps.email_pkg.t_from;
      v_to                apps.email_pkg.recip_array;
      v_cc                apps.email_pkg.recip_array;
      v_bcc               apps.email_pkg.recip_array;
      v_prcl              pc_parcel%ROWTYPE;
      v_carrier           pc_parcel.carrier%TYPE;
  BEGIN
      v_prcl  := pc_parcel_cru.get_pc_parcel(p_prcl_id);
      v_to(1) := p_eml_addr;
      
      SELECT
          decode(v_prcl.pc_carrier_id, 0, v_prcl.carrier, title)
      INTO v_carrier
      FROM pc_carrier
      WHERE id = v_prcl.pc_carrier_id;
      
      <<get_template>>
      BEGIN
        apps.email_pkg.get_template(
          p_tmplt_id => c_template_id, 
          p_body => v_body,
          p_subject => v_subject, 
          p_from => v_from);
      END;
  
      <<replace_tags>>
      BEGIN
          apex_debug_message.log_message(c_proc_nm || ' Starting to replace tags in template');
          v_body := REPLACE(v_body, c_person_name, person_pkg.get_name_initcap(v_prcl.prsn_id));
          v_body := REPLACE(v_body, c_area_name, p_area);
          v_body := REPLACE(v_body, c_track_num, v_prcl.track_num);
          v_body := REPLACE(v_body, c_carrier, v_carrier);
          v_body := REPLACE(v_body, c_receive_dt, to_char(v_prcl.receive_dt, 'Mon DD, YYYY HH:MIAM'));
          apex_debug_message.log_message(c_proc_nm || ' populated tags successfully', false, 4);
      EXCEPTION
          WHEN OTHERS THEN
          apex_debug_message.log_message(c_proc_nm ||
              ' error replacing tags in template' || sqlerrm, false, 4);
          raise_application_error( - 20000,
              'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      
      IF(NOT p_testing) THEN
          wwv_flow_api.set_security_group_id(p_security_group_id =>
              apex_util.find_security_group_id(p_workspace => 'HMS'));
          <<send_email>>
--          BEGIN
            email_pkg.send(
                p_to                => v_to, 
                p_cc                => v_cc, 
                p_bcc               => v_bcc,
                p_from              => v_from, 
                p_subject           => v_subject, 
                p_html              => v_body,
                p_prsn_id           => p_prsn_id, 
                p_app_alias         => NULL,
                p_stud_viewable_ind => 'Y', 
                p_email_id          => v_new_email_id);
--          EXCEPTION
--            WHEN OTHERS THEN
--              RAISE;
--          END;    
      ELSE
          dbms_output.put_line(v_body) ;
      END IF;
    apex_debug_message.log_message(c_proc_nm || ' ' || c_success_msg, false, 4);
  END send_pkg_received;

  PROCEDURE send_susp_pkg_received(
      p_prsn_id   person.id%TYPE,
      p_eml_addr  VARCHAR2,
      p_prcl_id   pc_parcel.id%TYPE,
      p_area      area.title%TYPE,
      p_testing     BOOLEAN DEFAULT FALSE
  )AS
      c_template_id       CONSTANT email_template.id%type := 100353;
      c_proc_nm           CONSTANT VARCHAR2(50) DEFAULT 'email_ua_pkg.send_susp_pkg_received';
      c_success_msg       CONSTANT VARCHAR2(100) DEFAULT 'Email sent to student successfully';
      c_person_name       CONSTANT VARCHAR2(50) DEFAULT '#PERSON_NAME#';
      c_area_name         CONSTANT VARCHAR2(50) DEFAULT '#AREA_NAME#';
      c_track_num         CONSTANT VARCHAR2(50) DEFAULT '#TRACK_NUM#';
      c_carrier           CONSTANT VARCHAR2(50) DEFAULT '#CARRIER#';
      c_receive_dt        CONSTANT VARCHAR2(50) DEFAULT '#RECEIVE_DT#';
      v_new_email_id      email.id%type;
      v_body              CLOB;
      v_subject           apps.email_pkg.t_subject;
      v_from              apps.email_pkg.t_from;
      v_to                apps.email_pkg.recip_array;
      v_cc                apps.email_pkg.recip_array;
      v_bcc               apps.email_pkg.recip_array;
      v_prcl              pc_parcel%ROWTYPE;
      v_carrier           pc_parcel.carrier%TYPE;
  BEGIN
      v_prcl  := pc_parcel_cru.get_pc_parcel(p_prcl_id);
      v_to(1) := p_eml_addr;
      
      SELECT
          decode(v_prcl.pc_carrier_id, 0, v_prcl.carrier, title)
      INTO v_carrier
      FROM pc_carrier
      WHERE id = v_prcl.pc_carrier_id;
      
      <<get_template>>
      BEGIN
        apps.email_pkg.get_template(
          p_tmplt_id => c_template_id, 
          p_body => v_body,
          p_subject => v_subject, 
          p_from => v_from);
      END;
  
      <<replace_tags>>
      BEGIN
          apex_debug_message.log_message(c_proc_nm || ' Starting to replace tags in template');
          v_body := REPLACE(v_body, c_person_name, person_pkg.get_name_initcap(v_prcl.prsn_id));
          v_body := REPLACE(v_body, c_area_name, p_area);
          v_body := REPLACE(v_body, c_track_num, v_prcl.track_num);
          v_body := REPLACE(v_body, c_carrier, v_carrier);
          v_body := REPLACE(v_body, c_receive_dt, to_char(v_prcl.receive_dt, 'Mon DD, YYYY HH:MIAM'));
          apex_debug_message.log_message(c_proc_nm || ' populated tags successfully', false, 4);
      EXCEPTION
          WHEN OTHERS THEN
          apex_debug_message.log_message(c_proc_nm ||
              ' error replacing tags in template' || sqlerrm, false, 4);
          raise_application_error( - 20000,
              'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      
      IF(NOT p_testing) THEN
          wwv_flow_api.set_security_group_id(p_security_group_id =>
              apex_util.find_security_group_id(p_workspace => 'HMS'));
          <<send_email>>
--          BEGIN
            email_pkg.send(
                p_to                => v_to, 
                p_cc                => v_cc, 
                p_bcc               => v_bcc,
                p_from              => v_from, 
                p_subject           => v_subject, 
                p_html              => v_body,
                p_prsn_id           => p_prsn_id, 
                p_app_alias         => NULL,
                p_stud_viewable_ind => 'N', 
                p_email_id          => v_new_email_id);
--          EXCEPTION
--            WHEN OTHERS THEN
--              RAISE;
--          END;    
      ELSE
          dbms_output.put_line(v_body) ;
      END IF;
    apex_debug_message.log_message(c_proc_nm || ' ' || c_success_msg, false, 4);
  END send_susp_pkg_received;  
  
  PROCEDURE send_pkg_returned(
      p_prsn_id     person.id%TYPE,
      p_eml_addr    VARCHAR2,
      p_prcl_id     pc_parcel.id%TYPE,
      p_area        area.title%TYPE,
      p_return_dt   date,
      p_testing     BOOLEAN DEFAULT FALSE
  )AS
      c_template_id       CONSTANT email_template.id%type := 100352;
      c_proc_nm           CONSTANT VARCHAR2(50) DEFAULT 'email_ua_pkg.send_pkg_returned';
      c_success_msg       CONSTANT VARCHAR2(100) DEFAULT 'Email sent to student successfully';
      c_person_name       CONSTANT VARCHAR2(50) DEFAULT '#PERSON_NAME#';
      c_area_name         CONSTANT VARCHAR2(50) DEFAULT '#AREA_NAME#';
      c_track_num         CONSTANT VARCHAR2(50) DEFAULT '#TRACK_NUM#';
      c_carrier           CONSTANT VARCHAR2(50) DEFAULT '#CARRIER#';
      c_receive_dt        CONSTANT VARCHAR2(50) DEFAULT '#RECEIVE_DT#';
      c_return_dt         CONSTANT VARCHAR2(50) DEFAULT '#RETURN_DT#';
      v_new_email_id      email.id%type;
      v_body              CLOB;
      v_subject           apps.email_pkg.t_subject;
      v_from              apps.email_pkg.t_from;
      v_to                apps.email_pkg.recip_array;
      v_cc                apps.email_pkg.recip_array;
      v_bcc               apps.email_pkg.recip_array;
      v_prcl              pc_parcel%ROWTYPE;
      v_carrier           pc_parcel.carrier%TYPE;
  BEGIN
      v_prcl  := pc_parcel_cru.get_pc_parcel(p_prcl_id);
      v_to(1) := p_eml_addr;
      
      SELECT
          decode(v_prcl.pc_carrier_id, 0, v_prcl.carrier, title)
      INTO v_carrier
      FROM pc_carrier
      WHERE id = v_prcl.pc_carrier_id;
      
      <<get_template>>
      BEGIN
        apps.email_pkg.get_template(
          p_tmplt_id => c_template_id, 
          p_body => v_body,
          p_subject => v_subject, 
          p_from => v_from);
      END;
  
      <<replace_tags>>
      BEGIN
          apex_debug_message.log_message(c_proc_nm || ' Starting to replace tags in template');
          v_body := REPLACE(v_body, c_person_name, person_pkg.get_name_initcap(v_prcl.prsn_id));
          v_body := REPLACE(v_body, c_area_name, p_area);
          v_body := REPLACE(v_body, c_track_num, v_prcl.track_num);
          v_body := REPLACE(v_body, c_carrier, v_carrier);
          v_body := REPLACE(v_body, c_receive_dt, to_char(v_prcl.receive_dt, 'Mon DD, YYYY HH:MIAM'));
          v_body := REPLACE(v_body, c_return_dt, to_char(p_return_dt, 'Mon DD, YYYY HH:MIAM'));
          apex_debug_message.log_message(c_proc_nm || ' populated tags successfully', false, 4);
      EXCEPTION
          WHEN OTHERS THEN
          apex_debug_message.log_message(c_proc_nm ||
              ' error replacing tags in template' || sqlerrm, false, 4);
          raise_application_error( - 20000,
              'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      
      IF(NOT p_testing) THEN
          wwv_flow_api.set_security_group_id(p_security_group_id =>
              apex_util.find_security_group_id(p_workspace => 'HMS'));
          <<send_email>>
--          BEGIN
            email_pkg.send(
                p_to                => v_to, 
                p_cc                => v_cc, 
                p_bcc               => v_bcc,
                p_from              => v_from, 
                p_subject           => v_subject, 
                p_html              => v_body,
                p_prsn_id           => p_prsn_id, 
                p_app_alias         => NULL,
                p_stud_viewable_ind => 'Y', 
                p_email_id          => v_new_email_id);
--          EXCEPTION
--            WHEN OTHERS THEN
--              RAISE;
--          END;    
      ELSE
          dbms_output.put_line(v_body) ;
      END IF;
    apex_debug_message.log_message(c_proc_nm || ' ' || c_success_msg, false, 4);
  END send_pkg_returned;  
--------------------------------------------------------
--    DDL for send_contract_complete_to_stud
--------------------------------------------------------
PROCEDURE send_contract_complete_to_stud(
    p_offer_id offer.id%TYPE,
    p_fl_prepaid_num contract_ua.prepaid_num%TYPE,
    p_testing BOOLEAN DEFAULT FALSE)
AS
  v_new_email_id email.id%type;
  v_body CLOB;
  v_subject apps.email_pkg.t_subject;
  v_from apps.email_pkg.t_from;
  v_body_sub CLOB;
  v_body_sub2 CLOB;
  v_body_sub3 CLOB;
  v_subject_sub apps.email_pkg.t_subject;
  v_from_sub apps.email_pkg.t_from;
  v_to apps.email_pkg.recip_array;
  v_cc apps.email_pkg.recip_array;
  v_bcc apps.email_pkg.recip_array;
  -- Look up the email template to use
  v_template_id       CONSTANT email_template.id%type := 9;
  v_template_id2      CONSTANT email_template.id%type := 13;
  v_template_id3      CONSTANT email_template.id%type := 14;
  v_template_id4      CONSTANT email_template.id%type := 15;
  v_name_tag          CONSTANT VARCHAR(50) DEFAULT '#NAME#';
  v_offer_rec offer%ROWTYPE;
  v_agree_id agreement.id%TYPE;
  v_email_addr_types objectidlist := objectidlist(1,90);
  v_proc_nm CONSTANT VARCHAR2(50) := 'email_ua_pkg.send_contract_complete_to_stud';
  v_success_msg VARCHAR2(100) := 'Email sent to student successfully';
  v_needs_parent_consent BOOLEAN;
  v_is_arp_waived CHAR(1);
  v_reason_not_charged VARCHAR2(2000);
  v_entered_fl_prepaid_num_valid CHAR(1);
  v_chrg_amount NUMBER;
  v_chrg_amount_text VARCHAR2(100);
  v_first_conu_rec contract_ua%ROWTYPE;
  --------------------------------------------------------
  -- Need to add Contract Terms and Conditions PDF report
  --------------------------------------------------------
  v_Pdf                    BLOB;
  v_Mime                   Varchar2(400);
BEGIN


  v_offer_rec := offer_pkg.get_offer(p_offer_id);
  v_needs_parent_consent :=
    CASE
      WHEN (person_ua_pkg.calc_parent_sig_status_id(v_offer_rec.prsn_id ) = 2) THEN TRUE
      ELSE FALSE
    END;
  IF(v_offer_rec.id IS NULL) THEN
    apps.logger.error(v_proc_nm ||
      ' Trying to send email for offer but no offer found with id: ' || p_offer_id);
    RAISE_APPLICATION_ERROR(-20101, v_proc_nm || ' Required data Offer id invalid');
  END IF;
  v_first_conu_rec := contract_ua_pkg.get_contract_ua(
    contract_ua_pkg.get_conu_id_for_term(v_offer_rec.prsn_id,v_offer_rec.start_trm_id));

  v_agree_id := NVL(offer_pkg.get_agree_id(v_offer_rec.id),0);
  -- find out if their advance rent prepayment charge has been waived

  v_entered_fl_prepaid_num_valid:= person_ua_pkg.is_fl_prepaid_acct_num_valid(
    p_prsn_id => v_offer_rec.prsn_id, p_fl_prepaid_acc_num => p_fl_prepaid_num,
    p_conu_id => NULL);

  -- Controls who email is sent to
  v_to := person_pkg.get_email_addresses_in_array(v_offer_rec.prsn_id,v_email_addr_types);
  BEGIN
    -- get the template and subject from the database for this email
    apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
      p_subject => v_subject, p_from => v_from) ;
    -- under 18 instructions
    IF(v_needs_parent_consent) THEN
      BEGIN
        apps.email_pkg.get_template(p_tmplt_id => v_template_id2,
          p_body => v_body_sub, p_subject => v_subject_sub, p_from => v_from_sub) ;
      EXCEPTION
        WHEN OTHERS THEN
          raise_application_error( - 20000, v_proc_nm ||
            ' No template exist for the requested template: ' ||
            v_template_id2 || ' Error:' ||sqlerrm) ;
      END;
    END IF;
    -- Florida prepaid instructions template
    IF(v_entered_fl_prepaid_num_valid = 'Y') THEN
      BEGIN
        apps.email_pkg.get_template(p_tmplt_id => v_template_id3,
          p_body => v_body_sub2, p_subject => v_subject_sub, p_from => v_from_sub) ;
      EXCEPTION
        WHEN OTHERS THEN
          raise_application_error( - 20000, v_proc_nm ||
          ' No template exist for the requested template: ' ||
          v_template_id3 || ' Error:' ||sqlerrm) ;
      END;
    END IF;
    -- payment instructions
    IF(v_first_conu_rec.arp_waived_ind = 'N' OR v_first_conu_rec.arp_waived_ind is null) THEN
      BEGIN
        apps.email_pkg.get_template(p_tmplt_id => v_template_id4,
          p_body => v_body_sub3, p_subject => v_subject_sub, p_from => v_from_sub) ;
      EXCEPTION
        WHEN OTHERS THEN
          raise_application_error( - 20000, v_proc_nm ||
            ' No template exist for the requested template: ' ||
            v_template_id4 || ' Error:' ||sqlerrm) ;
      END;
    ELSE
      v_reason_not_charged := contract_ua_pkg.get_arpp_defer_t_title(
        p_conu_id => v_first_conu_rec.id);
    END IF;
  EXCEPTION
  WHEN OTHERS THEN
    raise_application_error( - 20000, v_proc_nm ||
      ' No template exist for the requested template: ' ||
      v_template_id || ' Error:' ||sqlerrm) ;
  END;
  v_chrg_amount := agreement_pkg.get_prepaid_chrg_amt(v_agree_id);
  v_chrg_amount_text :=
  CASE WHEN (v_chrg_amount = 0) THEN '0.00 (Deferred)' || v_reason_not_charged
       ELSE to_char(v_chrg_amount,'9,990.00')
  END;
  BEGIN
    apex_debug_message.log_message(v_proc_nm || ' Starting to replace tags in template');
    -- these will handle the sub templates
    v_body := REPLACE(v_body, '#UNDER_18_INSTRUCTIONS#', v_body_sub);
    apps.logger.trace('a');
    v_body := REPLACE(v_body, '#FL_PREPAID_INSTRUCTIONS#', v_body_sub2);
    apps.logger.trace('b');
    v_body := REPLACE(v_body, '#PAYMENT_INSTRUCTIONS#', v_body_sub3);
    apps.logger.trace('c');
    -- set the values for the tags
    v_subject := REPLACE(v_subject, '#TERM_GRPT_NM#',v_offer_rec.trm_grp_full_nm);
    apps.logger.trace('d');
    v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name_initcap(v_offer_rec.prsn_id)) ;
    apps.logger.trace('1');
    v_body := REPLACE(v_body, '#PRSN_INFO#', person_pkg.disp_prsn_info(v_offer_rec.prsn_id,'','N','N','N','N')) ;
    apps.logger.trace('2');
    v_body := REPLACE(v_body, '#TERM_GRPT_NM#',v_offer_rec.trm_grp_full_nm) ;
    apps.logger.trace('3');
    v_body := REPLACE(v_body, '#ARP_AMOUNT#', v_chrg_amount_text) ;
    apps.logger.trace('4');
    v_body := REPLACE(v_body, '#OFFER_DUE_DT#', to_char(v_offer_rec.due_dt,'MM/DD/YYYY'));
    apps.logger.trace('5');
    v_body := REPLACE(v_body, '#UA_PAY_INSTR_URL#', ext_link_pkg.get_link(21)) ;
    apps.logger.trace('6');
    v_body := REPLACE(v_body, '#FACEBOOK_URL#', ext_link_pkg.get_link(23)) ;
    apps.logger.trace('7');
    v_body := REPLACE(v_body, '#TWITTER_URL#', ext_link_pkg.get_link(22)) ;
    apps.logger.trace('8');
    v_body := REPLACE(v_body, '#HOUSING_URL#', ext_link_pkg.get_link(24)) ;
    apps.logger.trace('9');
    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    apex_debug_message.log_message(v_proc_nm || ' populated tags successfully', false, 4);
    EXCEPTION
      WHEN OTHERS THEN
        apex_debug_message.log_message(v_proc_nm ||
          ' error replacing tags in template' || sqlerrm, false, 4);
        raise_application_error( - 20000,
          'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  IF(NOT p_testing) THEN
    wwv_flow_api.set_security_group_id(p_security_group_id =>
        apex_util.find_security_group_id(p_workspace => 'HMS'));
    email_pkg.send(p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_offer_rec.prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id);
  ELSE
    dbms_output.put_line(v_body) ;
  END IF;
  apex_debug_message.log_message(v_proc_nm || ' ' || v_success_msg, false, 4);
END send_contract_complete_to_stud;

--------------------------------------------------------
--    DDL for change_request_body
--------------------------------------------------------
  FUNCTION change_request_body(
    p_body  VARCHAR2
  , p_prsn_id   person.id%TYPE
  , p_ufid      person.ufid%TYPE
  , p_req_row   contract_change_req%ROWTYPE
  )RETURN VARCHAR2 AS
    v_body  VARCHAR2(32767);
    v_new_term_tag        CONSTANT VARCHAR(25) DEFAULT '#NEW_TERM#';
    v_fee_waive_req_tag   CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVE_REQ#';
    v_docs_rec_tag        CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_fee_text_tag        CONSTANT VARCHAR(25) DEFAULT '#FEE_TEXT#';
    v_fee_decision_tag    CONSTANT VARCHAR(25) DEFAULT '#DECISION#';
  BEGIN
    v_body := p_body;
    v_body := REPLACE(v_body,
      email_pkg.c_full_name_tag, person_pkg.get_name_initcap(p_prsn_id)) ;
    v_body := REPLACE(v_body,
      email_pkg.c_first_name_tag, INITCAP( person_pkg.get_first_name(p_prsn_id)) );
    v_body := REPLACE(v_body,
      email_pkg.c_term_tag, contract_change_req_pkg.get_from_term_text(p_req_row.id));
    v_body := REPLACE(v_body,
      v_new_term_tag, contract_change_req_pkg.get_to_term_text(p_req_row.id));
    v_body := REPLACE(v_body, email_pkg.c_reason_tag, nvl(p_req_row.rsn, 'N/A'));
    IF p_req_row.to_trm_tgrp_id = 7 THEN
      v_body := REPLACE(v_body, email_pkg.c_amount, '$0.00');
    ELSE
      v_body := REPLACE(v_body,
        email_pkg.c_amount,
        to_char(contract_change_req_pkg.get_convenience_fee_amount(), '$9,999.00'));
    END IF;
    v_body := REPLACE(v_body,
      v_docs_rec_tag, CASE WHEN p_req_row.cont_sur_id IS NULL THEN 'None' ELSE 'Received' END);
    v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(p_prsn_id));
--      v_body := REPLACE(v_body, email_pkg.c_address, );

    IF INSTR(v_body, email_pkg.c_link_tag, 1, 1) <> 0 THEN
      v_body := REPLACE(v_body, email_pkg.c_link_tag,
          ext_link_pkg.format_link(
            p_url => apps.url_redirect_pkg.prepare_url(
              p_url => config_pkg.get_value(
                'HTTP_SERVER') || '/f?p=501:61:' || v('APP_SESSION') || '::::P61_UFID,P61_CHNG_REQ_ID:' ||
                p_ufid || ',' || p_req_row.id
          ),
        p_label=> 'HERE'));
    END IF;

    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    RETURN v_body;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '|| sqlerrm) ;
  END;
--------------------------------------------------------
--    DDL for send_conu_chng_req
--------------------------------------------------------
  PROCEDURE send_conu_chng_req(
      p_prsn_id person.id%TYPE,
      p_template_id email_template.id%TYPE,
      p_req_id  contract_change_req.id%TYPE,
      p_app_alias application.updt_sys%type,
      p_is_staff  BOOLEAN
  ) AS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    -- Look up the email template to use
    v_template_id email_template.id%type := p_template_id;
--    v_name_tag CONSTANT VARCHAR(25) DEFAULT '#NAME#';

    v_email_addr_types objectidlist;

    v_gatorlink_email email_addr.eml_addr%TYPE;
    v_url_term VARCHAR2( 500);
    v_url_select_apt VARCHAR2( 500);
    v_url_pay_app_fee VARCHAR2( 500);

    v_req_rec     contract_change_req%ROWTYPE;
    v_ufid        person.ufid%TYPE;
  BEGIN
    /* users should pass in in the change request id, so
    we can use the information in the future.*/
    v_ufid := person_pkg.get_ufid(p_prsn_id);
    v_req_rec := contract_change_req_cru.get_contract_change_req(p_req_id);

    IF p_is_staff THEN
      v_to := email_pkg.get_department_recip(7); -- UA Admin
    ELSE
      IF person_pkg.email_parent(p_prsn_id) = 'Y' THEN
        v_email_addr_types := objectidlist(90,1,17);
      ELSE
        v_email_addr_types := objectidlist(90,1);
      END IF;
      v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    END IF;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(
        p_tmplt_id => v_template_id,
        p_body => v_body,
        p_subject => v_subject,
        p_from => v_from
      );
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    v_body := change_request_body(v_body, p_prsn_id, v_ufid, v_req_rec);

    BEGIN
      wwv_flow_api.set_security_group_id(p_security_group_id =>
        apex_util.find_security_group_id(p_workspace => 'HMS'));
      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => p_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => CASE WHEN p_is_staff THEN 'N' ELSE 'Y' END,
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem sending email: '||sqlerrm) ;
    END;
  END;

--------------------------------------------------------
--    DDL for send_finance_req_alert
--------------------------------------------------------
  PROCEDURE send_finance_req_alert(
      p_prsn_id person.id%TYPE,
      p_template_id email_template.id%TYPE,
      p_req_id  contract_change_req.id%TYPE,
      p_app_alias application.updt_sys%type
  ) AS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    -- Look up the email template to use
    v_template_id email_template.id%type := p_template_id;
--    v_name_tag CONSTANT VARCHAR(25) DEFAULT '#NAME#';

    v_email_addr_types objectidlist;

    v_gatorlink_email email_addr.eml_addr%TYPE;
    v_url_term VARCHAR2( 500);
    v_url_select_apt VARCHAR2( 500);
    v_url_pay_app_fee VARCHAR2( 500);

    v_req_row     contract_change_req%ROWTYPE;
    v_ufid        person.ufid%TYPE;
  BEGIN
    /* users should pass in in the change request id,
    so we can use the information in the future.*/
    v_ufid := person_pkg.get_ufid(p_prsn_id);
    v_req_row := contract_change_req_cru.get_contract_change_req(p_req_id);

    v_to := email_pkg.get_department_recip(24); -- GFH Bill Accounts
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(
        p_tmplt_id => v_template_id,
        p_body => v_body,
        p_subject => v_subject,
        p_from => v_from
      );
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      v_body := change_request_body(v_body, p_prsn_id, v_ufid, v_req_row);
      -- set the values for the tags

    wwv_flow_api.set_security_group_id(p_security_group_id =>
      apex_util.find_security_group_id(p_workspace => 'HMS'));
    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => p_prsn_id,
         p_app_alias => p_app_alias,
         p_stud_viewable_ind => 'Y',
         p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END;

--------------------------------------------------------
--    DDL for send_contu_need_med_doc_stud
--------------------------------------------------------
PROCEDURE send_contu_need_med_doc_stud(
    p_offer_id offer.id%TYPE )
AS
  v_new_email_id email.id%type;
  v_body CLOB;
  v_subject apps.email_pkg.t_subject;
  v_from apps.email_pkg.t_from;
  v_body2 CLOB;
  v_subject2 apps.email_pkg.t_subject;
  v_from2 apps.email_pkg.t_from;
  v_to apps.email_pkg.recip_array;
  v_cc apps.email_pkg.recip_array;
  v_bcc apps.email_pkg.recip_array;
  -- Look up the email template to use
  v_template_id       CONSTANT email_template.id%type := 10;
  -- Look up the email template to use
  v_template_id2      CONSTANT email_template.id%type := 12;
  v_offer_rec offer%ROWTYPE;
  v_gatorlink_email email_addr.eml_addr%TYPE;
  v_email_addr_types objectidlist := objectidlist(1,90);
  v_proc_nm CONSTANT VARCHAR2(50) := 'email_ua_pkg.send_contu_need_med_doc_stud';
  v_success_msg VARCHAR2(100) := 'Email sent to student successfully';
BEGIN
  v_offer_rec := offer_pkg.get_offer(p_offer_id);
  IF(v_offer_rec.id IS NULL) THEN
    apps.logger.error(
      'Trying to send email for offer but no offer found with id: ' || p_offer_id);
    RAISE_APPLICATION_ERROR(-20101, 'Required data Offer id invalid');
  END IF;
  -- Controls who email is sent to
  v_to := person_pkg.get_email_addresses_in_array(v_offer_rec.prsn_id,v_email_addr_types);
  BEGIN
    -- get the template and subject from the database for this email
    apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
      p_subject => v_subject, p_from => v_from) ;
    apps.email_pkg.get_template(p_tmplt_id => v_template_id2, p_body => v_body2,
      p_subject => v_subject2, p_from => v_from2) ;
  EXCEPTION
  WHEN OTHERS THEN
    raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
  END;
  BEGIN
    -- set the values for the tags
    -- this template uses a subtemplate
    v_body := REPLACE(v_body, '#SUB_TEMPLATE#',v_body2) ;
    v_body := REPLACE(v_body,
      email_pkg.c_full_name_tag, person_pkg.get_name_initcap(v_offer_rec.prsn_id)) ;
    v_body := REPLACE(v_body, '#MED_AND_DISAB_URL#',ext_link_pkg.get_link(17)) ;
    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());
    apex_debug_message.log_message(v_proc_nm || ' populated tags successfully', false, 4);
  EXCEPTION
  WHEN OTHERS THEN
    apex_debug_message.log_message(v_proc_nm ||
      ' error replacing tags in template' || sqlerrm, false, 4);
    raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
  END;
  email_pkg.send(p_to => v_to, p_cc => v_cc, p_bcc => v_bcc, p_from => v_from,
    p_subject => v_subject, p_html => v_body, p_prsn_id => v_offer_rec.prsn_id,
    p_app_alias => null, p_stud_viewable_ind => 'Y',
    p_email_id => v_new_email_id);
--  dbms_output.put_line(v_body) ;
  apex_debug_message.log_message(v_proc_nm || ' ' || v_success_msg, false, 4);
END send_contu_need_med_doc_stud;
--------------------------------------------------------
--    DDL for send_contu_need_parent_sig
--------------------------------------------------------
PROCEDURE send_contu_need_parent_sig(
    p_offer_id offer.id%TYPE,
    p_testing BOOLEAN DEFAULT FALSE)
AS
  v_new_email_id email.id%type;
  v_body CLOB;
  v_subject apps.email_pkg.t_subject;
  v_from apps.email_pkg.t_from;
  v_to apps.email_pkg.recip_array;
  v_cc apps.email_pkg.recip_array;
  v_bcc apps.email_pkg.recip_array;
  -- Look up the email template to use
  v_template_id       CONSTANT email_template.id%type := 11;
  v_full_nm VARCHAR2(200);
  v_offer_rec offer%ROWTYPE;
  v_email_addr_types objectidlist := objectidlist(17,200);
  v_proc_nm CONSTANT VARCHAR2(50) := 'email_ua_pkg.send_contu_need_parent_sig ';
  v_success_msg VARCHAR2(100) := 'Email sent to parent successfully';

BEGIN
  apps.logger.info(v_proc_nm || 'Starting');
  v_offer_rec := offer_pkg.get_offer(p_offer_id);

  IF(v_offer_rec.id IS NULL) THEN
    apps.logger.error(
      v_proc_nm || ' Trying to send email for offer but no offer found with id: ' || p_offer_id);
    RAISE_APPLICATION_ERROR(-20101, 'Required data Offer id invalid');
  END IF;
  -- Controls who email is sent to
  v_to := person_pkg.get_email_addresses_in_array(v_offer_rec.prsn_id,v_email_addr_types);
  apps.logger.trace(v_proc_nm || 'Sending email to parent at:' || v_to(1));
  BEGIN
    -- get the template and subject from the database for this email
    apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
      p_subject => v_subject, p_from => v_from) ;
  EXCEPTION
  WHEN OTHERS THEN
    raise_application_error( - 20000, v_proc_nm ||
      ' No template exist for the requested template: ' ||
      v_template_id || ' Error:' ||sqlerrm) ;
  END;
  BEGIN
    v_full_nm := person_pkg.get_name_initcap(v_offer_rec.prsn_id);
    -- set the values for the tags
    v_subject := REPLACE(v_subject, email_pkg.c_full_name_tag, v_full_nm);
    v_body := REPLACE(v_body,
      email_pkg.c_eq_item_due, to_char(v_offer_rec.due_dt,'MM/DD/YYYY HH:MIpm')) ;
    v_body := REPLACE(v_body,
      email_pkg.c_ufid_tag, person_pkg.get_ufid(v_offer_rec.prsn_id)) ;
    v_body := REPLACE(v_body, email_pkg.c_full_name_tag,v_full_nm) ;
    SYS.dbms_output.put_line('1');
    v_body := REPLACE(v_body, '#TERM_GROUP#',v_offer_rec.trm_grp_full_nm);
    v_body := REPLACE(v_body, '#TERM_AND_COND_LINK#',ext_link_pkg.get_formal_link(20));
    v_body := REPLACE(v_body,
      '#SPEC_CD#',agreement_pkg.get_special_code(offer_pkg.get_agree_id(v_offer_rec.id)));
    SYS.dbms_output.put_line('1.1');

    v_body := REPLACE(v_body,
      '#PARENT_SIGNATURE_LINK#', apps.url_redirect_pkg.prepare_url(
      p_url=>'f?p=650:6:' || chr(38) || 'SESSION.::::P6_AGREE_ID:' ||
      offer_pkg.get_agree_id(v_offer_rec.id), p_expire_dt=>null)) ;

    SYS.dbms_output.put_line('2');
    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());
    apex_debug_message.log_message(v_proc_nm ||
      ' populated tags successfully', false, 4);
  EXCEPTION
  WHEN OTHERS THEN
    apex_debug_message.log_message(v_proc_nm ||
      ' error replacing tags in template' || sqlerrm, false, 4);
    raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
  END;
  IF(NOT p_testing) THEN
    -- This one does not show up in students email history
    wwv_flow_api.set_security_group_id(p_security_group_id =>
      apex_util.find_security_group_id(p_workspace => 'HMS'));
    email_pkg.send(p_to => v_to, p_cc => v_cc, p_bcc => v_bcc, p_from => v_from,
      p_subject => v_subject, p_html => v_body, p_prsn_id => v_offer_rec.prsn_id,
      p_app_alias => NULL, p_stud_viewable_ind => 'N', p_email_id => v_new_email_id);
  ELSE
    dbms_output.put_line(v_body) ;
  END IF;
  apps.logger.info(v_proc_nm || ' Done ' || v_success_msg);
END send_contu_need_parent_sig;

--------------------------------------------------
--------------------------------------------------
  PROCEDURE send_cki_appt(
    p_config_id checkin_appt_config.id%TYPE
  , p_time_id   checkin_appt_time.id%TYPE
  , p_asg_id    assign.id%TYPE)
  AS
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject ;
    v_body  VARCHAR2(32767);

    v_email_addr_types objectidlist := objectidlist(1,90);
    v_template_id CONSTANT email_template.ID%TYPE := 650;
    v_new_email_id email.id%type;

    v_config      checkin_appt_config%ROWTYPE;
    v_time        v_checkin_appt_time%ROWTYPE;
    v_person_id   person.id%TYPE;
    v_person      person%ROWTYPE;
    v_building    building%ROWTYPE;
  BEGIN
    v_person_id := assign_pkg.get_person(p_asg_id);
    v_person := person_cru.get_person(v_person_id);
    v_time  := checkin_appt_time_cru.get_v_checkin_appt_time(p_time_id);
    v_config := checkin_appt_config_cru.get_checkin_appt_config(p_config_id);
    v_building := building_cru.get_building(v_config.bldg_id);

    v_to := person_pkg.get_email_addresses_in_array(v_person_id,v_email_addr_types);

    BEGIN
      -- get the template from the database
      apps.email_pkg.get_template(p_tmplt_id => v_template_id,
        p_body => v_body, p_subject => v_subject, p_from=>v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template with id: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
        -- set the values for the tags
      v_body := REPLACE(v_body, email_pkg.c_name_tag, INITCAP(v_person.first_name));
      v_body := REPLACE(v_body,
        email_pkg.c_location, v_building.title);
      v_body := REPLACE(v_body,
        email_pkg.c_date_tag, to_char(v_config.checkin_dt, 'MM/DD/YYYY'));
      v_body := REPLACE(v_body, email_pkg.c_time_tag, v_time.checkin_time_period);

      v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

--    BEGIN
      DBMS_OUTPUT.PUT_LINE('Subject: ' || v_subject);
      DBMS_OUTPUT.PUT_LINE('From address: ' || v_from);
      DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) ||
        ' with person id:' || v_person_id);
      DBMS_OUTPUT.PUT_LINE(v_body);
      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_person_id,
           p_app_alias => utility_pkg.get_system,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id);

--    EXCEPTION
--      WHEN OTHERS THEN
--        raise_application_error( - 20000,
--        'Problem sending out email: '|| sqlerrm) ;
--    END;
  END;

  /**
*
* Called from page 502/92 when users submit a transfer reguest
* sending room transfer request confirmation email to UA students and UA admin
* staff
*/
  PROCEDURE send_trans_req_created(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type
  ) AS

    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    v_template_id                CONSTANT email_template.id%type := 823;
    v_name_tag                   CONSTANT VARCHAR(25) DEFAULT '#NAME#';
    v_curr_asg_tag               CONSTANT VARCHAR(100) DEFAULT '#CURR_ASG_INFO#';
    v_req_mov_dt_tag             CONSTANT VARCHAR(25) DEFAULT '#REQ_MOVE_DATE#';
    v_trans_rsn_type_tag         CONSTANT VARCHAR(25) DEFAULT '#TRANS_RSN_TYPE#';
    v_trans_rsn_tag              CONSTANT VARCHAR(25) DEFAULT '#TRANS_REASON#';
    v_pref_tag                   CONSTANT VARCHAR(25) DEFAULT '#PREFERENCES#';
    v_fee_waiver_tag             CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVER#';
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    v_prsn_id                    person.ID%TYPE;
    v_trans_rec                  trans_req%rowtype;
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_reason_type varchar2(50) :='' ;
    v_fee_waiver varchar2(5) :='No';

  BEGIN
    v_trans_rec := trans_req_cru.get_trans_req(p_trans_req_id);
    v_prsn_id := p_prsn_id;
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      -- set the values for the tags
      if v_trans_rec.trn_fee_waiver ='Y' then v_fee_waiver := 'Yes' ;
            else v_fee_waiver := 'No' ;
      end if;

      select title into v_reason_type
      from hmsdata.trans_req_rsn_type where id=v_trans_rec.trn_rq_rsn_t_id;

      v_body := REPLACE(v_body, v_name_tag, person_pkg.get_name_initcap(v_prsn_id)) ;
      v_body := REPLACE(v_body, v_curr_asg_tag,p_current_assignment) ;
      v_body := REPLACE(v_body,
        v_req_mov_dt_tag,to_char(v_trans_rec.prefer_trn_date,'MM/DD/YYYY')) ;
      v_body := REPLACE(v_body, v_trans_rsn_type_tag,v_reason_type) ;
      v_body := REPLACE(v_body, v_trans_rsn_tag,v_trans_rec.rsn_descr) ;
      v_body := REPLACE(v_body,
        v_pref_tag,trans_req_pkg.disp_trans_req_preference(v_trans_rec,0)) ;
      v_body := REPLACE(v_body, v_fee_waiver_tag,v_fee_waiver ) ;
      v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END send_trans_req_created;

   /**
*
* Called from page 502/94 when user accept an offer to the submitted transfer
* reguest sending room transfer offer acceptance confirmation email to UA
* students and UA admin staff
*/
  PROCEDURE send_trans_offer_accepted(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2,
      p_transfer_fee varchar2,
      p_rent_diff varchar2
  ) AS

    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    v_template_id                CONSTANT email_template.id%type := 822;
    v_name_tag                   CONSTANT VARCHAR(25) DEFAULT '#NAME#';
    v_curr_asg_tag               CONSTANT VARCHAR(100) DEFAULT '#CURR_ASG_INFO#';
    v_req_mov_dt_tag             CONSTANT VARCHAR(25) DEFAULT '#REQ_MOVE_DATE#';
    v_trans_rsn_type_tag         CONSTANT VARCHAR(25) DEFAULT '#TRANS_RSN_TYPE#';
    v_trans_rsn_tag              CONSTANT VARCHAR(25) DEFAULT '#TRANS_REASON#';
    v_pref_tag                   CONSTANT VARCHAR(25) DEFAULT '#PREFERENCES#';
    v_fee_waiver_tag             CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVER#';
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    v_new_assign_tag             CONSTANT VARCHAR(100) DEFAULT '#NEW_ASSIGN#';
    v_transfer_fee_tag           CONSTANT VARCHAR(25) DEFAULT '#TRANSFER_FEE#';
    v_rent_diff_tag              CONSTANT VARCHAR(25) DEFAULT '#RENT_DIFF#';
    v_approved_move_date_tag     CONSTANT VARCHAR(25) DEFAULT '#APPROVED_MOVE_DATE#';
    v_prsn_id                    person.ID%TYPE;
    v_trans_rec                  trans_req%rowtype;
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_reason_type varchar2(50) :='' ;
    v_fee_waiver varchar2(5) :='No';

  BEGIN
    v_trans_rec := trans_req_cru.get_trans_req(p_trans_req_id);
    v_prsn_id := p_prsn_id;
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      -- set the values for the tags
      if v_trans_rec.trn_fee_waiver ='Y' then v_fee_waiver := 'Yes' ;
            else v_fee_waiver := 'No' ;
      end if;

      select title into v_reason_type
      from hmsdata.trans_req_rsn_type where id=v_trans_rec.trn_rq_rsn_t_id;

      v_body := REPLACE(v_body, v_name_tag, person_pkg.get_name_initcap(v_prsn_id)) ;
      v_body := REPLACE(v_body, v_curr_asg_tag,p_current_assignment) ;
      v_body := REPLACE(v_body,
        v_req_mov_dt_tag,to_char(v_trans_rec.prefer_trn_date,'MM/DD/YYYY')) ;
      v_body := REPLACE(v_body, v_trans_rsn_type_tag,v_reason_type) ;
      v_body := REPLACE(v_body, v_trans_rsn_tag,v_trans_rec.rsn_descr) ;
      v_body := REPLACE(v_body,
        v_pref_tag,trans_req_pkg.disp_trans_req_preference(v_trans_rec,0)) ;
      v_body := REPLACE(v_body, v_fee_waiver_tag,v_fee_waiver ) ;
      v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());
      v_body := REPLACE(v_body, v_new_assign_tag,p_new_assgin ) ;
      v_body := REPLACE(v_body, v_transfer_fee_tag,p_transfer_fee ) ;
      v_body := REPLACE(v_body,
        v_rent_diff_tag,p_rent_diff ) ;
      v_body := REPLACE(v_body,
        v_approved_move_date_tag ,to_char(v_trans_rec.approved_move_dt,'MM/DD/YYYY') ) ;

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END ;


     /**
*
* Called from page 502/97 when user reject an offer to the submitted transfer
* reguest sending room transfer offer rejection confirmation email to UA
* students and UA admin staff
*/
  PROCEDURE send_trans_offer_rejected(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2
  ) AS

    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    v_template_id                CONSTANT email_template.id%type := 821;
    v_name_tag                   CONSTANT VARCHAR(25) DEFAULT '#NAME#';
    v_curr_asg_tag               CONSTANT VARCHAR(100) DEFAULT '#CURR_ASG_INFO#';
    v_req_mov_dt_tag             CONSTANT VARCHAR(25) DEFAULT '#REQ_MOVE_DATE#';
    v_trans_rsn_type_tag         CONSTANT VARCHAR(25) DEFAULT '#TRANS_RSN_TYPE#';
    v_trans_rsn_tag              CONSTANT VARCHAR(25) DEFAULT '#TRANS_REASON#';
    v_pref_tag                   CONSTANT VARCHAR(25) DEFAULT '#PREFERENCES#';
    v_fee_waiver_tag             CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVER#';
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    v_new_assign_tag             CONSTANT VARCHAR(100) DEFAULT '#NEW_ASSIGN#';
    v_approved_move_date_tag     CONSTANT VARCHAR(25) DEFAULT '#APPROVED_MOVE_DATE#';
    v_prsn_id                    person.ID%TYPE;
    v_trans_rec                  trans_req%rowtype;
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_reason_type varchar2(50) :='' ;
    v_fee_waiver varchar2(5) :='No';

  BEGIN
    v_trans_rec := trans_req_cru.get_trans_req(p_trans_req_id);
    v_prsn_id := p_prsn_id;
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
      p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      -- set the values for the tags
      if v_trans_rec.trn_fee_waiver ='Y' then v_fee_waiver := 'Yes' ;
            else v_fee_waiver := 'No' ;
      end if;

      select title into v_reason_type
      from hmsdata.trans_req_rsn_type where id=v_trans_rec.trn_rq_rsn_t_id;

      v_body := REPLACE(v_body, v_name_tag, person_pkg.get_name_initcap(v_prsn_id)) ;
      v_body := REPLACE(v_body, v_curr_asg_tag,p_current_assignment) ;
      v_body := REPLACE(v_body,
        v_req_mov_dt_tag,to_char(v_trans_rec.prefer_trn_date,'MM/DD/YYYY')) ;
      v_body := REPLACE(v_body, v_trans_rsn_type_tag,v_reason_type) ;
      v_body := REPLACE(v_body, v_trans_rsn_tag,v_trans_rec.rsn_descr) ;
      v_body := REPLACE(v_body,
        v_pref_tag,trans_req_pkg.disp_trans_req_preference(v_trans_rec,0)) ;
      v_body := REPLACE(v_body, v_fee_waiver_tag,v_fee_waiver ) ;
      v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());
      v_body := REPLACE(v_body, v_new_assign_tag,p_new_assgin ) ;
      v_body := REPLACE(v_body,
        v_approved_move_date_tag ,to_char(v_trans_rec.approved_move_dt,'MM/DD/YYYY') ) ;

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END ;

    /**
*
* Called from page 501/97 when staff approve a ua room transfer reguest and
* offer a space sending room transfer request approve/offer confirmation email
* to UA students
*/
  PROCEDURE send_trans_req_approved(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type,
      p_new_assgin varchar2,
      p_transfer_fee varchar2,
      p_rent_diff varchar2
  ) AS

    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    v_template_id                CONSTANT email_template.id%type := 820;
    v_name_tag                   CONSTANT VARCHAR(25) DEFAULT '#NAME#';
    v_curr_asg_tag               CONSTANT VARCHAR(100) DEFAULT '#CURR_ASG_INFO#';
    v_req_mov_dt_tag             CONSTANT VARCHAR(25) DEFAULT '#REQ_MOVE_DATE#';
    v_trans_rsn_type_tag         CONSTANT VARCHAR(25) DEFAULT '#TRANS_RSN_TYPE#';
    v_trans_rsn_tag              CONSTANT VARCHAR(25) DEFAULT '#TRANS_REASON#';
    v_pref_tag                   CONSTANT VARCHAR(25) DEFAULT '#PREFERENCES#';
    v_fee_waiver_tag             CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVER#';
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    v_new_assign_tag             CONSTANT VARCHAR(100) DEFAULT '#NEW_ASSIGN#';
    v_transfer_fee_tag           CONSTANT VARCHAR(25) DEFAULT '#TRANSFER_FEE#';
    v_rent_diff_tag              CONSTANT VARCHAR(25) DEFAULT '#RENT_DIFF#';
    v_approved_move_date_tag     CONSTANT VARCHAR(25) DEFAULT '#APPROVED_MOVE_DATE#';
    v_prsn_id                    person.ID%TYPE;
    v_trans_rec                  trans_req%rowtype;
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_reason_type varchar2(50) :='' ;
    v_fee_waiver varchar2(5) :='No';

  BEGIN
    v_trans_rec := trans_req_cru.get_trans_req(p_trans_req_id);
    v_prsn_id := p_prsn_id;
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      -- set the values for the tags
      if v_trans_rec.trn_fee_waiver ='Y' then v_fee_waiver := 'Yes' ;
            else v_fee_waiver := 'No' ;
      end if;

      select title into v_reason_type
      from hmsdata.trans_req_rsn_type where id=v_trans_rec.trn_rq_rsn_t_id;

      v_body := REPLACE(v_body, v_name_tag, person_pkg.get_name_initcap(v_prsn_id)) ;
      v_body := REPLACE(v_body, v_curr_asg_tag,p_current_assignment) ;
      v_body := REPLACE(v_body,
        v_req_mov_dt_tag,to_char(v_trans_rec.prefer_trn_date,'MM/DD/YYYY')) ;
      v_body := REPLACE(v_body, v_trans_rsn_type_tag,v_reason_type) ;
      v_body := REPLACE(v_body, v_trans_rsn_tag,v_trans_rec.rsn_descr) ;
      v_body := REPLACE(v_body,
        v_pref_tag,trans_req_pkg.disp_trans_req_preference(v_trans_rec,0)) ;
      v_body := REPLACE(v_body, v_fee_waiver_tag,v_fee_waiver ) ;
      v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());
      v_body := REPLACE(v_body, v_new_assign_tag,p_new_assgin ) ;
      v_body := REPLACE(v_body, v_transfer_fee_tag,p_transfer_fee ) ;
      v_body := REPLACE(v_body, v_rent_diff_tag,p_rent_diff ) ;
      v_body := REPLACE(v_body,
        v_approved_move_date_tag ,to_char(v_trans_rec.approved_move_dt,'MM/DD/YYYY') ) ;

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END ;

   /**
*
* Called from page 501/97 when staff denys a room transfer request
* sending room transfer request denial confirmation email to UA students and
* UA admin staff
*/
  PROCEDURE send_trans_req_denied(
      p_trans_req_id trans_req.id%type,
      p_app_alias trans_req.updt_sys%type,
      p_current_assignment varchar2,
      p_prsn_id person.id%type
  ) AS

    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    v_template_id                CONSTANT email_template.id%type := 819;
    v_name_tag                   CONSTANT VARCHAR(25) DEFAULT '#NAME#';
    v_curr_asg_tag               CONSTANT VARCHAR(100) DEFAULT '#CURR_ASG_INFO#';
    v_req_mov_dt_tag             CONSTANT VARCHAR(25) DEFAULT '#REQ_MOVE_DATE#';
    v_trans_rsn_type_tag         CONSTANT VARCHAR(25) DEFAULT '#TRANS_RSN_TYPE#';
    v_trans_rsn_tag              CONSTANT VARCHAR(25) DEFAULT '#TRANS_REASON#';
    v_pref_tag                   CONSTANT VARCHAR(25) DEFAULT '#PREFERENCES#';
    v_fee_waiver_tag             CONSTANT VARCHAR(25) DEFAULT '#FEE_WAIVER#';
    v_deny_reason_tag            CONSTANT VARCHAR(25) DEFAULT '#DENY_REASON#';
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    v_prsn_id                    person.ID%TYPE;
    v_trans_rec                  trans_req%rowtype;
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_reason_type varchar2(50) :='' ;
    v_fee_waiver varchar2(5) :='No';

  BEGIN
    v_trans_rec := trans_req_cru.get_trans_req(p_trans_req_id);
    v_prsn_id := p_prsn_id;
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      -- set the values for the tags
      if v_trans_rec.trn_fee_waiver ='Y' then v_fee_waiver := 'Yes' ;
            else v_fee_waiver := 'No' ;
      end if;

      select title into v_reason_type
      from hmsdata.trans_req_rsn_type where id=v_trans_rec.trn_rq_rsn_t_id;

      v_body := REPLACE(v_body, v_name_tag, person_pkg.get_name_initcap(v_prsn_id)) ;
      v_body := REPLACE(v_body, v_curr_asg_tag,p_current_assignment) ;
      v_body := REPLACE(v_body,
        v_req_mov_dt_tag,to_char(v_trans_rec.prefer_trn_date,'MM/DD/YYYY')) ;
      v_body := REPLACE(v_body, v_trans_rsn_type_tag,v_reason_type) ;
      v_body := REPLACE(v_body, v_trans_rsn_tag,v_trans_rec.rsn_descr) ;
      v_body := REPLACE(v_body, v_pref_tag,
        trans_req_pkg.disp_trans_req_preference(v_trans_rec,0)) ;
      v_body := REPLACE(v_body, v_fee_waiver_tag,v_fee_waiver ) ;
      v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());
      v_body := REPLACE(v_body, v_deny_reason_tag,v_trans_rec.deny_rsn_descr) ;


      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_prsn_id,
           p_app_alias => p_app_alias,
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END send_trans_req_denied;
-------------------------------------
-------------------------------------

  FUNCTION text_replace(
    p_body        VARCHAR2
  , p_first_name  VARCHAR2
  , p_full_name   VARCHAR2
  , p_term        VARCHAR2
  , p_reason      VARCHAR2
  , p_amount      VARCHAR2
  , p_docs        VARCHAR2
  , p_ufid        VARCHAR2
  , p_address     VARCHAR2
  )RETURN VARCHAR2 AS
    v_body    VARCHAR2(32767);
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
  BEGIN
    apps.logger.debug('Entered text_replace.');
    v_body := p_body;
    v_body := REPLACE(v_body, email_pkg.c_first_name_tag, p_first_name);
    v_body := REPLACE(v_body, email_pkg.c_full_name_tag, p_full_name);
    v_body := REPLACE(v_body, email_pkg.c_term_tag, p_term);
    v_body := REPLACE(v_body, email_pkg.c_reason_tag , p_reason);
    v_body := REPLACE(v_body, c_hmssig, email_pkg.get_signature());
    v_body := REPLACE(v_body, email_pkg.c_amount, p_amount);
    v_body := REPLACE(v_body, c_docs_tag, p_docs);
    v_body := REPLACE(v_body, email_pkg.c_ufid_tag, p_ufid);
    v_body := REPLACE(v_body, email_pkg.c_address, p_address);

    apps.logger.debug('Exited text_replace.');
    RETURN v_body;
  END;

-------------------------------------
  FUNCTION get_location(
    p_conu_id   contract_ua.id%TYPE
  ) RETURN VARCHAR2 AS
    v_loc   VARCHAR2(32767);
  BEGIN
    apps.logger.debug('Entered get location.');

    SELECT bldg.title || ' - ' || rm.rm_num INTO v_loc
    FROM contract_ua cnt
    INNER JOIN (
      SELECT row_number() OVER (PARTITION BY conu_id ORDER BY begin_dt DESC) rn, ag.*
      FROM assign ag
      WHERE conu_id = p_conu_id
        AND deleted_ind = 'N'
    ) asg
      ON asg.conu_id = cnt.id
        AND asg.deleted_ind = 'N'
        AND asg.rn = 1
    INNER JOIN space_room_term srt
      ON asg.sp_rm_trm_id = srt.id AND srt.deleted_ind = 'N'
    INNER JOIN space sp
      ON srt.sp_id = sp.id
    INNER JOIN bedroom bd
      ON sp.bedrm_id = bd.id
    INNER JOIN room rm
      ON bd.rm_id = rm.id
    INNER JOIN building bldg
      ON rm.bldg_id = bldg.id
    WHERE cnt.id = p_conu_id;

    apps.logger.debug('Exited get location.');
    RETURN v_loc;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      apps.logger.debug('Exited get location with exception.');
      RETURN 'N/A';
  END;

-------------------------------------
  FUNCTION compute_text_replace_values(
    p_body        VARCHAR2
  , p_prsn_id     person.id%TYPE
  , p_req         cancel_req%ROWTYPE
  )RETURN VARCHAR2 AS
    v_ufid                person.ufid%TYPE;
    v_amount_txt          VARCHAR2(32767);
    v_amount              acc_trans.amount%TYPE;
    v_doc_txt             VARCHAR2(32767);
    v_terms               VARCHAR2(32767);
    v_req_rec             cancel_req%rowtype;
    v_cnc_rsn             cancel_rsn_type.title%TYPE;
    v_conu_id             contract_ua.id%TYPE;
    v_loc                 VARCHAR2(32767);
  BEGIN
    apps.logger.debug('Enter compute_text_replace_values');

    v_ufid  := person_pkg.get_ufid(p_prsn_id);
--    v_req_rec := cancel_req_cru.get_cancel_req(p_cancel_req_id);
    SELECT title INTO v_cnc_rsn FROM cancel_rsn_type WHERE id = p_req.cnc_rsn_t_id;

    apps.logger.debug('Obtained cancel reason title.');

    SELECT conu_id INTO v_conu_id
    FROM (
      SELECT conu_id
      FROM cancel_req_conu rc
      INNER JOIN contract_ua conu
        ON conu.id = rc.conu_id
      WHERE rc.cnc_rq_id = p_req.id
      ORDER BY conu.trm_id
    )WHERE rownum = 1;

    SELECT LISTAGG(trm.title, ',') WITHIN GROUP (ORDER BY conu.trm_id) INTO v_terms
    FROM cancel_req_conu rc
    INNER JOIN contract_ua conu
      ON conu.id = rc.conu_id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE rc.cnc_rq_id = p_req.id;

    apps.logger.debug('Obtained first contract term record.');

    v_amount := p_req.fee_amount;

    apps.logger.debug('Calculated cancel amount.');

    v_amount_txt :=
        CASE
          WHEN v_amount < 0 THEN
            '$' || abs(v_amount) || ' Credit'
          WHEN v_amount > 0 THEN
            '$' || abs(v_amount) || ' Charge'
          WHEN v_amount = 0 THEN
            'No Additional Charge'
        END;

    apps.logger.debug('Calculated amount text.');

    v_doc_txt :=
        CASE
          WHEN p_req.cont_sur_id IS NULL AND p_req.faxed_ind = 'Y'
           AND p_req.mailed_ind = 'N' THEN
            'Documentation Faxed'
          WHEN p_req.cont_sur_id IS NULL AND p_req.faxed_ind = 'N'
           AND p_req.mailed_ind = 'Y' THEN
            'Documentation Mailed'
          WHEN p_req.cont_sur_id IS NULL AND p_req.faxed_ind = 'Y'
           AND p_req.mailed_ind = 'Y' THEN
            'Documentation Mailed ' || chr(38) || ' Faxed'
          WHEN p_req.cont_sur_id IS NOT NULL AND p_req.faxed_ind = 'Y'
           AND p_req.mailed_ind = 'N' THEN
            'Documentation Uploaded ' || chr(38) || ' Faxed'
          WHEN p_req.cont_sur_id IS NOT NULL AND p_req.faxed_ind = 'N'
           AND p_req.mailed_ind = 'Y' THEN
            'Documentation Uploaded ' || chr(38) || ' Mailed'
          WHEN p_req.cont_sur_id IS NOT NULL AND p_req.faxed_ind = 'Y'
           AND p_req.mailed_ind = 'Y' THEN
            'Documentation Uploaded, Mailed ' || chr(38) || ' Faxed'
          ELSE
            'Documentation Not Provided'
          END;

    apps.logger.debug('Calculated documentation text.');
    apps.logger.debug('Contract id: ' || v_conu_id);

    v_loc := get_location(v_conu_id);
    apps.logger.debug('Calculated location text.');
    RETURN text_replace(
            p_body, person_pkg.get_first_name(p_prsn_id),
            person_pkg.get_name(p_prsn_id), v_terms,
            v_cnc_rsn, v_amount_txt, v_doc_txt, v_ufid, v_loc);
  END;

-------------------------------------
-------------------------------------
  PROCEDURE send_canc_req(
    p_req             cancel_req%ROWTYPE
  , p_template_id     NUMBER
  ) AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_amount                     acc_trans.amount%TYPE;
    v_docs                       VARCHAR2(300);

    v_agree                      agreement%ROWTYPE;
  BEGIN
    v_agree := agreement_pkg.get_agreement(p_req.agree_id);
    v_to := person_pkg.get_email_addresses_in_array(v_agree.prsn_id,v_email_addr_types);
--    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    BEGIN
      apps.logger.debug('Person: ' || v_agree.prsn_id);
      apps.logger.debug('Agree: ' || p_req.agree_id);
      apps.logger.debug('Cancel Req: ' || p_req.agree_id);
      apps.logger.debug('Body: ' || v_body);

      v_body := compute_text_replace_values(v_body, v_agree.prsn_id, p_req);
      v_subject := REPLACE(v_subject, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_agree.prsn_id));

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => v_agree.prsn_id,
           p_app_alias => 'CNCCONT',
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      dbms_output.put_line(v_body) ;
--    EXCEPTION
--    WHEN OTHERS THEN
--      raise_application_error( - 20000,
--      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;
  END;

-------------------------------------
  PROCEDURE send_canc_req_approved(
    p_req      cancel_req%ROWTYPE
  ) AS
  BEGIN
    send_canc_req(p_req, 145);
  END;

--------------------------------------
  PROCEDURE send_canc_req_w_wh_check(
    p_req      cancel_req%ROWTYPE
  ) AS
  BEGIN
    send_canc_req(p_req, 146);
  END;

--------------------------------------
  PROCEDURE send_canc_req_withdraw(
    p_req      cancel_req%ROWTYPE
  )AS
  BEGIN
    send_canc_req(p_req, 149);
  END;

  --------------------------------------
  PROCEDURE send_canc_req_cnc_admit(
    p_req      cancel_req%ROWTYPE
  )AS
  BEGIN
    send_canc_req(p_req, 150);
  END;

--------------------------------------
  PROCEDURE send_canc_req_approve_job(
    p_req      cancel_req%ROWTYPE
  ) AS
  BEGIN
    send_canc_req(p_req, 147);
  END;

--------------------------------------
  PROCEDURE send_canc_req_deny_job(
    p_req      cancel_req%ROWTYPE
  ) AS
  BEGIN
    send_canc_req(p_req, 148);
  END;


--------------------------------------
-- DDL
--------------------------------------
  FUNCTION canc_ap_text_replace(
    p_body        VARCHAR2
  , p_prsn_id     person.id%TYPE
  , p_req         cancel_req%ROWTYPE
  ) RETURN VARCHAR2 AS
    v_body        VARCHAR2(32767);
    v_cnc_rsn     cancel_rsn_type.title%TYPE;
    v_cnc_rsn_tag   VARCHAR2(32767) := '#REASON#';
    v_fee_tag       VARCHAR2(32767) := '#FEE#';

    v_terms         VARCHAR2(32767);
    v_amount        NUMBER;
  BEGIN
    v_body := p_body;

--    v_req_rec := cancel_req_cru.get_cancel_req(p_cancel_req_id);
    apps.logger.debug('Got Request Record');
    SELECT title INTO v_cnc_rsn FROM cancel_rsn_type WHERE id = p_req.cnc_rsn_t_id;
    apps.logger.debug('Got Cancel Reason Type Text');

    SELECT LISTAGG(trm.title, ',') WITHIN GROUP (ORDER BY trm.id) terms INTO v_terms
    FROM cancel_req_conu rc
    INNER JOIN contract_ua conu
      ON conu.id = rc.conu_id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE rc.cnc_rq_id = p_req.id;

    apps.logger.debug('Got contract terms being cancelled');


    v_body := REPLACE(v_body, v_fee_tag, to_char(p_req.fee_amount, '$999,990.00'));

    apps.logger.debug('Got Cancellation fee amount.');

    v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name_initcap(p_prsn_id)) ;
    v_body := REPLACE(v_body, email_pkg.c_terms_tag, v_terms) ;
    v_body := REPLACE(v_body, v_cnc_rsn_tag, v_cnc_rsn);
    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    IF p_req.move_out_dt IS NULL THEN
      v_body := REPLACE(v_body, '#MOVEOUT#', '');
    ELSE
      v_body := REPLACE(v_body, '#MOVEOUT#', 'You are scheduled to checkout before ' || to_char(p_req.move_out_dt, 'MM/DD/YYYY') || ' at 4:00 pm.');
    END IF;

    apps.logger.debug('Replaced all values');

    RETURN v_body;
  END;


--------------------------------------
-- DDL
--------------------------------------
  FUNCTION canc_ap_text_replace_stf(
    p_body        VARCHAR2
  , p_prsn_id     person.id%TYPE
  , p_req         cancel_req%ROWTYPE
  ) RETURN VARCHAR2 AS
    v_body        VARCHAR2(32767);
    v_cnc_rsn     cancel_rsn_type.title%TYPE;
    v_cnc_rsn_tag   VARCHAR2(32767) := '#REASON#';
    v_fee_tag       VARCHAR2(32767) := '#FEE#';

    v_terms         VARCHAR2(32767);
    v_amount        NUMBER;
  BEGIN
    v_body := p_body;

    apps.logger.debug('Got Request Record');

    apps.logger.debug('Cancel Reason id: ' ||  p_req.cnc_rsn_t_id);
    SELECT title INTO v_cnc_rsn FROM cancel_rsn_type WHERE id = p_req.cnc_rsn_t_id;
    apps.logger.debug('Got Cancel Reason Type Text');

    SELECT LISTAGG(trm.title, ',') WITHIN GROUP (ORDER BY trm.id) terms INTO v_terms
    FROM cancel_req_conu rc
    INNER JOIN contract_ua conu
      ON conu.id = rc.conu_id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE rc.cnc_rq_id = p_req.id;

    apps.logger.debug('Got contract terms being cancelled');

    v_body := REPLACE(v_body, v_fee_tag, to_char(p_req.fee_amount, '$999,990.00'));

    apps.logger.debug('Got Cancellation fee amount.');

    v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name_initcap(p_prsn_id)) ;
    v_body := REPLACE(v_body, email_pkg.c_terms_tag, v_terms) ;
    v_body := REPLACE(v_body, v_cnc_rsn_tag, v_cnc_rsn);
    v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    IF p_req.move_out_dt IS NULL THEN
      v_body := REPLACE(v_body, '#MOVEOUT#', '');
    ELSE
      v_body := REPLACE(v_body, email_pkg.c_moveout, 'Move out date: ' || to_char(p_req.move_out_dt, 'MM/DD/YYYY'));
    END IF;

    apps.logger.debug('Replaced all values');

    RETURN v_body;
  END;


  -------------------------------------
-------------------------------------
  PROCEDURE send_canc_req_ap(
    p_req           cancel_req%ROWTYPE
  , p_template_id   NUMBER
  ) AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_amount                     acc_trans.amount%TYPE;
    v_docs                       VARCHAR2(300);

    v_agree                       agreement%ROWTYPE;
  BEGIN
    v_agree := agreement_pkg.get_agreement(p_req.agree_id);
    v_to := person_pkg.get_email_addresses_in_array(v_agree.prsn_id,v_email_addr_types);

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    apps.logger.debug('Person: ' || v_agree.prsn_id);
    apps.logger.debug('Agree: ' || p_req.agree_id);
    apps.logger.debug('Body: ' || v_body);

    BEGIN
      v_body := canc_ap_text_replace(v_body, v_agree.prsn_id, p_req);
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_agree.prsn_id,
         p_app_alias => 'CNCCONTAP',
         p_stud_viewable_ind => 'Y',
         p_email_id => v_new_email_id) ;
    dbms_output.put_line(v_body) ;
  END;

--------------------------------------------------------
--    DDL
--------------------------------------------------------
  PROCEDURE send_canc_ap_req(
    p_req      cancel_req%ROWTYPE
  )AS
  BEGIN
    send_canc_req_ap(p_req, 151);
  END;
--------------------------------------------------------
--    DDL
--------------------------------------------------------
  PROCEDURE send_canc_ap_req_approved(
    p_req      cancel_req%ROWTYPE
  )AS
  BEGIN
    send_canc_req_ap(p_req, 152);
  END;
--------------------------------------------------------
--    DDL
--------------------------------------------------------
  PROCEDURE send_canc_ap_req_denied(
    p_req      cancel_req%ROWTYPE
  )AS
  BEGIN
    send_canc_req_ap(p_req, 153);
  END;

--------------------------------------------------------
--    DDL
--------------------------------------------------------
  PROCEDURE send_canc_req_accounts(
    p_req      cancel_req%ROWTYPE
  )AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_amount                     acc_trans.amount%TYPE;
    p_template_id                NUMBER := 154;
    v_docs                       VARCHAR2(300);

    v_agree                      agreement%ROWTYPE;
    v_rsn                        cancel_rsn_type%ROWTYPE;
    v_conu                       contract_ua%ROWTYPE;
    v_term                       VARCHAR2(10000);
    v_status                     VARCHAR2(10000);
    v_defer                      VARCHAR2(10000);
    v_adjustment                 VARCHAR2(10000);
  BEGIN
    v_to(1) := 'HREFITSAccountsContractCancel@housing.ufl.edu'; --person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_rsn := cancel_rsn_type_cru.get_cancel_rsn_type(p_req.cnc_rsn_t_id);
    v_agree := agreement_pkg.get_agreement(p_req.agree_id);

    SELECT
      LISTAGG(trm.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) term,
      LISTAGG(cst.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) status,
      LISTAGG(conu.defer_stat, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) defer_stat
    INTO v_term, v_status, v_defer
    FROM agreement agr
    INNER JOIN (SELECT CASE
        WHEN c.defer_t_id IS NOT NULL THEN
          'Rent Deferment'
        WHEN c.arp_defer_t_id IS NOT NULL THEN
          'ARP Deferment'
        ELSE
          'NA'
        END defer_stat, c.* FROM contract_ua c WHERE c.agree_id = p_req.agree_id) conu
      ON conu.agree_id = agr.id
    INNER JOIN cancel_req_conu rcon
      ON rcon.conu_id = conu.id AND rcon.cnc_rq_id = p_req.id
    INNER JOIN contract_stat_type cst
      ON conu.con_st_id = cst.id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE agr.id = p_req.agree_id
--      AND trm.halls_close_dt > sysdate
    GROUP BY agr.id
    ;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    BEGIN
      CASE WHEN p_req.fee_amount > 0 THEN
          v_adjustment := '$'||p_req.fee_amount || ' Charge';
        WHEN p_req.fee_amount = 0 THEN
          v_adjustment := '$0';
        WHEN p_req.fee_amount < 0 THEN
          v_adjustment := '$'|| ABS(p_req.fee_amount) || ' Credit';
      END CASE;

      v_subject := REPLACE(v_subject, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_term_tag, v_term);
      v_body := REPLACE(v_body, email_pkg.c_reason_tag, v_rsn.title || ' - ' || p_req.rsn_text);
      v_body := REPLACE(v_body, email_pkg.c_status, v_status);
      v_body := REPLACE(v_body, email_pkg.c_deferment, v_defer);
      v_body := REPLACE(v_body, email_pkg.c_amount, v_adjustment);
      v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_agree.prsn_id,
         p_app_alias => 'CNCCONTAP',
         p_stud_viewable_ind => 'N',
         p_email_id => v_new_email_id) ;
    dbms_output.put_line(v_body) ;
  END;

-------------------------------------------
  PROCEDURE send_canc_req_accounts_stf(
    p_req      cancel_req%ROWTYPE
  )AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_amount                     acc_trans.amount%TYPE;
    p_template_id                NUMBER := 156;
    v_docs                       VARCHAR2(300);

    v_agree                      agreement%ROWTYPE;
    v_rsn                        cancel_rsn_type%ROWTYPE;
    v_conu                       contract_ua%ROWTYPE;

    v_term                       VARCHAR2(10000);
    v_status                     VARCHAR2(10000);
    v_defer                      VARCHAR2(10000);
    v_adjustment                 VARCHAR2(100);
  BEGIN
    v_to(1) := 'HREFITSAccountsContractCancel@housing.ufl.edu'; --person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_rsn := cancel_rsn_type_cru.get_cancel_rsn_type(p_req.cnc_rsn_t_id);
    v_agree := agreement_pkg.get_agreement(p_req.agree_id);

    --- Tuann 07/11/2017
    --- commented out
          --AND trm.halls_close_dt > sysdate
    SELECT
      LISTAGG(trm.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) term,
      LISTAGG(cst.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) status,
      LISTAGG(conu.defer_stat, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) defer_stat
    INTO v_term, v_status, v_defer
    FROM agreement agr
    INNER JOIN (SELECT CASE
        WHEN c.defer_t_id IS NOT NULL THEN
          'Rent Deferment'
        WHEN c.arp_defer_t_id IS NOT NULL THEN
          'ARP Deferment'
        ELSE
          'NA'
        END defer_stat, c.* FROM contract_ua c WHERE c.agree_id = p_req.agree_id) conu
      ON conu.agree_id = agr.id
    INNER JOIN contract_stat_type cst
      ON conu.con_st_id = cst.id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE agr.ID = p_req.agree_id
      --AND trm.halls_close_dt > sysdate
    GROUP BY agr.id
    ;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    BEGIN

      CASE WHEN p_req.fee_amount > 0 THEN
          v_adjustment := '$'||p_req.fee_amount || ' Charge';
        WHEN p_req.fee_amount = 0 THEN
          v_adjustment := '$0';
        WHEN p_req.fee_amount < 0 THEN
          v_adjustment := '$'|| ABS(p_req.fee_amount) || ' Credit';
      END CASE;

      v_subject := REPLACE(v_subject, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_term_tag, v_term);
      v_body := REPLACE(v_body, email_pkg.c_reason_tag, v_rsn.title || ' - ' || p_req.rsn_text);
      v_body := REPLACE(v_body, email_pkg.c_status, v_status);
      v_body := REPLACE(v_body, email_pkg.c_deferment, v_defer);
      v_body := REPLACE(v_body, email_pkg.c_amount, v_adjustment);

      IF p_req.move_out_dt IS NOT NULL THEN
        v_body := REPLACE(v_body, email_pkg.c_moveout, 'Move out date: ' || to_char(p_req.move_out_dt, 'MM/DD/YYYY'));
      ELSE
        v_body := REPLACE(v_body, email_pkg.c_moveout, '');
      END IF;
      IF p_req.move_out_dt IS NOT NULL THEN
        v_body := REPLACE(v_body, '#BILLING_DATE#', 'Suggested End Billing Date: ' || to_char(p_req.move_out_dt, 'MM/DD/YYYY'));
      ELSE
        v_body := REPLACE(v_body, '#BILLING_DATE#', 'Suggested End Billing Date: Not Provided');
      END IF;

      v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_agree.prsn_id,
         p_app_alias => 'CNCCONTAP',
         p_stud_viewable_ind => 'N',
         p_email_id => v_new_email_id) ;
    dbms_output.put_line(v_body) ;
  END;

-------------------------------------------
-------------------------------------------
  PROCEDURE send_canc_rev_accounts(
    l_ids         objectidlist
  , p_agree       agreement%ROWTYPE
  )AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_email_addr_types           objectidlist := objectidlist(1,90);
    v_amount                     acc_trans.amount%TYPE;
    p_template_id                NUMBER := 157;
    v_docs                       VARCHAR2(300);

--    v_agree                      agreement%ROWTYPE;
    v_conu                       contract_ua%ROWTYPE;
    v_term                       VARCHAR2(10000);
    v_status                     VARCHAR2(10000);
    v_defer                      VARCHAR2(10000);
  BEGIN
    v_to(1) := 'HREFITSAccountsContractCancel@housing.ufl.edu'; --person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
--    v_agree := p_agree;

    SELECT
      LISTAGG(trm.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) term,
      LISTAGG(cst.title, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) status,
      LISTAGG(conu.defer_stat, '/') WITHIN GROUP (ORDER BY trm.halls_open_dt) defer_stat
    INTO v_term, v_status, v_defer
    FROM agreement agr
    INNER JOIN (SELECT CASE
        WHEN c.defer_t_id IS NOT NULL THEN
          'Rent Deferment'
        WHEN c.arp_defer_t_id IS NOT NULL THEN
          'ARP Deferment'
        ELSE
          'NA'
        END defer_stat, c.*
        FROM contract_ua c
        WHERE id IN (SELECT * FROM TABLE(l_ids))
    ) conu
      ON conu.agree_id = agr.id
    INNER JOIN contract_stat_type cst
      ON conu.con_st_id = cst.id
    INNER JOIN term trm
      ON conu.trm_id = trm.id
    WHERE agr.id = p_agree.id
      AND trm.halls_close_dt > sysdate
    GROUP BY agr.id
    ;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    BEGIN
      v_subject := REPLACE(v_subject, email_pkg.c_ufid_tag, person_pkg.get_ufid(p_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name(p_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(p_agree.prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_term_tag, v_term);
      v_body := REPLACE(v_body, email_pkg.c_status, v_status);
      v_body := REPLACE(v_body, email_pkg.c_deferment, v_defer);
      v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());
      v_body := REPLACE(v_body, email_pkg.c_moveout, '');

    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => p_agree.prsn_id,
         p_app_alias => 'CNCCONTAP',
         p_stud_viewable_ind => 'N',
         p_email_id => v_new_email_id) ;
    dbms_output.put_line(v_body) ;
  END;

-------------------------------------------
-------------------------------------------
  PROCEDURE send_canc_stf_req_student(
    p_req     cancel_req%ROWTYPE
  )AS
    v_new_email_id               email.id%type;
    v_body                       CLOB;
    v_from                       apps.email_pkg.t_from;
    v_subject                    apps.email_pkg.t_subject;
    v_to                         apps.email_pkg.recip_array;
    v_cc                         apps.email_pkg.recip_array;
    v_bcc                        apps.email_pkg.recip_array;
    c_hmssig                     CONSTANT VARCHAR(25) DEFAULT '#HMSSIG#';
    c_docs_tag                   CONSTANT VARCHAR(25) DEFAULT '#DOCS_RECEIVED#';
    v_email_addr_types           objectidlist := objectidlist(1,90);
    p_template_id                NUMBER := 155;
    v_amount                     acc_trans.amount%TYPE;
    v_docs                       VARCHAR2(300);

    v_agree                     agreement%ROWTYPE;
  BEGIN

    v_agree := agreement_pkg.get_agreement(p_req.agree_id);
    v_to := person_pkg.get_email_addresses_in_array(v_agree.prsn_id,v_email_addr_types);

--    v_cc := email_pkg.get_department_recip(19) ; --UA Admin
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => p_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || p_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    apps.logger.debug('Person: ' || v_agree.prsn_id);
    apps.logger.debug('Agree: ' || v_agree.id);
    apps.logger.debug('Cancel Req: ' || v_agree.id);
    apps.logger.debug('Body: ' || v_body);

    BEGIN
      v_body := canc_ap_text_replace_stf(v_body, v_agree.prsn_id, p_req);
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_agree.prsn_id,
         p_app_alias => 'CNCCONTAP',
         p_stud_viewable_ind => 'Y',
         p_email_id => v_new_email_id) ;
    dbms_output.put_line(v_body) ;
  END;

--------------------------------------------------------
--    DDL
--------------------------------------------------------
PROCEDURE send_rm_select_confirmed(
    p_sp_rm_trm_id space_room_term.id%TYPE,
    p_prsn_id person.id%TYPE,
    p_testing VARCHAR2 DEFAULT 'N',
    p_only_bldg_type_ind VARCHAR2 DEFAULT 'N'
)
AS
  v_new_email_id email.id%type;
  v_body CLOB;
  v_subject apps.email_pkg.t_subject;
  v_from apps.email_pkg.t_from;
  v_to apps.email_pkg.recip_array;
  v_cc apps.email_pkg.recip_array;
  v_bcc apps.email_pkg.recip_array;
  -- Look up the email template to use
  v_template_id       CONSTANT email_template.id%type := 16;

  v_term_grp_nm offer.trm_grp_full_nm%TYPE;
  v_prsn_nm VARCHAR2(500);
  v_bldg_nm building.title%TYPE;
  v_rm_num room.rm_num%TYPE;
  v_bedroom bedroom.title%TYPE;
  v_rm_type room_type.title%TYPE;
  v_agree_id agreement.id%TYPE;
  v_email_addr_types objectidlist := objectidlist(1,90);
  v_proc_nm CONSTANT VARCHAR2(50) := 'email_ua_pkg.send_rm_select_confirmed';
  v_success_msg VARCHAR2(100) := 'Email sent to student as room selection confirmation sent successfully';

BEGIN
  -- gather up required data
  SELECT
    offer_pkg.get_trm_grp_full_nm(oac.offer_id),
    person_pkg.get_name_initcap(oac.prsn_id),
    ass.bldg_nm hall_name,
    ass.rm_num room_num,
    ass.bedrm_nm,
    r_type.title,
    oac.agree_id
  INTO
    v_term_grp_nm,
    v_prsn_nm,
    v_bldg_nm,
    v_rm_num,
    v_bedroom,
    v_rm_type,
    v_agree_id
  FROM assign ass
  JOIN space_room_term srt
    ON srt.id = ass.sp_rm_trm_id
   AND srt.id = p_sp_rm_trm_id
  JOIN room_term rt
    ON rt.id = srt.rm_trm_id
  JOIN room_type r_type
    ON r_type.id = rt.rm_t_id
  JOIN v_ua_offer_agree_cont oac
    ON oac.conu_id = ass.conu_id
   AND oac.prsn_id = p_prsn_id
   WHERE ass.deleted_ind = 'N';

  -- Controls who email is sent to
  v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
      EXCEPTION WHEN OTHERS THEN
        raise_application_error( - 20000, v_proc_nm ||
        ' No template exist for the requested template: ' ||
        v_template_id || ' Error:' ||sqlerrm) ;
    END;

    BEGIN
      apex_debug_message.log_message(v_proc_nm || ' Starting to replace tags in template');
      v_subject := REPLACE(v_subject, '#TERMS#', v_term_grp_nm);
      v_body    := REPLACE(v_body, '#NAME#', v_prsn_nm);
      v_body    := REPLACE(v_body, '#TERMS#', v_term_grp_nm);
      v_body    := REPLACE(v_body, '#HALL_NAME#', v_bldg_nm);
      IF(p_only_bldg_type_ind = 'Y') THEN
        -- this is for incoming where they only see building and type
        v_body  := REPLACE(v_body, '#ROOM_NUMBER#', '');
        v_body  := REPLACE(v_body, '#BEDROOM#', '');
        v_body  := REPLACE(v_body, '#ROOMMATE_LABEL#','Roommate Group:');
        v_body  := REPLACE(v_body, '#ROOMMATE_INFO#',
            NVL(housingmate_pkg.disp_roommate_grp_members(
                p_prsn_id => p_prsn_id,
                p_agree_id => v_agree_id),'No roommate group.'));
      ELSE
        v_body  := REPLACE(v_body, '#ROOM_NUMBER#', 'Room: '||v_rm_num);
        v_body  := REPLACE(v_body, '#BEDROOM#', v_bedroom);
        v_body  := REPLACE(v_body, '#ROOMMATE_LABEL#','Requested Roommates:');
        v_body  := REPLACE(v_body, '#ROOMMATE_INFO#',
                     room_selection_ua_pkg.get_requested_roommate_info(
                         p_sp_rm_trm_id => p_sp_rm_trm_id,
                         p_held_by_prsn_id => p_prsn_id,
                         p_use_html => 'N'));
      END IF;

      v_body    := REPLACE(v_body, '#ROOM_TYPE#', v_rm_type);

      --apex_debug_message.log_message(v_proc_nm || ' p_sp_rm_trm_id:' || p_sp_rm_trm_id, false, 4);
      --apex_debug_message.log_message(v_proc_nm || ' populated tags successfully', false, 4);
    EXCEPTION
      WHEN OTHERS THEN
        apex_debug_message.log_message(v_proc_nm ||
          ' error replacing tags in template' || sqlerrm, false, 4);
        raise_application_error( - 20000,
          'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

  IF(p_testing = 'N') THEN
    email_pkg.send(p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => p_prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id);
  ELSE
    dbms_output.put_line(v_body) ;
  END IF;

  apps.logger.debug(v_proc_nm || ' ' || v_success_msg);
END send_rm_select_confirmed;
--------------------------------------------------------
--    DDL
--------------------------------------------------------
PROCEDURE send_rmmt_request_notice(
    p_sp_rm_trm_id space_room_term.id%TYPE,
    p_held_by_prsn_id person.id%TYPE,
    p_held_for_prsn_id person.id%TYPE,
    p_testing VARCHAR2 DEFAULT 'N'
)
AS
  v_new_email_id email.id%type;
  v_body CLOB;
  v_subject apps.email_pkg.t_subject;
  v_from apps.email_pkg.t_from;
  v_to apps.email_pkg.recip_array;
  v_cc apps.email_pkg.recip_array;
  v_bcc apps.email_pkg.recip_array;
  -- Look up the email template to use
  v_template_id       CONSTANT email_template.id%type := 17;

  v_sp_rm_trm_id space_room_term.id%TYPE;
  v_term_grp_nm offer.trm_grp_full_nm%TYPE;
  v_requester_nm VARCHAR2(500);
  v_requester_email VARCHAR2(500);
  v_requested_nm VARCHAR2(500);
  v_bldg_nm building.title%TYPE;
  v_rm_num room.rm_num%TYPE;
  v_rm_type room_type.title%TYPE;
  v_trm_id term.id%TYPE;
  v_held_on_ts space_room_term.held_on_ts%TYPE;
  v_email_addr_types objectidlist := objectidlist(1,90);
  v_proc_nm CONSTANT VARCHAR2(50) := 'email_ua_pkg.send_rmmt_request_notice';
  v_success_msg VARCHAR2(100) := 'Email sent to student as roommate request sent successfully';

BEGIN
  apex_debug_message.log_message('p_sp_rm_trm_id:'||p_sp_rm_trm_id||' p_held_by_prsn_id"'||
    p_held_by_prsn_id||' p_held_for_prsn_id:'||p_held_for_prsn_id, false, 4);
  -- gather up required data
  SELECT
      srt.id,
      person_pkg.get_name_initcap(srt.held_by),
      person_pkg.get_name_initcap(srt.held_for),
      srt.held_on_ts,
      rm_info.bldg_title,
      rm_info.rm_num,
      rt.title room_type,
      rm_trm.trm_id,
      person_pkg.get_email_address(p_held_by_prsn_id,1)
    INTO
      v_sp_rm_trm_id,
      v_requester_nm,
      v_requested_nm,
      v_held_on_ts,
      v_bldg_nm,
      v_rm_num,
      v_rm_type,
      v_trm_id,
      v_requester_email
    FROM
      space_room_term srt
    JOIN v_ua_space_room_term rm_info
      ON rm_info.sp_rm_trm_id = srt.id
    JOIN room_term rm_trm
      ON rm_trm.id = srt.rm_trm_id
    JOIN room_type rt
      ON rt.id = rm_trm.rm_t_id
    WHERE
      srt.held_by = p_held_by_prsn_id
      AND srt.held_for = p_held_for_prsn_id
      AND srt.held_by = p_held_by_prsn_id
      AND srt.held_on_ts IS NOT NULL
      AND srt.id = p_sp_rm_trm_id;
--      held_on_ts =
--      (SELECT
--         MAX(srt2.held_on_ts)
--       FROM
--         space_room_term srt2
--       WHERE
--         srt2.held_by = p_held_by_prsn_id
--         AND srt2.held_for = p_held_for_prsn_id
--         AND srt2.held_on_ts IS NOT NULL);

    apex_debug_message.log_message('next', false, 4);
    SELECT
      offer_pkg.get_trm_grp_full_nm(agr.offer_id)
    INTO
      v_term_grp_nm
    FROM
      assign asg
      JOIN contract_ua con
        ON con.ID = asg.conu_id
      JOIN agreement agr
        ON agr.ID = con.agree_id
    WHERE
      agr.prsn_id = p_held_by_prsn_id
      AND con.trm_id = v_trm_id
      AND asg.deleted_ind = 'N';

  -- Controls who email is sent to
  v_to := person_pkg.get_email_addresses_in_array(p_held_for_prsn_id,v_email_addr_types);

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
      EXCEPTION WHEN OTHERS THEN
        raise_application_error( - 20000, v_proc_nm ||
        ' No template exist for the requested template: ' ||
        v_template_id || ' Error:' ||sqlerrm) ;
    END;

    BEGIN
      apex_debug_message.log_message(v_proc_nm || ' Starting to replace tags in template');
      v_subject := REPLACE(v_subject, '#REQUESTER_NAME#', v_requester_nm);
      v_body    := REPLACE(v_body, '#NAME#', v_requested_nm);
      v_body    := REPLACE(v_body, '#REQUESTER_NAME#', v_requester_nm);
      v_body    := REPLACE(v_body, '#EMAIL_ADDRESS#', v_requester_email);
      v_body    := REPLACE(v_body, '#TERMS#', v_term_grp_nm);
      v_body    := REPLACE(v_body, '#HALL_NAME#', v_bldg_nm);
      v_body    := REPLACE(v_body, '#ROOM_NUMBER#', v_rm_num);
      v_body    := REPLACE(v_body, '#ROOM_TYPE#', v_rm_type);
      v_body    := REPLACE(v_body, '#EXPIRE_TIME#', TO_CHAR(TRUNC(v_held_on_ts) - 1,'Day MM/DD/YYYY')||' at midnight');
      apps.logger.trace('a');
      v_body    := REPLACE(v_body, '#ROOM_SELECTION_LINK2#', config_pkg.get_value(
                'HTTP_SERVER') ||config_pkg.get_value('UA_RS_ACCEPT_REJ_RMMT_REQ'));
      v_body := REPLACE(v_body, '#ROOM_SELECTION_LINK#',
          ext_link_pkg.format_link(
            p_url => apps.url_redirect_pkg.prepare_url(
              p_url => config_pkg.get_value(
                'HTTP_SERVER') || '/f?p=502:199:' || v('APP_SESSION') || ':::::'
          ),
        p_label=> 'HERE'));


    apex_debug_message.log_message(v_proc_nm || ' populated tags successfully', false, 4);
    EXCEPTION
      WHEN OTHERS THEN
        apex_debug_message.log_message(v_proc_nm ||
          ' error replacing tags in template' || sqlerrm, false, 4);
        raise_application_error( - 20000,
          'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

  IF(p_testing = 'N') THEN
    email_pkg.send(p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => p_held_for_prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id);
  ELSE
    dbms_output.put_line(v_body) ;
  END IF;

  apex_debug_message.log_message(v_proc_nm || ' ' || v_success_msg, false, 4);
END send_rmmt_request_notice;
--------------------------------------------------------------------------------
-- FLPP                                                                       --
--------------------------------------------------------------------------------
   PROCEDURE send_flpp_noncovered_charge(
      p_trn_id        acc_trans.id%type,
      p_conu_id       contract_ua.id%type,
      p_prsn_id       person.id%type
  ) AS
  v_new_email_id      email.id%type;
  v_body              CLOB;
  v_subject           apps.email_pkg.t_subject;
  v_from              apps.email_pkg.t_from;
  v_template_id       CONSTANT email_template.id%type := 923;
  v_to                apps.email_pkg.recip_array;
  v_cc                apps.email_pkg.recip_array;
  v_bcc               apps.email_pkg.recip_array;
  v_email_addr_types  objectidlist := objectidlist(1,90);
  v_email_addr_parent objectidlist := objectidlist(17,200);
  v_contract          CONTRACT_UA%Rowtype;
  v_acc_trn           ACC_TRANS%Rowtype;
  v_proc              VARCHAR2(100) := 'EMAIL_UA_PKG.send_flpp_noncovered_charge';
  v_parent_txt        VARCHAR2(2000);

  BEGIN

    APEX_DEBUG_MESSAGE.Log_Message(v_proc||' BEGIN '  ,false,4);
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_parent_txt := '<pre>
Dear Family Member of #PERSON_NAME#,

Your Gator recently received the following email.

Go Gators

_______________________________________

</pre>';
    -----------------------------------------------------------------------------
    BEGIN
      SELECT * INTO v_contract
        FROM contract_ua
       WHERE id = p_conu_id;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No contract exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    -----------------------------------------------------------------------------
    -----------------------------------------------------------------------------
    BEGIN
      SELECT * INTO v_acc_trn
        FROM ACC_TRANS
       WHERE id = p_trn_id;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No charge transaction exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    -----------------------------------------------------------------------------
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    v_subject := REPLACE(v_subject, '#TERM_GRPT_NM#', TERM_PKG.get_term_name(v_contract.trm_id));
    v_body := REPLACE(v_body, '#PERSON_NAME#', person_pkg.get_name_initcap(p_prsn_id)) ;
    v_body := REPLACE(v_body, '#REASON#', v_contract.PREPAID_DEC_REASON) ;
    v_body := REPLACE(v_body, '#FLPP#', v_contract.PREPAID_NUM) ;
    v_body := REPLACE(v_body, '#AMOUNT#', to_char(v_acc_trn.AMOUNT,'FML999G999G999G999G990D00')) ;
    v_body := REPLACE(v_body, '#DUE_DATE#', to_char(v_acc_trn.SFA_DUE_DT,'MM/DD/YYYY')) ;
    v_body := REPLACE(v_body, '#CONTRACT_CREATED_DATE#', to_char(v_contract.CREATE_TS,'MM/DD/YYYY')) ;
    v_body := REPLACE(v_body, '#TERM_NAME#', TERM_PKG.get_term_name(v_contract.trm_id)) ;
    --v_body := REPLACE(v_body, '#UFID#', person_pkg.get_name_initcap(p_prsn_id)) ;

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => p_prsn_id,
           p_app_alias => 'FLPP',
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;

      IF hmsdata.person_pkg.get_parent_email_addresses(p_prsn_id,' ') IS NOT NULL THEN
         v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_parent);
         v_body := v_parent_txt||v_body;
         v_body := REPLACE(v_body, '#PERSON_NAME#', person_pkg.get_name_initcap(p_prsn_id)) ;
         email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => p_prsn_id,
           p_app_alias => 'FLPP',
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      END IF;   --- person_pkg.email_parent(p_prsn_id) = 'Y'

    APEX_DEBUG_MESSAGE.Log_Message(v_proc||' END '  ,false,4);
   END send_flpp_noncovered_charge;

   PROCEDURE send_flpp_validated(
      p_trn_id        acc_trans.id%type,
      p_conu_id       contract_ua.id%type,
      p_prsn_id       person.id%type
  ) AS
  v_new_email_id      email.id%type;
  v_body              CLOB;
  v_subject           apps.email_pkg.t_subject;
  v_from              apps.email_pkg.t_from;
  v_template_id       CONSTANT email_template.id%type := 964;
  v_to                apps.email_pkg.recip_array;
  v_cc                apps.email_pkg.recip_array;
  v_bcc               apps.email_pkg.recip_array;
  v_email_addr_types  objectidlist := objectidlist(1,90);
  v_email_addr_parent objectidlist := objectidlist(17,200);
  v_contract          CONTRACT_UA%Rowtype;
  v_acc_trn           ACC_TRANS%Rowtype;
  v_proc              VARCHAR2(100) := 'EMAIL_UA_PKG.send_flpp_noncovered_charge';
  v_parent_txt        VARCHAR2(2000);
  BEGIN

    APEX_DEBUG_MESSAGE.Log_Message(v_proc||' BEGIN '  ,false,4);
    v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_types);
    v_parent_txt := '<pre>
Dear Family Member of #PERSON_NAME#,

Your Gator recently received the following email.

Go Gators

_______________________________________

</pre>';
    -----------------------------------------------------------------------------
    BEGIN
      SELECT * INTO v_contract
        FROM contract_ua
       WHERE id = p_conu_id;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No contract exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    -----------------------------------------------------------------------------
    -----------------------------------------------------------------------------
    BEGIN
      SELECT * INTO v_acc_trn
        FROM ACC_TRANS
       WHERE id = p_trn_id;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No charge transaction exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    -----------------------------------------------------------------------------
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body,
        p_subject => v_subject, p_from => v_from) ;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;
    v_subject := REPLACE(v_subject, '#TERM_GRPT_NM#', TERM_PKG.get_term_name(v_contract.trm_id));
    v_body := REPLACE(v_body, '#PERSON_NAME#', person_pkg.get_name_initcap(p_prsn_id)) ;

    v_body := REPLACE(v_body, '#TERM_NAME#', TERM_PKG.get_term_name(v_contract.trm_id)) ;
    v_body := REPLACE(v_body, '#HMSSIG#', email_pkg.get_signature());

      email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => p_prsn_id,
           p_app_alias => 'FLPP',
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;

      IF hmsdata.person_pkg.get_parent_email_addresses(p_prsn_id,' ') IS NOT NULL THEN
         v_to := person_pkg.get_email_addresses_in_array(p_prsn_id,v_email_addr_parent);
         v_body := v_parent_txt||v_body;
         v_body := REPLACE(v_body, '#PERSON_NAME#', person_pkg.get_name_initcap(p_prsn_id)) ;

         email_pkg.send(p_to => v_to,
           p_cc => v_cc,
           p_bcc => v_bcc,
           p_from => v_from,
           p_subject => v_subject,
           p_html => v_body,
           p_prsn_id => p_prsn_id,
           p_app_alias => 'FLPP',
           p_stud_viewable_ind => 'Y',
           p_email_id => v_new_email_id) ;
      END IF;   --- person_pkg.email_parent(p_prsn_id) = 'Y'
    APEX_DEBUG_MESSAGE.Log_Message(v_proc||' END '  ,false,4);
  END send_flpp_validated;

--------------------------------------------------------
--    DDL for send_rmsel_on_notice
--------------------------------------------------------
  PROCEDURE send_rmsel_on_notice(
      p_conu_id contract_ua.id%TYPE
  ) AS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1;

    v_email_addr_types objectidlist := objectidlist(1,90);
    v_email_addr_parent_stud objectidlist := objectidlist(1,90,17,200);

    v_gatorlink_email email_addr.eml_addr%TYPE;
    v_url_term VARCHAR2( 500);
    v_url_select_apt VARCHAR2( 500);
    v_url_pay_app_fee VARCHAR2( 500);

    v_req_row     contract_change_req%ROWTYPE;
    v_ufid        person.ufid%TYPE;
    v_conu_rec    contract_ua%ROWTYPE;
    v_agree_rec   agreement%ROWTYPE;
    v_prsn_rec    person%ROWTYPE;
  BEGIN
    -- get contract agreement and person
    v_conu_rec := contract_ua_pkg.get_contract_ua(p_conu_id);
    -- get agreement
    v_agree_rec := agreement_pkg.get_agreement(v_conu_rec.agree_id);
    -- get person
    v_prsn_rec := person_cru.get_person(v_agree_rec.prsn_id);

    -- Controls who email is sent to, if they are supposed to include parent
    -- on all emails then use the other object type list.
    IF(person_pkg.email_parent(v_agree_rec.prsn_id) = 'N') THEN
      v_to := person_pkg.get_email_addresses_in_array(v_prsn_rec.id,v_email_addr_types);
    ELSE
      v_to := person_pkg.get_email_addresses_in_array(v_prsn_rec.id,v_email_addr_parent_stud);
    END IF;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(
        p_tmplt_id => v_template_id,
        p_body => v_body,
        p_subject => v_subject,
        p_from => v_from
      );
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    -- set the values for the tags
    BEGIN
      NULL; -- nothing right now
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    -- SEND OUT THE EMAIL
    BEGIN
      wwv_flow_api.set_security_group_id(p_security_group_id =>
          apex_util.find_security_group_id(p_workspace => 'HMS'));
      email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_prsn_rec.id,
         p_app_alias => v('APP_ALIAS'),
         p_stud_viewable_ind => 'Y',
         p_email_id => v_new_email_id) ;
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem actually sending the email: '||sqlerrm) ;
    END;
  END;

--------------------------------------------------------
--    DDL for send_secured_deferment
--------------------------------------------------------
  PROCEDURE send_secured_deferment(
    p_prsn_id   person.id%TYPE
  , p_conu_id   contract_ua.id%TYPE
  )AS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 28;

    v_email_addr_types objectidlist := objectidlist(1,90);
    v_email_addr_parent_stud objectidlist := objectidlist(1,90,17,200);

    v_gatorlink_email email_addr.eml_addr%TYPE;
    v_url_term VARCHAR2(500);
    v_url_select_apt VARCHAR2(500);
    v_url_pay_app_fee VARCHAR2(500);

    v_ufid        person.ufid%TYPE;
    v_conu        contract_ua%ROWTYPE;
    v_term        term%ROWTYPE;
    v_prsn        person%ROWTYPE;
    v_agree       agreement%ROWTYPE;
    v_offer_due_dt       offer.due_dt%TYPE;
  BEGIN
    -- get contract agreement and person
    v_conu := contract_ua_pkg.get_contract_ua(p_conu_id);
    -- get person
    v_prsn := person_cru.get_person(p_prsn_id);
    v_term := term_pkg.get_term(v_conu.trm_id);

    -- Controls who email is sent to, if they are supposed to include parent
    -- on all emails then use the other object type list.
    IF(person_pkg.email_parent(p_prsn_id) = 'N') THEN
      v_to := person_pkg.get_email_addresses_in_array(v_prsn.id,v_email_addr_types);
    ELSE
      v_to := person_pkg.get_email_addresses_in_array(v_prsn.id,v_email_addr_parent_stud);
    END IF;

    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(
        p_tmplt_id => v_template_id,
        p_body => v_body,
        p_subject => v_subject,
        p_from => v_from
      );
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000, 'No template exist for the requested template: ' || v_template_id || ' Error:' ||sqlerrm ) ;
    END;

    -- set the values for the tags
    BEGIN
      --name  email_pkg.c_full_name_tag
      --term  email_pkg.c_term_tag
      --date  email_pkg.c_date_tag

      v_body := REPLACE(v_body, email_pkg.c_full_name_tag, person_pkg.get_name_initcap(p_prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(p_prsn_id));
      v_body := REPLACE(v_body, email_pkg.c_term_tag, v_term.title);
      v_body := REPLACE(v_body, email_pkg.c_date_tag, to_char(v_term.ua_con_rent_defer_dt, 'MM/DD/YYYY'));
      v_body := REPLACE(v_body, email_pkg.c_time_tag, to_char(sysdate, 'MM/DD/YYYY HH:MI PM'));

      IF v_conu.defer_dt IS NOT NULL THEN
        v_body := REPLACE(v_body, email_pkg.c_date_tag, to_char(v_conu.defer_dt, 'MM/DD/YYYY'));
      ELSE
        SELECT due_dt INTO v_offer_due_dt
        FROM offer ofr
        INNER JOIN agreement agr
          ON agr.offer_id = ofr.id
        WHERE agr.id = v_conu.agree_id;

        v_body := REPLACE(v_body, email_pkg.c_date_tag, to_char(v_offer_due_dt, 'MM/DD/YYYY'));
      END IF;

    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000, 'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    -- SEND OUT THE EMAIL
    BEGIN
      wwv_flow_api.set_security_group_id(p_security_group_id =>
          apex_util.find_security_group_id(p_workspace => 'HMS'));
      email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => v_prsn.id,
         p_app_alias => v('APP_ALIAS'),
         p_stud_viewable_ind => 'Y',
         p_email_id => v_new_email_id) ;
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem actually sending the email: '||sqlerrm) ;
    END;
  END;

  PROCEDURE roommate_invite( p_hsngmate_invite_id IN housingmate_invite.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1200;

    v_email_addr_types objectidlist := objectidlist(1,90);

    v_gatorlink_email email_addr.eml_addr%TYPE;

    v_url_term VARCHAR2(500);

    v_hsngmate_inv v_current_hsngmate_inv%ROWTYPE;
    v_requester_agree agreement%ROWTYPE;
    v_requested_agree agreement%ROWTYPE;
    v_prsn person%ROWTYPE;
  BEGIN
    SELECT *
    INTO v_hsngmate_inv
    FROM v_current_hsngmate_inv
    WHERE id = p_hsngmate_invite_id;

    v_requester_agree := agreement_pkg.get_agreement( v_hsngmate_inv.inviter_agree_id );
    v_requested_agree := agreement_pkg.get_agreement( v_hsngmate_inv.invitee_agree_id );
    v_prsn := person_cru.get_person( v_requester_agree.prsn_id );

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_subject := REPLACE( v_subject, '#REQUESTER_NAME#', person_pkg.get_name_initcap( v_requester_agree.prsn_id ) );
    v_body := REPLACE( v_body, '#REQUESTEE_FNAME#', person_pkg.get_first_name( v_requested_agree.prsn_id ) );
    v_body := REPLACE( v_body, '#REQUESTER_NAME#', person_pkg.get_name_initcap( v_requester_agree.prsn_id ) );
    v_body := REPLACE( v_body, '#REQUESTER_GENDER_PRONOUN#', CASE v_prsn.sex WHEN 'M' THEN 'his' WHEN 'F' THEN 'her' ELSE 'his/her' END );
    v_body := REPLACE( v_body, '#PREF_LINK#',
      ext_link_pkg.format_link(
        p_url => config_pkg.get_value('HTTP_SERVER') || config_pkg.get_value( 'UA_ROOM_PREF_PATH' ),
        p_label => 'Room Preferences' ) );
    v_body := REPLACE( v_body, '#EXPIRY#', v_hsngmate_inv.disp_expiry );
    v_body := REPLACE( v_body, '#UA_EMAIL_ADDRESS#', config_pkg.get_value( 'UA_EMAIL_ADDRESS' ) );

    v_to := person_pkg.get_email_addresses_in_array( v_requested_agree.prsn_id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_requested_agree.prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END roommate_invite;


  PROCEDURE send_cnc_mv_out_to_area(
    p_prsn_id   person.id%TYPE
  , p_area_id   area.id%TYPE
  , p_name      VARCHAR2
  , p_ufid      VARCHAR2
  , p_terms     VARCHAR2
  , p_bldg      VARCHAR2
  , p_room      VARCHAR2
  , p_space     VARCHAR2
  , p_date      VARCHAR2
  , p_time      VARCHAR2
  )AS
  v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 90;
  BEGIN
    --Send email to appropriate area
    v_to := email_pkg.get_recip_by_area_id(p_area_id);
    BEGIN
      -- get the template and subject from the database for this email
      apps.email_pkg.get_template(
        p_tmplt_id => v_template_id,
        p_body => v_body,
        p_subject => v_subject,
        p_from => v_from
      );
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'No template exist for the requested template: ' || v_template_id ||
      ' Error:' ||sqlerrm) ;
    END;

    -- set the values for the tags
    BEGIN
      v_body := REPLACE( v_body, email_pkg.c_full_name_tag, p_name);
      v_body := REPLACE( v_body, email_pkg.c_ufid_tag, p_ufid);
      v_body := REPLACE( v_body, email_pkg.c_terms_tag, p_terms);
      v_body := REPLACE( v_body, email_pkg.c_building, p_bldg);
      v_body := REPLACE( v_body, email_pkg.c_room, p_room);
      v_body := REPLACE( v_body, email_pkg.c_space, p_space);
      v_body := REPLACE( v_body, email_pkg.c_date_tag, p_date);
      v_body := REPLACE( v_body, email_pkg.c_time_tag, p_time);
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( - 20000,
      'Problem replacing tags in the template with data: '||sqlerrm) ;
    END;

    -- SEND OUT THE EMAIL
    BEGIN
      wwv_flow_api.set_security_group_id(p_security_group_id =>
          apex_util.find_security_group_id(p_workspace => 'HMS'));
      email_pkg.send(p_to => v_to,
         p_cc => v_cc,
         p_bcc => v_bcc,
         p_from => v_from,
         p_subject => v_subject,
         p_html => v_body,
         p_prsn_id => p_prsn_id,
         p_app_alias => v('APP_ALIAS'),
         p_stud_viewable_ind => 'N',
         p_email_id => v_new_email_id) ;
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem actually sending the email: '||sqlerrm) ;
    END;
  END;

  /*
  *  This procedure sends out an email to the resident that has checked out an item
  *  and that item is passed it's due date.
  */

  /*
  *  This procedure sends out an email to the resident that has checked out an item
  *  confirming their item checkout and informing them of the due date.
  */
  PROCEDURE send_eq_checkout_conf_to_res(
      p_co_id eq_checkout.id%TYPE
  ) AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id CONSTANT email_template.ID%TYPE := 347;

    v_co_rec eq_checkout%rowtype;
    v_item_rec eq_item%ROWTYPE;
    v_item_t_rec eq_item_type%ROWTYPE;
    v_new_email_id email.id%type;
    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    NULL;
    /* users would pass in in a checkout id. */
    v_co_rec := equip_pkg.get_co_rec(p_co_id);
    v_item_rec := equip_pkg.get_item_rec(v_co_rec.eq_itm_id);
    v_item_t_rec := equip_pkg.get_item_t_rec(v_item_rec.eq_itm_t_id);
    /* Email sent to UF email address */
    v_to := person_pkg.get_email_addresses_in_array(v_co_rec.prsn_id,v_email_addr_types);

      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject =>
        v_subject, p_from=>v_from) ;
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
        v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_co_rec.prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_prsn_name_tag, person_pkg.get_name_initcap(v_co_rec.prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_eq_item, v_item_rec.descr);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_no, v_item_rec.item_no);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_type, v_item_t_rec.title);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_due,
          to_char(equip_pkg.calc_due_date(v_co_rec.checkout_dt, v_item_t_rec.checkout_dur), 'MM/DD/YYYY'));
        v_body := REPLACE(v_body, email_pkg.c_location, equip_pkg.get_location_name(v_item_rec.eq_loc_id));
        v_body := REPLACE(v_body, email_pkg.c_housing_dept_name_tag, config_pkg.get_value('HOUSING_DEPT_NAME'));
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => v_co_rec.prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || v_co_rec.prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      apex_debug_message.log_message('PROCEDURE send_eq_checkout_conf_to_res ' ||
      ' sent email to person id:' || v_co_rec.prsn_id || ' with email address ' ||  v_to(1) ||
      ' for invoice id: ' || v_co_rec.id, false, 3) ;
  END;

  /*
  *  This procedure sends out an email to the resident that has checked out an item
  *  and that item is passed it's due date.
  */
  FUNCTION send_eq_checkout_overdue(
      p_co_id IN  eq_checkout.id%TYPE
    , p_first IN  BOOLEAN
  ) RETURN email.id%TYPE AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id email_template.ID%TYPE;

    v_co_rec eq_checkout%rowtype;
    v_item_rec eq_item%ROWTYPE;
    v_item_t_rec eq_item_type%ROWTYPE;
    v_new_email_id email.id%type;
    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    IF p_first THEN
      v_template_id := 348;
    ELSE
      v_template_id := 349;
    END IF;

    /* users would pass in in a checkout id. */
    v_co_rec := equip_pkg.get_co_rec(p_co_id);
    v_item_rec := equip_pkg.get_item_rec(v_co_rec.eq_itm_id);
    v_item_t_rec := equip_pkg.get_item_t_rec(v_item_rec.eq_itm_t_id);
    /* Email sent to UF email address */
    v_to := person_pkg.get_email_addresses_in_array(v_co_rec.prsn_id,v_email_addr_types);

  --  FOR i IN 1..v_to.COUNT LOOP
  --    apps.logger.debug(p_message => 'to: ' || v_to(i), p_db_log => TRUE);
  --  END LOOP;
      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject =>
        v_subject, p_from=>v_from) ;
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
        v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_co_rec.prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_prsn_name_tag, person_pkg.get_name_initcap(v_co_rec.prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_eq_item, v_item_rec.descr);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_no, v_item_rec.item_no);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_type, v_item_t_rec.title);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_due, to_char(equip_pkg.calc_due_date(v_co_rec.checkout_dt, v_item_t_rec.checkout_dur), 'MM/DD/YYYY'));
        v_body := REPLACE(v_body, email_pkg.c_location, equip_pkg.get_location_name(v_item_rec.eq_loc_id));
        v_body := REPLACE(v_body, email_pkg.c_housing_dept_name_tag, config_pkg.get_value('HOUSING_DEPT_NAME'));
        v_body := REPLACE(v_body, email_pkg.c_days_overdue,
          to_char(trunc(sysdate) - trunc(equip_pkg.calc_due_date(v_co_rec.checkout_dt, v_item_t_rec.checkout_dur))));
        v_subject := REPLACE(v_subject, email_pkg.c_days_overdue,
          to_char(trunc(sysdate) - trunc(equip_pkg.calc_due_date(v_co_rec.checkout_dt, v_item_t_rec.checkout_dur))));
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => v_co_rec.prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || v_co_rec.prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      apex_debug_message.log_message('PROCEDURE send_ad_inv_created_to_student ' ||
      ' sent email to person id:' || v_co_rec.prsn_id || ' with email address ' ||  v_to(1) ||
      ' for invoice id: ' || v_co_rec.id, false, 3) ;
      RETURN v_new_email_id;
  END;


--------------------------------------------------------
--  DDL for Function get_email_addresses_in_array
--------------------------------------------------------
FUNCTION get_area_eml_addr_in_array(
    p_loc    eq_location%ROWTYPE
  ) RETURN apps.email_pkg.recip_array
AS
  v_addresses apps.email_pkg.recip_array;

BEGIN

SELECT v.eml_addr bulk collect 
  INTO v_addresses
  FROM v_area_email_recips v
  WHERE v.area_id = p_loc.area_id
  ;
  RETURN v_addresses;

END;

--------------------------------------------------------
--  DDL for Function get_email_addresses_in_array
--------------------------------------------------------
  FUNCTION get_org_eml_addr_in_array(
    p_loc   eq_location%ROWTYPE
  , p_recip_array apps.email_pkg.recip_array)
  RETURN apps.email_pkg.recip_array
  AS
    v_addr apps.email_pkg.recip_array;
    v_index NUMBER := 0;
    v_index_total NUMBER := 0;
    v_rtn email_addr.eml_addr%TYPE;
    v_match BOOLEAN := FALSE;

    CURSOR c_eml (p_area_id area.id%TYPE) IS
    SELECT usr.eml_addr
    FROM area_organization_user usr
    INNER JOIN area_organization org
      ON usr.ar_org_id = org.id
    WHERE usr.username LIKE '%BUSINESSMANAGER'
      AND org.area_id = p_area_id
    ;
  BEGIN
    FOR i IN 1.. p_recip_array.COUNT
    LOOP
      v_addr(i) := LOWER(p_recip_array(i));
    END LOOP;

    FOR r_eml IN c_eml(p_loc.area_id) LOOP
      v_match := FALSE; -- RESET before check
      FOR j IN 1.. v_index
      LOOP
        IF(trim(lower(r_eml.eml_addr)) = trim(lower(v_addr(j))))THEN
          v_match := TRUE;
        END IF;
      END LOOP;
      IF(NOT v_match) THEN
        v_index         := v_index + 1; -- Just increment my index and it should start at 1
        v_addr(v_index) := LOWER(r_eml.eml_addr);
        dbms_output.put_line('Added email adderss:' || v_addr(v_index));
      END IF;
    END LOOP;

    v_index_total := v_index_total + v_index;

    IF(v_index_total = 0) THEN
      RAISE_APPLICATION_ERROR(-20002, 'No email address found for eq_location:' || p_loc.id);
    ELSE
      RETURN v_addr;
    END IF;
  END;

  /*
  *  This procedure sends out an email to the resident that has checked out an item
  *  and that item is passed it's due date.
  */
  PROCEDURE send_eq_lost_item(
    p_co_id   eq_checkout.id%TYPE
  )AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id CONSTANT email_template.ID%TYPE := 350;

    v_co_rec eq_checkout%rowtype;
    v_item_rec eq_item%ROWTYPE;
    v_item_t_rec eq_item_type%ROWTYPE;
    v_loc        eq_location%ROWTYPE;
    v_org_rec   area_organization%ROWTYPE;
    v_new_email_id email.id%type;

    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    NULL;
    /* users would pass in in a checkout id. */
    v_co_rec := equip_pkg.get_co_rec(p_co_id);
    v_item_rec := equip_pkg.get_item_rec(v_co_rec.eq_itm_id);
    v_item_t_rec := equip_pkg.get_item_t_rec(v_item_rec.eq_itm_t_id);
    v_loc := equip_pkg.get_loc_rec(v_item_rec.eq_loc_id);

    /* Email sent to UF email address */
    v_to := get_area_eml_addr_in_array(v_loc);
    IF v_item_rec.activity_card_ind = 'Y' THEN
      v_to := get_org_eml_addr_in_array(v_loc, v_to);
    END IF;

    IF v_item_rec.ar_org_id IS NOT NULL THEN
      SELECT * INTO v_org_rec
      FROM area_organization
      WHERE id = v_item_rec.ar_org_id;
    END IF;
      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject => v_subject, p_from=>v_from) ;
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
--        v_body := REPLACE(v_body, email_pkg.c_ufid_tag, person_pkg.get_ufid(v_co_rec.prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_prsn_name_tag, person_pkg.get_name_initcap(v_co_rec.prsn_id));

        v_body := REPLACE(v_body, email_pkg.c_full_name_tag, v_co_rec.updt_by);
        v_body := REPLACE(v_body, email_pkg.c_time_tag, to_char(v_co_rec.updt_ts, 'MM/DD/YYYY HH:MI:SS PM'));
        v_body := REPLACE(v_body, email_pkg.c_status, CASE WHEN v_item_rec.lost_ind = 'Y' THEN 'Lost' ELSE 'Damaged' END);

        IF v_item_rec.ar_org_id IS NOT NULL THEN
          v_body := REPLACE(v_body, '#ORGANIZATION#', 'Organization: '|| v_org_rec.name);
        ELSE
          v_body := REPLACE(v_body, '#ORGANIZATION#', '');
        END IF;

        v_body := REPLACE(v_body, email_pkg.c_email, person_pkg.get_gatorlink_by_prsn_id(v_co_rec.prsn_id) || '@ufl.edu');

        v_body := REPLACE(v_body, email_pkg.c_eq_item, v_item_rec.descr);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_no, v_item_rec.item_no);
        v_body := REPLACE(v_body, email_pkg.c_eq_item_type, v_item_t_rec.title);
        v_body := REPLACE(v_body, email_pkg.c_date_tag, to_char(v_co_rec.checkout_dt, 'MM/DD/YYYY HH:MI AM'));
        v_body := REPLACE(v_body, email_pkg.c_cost_tag, to_char(v_item_rec.cost, '$9,999.00'));

        v_body := REPLACE(v_body, email_pkg.c_location, equip_pkg.get_location_name(v_item_rec.eq_loc_id));
        v_body := REPLACE(v_body, email_pkg.c_housing_dept_name_tag, config_pkg.get_value('HOUSING_DEPT_NAME'));
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => v_co_rec.prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || v_co_rec.prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
      apex_debug_message.log_message('PROCEDURE send_eq_checkout_conf_to_res ' ||
      ' sent email to person id:' || v_co_rec.prsn_id || ' with email address ' ||  v_to(1) ||
      ' for invoice id: ' || v_co_rec.id, false, 3) ;
  END;


  PROCEDURE movein_complete( p_assign_id IN assign.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1324;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_gatorlink_email email_addr.eml_addr%TYPE;

    v_assign assign%ROWTYPE;
    v_prsn_id person.id%TYPE;
    v_address VARCHAR2( 1000 );
		v_sp_rm_trm_id space_room_term.id%TYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
		v_prsn_id := assign_pkg.get_person( p_assign_id );
    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_body := REPLACE( v_body, '#STUDENT_NAME#', person_pkg.get_name_initcap( v_prsn_id ) );
    v_body := REPLACE( v_body, '#ADDRESS#', space_room_term_pkg.display_sp_rm_trm_details( p_sp_rm_trm_id => v_assign.sp_rm_trm_id,
			p_show_building => 'Y',
			p_show_rm_num => 'Y',
			p_show_bedroom => 'Y',
			p_show_space => 'Y',
			p_show_area => 'N' ) );

    v_to := person_pkg.get_email_addresses_in_array( v_prsn_id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END movein_complete;


	-- 08/14/2017 Danann requested a move-in confirmation/survey email
	PROCEDURE move_in_survey( p_assign_id IN assign.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 100310;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_gatorlink_email email_addr.eml_addr%TYPE;

    v_assign assign%ROWTYPE;
    v_prsn_id person.id%TYPE;

		v_term term.title%TYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
		v_prsn_id := assign_pkg.get_person( p_assign_id );
    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

		SELECT term.title
		INTO v_term
		FROM v_ua_assign_status v
		JOIN term
			ON v.trm_id = term.id
		WHERE v.asg_id = p_assign_id;

    v_body := REPLACE( v_body, '#FIRST_NAME#', INITCAP( person_pkg.get_first_name( v_prsn_id ) ) );
    v_body := REPLACE( v_body, '#AREA_NAME#', v_assign.area_nm );
    v_body := REPLACE( v_body, '#TERM#', v_term );
    v_subject := REPLACE( v_subject, '#AREA_NAME#', v_assign.area_nm );

    v_to := person_pkg.get_email_addresses_in_array( v_prsn_id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_prsn_id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END move_in_survey;


  PROCEDURE moveout_complete( p_assign_id IN assign.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1343;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_gatorlink_email email_addr.eml_addr%TYPE;

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_address VARCHAR2( 1000 );
    v_terms VARCHAR2( 4000 );
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT area.title || ' - ' || b.title || '<br>' ||
     'Room ' || room.rm_num || space.descr
    INTO v_address
    FROM assign
    JOIN space_room_term srt
      ON assign.sp_rm_trm_id = srt.id
    JOIN space
      ON srt.sp_id = space.id
    JOIN room_term rt
      ON srt.rm_trm_id = rt.id
    JOIN room
      ON rt.rm_id = room.id
    JOIN building b
      ON room.bldg_id = b.id
    JOIN area
      ON b.area_id = area.id
    JOIN contract_ua c
      ON assign.conu_id = c.id
    JOIN agreement
      ON c.agree_id = agreement.id
    WHERE assign.id = p_assign_id
      AND assign.deleted_ind = 'N';

    SELECT LISTAGG( t.title ) WITHIN GROUP ( ORDER BY t.halls_open_dt )
    INTO v_terms
    FROM assign a
    JOIN contract_ua c
      ON a.conu_id = c.id
    JOIN term t
      ON c.trm_id = t.id
    WHERE a.id = p_assign_id
      AND NVL( c.canc_ind, 'N' ) = 'N';

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_body := REPLACE( v_body, '#FULLNAME#', person_pkg.get_name_initcap( v_person.id ) );
    v_body := REPLACE( v_body, '#UFID#', v_person.ufid );
    v_body := REPLACE( v_body, '#BUILDING_ROOM_SPACE#', v_address );
    v_body := REPLACE( v_body, '#HMS_URL#', config_pkg.get_value( 'HTTP_SERVER' ) );
    v_body := REPLACE( v_body, '#TERMS#', v_terms );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END moveout_complete;

  PROCEDURE temp_key_issued( p_assign_id IN assign.id%TYPE, p_key_id IN key.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1544;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key key%ROWTYPE;
		v_key_type key_type.title%TYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );
    v_key := key_cru.get_key( p_key_id );

		SELECT
			CASE
				WHEN id = 9 AND v_key.expected_return_dt IS NOT NULL
				THEN 'Acess Card'
				ELSE title
			END
		INTO v_key_type
		FROM key_type
		WHERE id = v_key.key_t_id;

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

		v_subject := REPLACE( v_subject, '#KEY_TYPE#', v_key_type );
		v_body := REPLACE( v_body, '#KEY_TYPE#', v_key_type );
    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#EXPECTED_RETURN_DATE#', TO_CHAR( v_key.expected_return_dt, 'MM/DD/YYYY HH:MIPM' ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END temp_key_issued;


  PROCEDURE temp_key_overdue( p_assign_id IN assign.id%TYPE, p_key_id IN key.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1545;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key key%ROWTYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );
    v_key := key_cru.get_key( p_key_id );

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#EXPECTED_RETURN_DATE#', TO_CHAR( v_key.expected_return_dt, 'MM/DD/YYYY HH:MIPM' ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END temp_key_overdue;


  PROCEDURE lockout_warning(
    p_assign_id IN assign.id%TYPE,
    p_lockout_cnt IN NUMBER,
    p_lockout_max IN NUMBER )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1546;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_person person%ROWTYPE;
    v_area_id area.id%TYPE;
    v_area_desk_link_id external_link.id%TYPE DEFAULT 241;
    v_term_id term.id%TYPE;
  BEGIN
    SELECT b.area_id, a.trm_id
    INTO v_area_id, v_term_id
    FROM v_assign_room_term a
    JOIN building b
      ON a.bldg_id = b.id
    JOIN area
      ON b.area_id = area.id
    WHERE a.id = p_assign_id;

    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_body := REPLACE( v_body, '#LOCKOUT_CNT#', p_lockout_cnt );
    v_body := REPLACE( v_body, '#LOCKOUT_LIMIT#', p_lockout_max );
    v_body := REPLACE( v_body, '#TERM#', term_pkg.get_term_name( v_term_id ) );
-- this tag was removed from the template on 23-JUN-2016
--    v_body := REPLACE( v_body, '#AREA_DESK_EMAIL#', email_pkg.disp_area_coordinators_email( v_area_id ) );
    v_body := REPLACE( v_body, '#AREA_DESK_LINK#', ext_link_pkg.get_formal_link( 241 ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END lockout_warning;


  PROCEDURE lockout_charge(
    p_assign_id IN assign.id%TYPE,
    p_lockout_cnt key_charge.lockout_cnt%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1547;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key key%ROWTYPE;
    v_rm_num room.rm_num%TYPE;
    v_area area.title%TYPE;
    v_building building.title%TYPE;
    v_term_id term.id%TYPE;
    v_lockout_cnt NUMBER;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT trm_id
    INTO v_term_id
    FROM v_assign_room_term
    WHERE id = p_assign_id;

    SELECT room.rm_num,
      area.title,
      b.title
    INTO v_rm_num,
      v_area,
      v_building
    FROM assign
    JOIN space_room_term srt
      ON assign.sp_rm_trm_id = srt.id
    JOIN space
      ON srt.sp_id = space.id
    JOIN room_term rt
      ON srt.rm_trm_id = rt.id
    JOIN room
      ON rt.rm_id = room.id
    JOIN building b
      ON room.bldg_id = b.id
    JOIN area
      ON b.area_id = area.id
    JOIN contract_ua c
      ON assign.conu_id = c.id
    JOIN agreement
      ON c.agree_id = agreement.id
    WHERE assign.id = p_assign_id
      AND assign.deleted_ind = 'N';

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_lockout_cnt := key_ua_pkg.lockout_cnt( p_asg_id => p_assign_id );

    v_subject := REPLACE( v_subject, '#LOCKOUT_CNT#', p_lockout_cnt );
    v_subject := REPLACE( v_subject, '#FNAME#', v_person.first_name );

    v_body := REPLACE( v_body, '#LOCKOUT_CNT#', p_lockout_cnt );
    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#ROOM_NUM#', v_rm_num );
    v_body := REPLACE( v_body, '#BUILDING#', v_building );
    v_body := REPLACE( v_body, '#AREA#', v_area );
    v_body := REPLACE( v_body, '#EXPECTED_RETURN_DATE#', v_key.expected_return_dt );
    v_body := REPLACE( v_body, '#TERM#', term_pkg.get_term_name( v_term_id ) );


    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END lockout_charge;


  PROCEDURE staff_lockout_charge(
    p_assign_id IN assign.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 100029;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key key%ROWTYPE;
    v_rm_num room.rm_num%TYPE;
    v_area area%ROWTYPE;
    v_building building.title%TYPE;
    v_term_id term.id%TYPE;
    v_space space.title%TYPE;
    v_loc eq_location%ROWTYPE;
    v_max_lockout NUMBER;
    v_lockout_cnt NUMBER;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT trm_id
    INTO v_term_id
    FROM v_assign_room_term
    WHERE id = p_assign_id;

    SELECT room.rm_num,
      area.id,
      b.title,
      space.title
    INTO v_rm_num,
      v_area.id,
      v_building,
      v_space
    FROM assign
    JOIN space_room_term srt
      ON assign.sp_rm_trm_id = srt.id
    JOIN space
      ON srt.sp_id = space.id
    JOIN room_term rt
      ON srt.rm_trm_id = rt.id
    JOIN room
      ON rt.rm_id = room.id
    JOIN building b
      ON room.bldg_id = b.id
    JOIN area
      ON b.area_id = area.id
    JOIN contract_ua c
      ON assign.conu_id = c.id
    JOIN agreement
      ON c.agree_id = agreement.id
    WHERE assign.id = p_assign_id
      AND assign.deleted_ind = 'N';

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_max_lockout := key_ua_pkg.max_lockout( p_asg_id => p_assign_id );
    v_lockout_cnt := key_ua_pkg.lockout_cnt( p_asg_id => p_assign_id );

    -- Danann 05/30/2017
    -- Let's change the Subject line for the notice of lockout emails to "Resident [UFID] has experienced a lockout."
    -- The subject line for the notice of EXCESSIVE lockout emails can be changed to "Resident [UFID] has experienced an EXCESSIVE lockout." That should clear up this confusion.
    IF v_lockout_cnt > v_max_lockout THEN
      --v_subject := 'Resident ' || v_person.ufid || ' has experienced an EXCESSIVE lockout';
        v_subject := 'The Resident has experienced an EXCESSIVE lockout';
			v_body := REPLACE( v_body, '#INSTRUCTIONS#', 'Please review and approve/deny the excessive lock-out fee for this resident on the HMS Key Charges site.' );
    ELSE
      --v_subject := 'Resident ' || v_person.ufid || ' has experienced a lockout';
      v_subject := 'The Resident has experienced a lockout';
			v_body := REPLACE( v_body, '#INSTRUCTIONS#', 'Please review the lockout information for this resident to ensure the information was logged accurately.' );
    END IF;

    v_subject := REPLACE( v_subject, '#UFID#', v_person.ufid );

    v_body := REPLACE( v_body, '#UFID#', v_person.ufid );
    v_body := REPLACE( v_body, '#FULL_NAME#', INITCAP( v_person.first_name ) || ' ' || INITCAP( v_person.last_name ) );
    v_body := REPLACE( v_body, '#HALL#', v_building );
    v_body := REPLACE( v_body, '#ROOM_NUM#', v_rm_num );
    v_body := REPLACE( v_body, '#SPACE_NUM#', v_space );
    v_body := REPLACE( v_body, '#TERM#', term_pkg.get_term_name( v_term_id ) );
    v_body := REPLACE( v_body, '#LOCKOUT_CNT#', v_lockout_cnt );
    v_body := REPLACE( v_body, '#MAX_LOCKOUT_CNT#', v_max_lockout );

    v_loc.area_id := v_area.id;
    v_to := get_area_eml_addr_in_array( v_loc );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'N', p_email_id => v_new_email_id );
  END staff_lockout_charge;



  PROCEDURE key_lost( p_assign_id IN assign.id%TYPE,
    p_key_id IN key.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1583;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key_type key_type.title%TYPE;
    v_term_id term.id%TYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT trm_id
    INTO v_term_id
    FROM v_assign_room_term
    WHERE id = p_assign_id;

    SELECT kt.title
    INTO v_key_type
    FROM key k
    JOIN key_type kt
      ON k.key_t_id = kt.id
    WHERE k.id = p_key_id;

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_subject := REPLACE( v_subject, '#FNAME#', v_person.first_name );

    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#KEY_TYPE#', v_key_type );
    v_body := REPLACE( v_body, '#CURRENT_TERM#', term_pkg.get_term_name( v_term_id ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END key_lost;


  PROCEDURE key_damaged( p_assign_id IN assign.id%TYPE,
    p_key_id IN key.id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1750;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key_type key_type.title%TYPE;
    v_term_id term.id%TYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT trm_id
    INTO v_term_id
    FROM v_assign_room_term
    WHERE id = p_assign_id;

    SELECT kt.title
    INTO v_key_type
    FROM key k
    JOIN key_type kt
      ON k.key_t_id = kt.id
    WHERE k.id = p_key_id;

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_subject := REPLACE( v_subject, '#FNAME#', v_person.first_name );

    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#KEY_TYPE#', v_key_type );
    v_body := REPLACE( v_body, '#CURRENT_TERM#', term_pkg.get_term_name( v_term_id ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
  END key_damaged;


  PROCEDURE lock_change(
    p_assign_id IN assign.id%TYPE,
    p_key_t_id IN key.key_t_id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1584;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_area_desk_link_id external_link.id%TYPE DEFAULT 241;

    v_person person%ROWTYPE;
    v_key_type key_type.title%TYPE;
    v_term_id term.id%TYPE;
  BEGIN
    SELECT trm_id
    INTO v_term_id
    FROM v_assign_room_term
    WHERE id = p_assign_id;

    SELECT title
    INTO v_key_type
    FROM key_type kt
    WHERE kt.id = p_key_t_id;

    -- disabled emailing roommates per meeting on 2/4/2016
    /*
    Not sending to roommates anymore
    FOR r IN
    ( SELECT v.asg_id
      FROM space_room_term srt
      JOIN v_ua_assign_status v
        ON srt.id = v.sp_rm_trm_id
      WHERE v.curr_res_ind = 'Y'
        AND v.asg_id <> p_assign_id
        AND srt.rm_trm_id =
        ( SELECT srt2.rm_trm_id
          FROM assign a2
          JOIN space_room_term srt2
            ON a2.sp_rm_trm_id = srt2.id
          WHERE a2.id = p_assign_id ) )
    LOOP
    */
    v_new_email_id := NULL;
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_subject := REPLACE( v_subject, '#FNAME#', v_person.first_name );

    v_body := REPLACE( v_body, '#FNAME#', v_person.first_name );
    v_body := REPLACE( v_body, '#CURRENT_TERM#', term_pkg.get_term_name( v_term_id ) );
    v_body := REPLACE( v_body, '#TYPE_KEY#', v_key_type );
    v_body := REPLACE( v_body, '#AREA_DESK_LINK#', ext_link_pkg.get_formal_link( 241 ) );

    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'Y', p_email_id => v_new_email_id );
    /* END LOOP; */
  END lock_change;


  PROCEDURE clerk_lock_change(
    p_assign_id IN assign.id%TYPE,
    p_key_t_id IN key.key_t_id%TYPE )
  IS
    v_new_email_id email.id%type;
    v_body CLOB;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_template_id email_template.id%type := 1871;

    v_email_addr_types objectidlist := objectidlist( 1,90 );

    v_assign assign%ROWTYPE;
    v_person person%ROWTYPE;
    v_key key%ROWTYPE;
    v_rm_num room.rm_num%TYPE;
    v_area area%ROWTYPE;
    v_building building.title%TYPE;
    v_key_type key_type.title%TYPE;
    v_loc eq_location%ROWTYPE;
  BEGIN
    v_assign := assign_cru.get_assign( p_assign_id );
    v_person := person_cru.get_person( assign_pkg.get_person( p_assign_id ) );

    SELECT title
    INTO v_key_type
    FROM key_type kt
    WHERE kt.id = p_key_t_id;

    SELECT room.rm_num,
      area.title,
      area.id,
      b.title
    INTO v_rm_num,
      v_area.title,
      v_area.id,
      v_building
    FROM assign
    JOIN space_room_term srt
      ON assign.sp_rm_trm_id = srt.id
    JOIN space
      ON srt.sp_id = space.id
    JOIN room_term rt
      ON srt.rm_trm_id = rt.id
    JOIN room
      ON rt.rm_id = room.id
    JOIN building b
      ON room.bldg_id = b.id
    JOIN area
      ON b.area_id = area.id
    JOIN contract_ua c
      ON assign.conu_id = c.id
    JOIN agreement
      ON c.agree_id = agreement.id
    WHERE assign.id = p_assign_id
      AND assign.deleted_ind = 'N';

    apps.email_pkg.get_template(
      p_tmplt_id => v_template_id,
      p_body => v_body,
      p_subject => v_subject,
      p_from => v_from );

    v_body := REPLACE( v_body, '#AREA#', v_area.title );
    v_body := REPLACE( v_body, '#BUILDING#', v_building );
    v_body := REPLACE( v_body, '#ROOM_NUM#', v_rm_num );
    v_body := REPLACE( v_body, '#KEY_TYPE#', v_key_type );
    v_body := REPLACE( v_body, '#KEY_CHARGES_LINK#',
      ext_link_pkg.format_link(
        p_url => config_pkg.get_value('HTTP_SERVER') || config_pkg.get_value( 'KEY_CHARGES_PATH' ),
        p_label => 'Key Charges' ) );

    v_loc.area_id := v_area.id;
    v_to := get_area_eml_addr_in_array( v_loc );

    email_pkg.send( p_to => v_to, p_cc => v_cc, p_bcc => v_bcc,
      p_from => v_from, p_subject => v_subject, p_html => v_body,
      p_prsn_id => v_person.id, p_app_alias => NULL,
      p_stud_viewable_ind => 'N', p_email_id => v_new_email_id );
  END clerk_lock_change;


----------------------------------------------
----------------------------------------------
  PROCEDURE alert_stud_rmmt_change(
    p_prsn_id       person.id%TYPE
  , p_trm_group     term_typegrp.title%TYPE
  )AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id CONSTANT email_template.ID%TYPE := 87;

    v_person    person%ROWTYPE;
    v_new_email_id email.id%type;

    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    NULL;
    /* users would pass in in a checkout id. */
    v_person := person_cru.get_person(p_prsn_id);

    /* Email sent to UF email address */
    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject => v_subject, p_from=>v_from) ;
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
        v_body := REPLACE(v_body, email_pkg.c_name_tag, person_pkg.get_name_initcap(p_prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_term_group_tag, p_trm_group);
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => p_prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || p_prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
  END;

----------------------------------------------
----------------------------------------------
  PROCEDURE alert_std_rmmt_chng_aft_chkin(
    p_prsn_id       person.id%TYPE
  , p_trm_group     term_typegrp.title%TYPE
  )AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id CONSTANT email_template.ID%TYPE := 88;

    v_person    person%ROWTYPE;
    v_new_email_id email.id%type;

    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    NULL;
    /* users would pass in in a checkout id. */
    v_person := person_cru.get_person(p_prsn_id);

    /* Email sent to UF email address */
    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject => v_subject, p_from=>v_from) ;
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
        v_body := REPLACE(v_body, email_pkg.c_name_tag, person_pkg.get_name_initcap(p_prsn_id));
        v_body := REPLACE(v_body, email_pkg.c_term_group_tag, p_trm_group);
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => p_prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || p_prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
  END;

  PROCEDURE send_rle_notification(
    p_prsn_id           person.id%TYPE
  , p_training_title    rle_training_session.descr%TYPE
  , p_date              rle_training_session.start_dt%TYPE
  , p_loc               VARCHAR2
  , p_body              VARCHAR2
  )AS
    v_body CLOB;
    v_to apps.email_pkg.recip_array;
    v_cc apps.email_pkg.recip_array;
    v_bcc apps.email_pkg.recip_array;
    v_from apps.email_pkg.t_from;
    v_subject apps.email_pkg.t_subject;
    v_template_id CONSTANT email_template.ID%TYPE := 50;

    v_person    person%ROWTYPE;
    v_new_email_id email.id%type;

    v_email_addr_types objectidlist := objectidlist(1,90);
  BEGIN
    NULL;
    /* users would pass in in a checkout id. */
    v_person := person_cru.get_person(p_prsn_id);

    /* Email sent to UF email address */
    v_to := person_pkg.get_email_addresses_in_array( v_person.id, v_email_addr_types );

      BEGIN
        -- get the template from the database
        apps.email_pkg.get_template(p_tmplt_id => v_template_id, p_body => v_body, p_subject => v_subject, p_from=>v_from) ;

        IF TRIM(p_body) IS NOT NULL THEN
          v_body := p_body;
        END IF;

      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'No template exist for the requested template with id: ' || v_template_id ||
        ' Error:' ||sqlerrm) ;
      END;
      BEGIN
        -- set the values for the tags
        v_body := REPLACE(v_body, email_pkg.c_name_tag, person_pkg.get_name_initcap(p_prsn_id));
        v_body := REPLACE(v_body, '#TRAINING_TITLE#', p_training_title);
        v_body := REPLACE(v_body, email_pkg.c_date_tag, to_char(p_date, 'MM/DD/YYYY HH:MI PM'));
        v_body := REPLACE(v_body, email_pkg.c_location, p_loc);
        v_body := REPLACE(v_body, email_pkg.c_hmssig, email_pkg.get_signature());

        email_pkg.send(p_to => v_to,
             p_cc => v_cc,
             p_bcc => v_bcc,
             p_from => v_from,
             p_subject => v_subject,
             p_html => v_body,
             p_prsn_id => p_prsn_id,
             p_app_alias => utility_pkg.get_system,
             p_stud_viewable_ind => 'Y',
             p_email_id => v_new_email_id);

        DBMS_OUTPUT.PUT_LINE('Sending it to: ' || v_to(1) || ' with person id:' || p_prsn_id);
        DBMS_OUTPUT.PUT_LINE(v_body);
      EXCEPTION
      WHEN OTHERS THEN
        raise_application_error( - 20000,
        'Problem replacing tags in the template with data: '||sqlerrm) ;
      END;
  END;

END EMAIL_UA_PKG;

/
