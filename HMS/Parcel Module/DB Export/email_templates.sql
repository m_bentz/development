SET DEFINE OFF;
DECLARE
    v_eml_templt    email_template%ROWTYPE;
BEGIN
    --100352  return
    v_eml_templt.title := 'Housing - Return to Sender';
    v_eml_templt.eml_templt := '<div style="white-space: pre">#PERSON_NAME#,</div><div style="white-space: pre"><br></div><div style="white-space: pre">We wanted to let you know that your package at the #AREA_NAME# Area Desk has been returned to sender.</div><div style="white-space: pre"><br></div><div style="white-space: pre">Below are the package details:</div><div style="white-space: pre">		</div><ul><li style="white-space: pre">Tracking ID: #TRACK_NUM#&nbsp;</li><li style="white-space: pre">Carrier:&nbsp;#CARRIER#</li><li style="white-space: pre">Delivered to the #AREA_NAME# Area Desk on #RECEIVE_DT#</li><li style="white-space: pre">Returned to sender by #AREA_NAME# Area Desk on #RETURN_DT#</li></ul><div style="white-space: pre"><br></div><div style="white-space: pre">Unfortunately, we can only hold resident packages for three to five (3-5) business days at our Area Desks.&nbsp;If the recipient is unable to pick up the package within that window and does not contact the Area Desk Clerk to request an extension, the package must be returned to sender.</div><div style="white-space: pre"><br></div><div style="white-space: pre">IF YOUR CARRIER IS USPS, your package will be returned to the UF Mail and Document Services Office and will be held for three (3) additional business days before it is returned to the original sender.</div><div style="white-space: pre"><br></div><div style="white-space: pre">UF Mail and Document Services Office:</div><div style="white-space: pre">Telephone:&nbsp;352-392-0629</div><div style="white-space: pre">Hours:&nbsp;Monday / Friday 8:00am / 5:00pm</div><div style="white-space: pre">Location:&nbsp;3030 Radio Road, Gainesville, Florida 32603</div><div style="white-space: pre"><br></div><div style="white-space: pre">IF YOUR CARRIER IS NOT USPS, please contact your carrier directly to receive further information regarding your package location and destination.</div><div style="white-space: pre"><br></div><div style="white-space: pre">If you have further questions regarding our package process, please contact us at Packages@housing.ufl.edu.</div><div style="white-space: pre"><br></div><div style="white-space: pre">Thank you,</div><div style="white-space: pre"><br></div><div style="white-space: pre">The #AREA_NAME# Area Desk Team</div>';
    v_eml_templt.funct_name := ' ';
    v_eml_templt.descr := 'Housing - Return to Sender';
    v_eml_templt.subject := 'Package Returning to Sender';
    v_eml_templt.from_addr := 'noreply@housing.ufl.edu';
    v_eml_templt.mailed_ind := 'N';
    v_eml_templt.is_coll_letter_ind := 'N';
    v_eml_templt.eml_templt_t_id := 1;
    
    apps.email_template_cru.sav(v_eml_templt);
    v_eml_templt := NULL;

    --100353 receive
    v_eml_templt.title := 'Housing - Received Package';
    v_eml_templt.eml_templt := '<div style="white-space: pre">Dear #PERSON_NAME#,</div><div style="white-space: pre"><br></div><div style="white-space: pre">There is a package waiting for you at the #AREA_NAME#&nbsp;Area Desk! Please come by as soon as possible to sign for and collect your package.</div><div style="white-space: pre"><br></div><div style="white-space: pre">In order to collect your package, you must:</div><ul><li style="white-space: pre">Provide your Gator 1 ID card to the area desk staff&nbsp;</li><li style="white-space: pre">Tell the area desk staff the date that this email was sent</li></ul><div style="white-space: pre"><br></div><div style="white-space: pre">If your package is not picked up within three (3) business days of the date delivery below, your package will be returned to the sender.</div><div style="white-space: pre"><br></div><div style="white-space: pre">This is the information for your package:</div><div style="white-space: pre">	</div><div style="white-space: pre">	Tracking ID: #TRACK_NUM#</div><div style="white-space: pre">	Carrier : #CARRIER#</div><div style="white-space: pre">	Delivered to #AREA_NAME# Area Desk #RECEIVE_DT#</div><div style="white-space: pre"><br></div><div style="white-space: pre">We look forward to seeing you soon!</div>';
    v_eml_templt.funct_name := ' ';
    v_eml_templt.descr := 'Housing - Received Package';
    v_eml_templt.subject := 'Package Received';
    v_eml_templt.from_addr := 'noreply@housing.ufl.edu';
    v_eml_templt.mailed_ind := 'N';
    v_eml_templt.is_coll_letter_ind := 'N';
    v_eml_templt.eml_templt_t_id := 1;

    apps.email_template_cru.sav(v_eml_templt);
    v_eml_templt := NULL;

    -- ?? suspicious received
    v_eml_templt.title := 'Housing - Received Suspicious Package';
    v_eml_templt.eml_templt := '<div style="white-space: pre">A suspicious package has been received at the #AREA_NAME# Area Desk</div><div style="white-space: pre"><br></div><div style="white-space: pre">Below are the package details:</div><ul><li style="white-space: pre">Recipient: #PERSON_NAME#		</li><li style="white-space: pre">Tracking ID : #TRACK_NUM#</li><li style="white-space: pre">Carrier:&nbsp;#CARRIER#</li><li style="white-space: pre">Delivered to the #AREA_NAME# Area Desk on #RECEIVE_DT#</li></ul><div style="white-space: pre"><br></div><div style="white-space: pre">Thank you,</div><div style="white-space: pre"><br></div><div style="white-space: pre">The #AREA_NAME# Area Desk Team</div>';
    v_eml_templt.funct_name := ' ';
    v_eml_templt.descr := 'Housing - Received Suspicious Package';
    v_eml_templt.subject := 'Suspicious Package Received';
    v_eml_templt.from_addr := 'noreply@housing.ufl.edu';
    v_eml_templt.mailed_ind := 'N';
    v_eml_templt.is_coll_letter_ind := 'N';
    v_eml_templt.eml_templt_t_id := 1;
    v_eml_templt.title := 'Housing - Received Suspicious Package';

    apps.email_template_cru.sav(v_eml_templt);
END;
/