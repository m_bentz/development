--------------------------------------------------------
--  Ref Constraints for Table PC_CARRIER
--------------------------------------------------------

  ALTER TABLE "PC_CARRIER" ADD CONSTRAINT "PC_CARRIER_CREATE_S_FK00" FOREIGN KEY ("CREATE_S_ID")
	  REFERENCES "PC_CARRIER$S" ("S_ID") DISABLE;
  ALTER TABLE "PC_CARRIER" ADD CONSTRAINT "PC_CARRIER_CURR_S_FK00" FOREIGN KEY ("CURR_S_ID")
	  REFERENCES "PC_CARRIER$S" ("S_ID") DISABLE;
