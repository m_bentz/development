--------------------------------------------------------
--  DDL for Package Body PC_PARCEL_LIST_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_PARCEL_LIST_PKG" AS

  FUNCTION get_list_by_area(
    p_area_id       area.id%TYPE
  )RETURN pc_parcel_list%ROWTYPE AS
  v_prcl_list   pc_parcel_list%ROWTYPE;
  BEGIN
    SELECT * 
    INTO v_prcl_list
    FROM pc_parcel_list prcl_list 
    WHERE prcl_list.area_id = p_area_id;
    RETURN v_prcl_list;
  END get_list_by_area;

END PC_PARCEL_LIST_PKG;

/
