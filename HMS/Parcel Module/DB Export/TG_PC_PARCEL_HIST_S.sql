--------------------------------------------------------
--  DDL for Trigger TG$PC_PARCEL_HIST$S
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TG$PC_PARCEL_HIST$S" 
  BEFORE INSERT OR UPDATE ON HMSDATA.pc_parcel_hist$s
  FOR EACH ROW
DECLARE
  cur_seq NUMBER;
  v_my_timestamp TIMESTAMP := SYSTIMESTAMP;
  v_migration_ind BOOLEAN := FALSE;
BEGIN
  IF SYS_CONTEXT('USERENV','CLIENT_INFO') = 'MIGRATION' THEN
    v_migration_ind := TRUE;
  END IF;

  IF(:NEW.updt_ts IS NULL OR v_migration_ind = FALSE) THEN
    :NEW.updt_ts := v_my_timestamp;
  END IF;

  IF(:NEW.updt_by IS NULL OR v_migration_ind = FALSE) THEN
    :NEW.updt_by := hmsdata.utility_pkg.get_user();
  END IF;

  IF(:NEW.updt_sys IS NULL OR v_migration_ind = FALSE) THEN
    :NEW.updt_sys := hmsdata.utility_pkg.get_system();
  END IF;

  IF(:NEW.apex_app_id IS NULL OR v_migration_ind = FALSE) THEN
    :NEW.apex_app_id := hmsdata.utility_pkg.get_apex_app_id();
  END IF;
  IF(:NEW.apex_page_id IS NULL OR v_migration_ind = FALSE) THEN
    :NEW.apex_page_id := hmsdata.utility_pkg.get_apex_page_id();
  END IF;

  IF INSERTING THEN
    IF :NEW.s_id IS NULL THEN
      -- No ID passed, get one from the sequence
      :NEW.s_id := HMSDATA.sq$pc_parcel_hist$s.NEXTVAL;
    ELSE
      -- ID was set via insert, so update the sequence
      cur_seq := HMSDATA.sq$pc_parcel_hist$s.NEXTVAL;
      WHILE cur_seq < :NEW.s_id LOOP
        cur_seq := HMSDATA.sq$pc_parcel_hist$s.NEXTVAL;
      END LOOP;
    END IF;
    :NEW.create_ts := :NEW.updt_ts;
    :NEW.create_by := :NEW.updt_by;
  END IF;

  IF UPDATING THEN
    IF :NEW.create_ts IS NOT NULL AND :NEW.create_ts <> :OLD.create_ts THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel_hist$s$s.create_ts on record #'||:OLD.id||' from "'
          ||:OLD.create_ts||'" to "'||:NEW.create_ts||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_ts := :OLD.create_ts;

    IF :NEW.create_by IS NOT NULL AND :NEW.create_by <> :OLD.create_by THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel_hist$s.create_by on record #'||:OLD.id||' from "'
          ||:OLD.create_by||'" to "'||:NEW.create_by||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_by := :OLD.create_by;
  END IF;

END;

/
ALTER TRIGGER "TG$PC_PARCEL_HIST$S" ENABLE;
