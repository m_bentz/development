--------------------------------------------------------
--  DDL for Package Body PC_TRACK_STAT_TYPE_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_TRACK_STAT_TYPE_PKG" AS

  FUNCTION get_new RETURN NUMBER AS
  BEGIN
    RETURN c_new;
  END get_new;

  FUNCTION get_waiting RETURN NUMBER AS
  BEGIN
    RETURN c_waiting;
  END get_waiting;

  FUNCTION get_forwarded RETURN NUMBER AS
  BEGIN
    RETURN c_forwarded;
  END get_forwarded;

  FUNCTION get_delivered RETURN NUMBER AS
  BEGIN
    RETURN c_delivered;
  END get_delivered;

  FUNCTION get_returning_to_sender RETURN NUMBER AS
  BEGIN
    RETURN c_returning_to_sender;
  END get_returning_to_sender;

END PC_TRACK_STAT_TYPE_PKG;

/
