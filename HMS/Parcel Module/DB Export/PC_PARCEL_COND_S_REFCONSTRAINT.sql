--------------------------------------------------------
--  Ref Constraints for Table PC_PARCEL_COND$S
--------------------------------------------------------

  ALTER TABLE "PC_PARCEL_COND$S" ADD CONSTRAINT "PRCL_COND$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_PARCEL_COND" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
