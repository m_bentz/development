--------------------------------------------------------
--  DDL for Trigger TG$PC_PARCEL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TG$PC_PARCEL" 
  BEFORE INSERT OR UPDATE ON HMSDATA.pc_parcel
  FOR EACH ROW
DECLARE
  -- Setting these to N as default sets it to N for the newly added rows
  v_non_res_first_name_m VARCHAR2(1 CHAR) := 'N';
  v_pc_carrier_id_m VARCHAR2(1 CHAR) := 'N';
  v_pc_carrier_id NUMBER := NULL;
  v_non_res_last_name_m VARCHAR2(1 CHAR) := 'N';
  v_return_rsn_m VARCHAR2(1 CHAR) := 'N';
  v_delete_ind_m VARCHAR2(1 CHAR) := 'N';
  v_prcl_list_id_m VARCHAR2(1 CHAR) := 'N';
  v_prcl_list_id NUMBER := NULL;
  v_prsn_id_m VARCHAR2(1 CHAR) := 'N';
  v_prsn_id NUMBER := NULL;
  v_trk_st_t_id_m VARCHAR2(1 CHAR) := 'N';
  v_trk_st_t_id NUMBER := NULL;
  v_track_num_m VARCHAR2(1 CHAR) := 'N';
  v_carrier_m VARCHAR2(1 CHAR) := 'N';
  v_misc_data_m VARCHAR2(1 CHAR) := 'N';
  v_receive_dt_m VARCHAR2(1 CHAR) := 'N';
  v_new_state_id NUMBER := NULL;
  cur_seq NUMBER;
  v_my_timestamp TIMESTAMP := SYSTIMESTAMP;
  v_migration_ind BOOLEAN := FALSE;
  v_anymod_ind BOOLEAN := FALSE;
BEGIN
  IF SYS_CONTEXT('USERENV','CLIENT_INFO') = 'MIGRATION' THEN
    v_migration_ind := TRUE;
  END IF;

  IF INSERTING THEN
    IF :NEW.id IS NULL THEN
      -- No ID passed, get one from the sequence
      :NEW.id := sq$pc_parcel.NEXTVAL;
    ELSE
      -- ID was set via insert, so update the sequence
      cur_seq := sq$pc_parcel.NEXTVAL;
      WHILE cur_seq <= :NEW.id LOOP
        cur_seq := sq$pc_parcel.NEXTVAL;
      END LOOP;
    END IF;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.non_res_first_name <> :OLD.non_res_first_name, TRUE)
      AND NOT (:NEW.non_res_first_name IS NULL
        AND :OLD.non_res_first_name IS NULL)
    )
  ) THEN
    v_non_res_first_name_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.pc_carrier_id <> :OLD.pc_carrier_id, TRUE)
      AND NOT (:NEW.pc_carrier_id IS NULL
        AND :OLD.pc_carrier_id IS NULL)
    )
  ) THEN
    v_pc_carrier_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.non_res_last_name <> :OLD.non_res_last_name, TRUE)
      AND NOT (:NEW.non_res_last_name IS NULL
        AND :OLD.non_res_last_name IS NULL)
    )
  ) THEN
    v_non_res_last_name_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.return_rsn <> :OLD.return_rsn, TRUE)
      AND NOT (:NEW.return_rsn IS NULL
        AND :OLD.return_rsn IS NULL)
    )
  ) THEN
    v_return_rsn_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.delete_ind <> :OLD.delete_ind, TRUE)
      AND NOT (:NEW.delete_ind IS NULL
        AND :OLD.delete_ind IS NULL)
    )
  ) THEN
    v_delete_ind_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.prcl_list_id <> :OLD.prcl_list_id, TRUE)
      AND NOT (:NEW.prcl_list_id IS NULL
        AND :OLD.prcl_list_id IS NULL)
    )
  ) THEN
    v_prcl_list_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.prsn_id <> :OLD.prsn_id, TRUE)
      AND NOT (:NEW.prsn_id IS NULL
        AND :OLD.prsn_id IS NULL)
    )
  ) THEN
    v_prsn_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.trk_st_t_id <> :OLD.trk_st_t_id, TRUE)
      AND NOT (:NEW.trk_st_t_id IS NULL
        AND :OLD.trk_st_t_id IS NULL)
    )
  ) THEN
    v_trk_st_t_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.track_num <> :OLD.track_num, TRUE)
      AND NOT (:NEW.track_num IS NULL
        AND :OLD.track_num IS NULL)
    )
  ) THEN
    v_track_num_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.carrier <> :OLD.carrier, TRUE)
      AND NOT (:NEW.carrier IS NULL
        AND :OLD.carrier IS NULL)
    )
  ) THEN
    v_carrier_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.misc_data <> :OLD.misc_data, TRUE)
      AND NOT (:NEW.misc_data IS NULL
        AND :OLD.misc_data IS NULL)
    )
  ) THEN
    v_misc_data_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.receive_dt <> :OLD.receive_dt, TRUE)
      AND NOT (:NEW.receive_dt IS NULL
        AND :OLD.receive_dt IS NULL)
    )
  ) THEN
    v_receive_dt_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  v_pc_carrier_id := :NEW.pc_carrier_id;
  v_prcl_list_id := :NEW.prcl_list_id;
  v_prsn_id := :NEW.prsn_id;
  v_trk_st_t_id := :NEW.trk_st_t_id;

  IF INSERTING OR v_anymod_ind THEN
    IF(:NEW.updt_ts IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_ts := v_my_timestamp;
    END IF;

    IF(:NEW.updt_by IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_by := hmsdata.utility_pkg.get_user();
    END IF;

    IF(:NEW.updt_sys IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_sys := hmsdata.utility_pkg.get_system();
    END IF;

    IF INSERTING THEN
      :NEW.create_ts := :NEW.updt_ts;
      :NEW.create_by := :NEW.updt_by;
    END IF;
  END IF;

  :NEW.apex_app_id := hmsdata.utility_pkg.get_apex_app_id();
  :NEW.apex_page_id := hmsdata.utility_pkg.get_apex_page_id();

  IF :NEW.updt_ts IS NULL THEN
    :NEW.updt_ts := :OLD.updt_ts;
  END IF;
  IF :NEW.updt_by IS NULL THEN
    :NEW.updt_by := :OLD.updt_by;
  END IF;
  IF :NEW.updt_sys IS NULL THEN
    :NEW.updt_sys := :OLD.updt_sys;
  END IF;

  IF UPDATING THEN
    IF :NEW.create_ts IS NOT NULL AND :NEW.create_ts <> :OLD.create_ts THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel.create_ts on record #'||:OLD.id||' from "'
          ||:OLD.create_ts||'" to "'||:NEW.create_ts||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_ts := :OLD.create_ts;

    IF :NEW.create_by IS NOT NULL AND :NEW.create_by <> :OLD.create_by THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel.create_by on record #'||:OLD.id||' from "'
          ||:OLD.create_by||'" to "'||:NEW.create_by||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_by := :OLD.create_by;

    IF :NEW.create_s_id IS NOT NULL AND :NEW.create_s_id <> :OLD.create_s_id THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel.create_s_id on record #'||:OLD.id||' from "'
          ||:OLD.create_s_id||'" to "'||:NEW.create_s_id||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.create_s_id := :OLD.create_s_id;

    IF :NEW.curr_s_id IS NOT NULL AND :NEW.curr_s_id <> :OLD.curr_s_id THEN
      hmsdata.audit_pkg.log_entry(
        p_msg => 'Change of pc_parcel.curr_s_id on record #'||:OLD.id||' from "'
          ||:OLD.curr_s_id||'" to "'||:NEW.curr_s_id||'".  Prevented.'
      , p_ts => v_my_timestamp
      , p_user => hmsdata.utility_pkg.get_user()
      );
    END IF;
    :NEW.curr_s_id := :OLD.curr_s_id;
  END IF;

  IF(
    INSERTING
    OR v_anymod_ind
    OR :NEW.create_s_id = -1
    OR :NEW.create_s_id IS NULL
    OR :NEW.curr_s_id = -1
    OR :NEW.curr_s_id IS NULL
  ) THEN
    INSERT INTO pc_parcel$s (
      id,
      prcl_list_id,
      prcl_list_id_m_ind,
      prsn_id,
      prsn_id_m_ind,
      trk_st_t_id,
      trk_st_t_id_m_ind,
      track_num,
      track_num_m_ind,
      carrier,
      carrier_m_ind,
      misc_data,
      misc_data_m_ind,
      receive_dt,
      receive_dt_m_ind,
      create_by,
      create_ts,
      updt_by,
      updt_ts,
      updt_sys,
      apex_app_id,
      apex_page_id,
      pc_carrier_id,
      pc_carrier_id_m_ind,
      non_res_first_name,
      non_res_first_name_m_ind,
      non_res_last_name,
      non_res_last_name_m_ind,
      return_rsn,
      return_rsn_m_ind,
      delete_ind,
      delete_ind_m_ind
    ) VALUES (
      :NEW.id,
      v_prcl_list_id,
      v_prcl_list_id_m,
      v_prsn_id,
      v_prsn_id_m,
      v_trk_st_t_id,
      v_trk_st_t_id_m,
      :NEW.track_num,
      v_track_num_m,
      :NEW.carrier,
      v_carrier_m,
      :NEW.misc_data,
      v_misc_data_m,
      :NEW.receive_dt,
      v_receive_dt_m,
      :NEW.create_by,
      :NEW.create_ts,
      :NEW.updt_by,
      :NEW.updt_ts,
      :NEW.updt_sys,
      :NEW.apex_app_id,
      :NEW.apex_page_id,
      v_pc_carrier_id,
      v_pc_carrier_id_m,
      :NEW.non_res_first_name,
      v_non_res_first_name_m,
      :NEW.non_res_last_name,
      v_non_res_last_name_m,
      :NEW.return_rsn,
      v_return_rsn_m,
      :NEW.delete_ind,
      v_delete_ind_m
    ) RETURNING s_id INTO v_new_state_id;
    -- Set the state ids for convenience
    :NEW.curr_s_id := v_new_state_id;
    IF INSERTING OR :NEW.create_s_id = -1 OR :NEW.create_s_id IS NULL THEN
      :NEW.create_s_id := v_new_state_id;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "TG$PC_PARCEL" ENABLE;
