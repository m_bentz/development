--------------------------------------------------------
--  DDL for Package APEX_UI_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "APEX_UI_PKG" AS 

/******************************************************************************/
/* HMSDATA.APEX_UI_PKG                                                        */

/* 
|  The functions in this package return the HTML for APEX alert elements.
|  
|  @param: p_text       The text to be displayed
|  @param: p_id         A css id for getting a hold of the element in javascript
|  @return: VARCHAR2    An HTML alert element
|
|  USAGE: Store the varchar returned by the functions in an APEX page element.
|         Then call createToastAlert(PAGE_ITEM, duration[optional]) 
|         or createMsgAlert(PAGE_ITEM, duration[optional]) functions in javascript.
|         
|         P1_TOAST := get_success_msg('This is my text', 'unique id');
|         createToastAlert("P1_TOAST", 3000);
|
|
|         Toasts by default will auto-dimiss after 3 seconds. A different time
|         can be specified in createToastAlert(PAGE_ITEM, duration). Not entering
|         a duration will result in the default behavior.
|
|         Messages by default need to be manually closed. They can auto-dismiss
|         by entering a duration into createMsgAlert(PAGE_ITEM, duration). Not
|         entering a duration will result in the default behavior.
*/

  FUNCTION get_success_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
  FUNCTION get_info_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
  FUNCTION get_error_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
   FUNCTION get_center_info_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
  FUNCTION get_success_toast(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
  FUNCTION get_info_toast(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;
  
  FUNCTION get_error_toast(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2;


END APEX_UI_PKG;

/
