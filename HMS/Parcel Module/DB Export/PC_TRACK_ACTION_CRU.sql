--------------------------------------------------------
--  DDL for Package PC_TRACK_ACTION_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_TRACK_ACTION_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_track_action
  PROCEDURE ins_pc_track_action (p_rec IN OUT pc_track_action%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_track_action
  PROCEDURE upd_pc_track_action(p_rec IN OUT pc_track_action%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_track_action
  FUNCTION get_pc_track_action(p_id IN NUMBER) RETURN pc_track_action%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_track_action
  PROCEDURE sav_pc_track_action(p_rec IN OUT pc_track_action%ROWTYPE);
END;

/
