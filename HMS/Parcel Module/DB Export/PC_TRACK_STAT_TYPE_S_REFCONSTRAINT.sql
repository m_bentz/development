--------------------------------------------------------
--  Ref Constraints for Table PC_TRACK_STAT_TYPE$S
--------------------------------------------------------

  ALTER TABLE "PC_TRACK_STAT_TYPE$S" ADD CONSTRAINT "TRK_ST_T$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_TRACK_STAT_TYPE" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
