--------------------------------------------------------
--  DDL for Trigger TG$PC_CARRIER_REGEX
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TG$PC_CARRIER_REGEX" 
  BEFORE INSERT OR UPDATE ON HMSDATA.pc_carrier_regex
  FOR EACH ROW
DECLARE
  -- Setting these to N as default sets it to N for the newly added rows
  v_pc_carrier_id_m VARCHAR2(1 CHAR) := 'N';
  v_pc_carrier_id NUMBER := NULL;
  v_pattern_m VARCHAR2(1 CHAR) := 'N';
  v_new_state_id NUMBER := NULL;
  cur_seq NUMBER;
  v_my_timestamp TIMESTAMP := SYSTIMESTAMP;
  v_migration_ind BOOLEAN := FALSE;
  v_anymod_ind BOOLEAN := FALSE;
BEGIN
  IF SYS_CONTEXT('USERENV','CLIENT_INFO') = 'MIGRATION' THEN
    v_migration_ind := TRUE;
  END IF;

  IF INSERTING THEN
    IF :NEW.id IS NULL THEN
      -- No ID passed, get one from the sequence
      :NEW.id := sq$pc_carrier_regex.NEXTVAL;
    ELSE
      -- ID was set via insert, so update the sequence
      cur_seq := sq$pc_carrier_regex.NEXTVAL;
      WHILE cur_seq <= :NEW.id LOOP
        cur_seq := sq$pc_carrier_regex.NEXTVAL;
      END LOOP;
    END IF;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.pc_carrier_id <> :OLD.pc_carrier_id, TRUE)
      AND NOT (:NEW.pc_carrier_id IS NULL
        AND :OLD.pc_carrier_id IS NULL)
    )
  ) THEN
    v_pc_carrier_id_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  IF(INSERTING
    OR (NVL(:NEW.pattern <> :OLD.pattern, TRUE)
      AND NOT (:NEW.pattern IS NULL
        AND :OLD.pattern IS NULL)
    )
  ) THEN
    v_pattern_m := 'Y';
    v_anymod_ind := TRUE;
  END IF;

  v_pc_carrier_id := :NEW.pc_carrier_id;

  IF INSERTING OR v_anymod_ind THEN
    IF(:NEW.updt_ts IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_ts := v_my_timestamp;
    END IF;

    IF(:NEW.updt_by IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_by := hmsdata.utility_pkg.get_user();
    END IF;

    IF(:NEW.updt_sys IS NULL OR v_migration_ind = FALSE) THEN
      :NEW.updt_sys := hmsdata.utility_pkg.get_system();
    END IF;

    IF INSERTING THEN
      :NEW.create_ts := :NEW.updt_ts;
      :NEW.create_by := :NEW.updt_by;
    END IF;
  END IF;

  :NEW.apex_app_id := hmsdata.utility_pkg.get_apex_app_id();
  :NEW.apex_page_id := hmsdata.utility_pkg.get_apex_page_id();

  IF :NEW.updt_ts IS NULL THEN
    :NEW.updt_ts := :OLD.updt_ts;
  END IF;
  IF :NEW.updt_by IS NULL THEN
    :NEW.updt_by := :OLD.updt_by;
  END IF;
  IF :NEW.updt_sys IS NULL THEN
    :NEW.updt_sys := :OLD.updt_sys;
  END IF;
  IF(
    INSERTING
    OR v_anymod_ind
    OR :NEW.create_s_id = -1
    OR :NEW.create_s_id IS NULL
    OR :NEW.curr_s_id = -1
    OR :NEW.curr_s_id IS NULL
  ) THEN
    INSERT INTO pc_carrier_regex$s (
      id,
      pc_carrier_id,
      pc_carrier_id_m_ind,
      pattern,
      pattern_m_ind
    ) VALUES (
      :NEW.id,
      v_pc_carrier_id,
      v_pc_carrier_id_m,
      :NEW.pattern,
      v_pattern_m
    ) RETURNING s_id INTO v_new_state_id;
    -- Set the state ids for convenience
    :NEW.curr_s_id := v_new_state_id;
    IF INSERTING OR :NEW.create_s_id = -1 OR :NEW.create_s_id IS NULL THEN
      :NEW.create_s_id := v_new_state_id;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "TG$PC_CARRIER_REGEX" ENABLE;
