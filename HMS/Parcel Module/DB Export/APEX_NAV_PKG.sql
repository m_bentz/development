--------------------------------------------------------
--  DDL for Package APEX_NAV_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "APEX_NAV_PKG" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  FUNCTION get_dialog_url(
    p_page      NUMBER,
    p_app       VARCHAR2,
    p_sess      VARCHAR2
  )RETURN VARCHAR2;  

END APEX_NAV_PKG;

/
