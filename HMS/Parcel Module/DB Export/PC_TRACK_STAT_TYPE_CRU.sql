--------------------------------------------------------
--  DDL for Package PC_TRACK_STAT_TYPE_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_TRACK_STAT_TYPE_CRU" IS
  --------------------------------------------------------------
  -- create procedure for table pc_track_stat_type
  PROCEDURE ins_pc_track_stat_type (p_rec IN OUT pc_track_stat_type%ROWTYPE);
  --------------------------------------------------------------
  -- update procedure for table pc_track_stat_type
  PROCEDURE upd_pc_track_stat_type(p_rec IN OUT pc_track_stat_type%ROWTYPE);
  --------------------------------------------------------------
  -- read function for table pc_track_stat_type
  FUNCTION get_pc_track_stat_type(p_id IN NUMBER) RETURN pc_track_stat_type%ROWTYPE;
  --------------------------------------------------------------
  -- save procedure for table pc_track_stat_type
  PROCEDURE sav_pc_track_stat_type(p_rec IN OUT pc_track_stat_type%ROWTYPE);
END;

/
