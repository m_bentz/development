--------------------------------------------------------
--  DDL for Table PC_TRACK_ACTION$S
--------------------------------------------------------

  CREATE TABLE "PC_TRACK_ACTION$S" 
   (	"S_ID" NUMBER(10,0), 
	"ID" NUMBER(10,0), 
	"TRK_ST_T_ID" NUMBER(10,0), 
	"TRK_ST_T_ID_M_IND" VARCHAR2(1 CHAR), 
	"TITLE" VARCHAR2(75 CHAR), 
	"TITLE_M_IND" VARCHAR2(1 CHAR), 
	"DESCR" VARCHAR2(200 CHAR), 
	"DESCR_M_IND" VARCHAR2(1 CHAR), 
	"RESULT_TRK_ST_T_ID" NUMBER(10,0), 
	"RESULT_TRK_ST_T_ID_M_IND" VARCHAR2(1 CHAR), 
	"PROMPT_IND" VARCHAR2(1 CHAR), 
	"PROMPT_IND_M_IND" VARCHAR2(1 CHAR), 
	"MSG" VARCHAR2(200 CHAR), 
	"MSG_M_IND" VARCHAR2(1 CHAR), 
	"QUERY" VARCHAR2(1000 CHAR), 
	"QUERY_M_IND" VARCHAR2(1 CHAR), 
	"CREATE_BY" VARCHAR2(20 CHAR) DEFAULT 'BATCH', 
	"CREATE_TS" TIMESTAMP (6) DEFAULT SYSTIMESTAMP, 
	"UPDT_BY" VARCHAR2(20 CHAR) DEFAULT 'BATCH', 
	"UPDT_TS" TIMESTAMP (6) DEFAULT SYSTIMESTAMP, 
	"UPDT_SYS" VARCHAR2(20 CHAR) DEFAULT 'BATCH', 
	"APEX_APP_ID" NUMBER(10,0), 
	"APEX_PAGE_ID" NUMBER(10,0), 
	"DIALOG_IND" VARCHAR2(1 CHAR), 
	"DIALOG_IND_M_IND" VARCHAR2(1 CHAR) DEFAULT 'N', 
	"HELP_TEXT" VARCHAR2(200 CHAR), 
	"HELP_TEXT_M_IND" VARCHAR2(1 CHAR) DEFAULT 'N', 
	"NO_RESULT_MSG" VARCHAR2(200 CHAR), 
	"NO_RESULT_MSG_M_IND" VARCHAR2(1 CHAR) DEFAULT 'N', 
	"DIALOG" VARCHAR2(20 CHAR), 
	"DIALOG_M_IND" VARCHAR2(1 CHAR) DEFAULT 'N'
   ) ;
