--------------------------------------------------------
--  DDL for Package Body PC_TRACK_STAT_TYPE_CRU
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_TRACK_STAT_TYPE_CRU" IS

  --------------------------------------------------------------
  -- create procedure for table pc_track_stat_type
  PROCEDURE ins_pc_track_stat_type(p_rec IN OUT pc_track_stat_type%ROWTYPE) IS
    v_id pc_track_stat_type.id%TYPE;
  BEGIN
    INSERT INTO pc_track_stat_type VALUES p_rec RETURNING id INTO p_rec.id;
  END ins_pc_track_stat_type;

  --------------------------------------------------------------
  -- update procedure for table pc_track_stat_type
  PROCEDURE upd_pc_track_stat_type(p_rec IN OUT pc_track_stat_type%ROWTYPE ) IS
    v_curr_ts TIMESTAMP := NULL;
  BEGIN
    -- find out what time stamp is in the db right now
    SELECT updt_ts INTO v_curr_ts FROM pc_track_stat_type WHERE id = p_rec.id;
    IF (p_rec.updt_ts = v_curr_ts) THEN
      UPDATE pc_track_stat_type SET row = p_rec WHERE ID = p_rec.id;
    ELSE
      RAISE_APPLICATION_ERROR(-20001, 'pc_track_stat_type_cru '
        || 'Current version of data in database has changed since user initiated '
        || 'update process. current timestamp = ' || v_curr_ts || ', item timestamp = '
        || p_rec.updt_ts || '.');
    END IF;
  END upd_pc_track_stat_type;

  --------------------------------------------------------------
  -- read function for table pc_track_stat_type
  FUNCTION get_pc_track_stat_type(p_id NUMBER) RETURN pc_track_stat_type%ROWTYPE IS
    v_rec pc_track_stat_type%ROWTYPE;
  BEGIN
    SELECT * INTO v_rec FROM pc_track_stat_type WHERE id = p_id;
    RETURN v_rec;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_pc_track_stat_type;

  -------------------------------------------------------------
  -- save procedure for table pc_track_stat_type
  PROCEDURE sav_pc_track_stat_type(p_rec IN OUT pc_track_stat_type%ROWTYPE ) IS
    v_id pc_track_stat_type.id%TYPE;
    v_exist_rec NUMBER;
  BEGIN
    SELECT id INTO v_exist_rec FROM pc_track_stat_type WHERE id = p_rec.id;
    upd_pc_track_stat_type(p_rec);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ins_pc_track_stat_type(p_rec);
  END sav_pc_track_stat_type;

END;

/
