--------------------------------------------------------
--  DDL for Package Body APEX_UI_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "APEX_UI_PKG" AS

/******************************************************************************/
/* HMSDATA.APEX_UI_PKG                                                        */

/* 
|  The functions in this package return the HTML for APEX alert elements.
|  
|  @param: p_text       The text to be displayed
|  @param: p_id         A css id for getting a hold of the element in javascript
|  @return: VARCHAR2    An HTML alert element
|
|  USAGE: Store the varchar returned by the functions in an APEX page element.
|         Then call createToastAlert(PAGE_ITEM, duration[optional]) 
|         or createMsgAlert(PAGE_ITEM, duration[optional]) functions in javascript.
|         
|         P1_TOAST := get_success_msg('This is my text', 'unique id');
|         createToastAlert("P1_TOAST", 3000);
|
|
|         Toasts by default will auto dimiss after 3 seconds. A different time
|         can be specified in createToastAlert(PAGE_ITEM, duration). Not entering
|         a duration will result in the default behavior.
|
|         Messages by default need to be manually closed. They can auto dismiss
|         by entering a duration into createMsgAlert(PAGE_ITEM, duration). Not
|         entering a duration will result in the default behavior.
*/

  FUNCTION get_success_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-msg" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-msg" id="' || p_id || '" >';
    END IF;
    v_content := v_content ||
      '<div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-check fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
        <div class="t-Alert-buttons">
          <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="Close Notification"><span class="t-Icon icon-close"></span></button>
          </div>
      </div>
    </div>
  </div>';
    RETURN v_content;
  END get_success_msg;
  
  FUNCTION get_info_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-msg" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-msg" id="' || p_id || '" >';
    END IF;
    v_content := v_content ||
      '<div  style="background-color:rgba(37, 120, 207,1);" class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-info-circle fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
        <div class="t-Alert-buttons">
          <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="Close Notification"><span class="t-Icon icon-close"></span></button>
          </div>
      </div>
    </div>
  </div>';
    RETURN v_content;
  END get_info_msg;
  
  FUNCTION get_error_msg(
    p_text      VARCHAR2
  , p_id        VARCHAR2
  )RETURN VARCHAR2 AS
    v_content     VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-msg" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-msg" id="' || p_id || '" >';
    END IF;
    v_content := v_content || '<div style="background-color:rgba(225, 56, 56, 1);"
      class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-times fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
        <div class="t-Alert-buttons">
          <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="Close Notification"><span class="t-Icon icon-close"></span></button> 
        </div>
      </div>
    </div>
  </div>';
    RETURN v_content;
  END get_error_msg;
  
  FUNCTION get_center_info_msg(
    p_text      VARCHAR2,
    p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-msg" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-msg" id="' || p_id || '" >';
    END IF;
    v_content := v_content || 
    '<div style="background-color: rgb(37, 120, 207);opacity: 1000;z-index: 2;width:  500px;height: 175px;position: absolute;top: 40%;left: 50%;transform: translateY(-50%) translateX(-50%);display: flex;align-items:  center;"
        class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
        <div class="t-Alert-icon " style="
    position: absolute;
    display: none;
    top: 0;">
            <span style="color:whitesmoke" class="fa fa-info-circle fa-2x"></span>
        </div>
        <div class="t-Alert-wrap" style="height: 80%;width: inherit">

            <div class="t-Alert-content" style="
    width: inherit;
    height: 100%;
    display:  flex;
    align-items: center;">
                <div class="t-Alert-header" style="
    font-size: 2em;">
                    <h2 class="t-Alert-title" style="
    font-size: 2.2rem;
    text-align: center;">' || p_text || '</h2>
                </div>
            </div>

        </div>
        <div class="t-Alert-buttons" style="
    position: absolute;
    right: 0;
    top: 0;
    transform: translateX(-250%);">
            <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="Close Notification">
                <span class="t-Icon icon-close"></span>
            </button>
        </div>
    </div>
</div>';
    RETURN v_content;
  END get_center_info_msg;
  
  FUNCTION get_success_toast(
    p_text      VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-toast" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-toast"  id="' || p_id || '" >';
    END IF;
    v_content := v_content || 
    '<div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-check fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
      </div>
    </div>
  </div>';
  RETURN v_content;
  END get_success_toast;
  
  FUNCTION get_info_toast(
    p_text      VARCHAR2
   ,p_id        VARCHAR2 := NULL
   )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-toast" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-toast" id="' || p_id || '" >';
    END IF;
    v_content := v_content || '<div style="background-color:rgba(37, 120, 207, 1);"
      class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-info-circle fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
      </div>
    </div>
  </div>';  
    RETURN v_content;
  END get_info_toast;
  
  FUNCTION get_error_toast(
    p_text    VARCHAR2
  , p_id        VARCHAR2 := NULL
  )RETURN VARCHAR2 AS
    v_content   VARCHAR2(32767);
  BEGIN
    IF p_id IS NULL THEN
      v_content := '<div class="t-Body-alert alert-toast" >';
    ELSE
      v_content := '<div class="t-Body-alert alert-toast" id="' || p_id || '" >';
    END IF;
    v_content := v_content || '<div style="background-color:rgba(225, 56, 56, 1);"
      class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page" id="t_Alert_Success" role="alert">
      <div class="t-Alert-wrap">
        <div class="t-Alert-icon ">
          <span style="color:whitesmoke" class="fa fa-times fa-2x"></span>
        </div>
        <div class="t-Alert-content">
          <div class="t-Alert-header">
            <h2 class="t-Alert-title">' || p_text || '</h2>
          </div>
        </div>
      </div>
    </div>
  </div>';  
    RETURN v_content;
  END get_error_toast;  

END APEX_UI_PKG;

/
