--------------------------------------------------------
--  Ref Constraints for Table PC_COND_STAT_TYPE$S
--------------------------------------------------------

  ALTER TABLE "PC_COND_STAT_TYPE$S" ADD CONSTRAINT "PC_COND_STAT$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_COND_STAT_TYPE" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
