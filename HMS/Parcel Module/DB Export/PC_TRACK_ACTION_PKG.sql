--------------------------------------------------------
--  DDL for Package PC_TRACK_ACTION_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PC_TRACK_ACTION_PKG" AS 

  c_external_receive CONSTANT NUMBER := 1;
  c_non_res_receive CONSTANT NUMBER := 2;
  c_res_forward CONSTANT NUMBER := 3;
  c_deliver CONSTANT NUMBER := 4;
  c_return_to_sender CONSTANT NUMBER := 5;
  c_internal_receive CONSTANT NUMBER := 6;
  c_del_return_to_waiting CONSTANT NUMBER := 7;
  c_rts_return_to_waiting CONSTANT NUMBER := 8;
  c_non_res_forward CONSTANT NUMBER := 9;
  
  
  FUNCTION get_external_receive RETURN NUMBER;
  FUNCTION get_non_res_receive RETURN NUMBER;
  FUNCTION get_res_forward RETURN NUMBER;
  FUNCTION get_deliver RETURN NUMBER;
  FUNCTION get_return_to_sender RETURN NUMBER;
  FUNCTION get_internal_receive RETURN NUMBER;
  FUNCTION get_del_return_to_waiting RETURN NUMBER;
  FUNCTION get_rts_return_to_waiting RETURN NUMBER;
  FUNCTION get_non_res_forward RETURN NUMBER;
  
  /* Returns the resulting status */ 
  FUNCTION get_result_status(
    p_id          PC_TRACK_ACTION.id%TYPE
  )RETURN PC_TRACK_STAT_TYPE%ROWTYPE;
  
  /*
    @param p_id: The id of the track action
    @param p_cond_id: The ID used in the WHERE clause to filter the query results
    
    @return: pc_track_action.query: The associated query for the track action
  
    This function builds the query for the specified track action. By doing this
    we avoid sending an entire query between apex pages. Instead we pass the ID's to
    opening dialog and call this function to get the appropriate query
  */
  FUNCTION build_query(
    p_id          PC_TRACK_ACTION.id%TYPE,
    p_cond_id     VARCHAR2,
    p_cond_id2    VARCHAR2 DEFAULT NULL
  )RETURN PC_TRACK_ACTION.query%TYPE;  
  
  /*
    @param p_id: The id of the track action
    @param p_cond: (Optional) Additional information to include in the headline
    
    @return pc_track_action.msg: The associated headline for the tracka ctioni
    
    This functions returns the headline for a track action. A headline is used
    for email templates as well as parcel history
  */
  FUNCTION get_headline(
    p_id          PC_TRACK_ACTION.id%TYPE,
    p_cond        VARCHAR2
  )RETURN PC_TRACK_ACTION.msg%TYPE;
  
  PROCEDURE add_action(
    p_coll_name   VARCHAR2,
    p_msg         VARCHAR2,
    p_descr       VARCHAR2,
    p_date        DATE);  

END PC_TRACK_ACTION_PKG;

/
