--------------------------------------------------------
--  Ref Constraints for Table PC_CARRIER_REGEX$S
--------------------------------------------------------

  ALTER TABLE "PC_CARRIER_REGEX$S" ADD CONSTRAINT "CARRIER_REG$S_MAIN_FK00" FOREIGN KEY ("ID")
	  REFERENCES "PC_CARRIER_REGEX" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
