SELECT *
FROM (
  WITH area_desk AS (
    SELECT
      id,
      title
    FROM area
    WHERE hsng_t_id = 1)
  SELECT
    person_pkg.get_med_img(prsn.id) AS "Img",
    initcap(prsn.first_name || ' ' || prsn.last_name) AS "Student",
    prsn.ufid AS "UFID",
    term.title AS "Semester",
    area_desk.title AS "Area",
    trim(LEADING '0' FROM room.rm_num) AS "Room Number",
    '<button type="button" data-prsn-ufid="'|| prsn.ufid ||'" data-area-id="' || area_desk.id || '" data-name="' || initcap(prsn.first_name || ' ' || prsn.last_name) || '" ' ||
         'class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>' AS Action
  FROM v_ua_assign_status assign
  INNER JOIN space_room_term srt
    ON assign.sp_rm_trm_id = srt.id
  INNER JOIN room_term rt
    ON srt.rm_trm_id = rt.id
  INNER JOIN room
    ON rt.rm_id = room.id
  INNER JOIN term
    ON rt.trm_id = term.id
  INNER JOIN building b
    ON room.bldg_id = b.id
  INNER JOIN area_desk 
    ON b.area_id = area_desk.id
  INNER JOIN contract_ua c
    ON assign.conu_id = c.id
  INNER JOIN agreement agree
    ON c.agree_id = agree.id
  INNER JOIN person prsn
    ON agree.prsn_id = prsn.id
  LEFT OUTER JOIN checkin_appt_resident car
    ON assign.asg_id = car.asg_id
  LEFT OUTER JOIN checkin_appt_config config
    ON car.cki_apt_cnf_id = config.id
  LEFT OUTER JOIN v_checkin_appt_time cat
    ON car.cki_apt_tm_id = cat.id
  WHERE ( assign.checkin_dt IS NULL OR assign.checkin_dt + 21 > SYSDATE )
    AND assign.deleted_ind = 'N'
    AND assign.end_dt > SYSDATE
    AND ( assign.same_sp_prev_trm_ind = 'N' OR assign.moved_in_ind = 'N' )
    AND NOT ( assign.moved_out_ind = 'N' AND assign.same_sp_prev_trm_ind = 'Y')
  ORDER BY decode(area_desk.id, :P213_DESK_AREA_ID, 1, 2), term.id asc, prsn.last_name);
UNION ;

SELECT *
FROM (
  WITH area_desk AS (
    SELECT
      id,
      title
    FROM area
    WHERE hsng_t_id = 1)
  SELECT
    person_pkg.get_med_img(prsn.id) AS "Img",
    initcap(prsn.first_name || ' ' || prsn.last_name) AS "Student",
    prsn.ufid AS "UFID",
    res.semester AS "Semester",
    area_desk.title AS "Area",
  --    res.area_village AS "Area",
    trim(LEADING '0' FROM res.room_apt) AS "Room",
    '<button type="button" data-prsn-ufid="'|| prsn.ufid ||'" data-area-id="' || area_desk.id || '" data-name="' || initcap(prsn.first_name || ' ' || prsn.last_name) || '" ' ||
       'class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>' AS Action
  FROM person prsn 
  INNER JOIN v_curr_next_res res 
      ON prsn.ufid = res.ufid 
  INNER JOIN area_desk 
      ON area.title LIKE res.area_village
  ORDER BY decode (area_desk.id, :P213_DESK_AREA, 1, 5), prsn.last_name asc));


SELECT *
FROM (
  WITH area_desk AS (
    SELECT
      id,
      title
    FROM area
    WHERE hsng_t_id = 1)
  SELECT
    person_pkg.get_med_img(prsn.id) AS "Img",
    initcap(prsn.first_name || ' ' || prsn.last_name) AS "Student",
    prsn.ufid AS "UFID",
    term.title AS "Semester",
    area_desk.title AS "Area",
    trim(LEADING '0' FROM room.rm_num) AS "Room Number",
    '<button type="button" data-prsn-ufid="'|| prsn.ufid ||'" data-area-id="' || area_desk.id || '" data-name="' || initcap(prsn.first_name || ' ' || prsn.last_name) || '" ' ||
         'class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>' AS Action
  FROM v_ua_assign_status assign
  INNER JOIN space_room_term srt
    ON assign.sp_rm_trm_id = srt.id
  INNER JOIN room_term rt
    ON srt.rm_trm_id = rt.id
  INNER JOIN room
    ON rt.rm_id = room.id
  INNER JOIN term
    ON rt.trm_id = term.id
  INNER JOIN building b
    ON room.bldg_id = b.id
  INNER JOIN area_desk 
    ON b.area_id = area_desk.id
  INNER JOIN contract_ua c
    ON assign.conu_id = c.id
  INNER JOIN agreement agree
    ON c.agree_id = agree.id
  INNER JOIN person prsn
    ON agree.prsn_id = prsn.id
  LEFT OUTER JOIN checkin_appt_resident car
    ON assign.asg_id = car.asg_id
  LEFT OUTER JOIN checkin_appt_config config
    ON car.cki_apt_cnf_id = config.id
  LEFT OUTER JOIN v_checkin_appt_time cat
    ON car.cki_apt_tm_id = cat.id
  WHERE ( assign.checkin_dt IS NULL OR assign.checkin_dt + 21 > SYSDATE )
    AND assign.deleted_ind = 'N'
    AND assign.end_dt > SYSDATE
    AND ( assign.same_sp_prev_trm_ind = 'N' OR assign.moved_in_ind = 'N' )
    AND NOT ( assign.moved_out_ind = 'N' AND assign.same_sp_prev_trm_ind = 'Y')
  ORDER BY decode(area_desk.id, :P213_DESK_AREA_ID, 1, 2), term.id asc, prsn.last_name)
UNION 
SELECT *
FROM (
  WITH area_desk AS (
    SELECT
      id,
      title
    FROM area
    WHERE hsng_t_id = 1)
  SELECT
    person_pkg.get_med_img(prsn.id) AS "Img",
    initcap(prsn.first_name || ' ' || prsn.last_name) AS "Student",
    prsn.ufid AS "UFID",
    res.semester AS "Semester",
    area_desk.title AS "Area",
    trim(LEADING '0' FROM res.room_apt) AS "Room",
    '<button type="button" data-prsn-ufid="'|| prsn.ufid ||'" data-area-id="' || area_desk.id || '" data-name="' || initcap(prsn.first_name || ' ' || prsn.last_name) || '" ' ||
       'class="select-res t-Button t-Button--icon t-Button--simple t-Button--iconLeft t-Button--gapRight t-Button--primary"><span aria-hidden="true"></span>Select Resident</button>' AS Action
  FROM person prsn 
  INNER JOIN v_curr_next_res res 
      ON prsn.ufid = res.ufid 
  INNER JOIN term
      ON res.semester = term.title
  INNER JOIN area_desk 
      ON area_desk.title LIKE res.area_village
  ORDER BY decode (area_desk.id, :P213_DESK_AREA, 1, 5), term.id asc, prsn.last_name asc);