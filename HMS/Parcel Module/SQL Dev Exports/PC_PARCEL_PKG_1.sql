--------------------------------------------------------
--  DDL for Package Body PC_PARCEL_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PC_PARCEL_PKG" AS

  FUNCTION is_location_valid(
    p_id       pc_parcel.id%TYPE,
    p_curr_area  area.id%TYPE
  )RETURN VARCHAR2 AS
    v_prcl      pc_parcel%ROWTYPE;
    v_prcl_area     v_curr_next_res.area_village%TYPE;
    v_curr_area     area.title%TYPE;
  BEGIN
    -- Get the parcel
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    /* TODO: REVISIT LOGIC and replace logic to work with ID and NOT title */
    -- Get the current area
    SELECT title
    INTO v_curr_area
    FROM area 
    WHERE id = p_curr_area;

    -- Get the assigned residents area
    SELECT area.title
    INTO v_prcl_area
    FROM pc_parcel prcl
    INNER JOIN pc_parcel_list prcl_list
      ON prcl.prcl_list_id = prcl_list.id
    INNER JOIN area
      ON prcl_list.area_id = area.id
    WHERE prcl.id = p_id;
    
--    SELECT area_village
--    INTO v_prcl_area
--    FROM v_curr_next_res
--    WHERE ufid = (
--        SELECT ufid
--        FROM person prsn
--        WHERE prsn.id = v_prcl.prsn_id)
--    GROUP BY area_village;

    IF v_curr_area LIKE v_prcl_area THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;  

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END is_location_valid;

  FUNCTION is_past_due(
    p_id        pc_parcel.id%TYPE,
    p_area_id   pc_parcel_list.area_id%TYPE
  )RETURN VARCHAR2 AS
    v_prcl          pc_parcel%ROWTYPE;
  BEGIN
    -- Get the parcel
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    IF v_prcl.trk_st_t_id <> 2 OR is_suspicious(p_id) = 'Y' THEN
      RETURN 'N';
    -- Check if the package has been waiting longer than the specified limit
    -- This only applies to parcels that are currently waiting
    ELSIF get_age(p_id, sysdate) > c_hold_limit THEN -- AND is_location_valid(p_id, p_area_id) = 'Y' THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;  
  END is_past_due;


  FUNCTION is_suspicious(
    p_id      pc_parcel.id%TYPE
  )RETURN VARCHAR2 AS
    v_susp    pc_parcel_cond.active_ind%TYPE;
  BEGIN

    -- Check to see if the parcel is suspicious
    SELECT cond.active_ind
    INTO v_susp
    FROM pc_parcel_cond cond
      INNER JOIN pc_cond_stat_type typ
      ON cond.pc_cond_stat_id = typ.id
    WHERE typ.id = 1
    AND cond.prcl_id = p_id;

    apps.logger.debug('Is suspicious: ' || v_susp);
    RETURN v_susp;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END is_suspicious;

  FUNCTION get_status(
    p_id      pc_parcel.id%TYPE
  )RETURN pc_track_stat_type.title%TYPE AS
    v_prcl    pc_parcel%ROWTYPE;
    v_prcl_status   pc_track_stat_type.title%TYPE;
  BEGIN
    -- Get the parcel
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    -- Get the status of the parcel
    SELECT title
    INTO v_prcl_status
    FROM pc_track_stat_type stat
    WHERE stat.id = v_prcl.trk_st_t_id;

    return v_prcl_status;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_status;

  FUNCTION get_age(
    p_id    pc_parcel.id%TYPE,
    p_dt    DATE
  )RETURN NUMBER AS
    v_prcl    pc_parcel%ROWTYPE;
  BEGIN
    -- Get the parcel
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    RETURN trunc(p_dt - v_prcl.receive_dt);
  END get_age;

  FUNCTION get_area(
    p_id      pc_parcel.id%TYPE
  )RETURN area%ROWTYPE AS
    v_area    area%ROWTYPE;
  BEGIN
    SELECT *
    INTO v_area
    FROM area
    WHERE id = (
        SELECT prcl_list.area_id
        FROM pc_parcel prcl
        INNER JOIN pc_parcel_list prcl_list
        ON prcl.prcl_list_id = prcl_list.id
        WHERE prcl.id = p_id
    );

    RETURN v_area;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_area;

  FUNCTION get_recipient(
    p_id      pc_parcel.id%TYPE
  )RETURN person%ROWTYPE AS
    v_prsn    person%ROWTYPE;
  BEGIN
    SELECT *
    INTO v_prsn
    FROM person
    WHERE id = (
        SELECT prsn_id
        FROM pc_parcel
        WHERE id = p_id
    );

    RETURN v_prsn;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_recipient;

  FUNCTION is_for_resident(
    p_id      pc_parcel.id%TYPE
  )RETURN VARCHAR2 AS
    v_prcl    pc_parcel%ROWTYPE;
  BEGIN
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);  

    IF v_prcl.prsn_id IS NOT NULL THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END is_for_resident;

  PROCEDURE create_conditions(
    p_id        pc_parcel.id%TYPE,
    p_options   VARCHAR2
  )AS
  v_cond_count     NUMBER := 4;
  v_parcel_cond    pc_parcel_cond%ROWTYPE;
  BEGIN
    FOR counter IN 1..v_cond_count
      LOOP
          v_parcel_cond := NULL;
          v_parcel_cond.prcl_id := p_id;
          v_parcel_cond.pc_cond_stat_id := counter;
          v_parcel_cond.active_ind := CASE WHEN INSTR(p_options, to_char(v_parcel_cond.pc_cond_stat_id)) <> 0 THEN 'Y' ELSE 'N' END;
          pc_parcel_cond_cru.sav_pc_parcel_cond(v_parcel_cond);
      END LOOP;
  END create_conditions;

  PROCEDURE update_conditions(
    p_id          pc_parcel.id%TYPE,
    p_options     VARCHAR2
  ) AS  
  BEGIN
    UPDATE pc_parcel_cond 
    SET active_ind = 
        CASE WHEN INSTR(p_options, to_char(pc_cond_stat_id)) <> 0 THEN 'Y' ELSE 'N' END
    WHERE prcl_id = p_id;
  END;

  PROCEDURE custom_log(
    p_id    pc_parcel.id%TYPE
  )AS
  v_prcl    pc_parcel%ROWTYPE;
  BEGIN
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    apps.logger.debug('Carrier: ' || v_prcl.carrier);
    apps.logger.debug('Status id: ' || v_prcl.trk_st_t_id);
    apps.logger.debug('Opt descr: ' || v_prcl.misc_data);
    apps.logger.debug('Rec date: ' || v_prcl.receive_dt);
    apps.logger.debug('track num: ' || v_prcl.track_num);
    apps.logger.debug('non res f name: ' || v_prcl.non_res_first_name);
    apps.logger.debug('non res l name: ' || v_prcl.non_res_last_name);
  END custom_log;

  FUNCTION set_values(
    p_id              pc_parcel.id%TYPE,
    p_prcl_list       pc_parcel.prcl_list_id%TYPE,
    p_trk_st_t_id     pc_parcel.trk_st_t_id%TYPE,
    p_track_num       pc_parcel.track_num%TYPE,
    p_carrier_id      pc_parcel.pc_carrier_id%TYPE,
    p_carrier_opt     pc_parcel.carrier%TYPE,
    p_misc_data       pc_parcel.misc_data%TYPE,
    p_return_rsn      pc_parcel.return_rsn%TYPE,
    p_receive_dt      pc_parcel.receive_dt%TYPE DEFAULT NULL,
    p_prsn_id         pc_parcel.prsn_id%TYPE DEFAULT NULL,
    p_non_res_f_name  pc_parcel.non_res_first_name%TYPE DEFAULT NULL,
    p_non_res_l_name  pc_parcel.non_res_last_name%TYPE DEFAULT NULL
  )RETURN pc_parcel%ROWTYPE AS
    v_prcl            pc_parcel%ROWTYPE;
  BEGIN
    v_prcl := pc_parcel_cru.get_pc_parcel(p_id);

    v_prcl.prcl_list_id := p_prcl_list;
    v_prcl.prsn_id := p_prsn_id;
    v_prcl.trk_st_t_id := p_trk_st_t_id;
    v_prcl.track_num := p_track_num;
    v_prcl.pc_carrier_id := p_carrier_id;
    v_prcl.carrier := p_carrier_opt;
    v_prcl.misc_data := p_misc_data;
    v_prcl.return_rsn := p_return_rsn;
    SELECT decode(p_receive_dt, NULL, v_prcl.receive_dt, p_receive_dt) INTO v_prcl.receive_dt FROM dual;-- := p_receive_dt;
    v_prcl.non_res_first_name := p_non_res_f_name;
    v_prcl.non_res_last_name := p_non_res_l_name;

    return v_prcl;
  END set_values;

END PC_PARCEL_PKG;

/
